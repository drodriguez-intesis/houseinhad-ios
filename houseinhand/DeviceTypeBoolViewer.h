//
//  DeviceTypeBoolViewer.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeBoolViewer : Device

@property (nonatomic, retain) NSString * textDefault;
@property (nonatomic, retain) NSString * textNo;
@property (nonatomic, retain) NSString * textYes;
@property (nonatomic, retain) NSNumber * valueNo;
@property (nonatomic, retain) NSNumber * valueYes;

@end
