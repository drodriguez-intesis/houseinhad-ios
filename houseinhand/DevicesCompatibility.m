//
//  DevicesCompatibility.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DevicesCompatibility.h"

@implementation DevicesCompatibility



//widgets
+(const NSArray *) getAvailableWidgetDevices{
	const NSArray *devicesType =@[
		@{@"class":@"TypeLightViewController",@"interface":@"SimpleButtonViewController",@"scenes":@YES}, //0
		@{@"class":@"TypeDimmerViewController",@"interface":@"SliderButtonsViewController",@"scenes":@YES}, //1
		@{@"class":@"TypeSceneViewController",@"interface":@"SimpleButtonViewController",@"scenes":@YES}, //2
		@{@"class":@"TypeSetpointViewController",@"interface":@"ViewerButtonsViewController",@"scenes":@YES}, //3
		@{@"class":@"TypeViewerViewController",@"interface":@"ViewerViewController",@"scenes":@NO}, //4
		@{@"class":@"TypeBlindViewController",@"interface":@"TwoButtonsViewController",@"scenes":@YES}, //5
		@{@"class":@"TypeRegBlindViewController",@"interface":@"SliderButtonsViewController",@"scenes":@YES}, //6
		@{@"class":@"TypeGradhermeticViewController",@"interface":@"ThreeButtonsViewController",@"scenes":@YES}, //7
		@{@"class":@"TypeVeluxViewController",@"interface":@"ThreeButtonsViewController",@"scenes":@YES}, //8
		@{@"class":@"TypeRgbViewController",@"interface":@"ThreeButtonsViewController",@"scenes":@YES}, //9
		@{@"class":@"TypeGenericSegmentedViewController",@"interface":@"SegmentedViewController",@"scenes":@YES}, //10
		@{@"class":@"TypeClima1bViewController",@"interface":@"ClimaViewController",@"scenes":@YES}, //11
		@{@"class":@"TypeClimaViewController",@"interface":@"ClimaViewController",@"scenes":@YES}, //12
		@{@"class":@"TypeIpViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //13
		@{@"class":@"TypeGenViewerViewController",@"interface":@"ViewerViewController",@"scenes":@NO}, //14
		@{@"class":@"TypeBoolViewerViewController",@"interface":@"ViewerViewController",@"scenes":@NO}, //15
		@{@"class":@"TypeTextViewerViewController",@"interface":@"ViewerViewController",@"scenes":@NO}, //16
  		@{@"class":@"TypeIpViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //17
  		@{@"class":@"TypeMultimediaViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //18
  		@{@"class":@"TypeMacroMultimediaViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //19
  		@{@"class":@"TypeJungSetpointViewController",@"interface":@"ViewerButtonsViewController",@"scenes":@NO}, //20
  		@{@"class":@"TypeBerkerSetpointViewController",@"interface":@"ViewerButtonsViewController",@"scenes":@NO}, //21
        @{@"class":@"TypePulseViewController",@"interface":@"SimpleButtonViewController",@"scenes":@YES}, //22
        //@{@"class":@"BannerViewController",@"interface":@"BannerViewController",@"scenes":@NO}, //22
        @{@"class":@"TypeGenericButtonsViewController",@"interface":@"ThreeButtonsViewController",@"scenes":@YES}, //23
        @{}, //24 - eis sound
  		@{@"class":@"TypeIpButtonViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //25
  		@{@"class":@"TypeIpViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //26
        @{@"class":@"TypeIpViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO}, //27
        @{}, //28 - modos AC intesis
        @{}, //29 - Intesis Ibox
        @{@"class":@"TypeIpWebViewViewController",@"interface":@"SimpleButtonViewController",@"scenes":@NO} //30 - Generic WebView
	];
	return devicesType;
}



//coreData types

+(const NSDictionary *) getAvailableDataEntityDevices{
	const NSDictionary *devTypes =@{
							@(TYPE_OBJECT_LIGHT): @"DeviceTypeLight",
		 					@(TYPE_OBJECT_DIMMER): @"DeviceTypeSlider",
		 					@(TYPE_OBJECT_KNX_SCENE): @"DeviceTypeLight",
		 					@(TYPE_OBJECT_SETPOINT): @"DeviceTypeSetpoint",
		 					@(TYPE_OBJECT_VIEWER): @"DeviceTypeLabel",
		 					@(TYPE_OBJECT_BLIND): @"DeviceTypeLight",
		 					@(TYPE_OBJECT_REG_BLIND): @"DeviceTypeSlider",
		 					@(TYPE_OBJECT_GRAD_BLIND): @"DeviceTypeGradhermetic",
		 					@(TYPE_OBJECT_VELUX_BLIND): @"DeviceTypeVelux",
		 					@(TYPE_OBJECT_RGB): @"DeviceTypeLight",
		 					@(TYPE_OBJECT_GENERIC_SEGMENTED): @"DeviceTypeSegmented",
		 					@(TYPE_OBJECT_CLIMA_1b): @"DeviceTypeClima",
		 					@(TYPE_OBJECT_CLIMA_1B): @"DeviceTypeClima",
		 					@(TYPE_OBJECT_IP_CAMERA): @"DeviceTypeIp",
	   						@(TYPE_OBJECT_IP_VIDEO_KNX): @"DeviceTypeIp",
		 					@(TYPE_OBJECT_GEN_VIEWER): @"DeviceTypeGenViewer",
		 					@(TYPE_OBJECT_BOOL_VIEWER): @"DeviceTypeBoolViewer",
		 					@(TYPE_OBJECT_TEXT_VIEWER): @"DeviceTypeLabel",
	   						@(TYPE_OBJECT_JUNG_SETPOINT): @"DeviceTypeSetpoint",
	   						@(TYPE_OBJECT_BERKER_SETPOINT): @"DeviceTypeSetpoint",
	   						@(TYPE_OBJECT_MULTIMEDIA_MACRO): @"DeviceTypeMultimedia",
	   						@(TYPE_OBJECT_MULTIMEDIA): @"DeviceTypeMultimedia",
	 						@(TYPE_OBJECT_PULSE_BUTTON): @"DeviceTypePulse",
                            @(TYPE_OBJECT_JUNG_TKM): @"DeviceTypeIp",
                            @(TYPE_OBJECT_IP_BUTTON): @"DeviceTypeIpButton",
                            @(TYPE_OBJECT_3_BUTTONS): @"DeviceType3Buttons",
                            @(TYPE_OBJECT_MOBOTIX_T24): @"DeviceTypeIp",
                            @(TYPE_OBJECT_IP_WEBVIEW_BUTTON): @"DeviceTypeIpButton"
		 };
	return devTypes;
}
@end
