//
//  ServerHandler.m
//  licenseTest
//
//  Created by Isaac Lozano on 8/30/13.
//  Copyright (c) 2013 Intesis. All rights reserved.
//

#import "ServerHandler.h"
#import "AFAppDotNetAPIClient.h"
#import "Reachability.h"
@implementation ServerHandler


+ (id)sharedInstance {
    static ServerHandler *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void) startMethod:(SHandlerMethod) method withInfo:(NSDictionary *) info usingBlock:(ServerHandlerBlock)delegate{
	_delegateBlock = delegate;
	
	[[AFAppDotNetAPIClient sharedClient] postPath:API_PATH parameters:[self setPostValuesMethod:method info:info] success:^(AFHTTPRequestOperation *operation, id JSON) {
		_delegateBlock(JSON);
	} failure:^(AFHTTPRequestOperation *operation, NSError *error) {
		_delegateBlock(nil);
	}];
}

-(NSDictionary *) setPostValuesMethod:(SHandlerMethod) method info:(NSDictionary *) info{
	
	const NSArray * postPath = @[
			@"checkUser",
			@"checkDevice",
		 	@"addDevice"];
	
	
	NSMutableDictionary *param = [@{
		  @"username": info[@"username"],
		@"password": info[@"password"],
		@"method": postPath[method],
	} mutableCopy];
	
	if (method == METHOD_ADD || method == METHOD_CHECK) {
		NSData *data = [NSJSONSerialization dataWithJSONObject:info[@"info"] options:0 error:nil];
		if (data){
			NSString *strInfo = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
			[param setObject:strInfo forKey:@"info"];
		}
	}

	return param;
}

@end
