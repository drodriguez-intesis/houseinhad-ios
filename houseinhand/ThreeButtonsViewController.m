//
//  ThreeButtonsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThreeButtonsViewController.h"

@interface ThreeButtonsViewController ()
@property (strong,nonatomic) UIImage *leftImage;
@property (strong,nonatomic) UIImage *rightImage;
@property (strong,nonatomic) UIImage *centerImage;
@end

@implementation ThreeButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _leftImage = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];
        _rightImage = [UIImage imageNamed:[newDevice valueForKey:@"image2"]];
        _centerImage = [UIImage imageNamed:[newDevice valueForKey:@"image3" ]];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
        _leftImage = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];
        _rightImage = [UIImage imageNamed:[newDevice valueForKey:@"image2"]];
        _centerImage = [UIImage imageNamed:[newDevice valueForKey:@"image3" ]];
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setImages];
}

-(void) setImages{
    [_leftButton setImage:_leftImage forState:UIControlStateNormal];
    [_leftButton setImage:_leftImage forState:UIControlStateHighlighted];
    [_rightButton setImage:_rightImage forState:UIControlStateNormal];
    [_rightButton setImage:_rightImage forState:UIControlStateHighlighted];
    [_centerButton setImage:_centerImage forState:UIControlStateNormal];
    [_centerButton setImage:_centerImage forState:UIControlStateHighlighted];
}

- (void)viewDidUnload
{
    _leftButton = nil;
    _rightButton = nil;
    _centerButton =nil;
	_rightImage = nil;
	_leftImage = nil;
	_centerImage = nil;
	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
