//
//  MultiCommand+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MultiCommand.h"

@interface MultiCommand (Parser)
+(MultiCommand *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc;

@end
