//
//  DevicesViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "SceneTelegram.h"
#import "Telegram+Parser.h"
#import "KNXCommunication.h"

#define STATUS_IND_VIEW_OFFSET			34
#define STATUS_IND_VIEW_SIZE				4
#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.05


@class ObjectsAimations;
@interface DevicesViewController (){
	SystemSoundID _buttonSound;

}
@property (strong,nonatomic) IBOutlet UILabel *deviceName;
@property (strong,nonatomic) UIView *statusIndView;
@property (strong,nonatomic) UIButton *sceneButton;
@property (strong,nonatomic) UITapGestureRecognizer *tapGesture;
@end

@implementation DevicesViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if (_device != newDevice) {
			_type = TYPE_CONTROL;
            _device = newDevice;
        }
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        if ([newDevice.allowEnvironment boolValue] == YES) {
			_type = TYPE_SCENE;
            _device = newDevice;
			_scene = scene;
        }
		else{
			return nil;
		}
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	if ([self respondsToSelector:@selector(checkStatus)]) {
		[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkSync) name:NOT_DID_REALLY_APP_BECOME_ACTIVE object:nil];
	}
    CGSize maximumLabelSize = CGSizeMake(self.view.bounds.size.width,self.view.bounds.size.height);
    CGSize expectedLabelSize = [_device.name sizeWithFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:22.0] constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    _deviceName.frame = CGRectMake(self.view.center.x- expectedLabelSize.width/2, self.deviceName.bounds.origin.y, expectedLabelSize.width , self.deviceName.bounds.size.height);
    _deviceName.text =_device.name;
	if ([_device.hasStatus boolValue] && _type == TYPE_CONTROL) {
		[self loadHasStatus];
	}
	else if (_type == TYPE_SCENE){
		[self addSceneGui];
	}
	[self loadSound];
}

-(void) loadHasStatus{
	[_device addObserver:self forKeyPath:@"isSynchronized" options:0 context:nil];
	[self loadStatusInd];
	[self loadTapOnTitle];
}

-(void) loadTapOnTitle{
	UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(manuallyReadStatus)];
	tap.numberOfTapsRequired = 1;
	tap.numberOfTouchesRequired = 1;
	_deviceName.userInteractionEnabled = YES;
	[_deviceName addGestureRecognizer:tap];
}

-(void) checkSync{
	[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(manuallyReadStatus) userInfo:nil repeats:NO];
}

-(void) manuallyReadStatus{
	[self checkStatus];
}

-(void) addSceneGui{
	if (_sceneButton == nil) {
		_sceneButton =[UIButton buttonWithType:UIButtonTypeCustom];
	}
	UIImage* buttonSelectedImage = [UIImage imageNamed:@"cancel"];
	_sceneButton.showsTouchWhenHighlighted = NO;
	[_sceneButton addTarget:self action:@selector(disableSceneWidget) forControlEvents:UIControlEventTouchUpInside];
	[_sceneButton setImage:buttonSelectedImage forState:UIControlStateNormal];
	[_sceneButton setImage:buttonSelectedImage forState:UIControlStateHighlighted];
	_sceneButton.frame = CGRectMake(self.view.bounds.size.width-50, self.deviceName.bounds.origin.y-1, buttonSelectedImage.size.width, buttonSelectedImage.size.height);
	[self.view addSubview:_sceneButton];
	
	if (_tapGesture == nil) {
		_tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(enableSceneWidget)];
	}
	_tapGesture.enabled = NO;
	_tapGesture.numberOfTapsRequired = 1;
	_tapGesture.numberOfTouchesRequired = 1;
	[self.view addGestureRecognizer:_tapGesture];
	if (_widgetSceneActive) {
		[self enableSceneWidget];
	}
	else{
		[self disableSceneWidget];
	}
}

-(void) disableSceneWidget{
	if (_type == TYPE_SCENE) {
		self.view.alpha = 0.3;
		_widgetSceneActive = NO;
		_sceneButton.hidden = YES;
		_sceneButton.userInteractionEnabled = NO;
		_tapGesture.enabled = YES;
		self.view.backgroundColor = [UIColor clearColor];
		self.view.layer.cornerRadius = 0.0;
	}
}

-(void) enableSceneWidget{
	if (_type == TYPE_SCENE) {
		self.view.alpha = 1.0;
		_widgetSceneActive = YES;
		_sceneButton.hidden = NO;
		_sceneButton.userInteractionEnabled = YES;
		_tapGesture.enabled = NO;
		self.view.backgroundColor = [UIColor colorWithWhite:.3 alpha:.8];
		self.view.layer.cornerRadius = 2.0;
	}
}

-(void) loadStatusInd{
	if (_statusIndView == nil) {
		_statusIndView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, STATUS_IND_VIEW_SIZE, STATUS_IND_VIEW_SIZE)];
		_statusIndView.backgroundColor = [UIColor whiteColor];
		_statusIndView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
		_statusIndView.layer.borderWidth = 1.0;
		_statusIndView.layer.cornerRadius = STATUS_IND_VIEW_SIZE / 2;
		[_statusIndView setCenter:CGPointMake(self.view.center.x, STATUS_IND_VIEW_OFFSET)];
		[self.view addSubview:_statusIndView];
		_statusIndView.hidden = YES;
	}
	[self updateStatusInd];
	
}

-(void) loadSound{
	NSString *buttonSoundPath = [[NSBundle mainBundle] pathForResource:@"button_sound" ofType:@"aifc"];
	NSURL *buttonSoundURL = [NSURL fileURLWithPath:buttonSoundPath];
	AudioServicesCreateSystemSoundID((__bridge CFURLRef)buttonSoundURL, &_buttonSound);
}

-(void) updateStatusInd{
	BOOL sync = [_device.isSynchronized boolValue];
	BOOL hasSt = [_device.hasStatus boolValue];
	if (!sync && hasSt) {
		_statusIndView.hidden = NO;
	}
	else{
		_statusIndView.hidden = YES;
	}
}

- (void)viewDidUnload
{
	[self.view removeGestureRecognizer:_tapGesture];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NOT_DID_REALLY_APP_BECOME_ACTIVE object:nil];
    _device = nil;
    _deviceName = nil;
	_statusIndView = nil;
	[super viewDidUnload];
}

-(void) dealloc{
	AudioServicesDisposeSystemSoundID(_buttonSound);
	if ([_device.hasStatus boolValue] && _type == TYPE_CONTROL && _device.observationInfo) {
		[_device removeObserver:self forKeyPath:@"isSynchronized"];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([object isKindOfClass:[Device class]]) {
		[self updateStatusInd];
	}
}

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}

-(IBAction)animationInitFinPressed:(id)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:^(BOOL finished){
            [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
    }];
}

-(void) animationUpdate:(id <ObjectsAnimations>) sender{
	if (ANIMATIONS_ENABLED) {
		UIControl *control = (UIControl *) sender;
		[UIView animateWithDuration:0.2 delay:0.01 options:UIViewAnimationOptionAutoreverse animations:^{
			control.alpha = 0.3;
		}completion:^(BOOL finished){
			control.alpha = 1.0;
			control.transform = CGAffineTransformIdentity;
		}];
	}
}

-(void) playButtonSound{
	if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_SOUNDS] boolValue]) {
		AudioServicesPlaySystemSound(_buttonSound);
	}
}

-(void) sendData:(NSNumber *) data address:(NSNumber *) address dpt:(NSString *) dpt type:(NSNumber *) type delay:(NSNumber *) delay{
	if (_type == TYPE_CONTROL) {
		//_device.isSynchronized = @NO;
		NSData *info = [Telegram getDataFromInfo:data dpt:dpt write:type.boolValue];
        if (info) {
            NSDictionary *dict = @{@"data": info,@"address":address};
            [NSTimer scheduledTimerWithTimeInterval:[delay floatValue] target:self selector:@selector(sendData:) userInfo:dict repeats:NO];
        }
	}
	else if (_type == TYPE_SCENE){
		[self enableSceneWidget];
	}
}

-(void) sendData:(NSTimer *) timer{
    NSDictionary * info = [timer userInfo];
	[[KNXCommunication sharedInstance] sendInfoToKnx:info[@"data"] address:info[@"address"] objectId:_device.objectID];
}

#pragma mark initMethods

-(CGFloat) readSceneStatus{
	return 0;
}

-(CGFloat) readControlStatus{
	return 0;
}

#pragma mark Get Scene Status

-(BOOL) needSceneStatus{
	return _widgetSceneActive;
}

-(NSArray *) getSceneStatus{
	return nil;
}
@end
