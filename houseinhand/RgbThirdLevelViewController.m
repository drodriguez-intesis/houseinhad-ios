//
//  RgbThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/13/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#define FIRST_PAGE			0
#define SECOND_PAGE		1
#define THIRD_PAGE			2

#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.07

#define TIMER_DELAY_TO_UPDATE			.5
#define TIMER_DELAY_TO_SEND				.5

#import <AudioToolbox/AudioToolbox.h>
#import "RgbThirdLevelViewController.h"
#import "Telegram+Parser.h"
#import "Device+Parser.h"
#import "KNXCommunication.h"
#import "Scene+Parser.h"
@interface RgbThirdLevelViewController ()
@property (strong,nonatomic) RSColorPickerView * colorPicker;
@property (strong,nonatomic) RSBrightnessSlider * brightnessSlider;
@property (nonatomic,strong) Telegram * rStatus;
@property (nonatomic,strong) Telegram * gStatus;
@property (nonatomic,strong) Telegram * bStatus;
@property (nonatomic,strong) Telegram * rControl;
@property (nonatomic,strong) Telegram * gControl;
@property (nonatomic,strong) Telegram * bControl;
@property (nonatomic) Device *device;
@property (assign,nonatomic) CGFloat rCurrent,gCurrent,bCurrent,brightCurrent;
@property (assign,nonatomic) BOOL readEnabled, isScene;
@property (strong,nonatomic) NSTimer *favouriteColorsTimer,*updateDelayTimer,*sendDelayTimer;
@property (strong,nonatomic) UISwipeGestureRecognizer * gesture;
-(IBAction)buttonTouched;
@end

@implementation RgbThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_device = newDevice;
		_isScene = NO;
		[self readControlStatus];
	//	[self initRgbView];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice sceneColors:(NSArray *) colors{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_device = newDevice;
		_isScene = YES;
		_rCurrent = [colors[0] floatValue] / 255.0;
		_gCurrent = [colors[1] floatValue] / 255.0;
		_bCurrent = [colors[2] floatValue] / 255.0;
    }
    return self;
}

-(void) readControlStatus{
	_rStatus = [_device getTelegramWithCode:0];
	_gStatus = [_device getTelegramWithCode:1];
	_bStatus = [_device getTelegramWithCode:2];
	_rControl = [_device getTelegramWithCode:3];
	_gControl = [_device getTelegramWithCode:4];
	_bControl = [_device getTelegramWithCode:5];
}

-(void) readSceneStatus:(Scene*) scene{
	
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	//[NSThread detachNewThreadSelector:@selector(loadRgbView) toTarget:self withObject:nil];
	[self loadRgbView];
	//[NSTimer scheduledTimerWithTimeInterval:0 target:self selector:@selector(loadRgbView) userInfo:nil repeats:NO];
	[_scrollView setContentSize:CGSizeMake(600, _scrollView.bounds.size.height)];
	_readEnabled = YES;
	[self initGestureRecognizer];
	[self loadPredefinedColors];
	if (_isScene) {
		[self updateColorPicker];
	}
	else{
		[self checkStatus];
		[self addObservers];
	}
}

-(void) addObservers{
	if ([_device.hasStatus boolValue]) {
		[_rStatus addObserver:self forKeyPath:@"value" options:0 context:nil];
		[_gStatus addObserver:self forKeyPath:@"value" options:0 context:nil];
		[_bStatus addObserver:self forKeyPath:@"value" options:0 context:nil];
	}
}

-(void) checkStatus{
    if ([_device.hasStatus boolValue] && ![_device.isSynchronized boolValue]) {
        [self sendData:@0 address:_rStatus.address dpt:_rStatus.dpt type:_rStatus.type delay:_rStatus.delay];
		[self sendData:@0 address:_gStatus.address dpt:_gStatus.dpt type:_gStatus.type delay:_rStatus.delay];
        [self sendData:@0 address:_bStatus.address dpt:_bStatus.dpt type:_bStatus.type delay:_rStatus.delay];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:object];
		[_updateDelayTimer invalidate];
		_updateDelayTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_DELAY_TO_UPDATE target:self selector:@selector(performUpdateProcess) userInfo:nil repeats:NO];
	}
}

-(void) performUpdateProcess{
//	if (_readEnabled) {
		[self updateColorPicker];
	//}
}

-(void) initColorPicker{
		_rCurrent =1.0;
		_gCurrent = 1.0;
		_bCurrent = 1.0;
}

-(void) update:(Telegram *) status{
	//NSLog(@"RGB STATUS IS:%@",status);
	if ([status isEqual:_rStatus] && status.value.floatValue/255.0 != _rCurrent){
		_rCurrent = status.value.floatValue/255.0;
	}
	else if ([status isEqual:_gStatus] && status.value.floatValue/255.0  != _gCurrent){
		_gCurrent = status.value.floatValue/255.0;
	}
	else if ([status isEqual:_bStatus] && status.value.floatValue/255.0  != _bCurrent){
		_bCurrent = status.value.floatValue/255.0;
	}
}

-(void) updateColorPicker{
	UIColor *selectionColor = [UIColor colorWithRed:_rCurrent green:_gCurrent blue:_bCurrent alpha:1.0];
	//NSLog(@"Updating with color...RED:%f --- GREEN:%f --- BLUE:%f",_rCurrent,_gCurrent,_bCurrent);
	[_colorPicker setSelectionColor:selectionColor];
}

-(void) dealloc{
	if ([_device.hasStatus boolValue]) {
		if (_rStatus.observationInfo) [_rStatus removeObserver:self forKeyPath:@"value"];
		if (_gStatus.observationInfo) [_gStatus removeObserver:self forKeyPath:@"value"];
		if (_bStatus.observationInfo) [_bStatus removeObserver:self forKeyPath:@"value"];
		if (_rControl.observationInfo) [_rControl removeObserver:self forKeyPath:@"value"];
		if (_gControl.observationInfo) [_gControl removeObserver:self forKeyPath:@"value"];
		if (_bControl.observationInfo) [_bControl removeObserver:self forKeyPath:@"value"];
	}
}

- (void)viewDidUnload
{
	_colorPicker = nil;
	_brightnessSlider = nil;
	_rStatus = nil;
	_gStatus = nil;
	_bStatus = nil;
	_rControl = nil;
	_bControl = nil;
	_gControl = nil;
	_device = nil;
	[_favouriteColorsTimer invalidate];
	_favouriteColorsTimer = nil;
	[_updateDelayTimer invalidate];
	_updateDelayTimer = nil;
	_sendDelayTimer = nil;
	[super viewDidUnload];
}

-(void) scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(IBAction)buttonTouched{
	NSInteger page =0;
	if (_paletteButton.isSelected) {
		page = 1;
	}
	switch (page) {
		case FIRST_PAGE:
			[_paletteButton setSelected:YES];
			[_favouriteButton setSelected:NO];
			_gesture.direction = UISwipeGestureRecognizerDirectionLeft;
			break;
		case SECOND_PAGE:
			[_paletteButton setSelected:NO];
			[_favouriteButton setSelected:YES];
			_gesture.direction = UISwipeGestureRecognizerDirectionRight;
			break;
		default:
			break;
	}
	NSInteger offset = page * self.view.bounds.size.width;
	[_scrollView setContentOffset:CGPointMake(offset, 0) animated:YES];
}

#pragma mark - ColorPicker 

-(void) initGestureRecognizer{
	_gesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(buttonTouched)];
	_gesture.direction = (UISwipeGestureRecognizerDirectionLeft);
	_gesture.delegate = self;
	[self.view addGestureRecognizer:_gesture];
}

-(void) loadRgbView{
	
	
	//colorPatch = [[UIView alloc] initWithFrame:CGRectMake(10.0, 400.0, 300.0, 30.0)];
	NSUInteger size = (DEVICE_IS_IPAD ? 320 : 240);
	NSUInteger yPickerOrigin = (DEVICE_IS_IPAD ?  self.view.bounds.size.height/2 - size/2 : 10);
	NSUInteger yBrightOrigin = (DEVICE_IS_IPAD ? self.view.bounds.size.height - 60 : 250);
	_colorPicker = [[RSColorPickerView alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2 - size/2, yPickerOrigin, size, size)];
	[_colorPicker setCropToCircle:YES];
	[_colorPicker setDelegate:self];
	if (!_isScene)[self initColorPicker];
	[_colorPicker setBackgroundColor:[UIColor clearColor]];
	
	_brightnessSlider = [[RSBrightnessSlider alloc] initWithFrame:CGRectMake(self.view.bounds.size.width/2 - size/2, yBrightOrigin, size, 30.0)];
	[_brightnessSlider setColorPicker:_colorPicker];
	UIView *view = _scrollView;
	if (DEVICE_IS_IPAD) view = self.view;
	[view addSubview:_colorPicker];
	[view addSubview:_brightnessSlider];
	[view bringSubviewToFront:_colorPicker];
	_colorView.layer.cornerRadius = _colorView.bounds.size.width/2;
	
	if	(!_isScene){
		[self update:_rStatus];
		[self update:_gStatus];
		[self update:_bStatus];
		[self updateColorPicker];
	}
}

-(void) loadFavColors{
	
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
}

-(void) loadPredefinedColors{
	for (UIButton *button in _predefinedColors) {
		if (!_isScene) [button addTarget:self action:@selector(colorStartTouched:) forControlEvents:UIControlEventTouchDown];
		[button addTarget:self action:@selector(animationInitPressed:) forControlEvents:UIControlEventTouchDown];
		[button addTarget:self action:@selector(colorFinTouched:) forControlEvents:UIControlEventTouchUpInside];
		[button addTarget:self action:@selector(animationFinPressed:) forControlEvents:UIControlEventTouchUpInside];
		[button addTarget:self action:@selector(animationFinPressed:) forControlEvents:UIControlEventTouchCancel];

		button.layer.cornerRadius = button.bounds.size.width/2;
		button.layer.borderWidth = 1;
		//button.layer.borderColor = [[UIColor whiteColor] CGColor];
		[self setColor:button];
	}
}

-(void) setColor:(UIButton *) button{
	NSDictionary *color = [[NSUserDefaults standardUserDefaults] objectForKey:[NSString stringWithFormat:@"%@_%d",USER_DEFAULTS_RGB,button.tag ]];
	
	NSNumber * red = [color valueForKey:@"red"];
	NSNumber * green = [color valueForKey:@"green"];
	NSNumber * blue = [color valueForKey:@"blue"];

	button.backgroundColor = [UIColor colorWithRed:[red floatValue]  green:[green floatValue] blue:[blue floatValue] alpha:1.0];
}

-(void) saveColor:(UIColor *) color position:(NSUInteger) position{
	const CGFloat *components = CGColorGetComponents(color.CGColor);
	NSDictionary * colorDict = @{@"red": @(components[0]),@"green": @(components[1]),@"blue": @(components[2])};
	[[NSUserDefaults standardUserDefaults] setObject:colorDict forKey:[NSString stringWithFormat:@"%@_%d",USER_DEFAULTS_RGB,position]];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark - ColorPicker Delegate
-(void)colorPickerDidChangeSelection:(RSColorPickerView *)cp {
	_readEnabled = NO;
	CGFloat alpha =0.0;
	[[cp selectionColor] getRed:&_rCurrent green:&_gCurrent blue:&_bCurrent alpha:&alpha];
	_colorView.backgroundColor = [cp selectionColor];
    _brightnessSlider.value = [cp brightness];
}

-(void) colorPickerDidEndSelection:(RSColorPickerView *)cp{
	_readEnabled = YES;
	_colorView.backgroundColor = [cp selectionColor];
    _brightnessSlider.value = [cp brightness];
	CGFloat alpha =0.0;
	[[cp selectionColor] getRed:&_rCurrent green:&_gCurrent blue:&_bCurrent alpha:&alpha];
	[self startSendingProcess];
}

#pragma mark - Sending data
-(void) startSendingProcess{
	if (_isScene) return;
	[_sendDelayTimer invalidate];
	_sendDelayTimer = [NSTimer scheduledTimerWithTimeInterval:TIMER_DELAY_TO_SEND target:self selector:@selector(endSendingProcess) userInfo:nil repeats:NO];
}

-(void) endSendingProcess{
	[self sendData:@(_rCurrent*255) address:_rControl.address dpt:_rControl.dpt type:_rControl.type delay:_rControl.delay];
	[self sendData:@(_gCurrent*255)  address:_gControl.address dpt:_gControl.dpt type:_gControl.type delay:@([_bControl.delay floatValue]+ .1)];
	[self sendData:@(_bCurrent*255)  address:_bControl.address dpt:_bControl.dpt type:_bControl.type delay:@([_bControl.delay floatValue]+ .2)];
}

-(void) sendData:(NSNumber *) data address:(NSNumber *) address dpt:(NSString *) dpt type:(NSNumber *) type delay:(NSNumber *) delay{
	NSData *info = [Telegram getDataFromInfo:data dpt:dpt write:type.boolValue];
	NSDictionary *dict = @{@"data": info,@"address":address};
    [NSTimer scheduledTimerWithTimeInterval:[delay floatValue] target:self selector:@selector(sendData:) userInfo:dict repeats:NO];
}

-(void) sendData:(NSTimer *) timer{
    NSDictionary * info = [timer userInfo];
	[[KNXCommunication sharedInstance] sendInfoToKnx:info[@"data"] address:info[@"address"] objectId:_device.objectID];
}

#pragma mark - Color Touched

-(void) colorStartTouched:(UIButton *) color{
	[_favouriteColorsTimer invalidate];
	_favouriteColorsTimer = nil;
	_favouriteColorsTimer = [NSTimer scheduledTimerWithTimeInterval:LONG_TOUCH target:self selector:@selector(longTouchFired:) userInfo:color repeats:NO];
}

-(void) colorFinTouched:(UIButton *) color{
	if (_isScene) {
		_colorView.backgroundColor = color.backgroundColor;
		[_colorPicker setSelectionColor:color.backgroundColor];
	}
	else if (_favouriteColorsTimer.isValid) {
		[_favouriteColorsTimer invalidate];
		_favouriteColorsTimer = nil;
		const CGFloat *components = CGColorGetComponents(color.backgroundColor.CGColor);
		[self sendData:@(components[0]*255) address:_rControl.address dpt:_rControl.dpt type:_rControl.type delay:_rControl.delay];
		[self sendData:@(components[1]*255)  address:_gControl.address dpt:_gControl.dpt type:_gControl.type delay:_rControl.delay];
		[self sendData:@(components[2]*255)  address:_bControl.address dpt:_bControl.dpt type:_bControl.type delay:_rControl.delay];
	}
}

-(void) longTouchFired:(NSTimer *) timer{
	AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
	UIButton * color = [timer userInfo];
	color.backgroundColor = _colorView.backgroundColor;
	[self saveColor:color.backgroundColor position:color.tag];
}

#pragma mark - Objects Animations delegate

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}

#pragma mark gesture recognizer

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
	CGPoint  gestureStartPoint = [touch locationInView:_scrollView];
	if(CGRectContainsPoint(_colorPicker.frame, gestureStartPoint)) return NO;
	return YES;
}


-(IBAction)dismissView{
	if (_isScene) {
		if (_delegateScene && [_delegateScene respondsToSelector:@selector(rgbDidEndChoosingColorScene:colors:)]) {
			[_delegateScene rgbDidEndChoosingColorScene:self colors:@[@(_rCurrent*255.0),@(_gCurrent*255.0),@(_bCurrent*255.0)]];
		}
	}
	[super dismissView];
}
@end
