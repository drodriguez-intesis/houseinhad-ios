//
//  Stay.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Stay.h"
#import "BackgroundStayImage.h"
#import "Config.h"
#import "Device.h"
#import "Stay.h"


@implementation Stay

@dynamic bckgColor;
@dynamic isSecure;
@dynamic level;
@dynamic name;
@dynamic order;
@dynamic passcode;
@dynamic thumbImage;
@dynamic useCustomImage;
@dynamic image;
@dynamic r_config;
@dynamic r_device;
@dynamic r_nextStay;
@dynamic r_prevStay;
@dynamic r_backgroundImage;

@end
