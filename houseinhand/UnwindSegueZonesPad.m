//
//  UnwindSegueZonesPad.m
//  houseinhand
//
//  Created by Isaac Lozano on 1/2/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "UnwindSegueZonesPad.h"
#import "InitialViewController.h"

@implementation UnwindSegueZonesPad


- (void)perform
{
	
	UIViewController *source = self.sourceViewController;
	UIViewController *destination = self.destinationViewController;
	InitialViewController * initial = (InitialViewController *)destination;
    [UIView transitionWithView:destination.view duration:.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
		initial.zonesView.alpha = 0.0;
		initial.zonesView.transform = CGAffineTransformMakeScale(.9, .9);
		[initial.staysView setCenter:CGPointMake(initial.staysView.center.x +144, initial.staysView.center.y)];
    } completion:^(BOOL finished) {
		initial.staysView.userInteractionEnabled = YES;
        [[[initial getStaysViewController] view] setUserInteractionEnabled:YES];
		initial.zonesView.hidden = YES;
		initial.zonesView.transform = CGAffineTransformIdentity;
		[initial.zonesView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
		[source removeFromParentViewController];
    }];
}

@end
