//
//  TwoButtonsViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"
#import "UIButton_Sub.h"
@interface TwoButtonsViewController : DevicesViewController
@property (strong,nonatomic) IBOutlet UIButton_Sub *leftButton;
@property (strong,nonatomic) IBOutlet UIButton_Sub *rightButton;

@end
