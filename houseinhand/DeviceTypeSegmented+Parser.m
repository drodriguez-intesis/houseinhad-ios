//
//  DeviceTypeSegmented+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeSegmented+Parser.h"
#import "Device+Parser.h"

#define KEY_NUM_SEGMENTS					@"numSegments"
#define KEY_READ_ONLY							@"readonly"

#define KEY_FIRST_VALUE							@"firstValue"
#define KEY_SECOND_VALUE					@"secondValue"
#define KEY_THIRD_VALUE						@"thirdValue"
#define KEY_FOURTH_VALUE						@"fourthValue"
#define KEY_FIFTH_VALUE							@"fifthValue"

#define KEY_FIRST_TEXT							@"firstText"
#define KEY_SECOND_TEXT						@"secondText"
#define KEY_THIRD_TEXT							@"thirdText"
#define KEY_FOURTH_TEXT						@"fourthText"
#define KEY_FIFTH_TEXT							@"fifthText"

@implementation DeviceTypeSegmented (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeSegmented *dev = (DeviceTypeSegmented *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.numSegments = (info[KEY_NUM_SEGMENTS] ? @([info[KEY_NUM_SEGMENTS] integerValue]): dev.numSegments);
    
    dev.readOnly = (info[KEY_READ_ONLY] ? @([info[KEY_READ_ONLY] boolValue]): dev.readOnly);
	
	dev.firstValue = (info[KEY_FIRST_VALUE] ? @([info[KEY_FIRST_VALUE] integerValue]): dev.firstValue);
	dev.secondValue = (info[KEY_SECOND_VALUE] ? @([info[KEY_SECOND_VALUE] integerValue]): dev.secondValue);
	dev.thirdValue = (info[KEY_THIRD_VALUE] ? @([info[KEY_THIRD_VALUE] integerValue]): dev.thirdValue);
	dev.fourthValue = (info[KEY_FOURTH_VALUE] ? @([info[KEY_FOURTH_VALUE] integerValue]): dev.fourthValue);
	dev.fifthValue = (info[KEY_FIFTH_VALUE] ? @([info[KEY_FIFTH_VALUE] integerValue]): dev.fifthValue);
	
	dev.firstText = (info[KEY_FIRST_TEXT] ? info[KEY_FIRST_TEXT]: dev.firstText);
	dev.secondText = (info[KEY_SECOND_TEXT] ? info[KEY_SECOND_TEXT]: dev.secondText);
	dev.thirdText = (info[KEY_THIRD_TEXT] ? info[KEY_THIRD_TEXT]: dev.thirdText);
	dev.fourthText = (info[KEY_FOURTH_TEXT] ? info[KEY_FOURTH_TEXT]: dev.fourthText);
	dev.fifthText = (info[KEY_FIFTH_TEXT] ? info[KEY_FIFTH_TEXT]: dev.fifthText);
	
	return dev;
}


@end
