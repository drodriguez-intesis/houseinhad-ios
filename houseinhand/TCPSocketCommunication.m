//
//  TCPSocketCommunication.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TCPSocketCommunication.h"
#import "KNXCommunication.h"

@interface TCPSocketCommunication ()
@property (strong,nonatomic) dispatch_queue_t rxQueue;
@property (strong,nonatomic) GCDAsyncSocket *socket;
@end
@implementation TCPSocketCommunication

-(void) connectToHost:(NSString *) ip onPort:(NSUInteger) port{
	if (_rxQueue == nil) {
		_rxQueue = dispatch_queue_create("multimediaQueue", NULL);
	}
	if (!_socket) {
		_socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
		if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_DISCONNECTED];
	}
	if (!_socket.isConnected) {
		[_socket connectToHost:ip onPort:port error:nil];
		if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_CONNECTING];
	}
}

-(void) connectToDevice:(DeviceTypeMultimedia *) device{
	if (_rxQueue == nil) {
		_rxQueue = dispatch_queue_create("multimediaQueue", NULL);
	}
	if (!_socket) {
		_socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
		if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_DISCONNECTED];
	}
	if (!_socket.isConnected) {
		NSString *ip = device.localIp;
		NSUInteger port = device.localPort.unsignedIntegerValue;
		if (![[KNXCommunication sharedInstance] connectionIsLocal]) {
			ip = [[KNXCommunication sharedInstance] getRemoteIp];
			port = device.remotePort.unsignedIntegerValue;
		}
		[_socket connectToHost:ip onPort:port error:nil];
		if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_CONNECTING];

	}
}

-(BOOL) isConnected{
	return _socket.isConnected;
}

-(void) disconnect{
	[_socket disconnect];
}

-(void) sendInfo:(NSString *) info{
	if (_socket.isConnected) {
		NSData* data = [info dataUsingEncoding:NSASCIIStringEncoding];
		[_socket writeData:data withTimeout:-1 tag:0];
	}
}

-(void) socket:(GCDAsyncSocket *)sock didConnectToHost:(NSString *)host port:(uint16_t)port{
	if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_CONNECTED];
	[sock readDataWithTimeout:-1 tag:0];
}

-(void) socket:(GCDAsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
	[sock readDataWithTimeout:-1 tag:0];
	NSString *str = [[NSString alloc] initWithData:data encoding:NSASCIIStringEncoding];
	if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didReadStringResponse:)]) [self.delegate tcpSocketCommunication:self didReadStringResponse:str];
}

-(void) socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err{
	if ([self.delegate respondsToSelector:@selector(tcpSocketCommunication:didChangeConnectionStatus:)]) [self.delegate tcpSocketCommunication:self didChangeConnectionStatus:STATUS_DISCONNECTED];
}

@end
