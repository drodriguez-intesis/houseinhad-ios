//
//  Stay+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Stay+Parser.h"
#import "Device+Parser.h"
#import "DevicesCompatibility.h"
#import "BackgroundStayImage.h"

#define KEY_NAME						@"name"
#define KEY_BCKG_COLOR			@"backgroundColor"
#define KEY_SECURE						@"locked"
#define KEY_IMAGE						@"image"
#define KEY_PASSCODE					@"passcode"
#define KEY_STAYS						@"stays"
#define KEY_DEVICES					@"devices"

#define KEY_DEVICE_TYPE				@"appCode"

@implementation Stay (Parser)

+(Stay *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	Stay *stay = (Stay *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	stay.order = @(idx);
	stay.name = (info[KEY_NAME] ? info[KEY_NAME]: stay.name);
	stay.bckgColor = (info[KEY_BCKG_COLOR] ? info[KEY_BCKG_COLOR]: stay.bckgColor);
	stay.passcode = (info[KEY_PASSCODE] ? info[KEY_PASSCODE]: stay.passcode);
	stay.image = (info[KEY_IMAGE] ? info[KEY_IMAGE]: stay.image);
	stay.isSecure = (info[KEY_SECURE] ? @([info[KEY_SECURE] integerValue]): stay.isSecure);
	stay.level = @0;
	
	
	//add stays array
	if (info[KEY_STAYS] && [info[KEY_STAYS] count] >0){
		[info[KEY_STAYS] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			Stay *st = [Stay insertWithInfo:obj order:idx intoManagedObjectContext:moc];
			st.level = @1;
			[stay addR_nextStayObject:st];
		}];
	}
	else if (info[KEY_DEVICES] && [info[KEY_DEVICES] count] >0){
		const NSDictionary *devTypes = [DevicesCompatibility getAvailableDataEntityDevices];
		[info[KEY_DEVICES] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			NSNumber * order = @([obj[KEY_DEVICE_TYPE] integerValue]);
			Class klass = NSClassFromString(devTypes[order]);
			Device *dev = (Device *)[klass insertWithInfo:obj order:idx intoManagedObjectContext:moc];
			if (dev)[stay addR_deviceObject:dev];
		}];
		

	}
	return stay;
}

-(NSArray *) getOrderedStays{
	if (!self.r_nextStay || self.r_nextStay.count == 0) return nil;
	return [[self.r_nextStay allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
}

-(NSArray *) getOrderedDevices{
	if (!self.r_device || self.r_device.count == 0) return nil;
	return [[self.r_device allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
}

-(NSArray *) getTelegramsWithAddress:(NSNumber *) address{
	__block NSNumber * adr= address;
	__block NSMutableArray *array = [NSMutableArray array];
	if (self.r_nextStay && self.r_nextStay.count >0) {
		[[self.r_nextStay allObjects] enumerateObjectsUsingBlock:^(Stay * obj, NSUInteger idx, BOOL *stop) {
			NSArray * telegrams = [obj getTelegramsWithAddress:adr];
			if (telegrams && telegrams.count>0) [array addObjectsFromArray:telegrams];
		}];
	}
	else if (self.r_device && self.r_device.count > 0) {
		[[self.r_device allObjects] enumerateObjectsUsingBlock:^(Device * obj, NSUInteger idx, BOOL *stop) {
			NSArray * telegrams = [obj getTelegramsWithAddress:adr];
			if (telegrams && telegrams.count>0) [array addObjectsFromArray:telegrams];
		}];
	}
	return array;
}


#pragma mark Background Image

-(void) storeBackgroundImage:(UIImage *) image{
	if	(!self.r_backgroundImage){
		self.r_backgroundImage = [NSEntityDescription insertNewObjectForEntityForName:@"BackgroundStayImage" inManagedObjectContext:self.managedObjectContext];
	}
	BackgroundStayImage *bckgIm = (BackgroundStayImage *) self.r_backgroundImage;
	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
		bckgIm.backgroundImage = UIImagePNGRepresentation(image);
	});
}

-(UIImage *) retrieveBackgroundImage{
	if	(!self.r_backgroundImage) return nil;
	BackgroundStayImage *bckgIm = (BackgroundStayImage *) self.r_backgroundImage;
	return [UIImage imageWithData:bckgIm.backgroundImage];
}

@end
