//
//  Config+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Config.h"
@class Network;
@interface Config (Parser)
+(void) deleteAllInContext:(NSManagedObjectContext *) moc;
+(Config *) insertConfig:(NSDictionary *) info isActive:(BOOL) isActive context:(NSManagedObjectContext *) moc;
+(Config *) getActiveConfigOfContext:(NSManagedObjectContext *) moc;
+(Config *) getDemoConfigOfContext:(NSManagedObjectContext *) moc;

-(NSArray *) getOrderedStays;
-(NSNumber *) getDevicesCount;
-(Network *) getNetworkOfType:(NSUInteger) type;
-(NSArray *) getTelegramsWithAddress:(NSNumber *) address;
-(NSArray *) getTelegramsWithAddress:(NSNumber *) address ofContext:(NSManagedObjectContext *) moc;
+(Config *) getActiveConfigOfContext:(NSManagedObjectContext *) moc prefetch:(NSArray *) prefetch;
@end
