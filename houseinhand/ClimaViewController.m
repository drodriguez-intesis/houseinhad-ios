//
//  ClimaViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ClimaViewController.h"
#import "DeviceTypeClima.h"
@interface ClimaViewController ()
@end

@implementation ClimaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_hasLock = [newDevice.hasLock boolValue];
	}
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima *)newDevice scene:(Scene *)scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		_hasLock = [newDevice.hasLock boolValue];
	}
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
	_centralSegmented.selectedSegmentIndex = UISegmentedControlNoSegment;
	UIFont *font =[UIFont fontWithName:@"HelveticaNeue" size:11.0];
	NSDictionary *attributes = @{UITextAttributeFont : font};
	[_centralSegmented setTitleTextAttributes:attributes forState:UIControlStateNormal];
	[self configureSegmented];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) configureSegmented{
    [_centralSegmented setTitle:NSLocalizedStringFromTableInBundle(@"Comfort", nil, CURRENT_LANGUAGE_BUNDLE, @"small segmented text") forSegmentAtIndex:0];
    [_centralSegmented setTitle:NSLocalizedStringFromTableInBundle(@"Standby", nil, CURRENT_LANGUAGE_BUNDLE, @"small segmented text") forSegmentAtIndex:1];
    [_centralSegmented insertSegmentWithTitle:NSLocalizedStringFromTableInBundle(@"Night", nil, CURRENT_LANGUAGE_BUNDLE, @"small segmented text") atIndex:2 animated:NO];
    [_centralSegmented insertSegmentWithTitle:NSLocalizedStringFromTableInBundle(@"Extremes", nil, CURRENT_LANGUAGE_BUNDLE, @"small segmented text") atIndex:3 animated:NO];
    if (_hasLock) {
        [_centralSegmented insertSegmentWithTitle:NSLocalizedStringFromTableInBundle(@"Lock", nil, CURRENT_LANGUAGE_BUNDLE, @"small segmented text") atIndex:4 animated:NO];
    }
}
@end
