//
//  DevicesViewControllerViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DevicesViewController.h"
#import "ThirdLevelViewController.h"
#import "ActionScrollViewController.h"
#import "PasswordViewController.h"
#import "NewActionController.h"
@interface DevicesViewControllerViewController : UIViewController <DevicesViewControllerDelegate,ThirdLevelViewControllerDelegate,ActionScrollViewControllerDelegate,PasswordViewControllerDelegate,PasswordViewControllerDelegate,NewActionControllerDelegate,UIActionSheetDelegate>
@property (assign) NSUInteger previousLevel;
@property (weak,nonatomic) IBOutlet UILabel *stayName;
@property (weak,nonatomic) IBOutlet UIScrollView *devicesScrollView;
@property (weak,nonatomic) IBOutlet UIButton *topOrangeImage;
@property (weak, nonatomic) IBOutlet UIButton *optionsButton;
@property (strong,nonatomic) id detailItem;
-(IBAction)resetScrollOffset;
-(IBAction)showOptionsMenu:(UIButton *) button;
-(void) reloadDetailItem;
@end
