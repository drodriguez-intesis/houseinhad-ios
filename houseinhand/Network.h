//
//  Network.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Settings;

@interface Network : NSManagedObject

@property (nonatomic, retain) NSString * ip;
@property (nonatomic, retain) NSNumber * nat;
@property (nonatomic, retain) NSNumber * port;
@property (nonatomic, retain) Settings *r_settings;

@end
