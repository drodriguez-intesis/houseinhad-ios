//
//  ActionPopupViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 11/30/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#define CELL_CHANGE_NAME		0
#define CELL_TAKE_IMAGE			1
#define CELL_SELECT_IMAGE			2
#define CELL_REMOVE_IMAGE		3
#define CELL_CHANGE_PSW			4


#define IMAGE_CROP_OFFSET_X			30
#define IMAGE_CROP_OFFSET_Y			20

#import "ActionPopupViewController.h"
#import "ProfilesCell.h"
#import "StayViewController.h"

@interface ActionPopupViewController ()
@property (weak,nonatomic) IBOutlet UILabel * titleName;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) NSArray * items;
@property (strong,nonatomic) UIImagePickerController *imagePicker;
@property (strong,nonatomic) UIView *overlayView;
@end

@implementation ActionPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title items:(NSArray *) items
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_name = title;
		_items = items;
    }
    return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self resizeFromItemsNumber];
    [self registerNib];
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(createImagePicker) userInfo:nil repeats:NO];
	_titleName.text = _name;

}


-(void) resizeFromItemsNumber{
	NSUInteger indexArr[] = {210,170,130,90,50,10,0};
	CGRect frame = self.view.frame;
	frame.size.height -=  indexArr[[_items count] -1];
	self.view.frame = frame;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

-(void) registerNib{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    UINib *topNib = [UINib nibWithNibName:@"ProfilesCell" bundle:classBundle];
    [[self tableView] registerNib:topNib forCellReuseIdentifier:@"Profiles"];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
	_items = nil;
	_titleName = nil;
	_name	 = nil;
	_imagePicker = nil;
	_overlayView = nil;

    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"Profiles";
    ProfilesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if (cell == nil) {
        cell = [[ProfilesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    }
	cell.name.text =  _items[[indexPath row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundColor = [UIColor clearColor];
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_items count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	switch ([indexPath row]) {
		case CELL_CHANGE_NAME:
			[self changeName];
			break;
		case CELL_TAKE_IMAGE:
			if (_imagePicker)[self presentViewController:_imagePicker animated:YES completion:nil];
			break;
		case CELL_SELECT_IMAGE:
			[self createLibraryPickerFromRect:tableView.frame];
			break;
		case CELL_REMOVE_IMAGE:
			[self deleteImage];
			//remove
			break;
		case CELL_CHANGE_PSW:
			//change psw
			break;
			
		default:
			break;
	}
}

#pragma mark ImagePicker

-(void) createImagePicker{
	if (_imagePicker == nil && [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
		_imagePicker =[[UIImagePickerController alloc] init];
		_imagePicker.delegate = self;
		_imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
		_imagePicker.showsCameraControls = YES;
		_imagePicker.allowsEditing = YES;
		_overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, IMAGE_CROP_SIZE_W, IMAGE_CROP_SIZE_H)];
		[_overlayView setCenter:CGPointMake(_imagePicker.view.center.x, _imagePicker.view.center.y - 50)];
		_overlayView.layer.borderWidth = 2;
		_overlayView.layer.borderColor = [[UIColor orangeColor] CGColor];
		_imagePicker.cameraOverlayView = _overlayView;
	}
}

-(void) createLibraryPickerFromRect:(CGRect) rect{
	UIImagePickerController *picker =[[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	picker.allowsEditing = YES;
	if (DEVICE_IS_IPAD) {
		UIPopoverController *popover = [[UIPopoverController alloc] initWithContentViewController:picker];
		popover.delegate = self;
		[popover presentPopoverFromRect:rect inView:self.parentViewController.view permittedArrowDirections:(UIPopoverArrowDirectionLeft|UIPopoverArrowDirectionRight) animated:YES];

	}
	else{
		[self presentViewController:picker animated:YES completion:nil];
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
	self.view.hidden = NO;
	UIImage *resizedImage = [info valueForKey:UIImagePickerControllerEditedImage];
	if (resizedImage == nil) {
		resizedImage = [info valueForKey:UIImagePickerControllerOriginalImage];
	}
	[self resizeImage:resizedImage];
	[picker dismissViewControllerAnimated:YES completion:nil];
}

-(void) resizeImage:(UIImage *) im{
	[_overlayView.superview convertPoint:_overlayView.frame.origin toView:nil];
	CGRect newRect = CGRectMake(_overlayView.frame.origin.x+IMAGE_CROP_OFFSET_X,_overlayView.frame.origin.y-IMAGE_CROP_OFFSET_Y,_overlayView.frame.size.width*2,_overlayView.frame.size.height*2);
	CGImageRef imageRef = CGImageCreateWithImageInRect([im CGImage], newRect);
	UIImage *img = [UIImage imageWithCGImage:imageRef];
	CGImageRelease(imageRef);
	
	if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_SAVE_IMAGES] boolValue]) {
		UIImageWriteToSavedPhotosAlbum(img, nil, nil, nil);
	}
	
	[self.delegate actionViewDidEndEditing:@{@"type": @"CHANGE_IMAGE",@"thumbImage": img} viewController:self];
}

-(void) deleteImage{
	[self.delegate actionViewDidEndEditing:@{@"type": @"DELETE_IMAGE"} viewController:self];
}

-(void) changeName{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"New Stay Name", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	alert.tag = CELL_CHANGE_NAME;
	[alert show];
	
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0 && alertView.tag == CELL_CHANGE_NAME) {
		NSString *newName = [[alertView textFieldAtIndex:0] text];
		[self.delegate actionViewDidEndEditing:@{@"type": @"CHANGE_NAME",@"name": newName} viewController:self];
	}
	else{
		[self.delegate actionViewDidEndEditing:nil viewController:self];
		
	}
}


@end
