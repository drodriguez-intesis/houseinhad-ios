//
//  EnvironmentsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "EnvironmentsViewController.h"
#import "EnvironmentsCell.h"
#import "AppDelegate.h"
#import "Scene.h"
#import "Config.h"
#import "KNXCommunication.h"
#import "Telegram+Parser.h"
#import "UIImage+Color.h"
#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.1
@interface EnvironmentsViewController ()
@property (weak,nonatomic) IBOutlet UIButton * editButton;
@property (weak,nonatomic) IBOutlet UITableView * tableView;
@property (strong,nonatomic) NSManagedObjectContext *envContext;
@property (strong,nonatomic) NSArray *environments;
@property (strong,nonatomic) Config *config;
@property (strong,nonatomic) Scene *currentScene;
@property (weak, nonatomic) IBOutlet UIImageView *topOrangeView;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
-(IBAction)editScenes;
-(IBAction)addScene;
-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender;
-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender;
@end



@implementation EnvironmentsViewController




- (void)viewDidLoad
{
	[super viewDidLoad];
    [self createContext];
	[self loadActiveConfig];
	[self loadEnvironments];
	_tableView.allowsSelectionDuringEditing = YES;
	_topOrangeView.backgroundColor	= APP_COLOR_ORANGE;
    [self setTranslations];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTranslations) name:NOT_LANGUAGE_CHANGED object:nil];
}

-(void) setTranslations{
    _titleName.text = NSLocalizedStringFromTableInBundle(@"Scenes", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of Navigation bar style");
}

-(void) createContext{
	AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _envContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	
	[_envContext setParentContext:app.managedObjectContext];
	
}

-(void) loadActiveConfig{
	AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	NSArray *configs = [_envContext executeFetchRequest:[app.managedObjectModel fetchRequestTemplateForName:@"fetchActiveConfig"] error:nil];
	if ([configs count]>0) {
		_config = configs[0];
	}
}

-(void) loadEnvironments{
	_environments = [[_config.r_scenes allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
}


-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

- (void)viewDidUnload
{
	_tableView	 = nil;
	_envContext = nil;
	_environments = nil;
	_config = nil;
	_currentScene = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOT_LANGUAGE_CHANGED object:nil];
    [super viewDidUnload];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"Environments";
    EnvironmentsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if (cell == nil) {
        cell = [[EnvironmentsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    }
	Scene *scene = (Scene *) _environments[[indexPath row]];
	cell.name.text = scene.name;
	
	cell.name.textAlignment = NSTextAlignmentCenter;
	return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rowUnselected_60"]];
		cell.backgroundColor = [UIColor clearColor];
		UIImage *image1 = [UIImage imageNamed:@"rowSettingsSelected"];
		cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
		image1 = nil;
	}
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_environments count];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (editingStyle == UITableViewCellEditingStyleDelete) {
		_currentScene = (Scene *) _environments[[indexPath row]];
		[_envContext deleteObject:_currentScene];
		[self reordenateDeletedScenesfromIndex:indexPath.row];
 		[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
		[self editScenes];
		[self updateLeftBarButton];
	}
	else if (editingStyle == UITableViewCellEditingStyleInsert) {
		[tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
	}
}

// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
	//logic
	_currentScene = (Scene *) _environments[[fromIndexPath row]];
	if (fromIndexPath.row < toIndexPath.row) {
		//nmes reordenar els inf toindexpath (-1)
	//	[self reordenateScenesType:REORDENATE_MOVE up:YES index:toIndexPath.row];
		[self reordenateScenes:YES fromIndex:fromIndexPath.row toIndex:toIndexPath.row];
	}
	else if (fromIndexPath.row > toIndexPath.row){
		//nmes reordenar sup toindexpath (+1)
		[self reordenateScenes:NO fromIndex:fromIndexPath.row toIndex:toIndexPath.row];
	}
	
	if (fromIndexPath.row != toIndexPath.row) {
		[self editScenes];
	}
}

-(void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self updateLeftBarButton];
}

-(void) updateLeftBarButton{
	_editButton.hidden = (_environments.count == 0 ? YES: NO);
}
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
	return YES;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	_currentScene = (Scene *) _environments[[indexPath row]];
	if (_tableView.isEditing) {
		[tableView deselectRowAtIndexPath:indexPath animated:YES];
		[self presentNextLevel:NO];
	}
	else{
		[self executeCurrentScene];
	}
}

-(void) reordenateScenes:(BOOL) up fromIndex:(NSUInteger) from toIndex:(NSUInteger) to{
	NSRange range;
	if (up) {
		range = NSMakeRange(from+1, abs(to-from));
	}
	else{
		range = NSMakeRange(to, abs(to-from));
	}
	NSArray *filteredArray = [_environments subarrayWithRange:range];
	[filteredArray enumerateObjectsUsingBlock:^(Scene * obj, NSUInteger idx, BOOL *stop) {
		if (!up) {
			obj.order = @([obj.order integerValue] +1);
		}
		else{
			obj.order = @([obj.order integerValue] -1);
		}
	}];
	_currentScene.order = @(to);
	[self saveContext];
	[self loadEnvironments];
}

-(void) reordenateDeletedScenesfromIndex:(NSUInteger) from{
	NSArray *filteredArray = [_environments subarrayWithRange:NSMakeRange(from+1, ([_environments count] -1) - from)];
	[filteredArray enumerateObjectsUsingBlock:^(Scene * obj, NSUInteger idx, BOOL *stop) {
		obj.order = @([obj.order integerValue] -1);
	}];
	[self saveContext];
	[self loadEnvironments];
}

-(void) executeCurrentScene{
	NSUInteger envNum = [_currentScene.r_sceneTelegram count];
	if (envNum > 0) {
		dispatch_async(dispatch_queue_create("ENVIRONMENTS_BACKGROUND", NULL), ^{
			for (SceneTelegram *tel in [_currentScene.r_sceneTelegram allObjects]) {
				if (tel.delay && tel.delay.floatValue > 0.0) {
					[self sendDelayedTelegram:tel];
				}
				else{
					[self sendData:tel];
				}
				sleep(.1);
			}
			dispatch_async(dispatch_get_main_queue(), ^{
				[_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
			});
		});
	}
	else{
		[_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
	}
}

-(void) sendDelayedTelegram:(SceneTelegram *) telegram{
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, telegram.delay.floatValue * NSEC_PER_SEC), dispatch_queue_create("ENVIRONMENTS_BACKGROUND_2", NULL), ^{
		[self sendData:telegram.value address:telegram.address dpt:telegram.dpt];
	});
}

-(void) sendData:(SceneTelegram *) telegram{
	[self sendData:telegram.value address:telegram.address dpt:telegram.dpt];
}

-(void) sendData:(NSNumber *) data address:(NSNumber *) address dpt:(NSString *) dpt{
	@synchronized ([Telegram class]){
		NSData *info = [Telegram getDataFromInfo:data dpt:dpt write:1];
		[[KNXCommunication sharedInstance] sendInfoToKnx:info address:address objectId:nil];
	}
}

-(IBAction)addScene{
	[self selectAddScene:YES];
	[self presentNextLevel:YES];
}

-(void) presentNextLevel:(BOOL) new{
	NSSet *stays = _config.r_nextStay;
	NSArray *orderedS = [stays sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
	EnvL1ViewController *env = [[EnvL1ViewController alloc] initWithNibName:@"EnvL1ViewController" bundle:nil context:_envContext stays:orderedS scene:_currentScene level:1];
	env.delegate = self;
	env.isNew = new;
	UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:env];
	[navi.navigationBar setBackgroundImage:[UIImage imageWithColor:APP_COLOR_ORANGE] forBarMetrics:UIBarMetricsDefault];
	[self presentViewController:navi animated:YES completion:^{
		if (_tableView.isEditing) {
			[self editScenes];
		}
	}];

}

-(IBAction)editScenes{
	if (_tableView.isEditing) {
		[_tableView setEditing:NO animated:YES];
		[self enableGestureRecognizer:YES];
		[_editButton setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
		[_editButton setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateHighlighted];
	}
	else if ([_environments count] >0){
		[_tableView setEditing:YES animated:YES];
		[self enableGestureRecognizer:NO];
		[_editButton setImage:[UIImage imageNamed:@"ok"] forState:UIControlStateNormal];
		[_editButton setImage:[UIImage imageNamed:@"ok"] forState:UIControlStateHighlighted];
	}
}

-(void) enableGestureRecognizer:(BOOL) enable{
	if ([self.parentViewController.view.gestureRecognizers count] > 0) {
		UIGestureRecognizer *gesture = (UIGestureRecognizer *)self.parentViewController.view.gestureRecognizers[0];
		gesture.enabled = enable;
	}
}

-(void) selectAddScene:(BOOL) add{
	if (add) {
		_currentScene =  [NSEntityDescription insertNewObjectForEntityForName:@"Scene" inManagedObjectContext:_envContext];
		_currentScene.name = NSLocalizedStringFromTableInBundle(@"New Scene", nil, CURRENT_LANGUAGE_BUNDLE, @"title of alertview and cell");
		[_config addR_scenesObject:_currentScene];
	}
}

-(void) didEndEditingScene:(EnvL1ViewController *)level saveChanges:(BOOL)save{
	if (save) {
		if (level.isNew) {
			_currentScene.order = @([_environments count]);
		}
		[self saveContext];
		[self loadEnvironments];
		[self.tableView reloadData];
	}
	else{
		if (level.isNew){
			[_envContext deleteObject:_currentScene];
		}
		else{
			_envContext = nil;
			[self createContext];
			[self loadActiveConfig];
			[self loadEnvironments];
			[self.tableView reloadData];
		}
	}
	[level dismissViewControllerAnimated:YES completion:nil];
}

-(void) saveContext{
	@synchronized (_envContext){
		[_envContext save:nil];
	}
}

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}
				   
				
				   
@end
