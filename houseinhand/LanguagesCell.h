//
//  LanguagesCell.h
//  houseinhand
//
//  Created by Isaac Lozano on 28/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LanguagesCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel *name;
@end
