//
//  SliderButtonsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SliderButtonsViewController.h"
#import "DeviceTypeSlider.h"
@interface SliderButtonsViewController ()
@property (strong,nonatomic) UIImage *leftImage;
@property (strong,nonatomic) UIImage *rightImage;
@end

@implementation SliderButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self initVarsWithDevice:newDevice];

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self initVarsWithDevice:newDevice];
    }
    return self;
}

-(void) initVarsWithDevice:(DeviceTypeSlider *) newDevice{
	_minValue = [newDevice.minValue integerValue];
	_maxValue = [newDevice.maxValue integerValue];
	_step = [newDevice.step integerValue];
	_leftImage = [UIImage imageNamed:newDevice.image1];
	_rightImage  = [UIImage imageNamed:newDevice.image2];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setImages];
    [self setSliderValues];
}

- (void)viewDidUnload
{
	_leftImage = nil;
	_rightImage = nil;
    _slider = nil;
    _leftButton = nil;
    _rightButton = nil;
	[super viewDidUnload];
}

-(void) setImages{
    [_leftButton setImage:_leftImage forState:(UIControlStateNormal)];
    [_leftButton setImage:_leftImage forState:(UIControlStateHighlighted)];
    [_rightButton setImage:_rightImage forState:(UIControlStateNormal)];
    [_rightButton setImage:_rightImage forState:(UIControlStateHighlighted)];
}

-(void) setSliderValues{
    [_slider setMinimumValue:_minValue];
    [_slider setMaximumValue:_maxValue];
    _slider.continuous = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
