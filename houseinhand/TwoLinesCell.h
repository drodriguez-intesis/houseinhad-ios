//
//  TwoLinesCell.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoLinesCell : UITableViewCell
@property (strong,nonatomic) IBOutlet UILabel * name;
@property (strong,nonatomic) IBOutlet UILabel * mac;
@property (strong,nonatomic) IBOutlet UILabel * ipPort;
@end
