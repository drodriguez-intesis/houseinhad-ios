//
//  DeviceTypePulse.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/7/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypePulse : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSNumber * pulseWidth;
@property (nonatomic, retain) NSNumber * valueOn;
@property (nonatomic, retain) NSNumber * valueOff;

@end
