//
//  NetworkSettingsViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 12/11/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "MBProgressHUD.h"
#import "Config.h"
@class NetworkSettingsViewController;
@protocol NetworkSettingsViewControllerDelegate <NSObject>

-(void) networkSettings:(ThirdLevelViewController *) thirdLevel didReadDiscoverResults:(NSArray *) results;

@end
@interface NetworkSettingsViewController : ThirdLevelViewController <UITextFieldDelegate,UIAlertViewDelegate,MBProgressHUDDelegate>
@property (assign,nonatomic) BOOL needRestartStartCom;
@property (strong,nonatomic) IBOutlet UITextField * localIp;
@property (strong,nonatomic) IBOutlet UITextField * localPort;
@property (strong,nonatomic) IBOutlet UITextField * publicIp;
@property (strong,nonatomic) IBOutlet UITextField * publicPort;
@property (weak, nonatomic) IBOutlet UISegmentedControl *modeSegmented;
@property (nonatomic, weak) id <NetworkSettingsViewControllerDelegate> networkDelegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil activeConfig:(Config *) config;
@end
