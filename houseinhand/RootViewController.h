//
//  RootViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SettingsViewController.h"
@class InitialViewController;
@class StayViewController;
@interface RootViewController : UIViewController 
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;


-(void) changeConfigFile;
@end
