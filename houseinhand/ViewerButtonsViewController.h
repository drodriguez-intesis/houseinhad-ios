//
//  ViewerButtonsViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"
#import "UIButton_Sub.h"
#import "UILabel_Sub.h"
@interface ViewerButtonsViewController : DevicesViewController 
@property (strong,nonatomic) IBOutlet UIButton_Sub *leftButton;
@property (strong,nonatomic) IBOutlet UIButton_Sub *rightButton;
@property (strong,nonatomic) IBOutlet UILabel_Sub *centerLabel;
@property (assign,nonatomic) CGFloat currentValue;
@end
