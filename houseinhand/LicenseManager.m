//
//  LicenseManager.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/13/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "LicenseManager.h"
#import "KeychainItemWrapper.h"
#import "ServerHandler.h"
#import "KNXCommunication.h"
#import "DeviceInformation.h"
#import "NSData+AES256.h"
#define LICENSE_VERSION			@"1.0"


#define ENCRIPTION_KEY			@";2eZ]`;m#/7j>=L_[2~^%\%2N%U_B=IJ"

typedef 	NS_ENUM (NSUInteger,LicenseType) {LICENSE_FREE = 0,LICENSE_CONVENTIONAL = 1,LICENSE_ADDITIONAL = 2,LICENSE_DEMO = 3,LICENSE_TRIAL = 4};


@interface LicenseManager ()
@property (assign,nonatomic) BOOL isValidLogin;
@end
@implementation LicenseManager


+ (id)sharedInstance {
    static LicenseManager *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(id) init{
	self = [super init];
	if (self) {
		_isValidLogin = NO;
	}
	return self;
}

-(void) remoteCheckWithStoredLoginResultBlock:(LicenseResultBlock) delegateBlock{
	NSDictionary *credentials = [self credentials];
	[self remoteCheckWithUsername:credentials[@"username"] password:credentials[@"password"]  resultBlock:delegateBlock];

}
-(void) remoteCheckWithUsername:(NSString *)username password:(NSString *) password resultBlock:(LicenseResultBlock) delegateBlock{
	_delegateBlock = delegateBlock;
	_isValidLogin = NO;
	__block NSDictionary * info = @{@"username": username,@"password":password};
	__weak LicenseManager *weakSelf = self;
	[[ServerHandler sharedInstance] startMethod:METHOD_VERIFY withInfo:info usingBlock:^(NSDictionary *json) {
		dispatch_async(dispatch_get_main_queue(), ^{
			if (json) {
				if (json[@"result"]) {
					[self storeCredentialsWithInfo:info secret:json[@"result"][@"secret"]];
					
					//FIXME Improve communication start License
					[KNXCommunication startCommunication];
						//
				}
				else{
					[self unassign];
				}
				if (_delegateBlock) weakSelf.delegateBlock(json);
			}
			else{
				if (_delegateBlock) weakSelf.delegateBlock(nil);
			}
		});
		
	}];

}

-(void) remoteCheckDeviceStoredLoginResultBlock:(LicenseResultBlock) delegateBlock{
	_delegateBlock = delegateBlock;
	NSDictionary *credentials = [self credentials];

	__block NSDictionary * info = @{@"username": credentials[@"username"],@"password": credentials[@"password"],@"method": @"checkDevice",@"info": [[DeviceInformation sharedInstance] deviceInformationForCheckDevice]};
	
	__block NSString *macAdr = info[@"info"][@"instCode"];
	__weak LicenseManager *weakSelf = self;
	[[ServerHandler sharedInstance] startMethod:METHOD_CHECK withInfo:info usingBlock:^(NSDictionary *json) {
		dispatch_async(dispatch_get_main_queue(), ^{
			if (json) {
				if (json[@"result"]) {
					if (json[@"result"][@"endDate"] && json[@"result"][@"licenseType"]){
						if ([json[@"result"][@"licenseType"] integerValue] == LICENSE_FREE) {
							[[NSUserDefaults standardUserDefaults] setObject:@"-" forKey:USER_DEFAULTS_EXPIRATION_DATE];
						}
						else{
							dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
								[self storeEndDate:json[@"result"][@"endDate"] ofAddress:macAdr];
							});
						}
					}
					[self updateDeviceAdCode];
                    [[NSUserDefaults standardUserDefaults] setValue:@YES forKey:USER_DEFAULTS_LOGGED];
				}
				else if (json[@"error"]) {
					[[NSUserDefaults standardUserDefaults] setObject:@"-" forKey:USER_DEFAULTS_EXPIRATION_DATE];
				}
				if (_delegateBlock) weakSelf.delegateBlock(json);
			}
			else{
				// has error
				if ([self canOfflineMode:macAdr]){
					if (_delegateBlock) weakSelf.delegateBlock(@{@"result": @"offline"});
				}
				else{
					if (_delegateBlock) weakSelf.delegateBlock(@{@"result": @"ko"});
				}
			}
		});
	}];
}

-(void) updateDeviceAdCode{
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_CODES" accessGroup:nil];
	[keychain setObject:[NSString stringWithFormat:@"%@",[[UIDevice currentDevice] identifierForVendor].UUIDString] forKey:(__bridge id)kSecAttrAccount];
}

-(BOOL) canOfflineMode:(NSString *) macAddress{
	NSMutableDictionary *infoDict = [self getInfoDict];
	if (infoDict){
		NSString *endDateString = infoDict[macAddress];
		NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
		[dateFormatter setDateFormat:@"yyyy-MM-dd"];
		NSDate *endDate = [dateFormatter dateFromString:endDateString];
		if (endDate) {
			if ([endDate compare:[NSDate date]] == NSOrderedDescending) {
				[[NSUserDefaults standardUserDefaults] setObject:endDateString forKey:USER_DEFAULTS_EXPIRATION_DATE];
				return YES;
			}
		}
	}
	[[NSUserDefaults standardUserDefaults] setObject:@"-" forKey:USER_DEFAULTS_EXPIRATION_DATE];
	return NO;
}

-(NSMutableDictionary *) getInfoDict{
	NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_OFFLINE_MODE];
	NSDictionary *infoDict;
	if (data){
		NSData *infoData= [data AES256DecryptWithKey:ENCRIPTION_KEY];
		if (infoData) {
			NSError *err;
			infoDict = [NSJSONSerialization JSONObjectWithData:infoData options:NSJSONReadingAllowFragments error:&err];
		}
	}
	
	return [infoDict mutableCopy];
}

-(void) storeEndDate:(NSString *) endDate ofAddress:(NSString *) macAddress{
	NSMutableDictionary *infoDictMutable = [self getInfoDict];
	if (!infoDictMutable) {
		infoDictMutable = [NSMutableDictionary dictionary];
	}
	/*
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
	[dateFormatter setDateFormat:@"yyyy-MM-dd"];
	NSDate *dateFromString = [[NSDate alloc] init];
	*/
	[infoDictMutable setObject:endDate forKey:macAddress];
	NSData *data = [NSJSONSerialization dataWithJSONObject:[NSDictionary dictionaryWithDictionary:infoDictMutable] options:0 error:nil];
	NSData *encriptData = [data AES256EncryptWithKey:ENCRIPTION_KEY];
	[[NSUserDefaults standardUserDefaults] setObject:encriptData forKey:USER_DEFAULTS_OFFLINE_MODE];
	[[NSUserDefaults standardUserDefaults] setObject:endDate forKey:USER_DEFAULTS_EXPIRATION_DATE];
}


-(void) remoteAddDeviceStoredLoginMac:(NSString *) mac resultBlock:(LicenseResultBlock) delegateBlock{
	_delegateBlock = delegateBlock;
	NSDictionary *credentials = [self credentials];
	
	__block NSDictionary * info = @{@"username": credentials[@"username"],@"password": credentials[@"password"],@"method": @"addDevice",@"info": [[DeviceInformation sharedInstance] deviceInformationForAddDeviceMacAddress:mac]};
	__weak LicenseManager *weakSelf = self;
	[[ServerHandler sharedInstance] startMethod:METHOD_ADD withInfo:info usingBlock:^(NSDictionary *json) {
		dispatch_async(dispatch_get_main_queue(), ^{
			if (json) {
				if (_delegateBlock) weakSelf.delegateBlock(json);
				if (json[@"result"]) {
					//	[self storeCredentialsWithInfo:info secret:json[@"result"][@"secret"]];
					
					//FIXME Improve communication start License
					//[KNXCommunication startCommunication];
					
				}
			}
			else{
				if (_delegateBlock) weakSelf.delegateBlock(nil);
			}
		});
	}];

}



- (NSString *) licenseVersion{
	return LICENSE_VERSION;
}

- (NSDictionary *) credentials{
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_LOGIN" accessGroup:nil];
	return (keychain ? @{@"username": [keychain objectForKey:(__bridge id)kSecAttrAccount],@"password": [keychain objectForKey:(__bridge id)kSecValueData],@"secret": [[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_SECRET]} : @{});
}

- (NSDictionary *) checkLicenseGetCredentials{
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_LOGIN" accessGroup:nil];
	return (_isValidLogin && keychain ? @{@"username": [keychain objectForKey:(__bridge id)kSecAttrAccount],@"password": [keychain objectForKey:(__bridge id)kSecValueData],@"secret":[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_SECRET]} : nil);
}
-(BOOL) hasValidLogin{
	return _isValidLogin;
}

-(BOOL) hasStoredAccount{
	NSDictionary * credentials = [self credentials];
	if (credentials && credentials[@"username"] && ![credentials[@"username"] isEqualToString:@""] && credentials[@"password"] && ![credentials[@"password"] isEqualToString:@""]) {
		return YES;
	}
	return NO;
}

-(void) unassign{
	_isValidLogin = NO;
	[[NSUserDefaults standardUserDefaults] setValue:@NO forKey:USER_DEFAULTS_LOGGED];
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_LOGIN" accessGroup:nil];
	[keychain setObject:@"" forKey:(__bridge id)kSecAttrAccount];
	[keychain setObject:@"" forKey:(__bridge id)kSecValueData];
	[[NSUserDefaults standardUserDefaults] setObject:@"" forKey:USER_DEFAULTS_SECRET];
	[[NSUserDefaults standardUserDefaults] setObject:[NSData data] forKey:USER_DEFAULTS_OFFLINE_MODE];
	[[NSUserDefaults standardUserDefaults] setObject:@"-" forKey:USER_DEFAULTS_EXPIRATION_DATE];

	//FIXME Improve communication start License
	[KNXCommunication stopCommunication];
}

-(void) storeCredentialsWithInfo:(NSDictionary *) info secret:(NSString *) secret{
	//keychain store
	_isValidLogin = YES;
	[[NSUserDefaults standardUserDefaults] setValue:@YES forKey:USER_DEFAULTS_LOGGED];
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_LOGIN" accessGroup:nil];
	[keychain setObject:@"com.houseinhand03" forKey: (__bridge id)kSecAttrService];
	[keychain setObject:(__bridge id)kSecAttrAccessibleWhenUnlocked forKey:(__bridge id)kSecAttrAccessible];
	[keychain setObject:info[@"username"] forKey:(__bridge id)kSecAttrAccount];
	[keychain setObject:info[@"password"] forKey:(__bridge id)kSecValueData];
	[[NSUserDefaults standardUserDefaults] setObject:secret forKey:USER_DEFAULTS_SECRET];
}

@end
