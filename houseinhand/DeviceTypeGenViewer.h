//
//  DeviceTypeGenViewer.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeGenViewer : Device

@property (nonatomic, retain) NSNumber * logic;
@property (nonatomic, retain) NSString * textNo;
@property (nonatomic, retain) NSString * textYes;

@end
