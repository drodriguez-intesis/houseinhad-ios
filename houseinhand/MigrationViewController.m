//
//  MigrationViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/12/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MigrationViewController.h"
#import "LicenseManager.h"
#define LICENSE_OLD_FILE_EXTENSION		@".hih"
@interface MigrationViewController ()
@property (strong,nonatomic) NSDictionary *credentials;
@property (weak, nonatomic) IBOutlet UIButton *licenseButton;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *licenseLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UILabel *configTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *registerTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *enjoyTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *licenseConditionsLabel;
@property (weak, nonatomic) IBOutlet UILabel *clientLabel;
@property (weak, nonatomic) IBOutlet UILabel *hihFuncLabel;
@property (weak, nonatomic) IBOutlet UILabel *fillUsernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *registerToLabel;
@property (weak, nonatomic) IBOutlet UILabel *filesMovedLabel;
@property (weak, nonatomic) IBOutlet UIImageView *topOrangeImage;
@property (weak, nonatomic) IBOutlet UIButton *licenseCode;
@end

@implementation MigrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
	[_scrollView setContentSize:CGSizeMake((DEVICE_IS_IPAD ? 2160:1280), _scrollView.contentSize.height)];
	_licenseCode.layer.borderColor = [APP_COLOR_ORANGE_DARK CGColor];
	_licenseCode.layer.cornerRadius = 2.0;
	_licenseCode.layer.borderWidth = 2.0;
	_topOrangeImage.backgroundColor = APP_COLOR_ORANGE;
	[self setTranslation];
}

-(void) setTranslation{
	_licenseLabel.text = NSLocalizedStringFromTableInBundle(@"Upgrading from previous version?", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_configTitleLabel.text = [NSString stringWithFormat:@"1. %@",NSLocalizedStringFromTableInBundle(@"Configuration", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view")];
	_titleName.text = NSLocalizedStringFromTableInBundle(@"Upgrading", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_registerTitleLabel.text = [NSString stringWithFormat:@"2. %@",NSLocalizedStringFromTableInBundle(@"Register", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view")];
	_accountTitleLabel.text = [NSString stringWithFormat:@"3. %@",NSLocalizedStringFromTableInBundle(@"Account", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view")];
	_enjoyTitleLabel.text = NSLocalizedStringFromTableInBundle(@"Enjoy it", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_licenseConditionsLabel.text = [NSString stringWithFormat:@"*%@",NSLocalizedStringFromTableInBundle(@"Until 4/12/2013. Subject to change without prior notice", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view")];
	_clientLabel.text = NSLocalizedStringFromTableInBundle(@"New to houseinhand? Contact us at sales@houseinhand.com", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_hihFuncLabel.text = [NSString stringWithFormat:@"%@*",NSLocalizedStringFromTableInBundle(@"Houseinhand is fully functional during free trial period", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view")];
	_fillUsernameLabel.text = NSLocalizedStringFromTableInBundle(@"Fill in with username and password in 'Account Settings' section", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_registerToLabel.text = NSLocalizedStringFromTableInBundle(@"Register to user.houseinhand.com to update your information", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	_filesMovedLabel.text = NSLocalizedStringFromTableInBundle(@"Files are moved to 'Old' folder, and also migrated to new Houseinhand configurations", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view");
	[_licenseCode setTitle:NSLocalizedStringFromTableInBundle(@"Already have a license code?", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view") forState:UIControlStateNormal];
	[_licenseCode setTitle:NSLocalizedStringFromTableInBundle(@"Already have a license code?", nil, CURRENT_LANGUAGE_BUNDLE, @"Migration view") forState:UIControlStateHighlighted];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeView {
	[[NSUserDefaults standardUserDefaults] setValue:@NO forKey:USER_DEFAULTS_SHOW_MIGRATION];
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)showMail:(UIButton *)sender {
	if ([[LicenseManager sharedInstance] hasValidLogin]) {
		if ([MFMailComposeViewController canSendMail]) {
			NSDictionary *credentials = [[LicenseManager sharedInstance] credentials];
			/* Set up the mail compose view and put in the body/attachment */
			__block NSData *data;
			MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
			NSString *bodyMessage = [NSString stringWithFormat:@"%@\n\n%@ %@\n%@ %@",NSLocalizedStringFromTableInBundle(@"Dear Houseinhand Team:\n\nPlease upgrade my .hih file and assign it to my user:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),NSLocalizedStringFromTableInBundle(@"Username:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),credentials[@"username"],	NSLocalizedStringFromTableInBundle(@"Secret:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_SECRET] ];
			[mailComposer setMessageBody:bodyMessage isHTML:NO];
			__block NSString *fileName;
			NSArray *path_array = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
			NSString *publicDocumentsDir = path_array[0];
			NSString *path2 = [publicDocumentsDir stringByAppendingPathComponent:@"Old"];
			NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path2 error:nil];
			[files enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
				if ([obj hasSuffix:LICENSE_OLD_FILE_EXTENSION]) {
					NSString *path = [path2 stringByAppendingPathComponent:obj];
					fileName = [NSString stringWithString:[obj lastPathComponent]];
					data = [NSData dataWithContentsOfFile:path];
					*stop = YES;
				}
			}];
			if (data){
				[mailComposer addAttachmentData:data mimeType:@"application/houseinhand" fileName:fileName];
				[mailComposer setToRecipients:@[@"sales@houseinhand.com"]];
				[mailComposer setSubject:@"HIH-Migration_License"];
				mailComposer.mailComposeDelegate = self;
				[self presentViewController:mailComposer animated:YES completion:nil];
			}
			else{
				[self showAlertWitTitle:NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview") message:NSLocalizedStringFromTableInBundle(@"No .hih file found in 'Old' Folder of iTunes File Sharing", nil, CURRENT_LANGUAGE_BUNDLE, @"message of UIalertview")];
			}
		}
	}
	else{
		[self showAlertWitTitle:NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview") message:NSLocalizedStringFromTableInBundle(@"Please sign in before upgrading the .hih file", nil, CURRENT_LANGUAGE_BUNDLE, @"message of UIalertview")];
	}	
}

-(void) showAlertWitTitle:(NSString *) title message:(NSString *) message{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Dismiss", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles: nil];
	alert.delegate = self;
	[alert show];
}
-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
	[controller dismissViewControllerAnimated:YES completion:^{
		if (result == MFMailComposeResultSent) {
			[self dismissViewControllerAnimated:YES completion:nil];
		}
	}];
}

-(void) scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
	NSUInteger page = scrollView.contentOffset.x/scrollView.bounds.size.width;
	[_pageControl setCurrentPage:page];

}


- (IBAction)viewTaped:(id)sender {
	NSUInteger newPage = (_pageControl.currentPage <3 ? _pageControl.currentPage+1 : 0);
	[_scrollView setContentOffset:CGPointMake(newPage*_scrollView.bounds.size.width, 0) animated:YES];
	[_pageControl setCurrentPage:newPage];
}
- (IBAction)showSafari {
	NSString *urlStr = @"http://houseinhand.com/houseinhand20/";
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	if ([language isEqualToString:@"es"] || [language isEqualToString:@"ca"]) {
		urlStr = @"http://www.houseinhand.com/es/houseinhand20/";
	}
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}

@end
