//
//  TypeGenericSegmentedViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeGenericSegmentedViewController.h"
#import "DeviceTypeSegmented.h"

@interface TypeGenericSegmentedViewController ()
@property (assign,nonatomic) NSUInteger numSegments;
@property (assign,nonatomic) NSInteger firstValue;
@property (assign,nonatomic) NSInteger secondValue;
@property (assign,nonatomic) NSInteger thirdValue;
@property (assign,nonatomic) NSInteger fourthValue;
@property (assign,nonatomic) NSInteger fifthValue;
@property (strong,nonatomic) NSString *firstText;
@property (strong,nonatomic) NSString *secondText;
@property (strong,nonatomic) NSString *thirdText;
@property (strong,nonatomic) NSString *fourthText;
@property (strong,nonatomic) NSString *fifthText;
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@property (assign,nonatomic) NSInteger selectedSegment;
@property (assign,nonatomic) BOOL isReadOnly;

@end

@implementation TypeGenericSegmentedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSegmented *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        // Custom initialization
        [self customizeWithDevice:newDevice];
        _selectedSegment = [self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSegmented*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self customizeWithDevice:newDevice];
        if (_isReadOnly) return nil;
        [self readControlStatus];
		_selectedSegment = [self readSceneStatus];

    }
    return self;
}

-(void) customizeWithDevice:(DeviceTypeSegmented *) newDevice{
	_numSegments = [newDevice.numSegments integerValue];
	_firstValue = [newDevice.firstValue integerValue];
	_secondValue = [newDevice.secondValue integerValue];
	_thirdValue = [newDevice.thirdValue integerValue];
	_fourthValue = [newDevice.fourthValue integerValue];
	_fifthValue = [newDevice.fifthValue integerValue];
	_firstText = newDevice.firstText;
	_secondText = newDevice.secondText;
	_thirdText = newDevice.thirdText;
	_fourthText = newDevice.fourthText;
	_fifthText = newDevice.fifthText;
    
    _isReadOnly = newDevice.readOnly.boolValue;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self configureSegmented];
    [self setActions];
    [super.centralSegmented setSelectedSegmentIndex:_selectedSegment];
	if (super.type == TYPE_CONTROL) {
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:YES];
	}
}

-(void) update:(BOOL) animated{
	NSInteger newVal = [self getSelectedSegment:_statusTelegram.value.integerValue];
	if (_selectedSegment !=	newVal) {
		_selectedSegment = newVal;
		[super.centralSegmented setSelectedSegmentIndex:_selectedSegment];
	}
}

-(void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
	_firstText = nil;
    _secondText = nil;
    _thirdText = nil;
    _fourthText = nil;
    _fifthText = nil;
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

-(CGFloat) readControlStatus{
    NSInteger status=UISegmentedControlNoSegment;
	_statusTelegram = [super.device getTelegramWithCode:0];
	_actionTelegram = [super.device getTelegramWithCode:1];
	if (_statusTelegram) status = [self getSelectedSegment:_statusTelegram.value.integerValue];

	return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=0;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[self getSelectedSegment:[scTel.value integerValue]];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) setActions{
    [super.centralSegmented addTarget:self action:@selector(segmentedPressed) forControlEvents:UIControlEventValueChanged];
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) configureSegmented{
    [super.centralSegmented setTitle:_firstText forSegmentAtIndex:0];
    [super.centralSegmented setTitle:_secondText forSegmentAtIndex:1];
	switch (_numSegments) {
        case 3:
            [super.centralSegmented insertSegmentWithTitle:_thirdText atIndex:2 animated:NO];
            break;
        case 4:
            [super.centralSegmented insertSegmentWithTitle:_thirdText atIndex:2 animated:NO];
            [super.centralSegmented insertSegmentWithTitle:_fourthText atIndex:3 animated:NO];
            break;
        case 5:
            [super.centralSegmented insertSegmentWithTitle:_thirdText atIndex:2 animated:NO];
            [super.centralSegmented insertSegmentWithTitle:_fourthText atIndex:3 animated:NO];
            [super.centralSegmented insertSegmentWithTitle:_fifthText atIndex:4 animated:NO];
            break;
        default:
            break;
    }
    if (_isReadOnly){
        [super.centralSegmented setUserInteractionEnabled:NO];
        super.centralSegmented.alpha = .8;
    }
}

-(NSInteger) getSelectedValue{
	NSInteger valueToSend = 0;
	const NSInteger  values []= {_firstValue,_secondValue,_thirdValue,_fourthValue,_fifthValue};
	valueToSend = values[_selectedSegment];
	return valueToSend;
}
-(NSInteger) getSelectedSegment:(NSInteger) realVal{
	const NSInteger  values []= {_firstValue,_secondValue,_thirdValue,_fourthValue,_fifthValue};
	for (NSUInteger i = 0; i<_numSegments; i++) {
		if (realVal == values[i]) {
			return i;
		}
	}
	return UISegmentedControlNoSegment;
}

-(void) segmentedPressed{
    _selectedSegment = super.centralSegmented.selectedSegmentIndex;
	[super sendData:@([self getSelectedValue]) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_actionTelegram.address,@"address",@([self getSelectedValue]),@"value",_actionTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
