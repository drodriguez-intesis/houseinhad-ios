//
//  SliderViewerViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"
#import "UILabel_Sub.h"
@interface SliderViewerViewController : DevicesViewController 
@property (strong,nonatomic) IBOutlet UISlider *slider;
@property (strong,nonatomic) IBOutlet UILabel_Sub *centerLabel;
@end
