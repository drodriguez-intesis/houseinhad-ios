//
//  TypeLightViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//
#import "TypeLightViewController.h"
#import "DeviceTypeLight.h"
@interface TypeLightViewController ()
@property (strong,nonatomic) UIImage *offStateImage;
@property (strong,nonatomic) UIImage *onStateImage;
@property (assign,nonatomic) BOOL isOn;
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@end

@implementation TypeLightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        // Custom initialization
        _isOn = (BOOL)[self readControlStatus];
        _offStateImage = [UIImage imageNamed:newDevice.image1];
        _onStateImage  = [UIImage imageNamed:newDevice.image2];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self readControlStatus];
		_isOn = (BOOL)[self readSceneStatus];
        _offStateImage = [UIImage imageNamed:newDevice.image1];
        _onStateImage  = [UIImage imageNamed:newDevice.image2];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setImages];
    [self setActions];
	if (super.type == TYPE_CONTROL) {
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (_isOn !=	_statusTelegram.value.boolValue) {
		_isOn = _statusTelegram.value.boolValue;
		[self updateCurrentImage:animated];
	}
}

-(void) dealloc{
	if ([super.device.hasStatus boolValue]  && super.type == TYPE_CONTROL) {
		[_statusTelegram removeObserver:self forKeyPath:@"value"];
	}
}

- (void)viewDidUnload
{
    _offStateImage = nil;
    _onStateImage = nil;
    _statusTelegram = nil;
    _actionTelegram = nil;
	[super viewDidUnload];
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

-(CGFloat) readControlStatus{
    BOOL status=NO;
	_statusTelegram = [super.device getTelegramWithCode:0];
	status =[_statusTelegram.value integerValue];
	_actionTelegram = [super.device getTelegramWithCode:1];

    return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=NO;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) setImages{
    [self updateCurrentImage:NO];
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void)buttonPressed:(id)sender{
    _isOn  = !_isOn;
    [self updateCurrentImage:NO];
    [super sendData:@(_isOn) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];
	//[super playButtonSound];
}

-(void) updateCurrentImage:(BOOL) animated{
	if (animated) {
		[super animationUpdate:super.centerButton];
	}
    if (_isOn) {
        [super.centerButton setImage:_onStateImage forState:(UIControlStateNormal)];
        [super.centerButton setImage:_offStateImage forState:(UIControlStateHighlighted)];
    }
    else{
        [super.centerButton setImage:_offStateImage forState:(UIControlStateNormal)];
        [super.centerButton setImage:_onStateImage forState:(UIControlStateHighlighted)];
    }
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_actionTelegram.address,@"address",@(_isOn),@"value",_actionTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end

