//
//  KNXDiscover.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "KNXDiscover.h"
#import "GCDAsyncUdpSocket.h"
#import "Reachability.h"

#define MESSAGE_SEARCH_REQUEST  							99
#define CODE_SEARCH_REQUEST 									0x0201
#define CODE_SEARCH_RESPONSE									514
#define LENGTH_SEARCH_REQUEST 								0x000E

#define TIMER_TIME_TO_SEARCH									4
#define TAG_SOCKET_SEARCH										14

#define HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO	0x0610
#define HPAI_LENGTH													0x08
#define IPV4_UDP															0x01

@interface KNXDiscover ()
@property (nonatomic,strong) GCDAsyncUdpSocket *discoverSocket;
@property (strong,nonatomic) dispatch_queue_t rxQueue;
@property (nonatomic,strong) NSMutableArray * discoverArray;
@end


@implementation KNXDiscover

-(KNXDiscover *) init{
	self = [super init];
	if (self){
		[self startQueue];
		[self initVars];
	}
	return self;
}

-(void) startQueue{
	if (_rxQueue == nil) {
		_rxQueue = dispatch_queue_create("discoverQueue", NULL);
	}
}

-(void) stopQueues{
	_rxQueue =nil;
}

-(void) endDiscover{
	[self stopQueues];
}

-(void) initVars{
	_discoverSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
    [_discoverSocket setUserData:@{@"tag": @(TAG_SOCKET_SEARCH)}];
	[_discoverSocket setIPv4Enabled:YES];
	[_discoverSocket setIPv6Enabled:NO];
	int r = arc4random() % 100;
	r +=12000;
	[_discoverSocket bindToPort:r error:nil];
	_discoverArray = [NSMutableArray array];	
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext{
	
	[self parseReceivedData:data socket:sock];
	
	if ((NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_SEARCH){
# if SHOW_RX
        NSLog(@"RX -----> Received search data:%@",data);
#endif
    }
}

-(void) parseReceivedData:(NSData *) data socket:(GCDAsyncUdpSocket *) sock{
    if (data.length> 2) {
        char initialCode [2];
        initialCode[0] = (char) (HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO >>8);
        initialCode [1] = (char) HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO;
        NSData *subdata = [data subdataWithRange:NSMakeRange(0,2)];
        if ([subdata isEqualToData:[NSData dataWithBytesNoCopy:&initialCode length:sizeof(initialCode) freeWhenDone:NO]]) {
            [self decodeMessageType:[data subdataWithRange:NSMakeRange(2, data.length -2)] socket:sock];
        }
        subdata = nil;
    }
}

-(void) decodeMessageType:(NSData *) data socket:(GCDAsyncUdpSocket *) sock{
    if (data.length> 2) {
        NSUInteger messageCode = 0;
        [data getBytes:&messageCode length:2];
       	switch (messageCode) {
			case CODE_SEARCH_RESPONSE:
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Search Response");
#endif
				[self parseSearchResponse:[data subdataWithRange:NSMakeRange(4, data.length -4)]];
				break;
            default:
                break;
        }
    }
}


-(void) sendDiscoverRequest{
	[_discoverSocket beginReceiving:nil];
	[self discoverRequest:MESSAGE_SEARCH_REQUEST];
}

-(void) createHeader:(NSMutableData *) data{
	char buffer [6];
    buffer[0] = (char) (HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO >>8);
    buffer [1] = (char) HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO;
    buffer [2] = (char) (CODE_SEARCH_REQUEST >>8);
	buffer [3] = (char) CODE_SEARCH_REQUEST;
	buffer [4] = (char) (LENGTH_SEARCH_REQUEST >>8);
	buffer [5] = (char) LENGTH_SEARCH_REQUEST;
    [data appendBytes:buffer length:sizeof(buffer)];
}

-(void) createDataHeader:(NSMutableData *) data{
    char  buffer [2];
    buffer [0] = HPAI_LENGTH;
	buffer [1] = IPV4_UDP;
	[data appendBytes:buffer length:sizeof(buffer)];
	[self setDiscoverEndpoint:data];
}

-(void) setDiscoverEndpoint:(NSMutableData *) data{
	uint16_t localPort = _discoverSocket.localPort_IPv4;
	
    char buffer [6];
    NSString * aux_ip = [self getIPAddress];
	NSString *aux;
	NSScanner * scan = [NSScanner scannerWithString:aux_ip];
	[scan scanUpToString:@"." intoString:&aux];
	buffer[0] = [aux intValue];
	[scan setScanLocation:[scan scanLocation]+1];
	[scan scanUpToString:@"." intoString:&aux];
	buffer[1] = [aux intValue];
	[scan setScanLocation:[scan scanLocation]+1];
	[scan scanUpToString:@"." intoString:&aux];
	buffer[2] = [aux intValue];
	[scan setScanLocation:[scan scanLocation]+1];
	[scan scanUpToString:@"/0" intoString:&aux];
	buffer[3] = [aux intValue];
	scan = nil;
	
	buffer [4] = (char) (localPort >>8);
	buffer [5] = (char) localPort;
    [data appendBytes:buffer length:sizeof(buffer)];
}

-(void) discoverRequest:(NSUInteger) messageType{
	GCDAsyncUdpSocket *sendSearch = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TIMER_TIME_TO_SEARCH * NSEC_PER_SEC), _rxQueue, ^{
		[_discoverSocket close];
		[_delegate knxSocket:self didEndDiscoverRequest:_discoverArray];
	});
	@synchronized (_discoverArray){
		[_discoverArray removeAllObjects];
	}
	NSMutableData *data = [NSMutableData data];
	@synchronized (data){
		[self createHeader:data];
		[self createDataHeader:data];
#if SHOW_TX
        NSLog(@"TX -----> Sending search:%@",data);
#endif
		[sendSearch sendData:data toHost:@"224.0.23.12" port:3671 withTimeout:-1 tag:22];
		[sendSearch closeAfterSending];
	}
	data= nil;
	sendSearch = nil;
}

#pragma mark - Parsing data


-(void) parseSearchResponse:(NSData *) data{
	NSString *ip = [self getIPFromData:[data subdataWithRange:NSMakeRange(2, 4)]];
	NSString *port = [self getPortFromData:[data subdataWithRange:NSMakeRange(6, 2)]];
	NSString *mac = [self getStringFromData:[data subdataWithRange:NSMakeRange(26, 6)]];
	NSString* name = [NSString stringWithUTF8String:[[data subdataWithRange:NSMakeRange(32, 29)] bytes]];
	@synchronized (_discoverArray){
		[_discoverArray addObject:@{@"ip": ip,@"port": port,@"mac": mac,@"name": name}];
	}
}

-(NSString *) getStringFromData:(NSData *) data{
	NSString *string = [NSString stringWithFormat:@"%@",data];
    NSString *string_1 =[string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *string_2 =[string_1 stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *string_3 =[string_2 stringByReplacingOccurrencesOfString:@">" withString:@""];
	return string_3;
}

-(NSString *) getIPFromData:(NSData *) data{
	unsigned char ip [4];
	[data getBytes:&ip length:sizeof(ip)];
	return [NSString stringWithFormat:@"%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]];
}

-(NSString *) getPortFromData:(NSData *) data{
	unsigned char char_port [2];
	[data getBytes:&char_port length:sizeof(char_port)];
	NSUInteger port = (char_port [0]<<8) | char_port[1];
	return [NSString stringWithFormat:@"%d",port];
}

- (NSString *)getIPAddress
{
    NSString *address = @"0.0.0.0";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([@(temp_addr->ifa_name) isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = @(inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr));
					//  mask = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_netmask)->sin_addr)];
					//  BOOL ok = [self checkIp:address mask:mask remoteIp:_connectionSocket.connectedHost];
					// if (!ok) {
					//     address = @"error";
					//  }
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}

@end
