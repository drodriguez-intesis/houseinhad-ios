//
//  EnvDevViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "EnvDevViewController.h"
#import "Device.h"
#import "Stay.h"
#import "Scene.h"
#import "DevicesCompatibility.h"
#import "ThirdLevelViewController.h"
#import "TypeRgbViewController.h"
#define Y_INITIAL_OFFSET 10
#define X_OFFSET 200
#define Y_OFFSET 10

@interface EnvDevViewController ()
@property (strong,nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic)  Stay *stay;
@property (strong,nonatomic)  Scene *scene;
@property (strong,nonatomic) PasswordViewController *passcodeVC;
@end

@implementation EnvDevViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil stay:(Stay *) stay scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_scene = scene;
		_stay = stay;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.navigationItem.title = _stay.name;

	/*UIButton* back =[UIButton buttonWithType:UIButtonTypeCustom];
	UIImage* buttonImage = [UIImage imageNamed:@"back"];
	[back addTarget:self action:@selector(goBack) forControlEvents:UIControlEventTouchUpInside];
	[back setImage: [UIImage imageNamed:@"back"] forState:UIControlStateNormal];
	[back setImage: [UIImage imageNamed:@"back"] forState:UIControlStateHighlighted];
	back.frame = CGRectMake(-20, 0, buttonImage.size.width+20, buttonImage.size.height);
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:back];
	self.navigationItem.leftBarButtonItem = backItem;*/
    [NSTimer scheduledTimerWithTimeInterval:.001 target:self selector:@selector(addToScrollView) userInfo:nil repeats:NO];

	[self startPasscode];
}



-(void) willMoveToParentViewController:(UIViewController *)parent{
	if (!parent) {
		[self saveWidgetsState];
	}
}
-(void) goBack {
	[self saveWidgetsState];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) viewDidUnload{
	_stay = nil;
	_scene = nil;
	_scrollView = nil;
	[super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[self recalculatePositions];
}

-(void) addToScrollView{
    Device *device;
    NSMutableSet *devices = [(NSMutableSet *)_stay mutableSetValueForKey:@"r_device"];
    NSArray *orderedS = [devices sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
    _scrollView.autoresizesSubviews = NO;
    for (NSUInteger i=0; i<[orderedS count]; i++) {
        device = orderedS[i];
        [self addOneDevice:@(i) set:device];
    }
}

-(void) addOneDevice:(NSNumber *) index set:(Device *) set{
	const NSArray *devicesType =[DevicesCompatibility getAvailableWidgetDevices];
//FIXME	if (index.unsignedIntegerValue >= devicesType.count) return;
	DevicesViewController *device;
	NSDictionary *dev = devicesType [[set.code integerValue]];
	if (!dev[@"scenes"] || ![dev[@"scenes"] boolValue]) return;
	Class klass = NSClassFromString(dev[@"class"]);
	device = [[klass alloc] initWithNibName:dev[@"interface"] bundle:nil device:set scene:_scene];
    if (device != nil) {
        device.delegate = self;
		BOOL hInc = [self calculateDevicePosition:device fromIndex:[self.childViewControllers count]];
        [self addChildViewController:device];
        [_scrollView addSubview:device.view];
		CGFloat h = device.view.bounds.size.height + Y_OFFSET;
		if (!hInc) {
			h = 0;
		}
        _scrollView.contentSize=CGSizeMake(_scrollView.bounds.size.width,_scrollView.contentSize.height + h);
        [self didMoveToParentViewController:device];
        device = nil;
    }

}

-(void) recalculatePositions{
	_scrollView.contentSize=CGSizeMake(0,0);
	[self.childViewControllers enumerateObjectsUsingBlock:^(DevicesViewController * dev, NSUInteger idx, BOOL *stop) {
		BOOL hInc = [self calculateDevicePosition:dev fromIndex:idx];
		CGFloat h = dev.view.bounds.size.height + Y_OFFSET;
		if (!hInc) {
			h = 0;
		}
        _scrollView.contentSize=CGSizeMake(_scrollView.bounds.size.width, _scrollView.contentSize.height + h);
	}];
}

-(BOOL) calculateDevicePosition:(DevicesViewController *) devView fromIndex:(NSUInteger) i{
	BOOL hInc = YES;
	CGFloat x = _scrollView.center.x;
	CGFloat y = _scrollView.contentSize.height + devView.view.bounds.size.height/2;
	if (DEVICE_IS_IPAD) {
		if ((i % 2) && UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
			//parell
			x = _scrollView.center.x + X_OFFSET;
			if (y != 0.0) {
				y = y - devView.view.bounds.size.height - Y_OFFSET;
                hInc = NO;
			}
		}
		else{
			//senar
			if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
				x = _scrollView.center.x - X_OFFSET;
			}
		}
	}
	y += Y_INITIAL_OFFSET ;

	devView.view.center = CGPointMake(x, y);
	return hInc;
}

-(IBAction)resetScrollOffset{
    [_scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void) saveWidgetsState{
	NSMutableArray *actions = [NSMutableArray array];
	[self.childViewControllers enumerateObjectsUsingBlock:^(DevicesViewController * widget, NSUInteger idx, BOOL *stop) {
		if ([widget respondsToSelector:@selector(getSceneStatus)]) {
			NSArray *act= [widget getSceneStatus];
			[actions addObjectsFromArray:act];
		}
	}];
	[self.delegate sceneDeviceDidSave:self actions:actions fromStay:_stay];
}

- (void)deviceViewControllerNextLevel:(DevicesViewController *)controller{
	[self toThirdLevel:controller];
}

-(void) toThirdLevel:(DevicesViewController *) device{
	RgbThirdLevelViewController *level;
	TypeRgbViewController *dev = (TypeRgbViewController *) device;
	NSString *extension = @"_iPad";
	switch ([device.device.code integerValue]) {
		case TYPE_OBJECT_RGB:
			level = [[RgbThirdLevelViewController alloc] initWithNibName:[NSString stringWithFormat:@"RgbThirdLevelViewController%@",(DEVICE_IS_IPAD ? extension : @"")] bundle:nil device:device.device sceneColors:dev.sceneColors];
			level.delegateScene = dev;
			break;
			default:
			return;
			break;
	}
	level.delegate =self;
    level.view.autoresizingMask = UIViewAutoresizingNone;
	[level.view setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
    level.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    [self.view addSubview:level.view];
    [self addChildViewController:level];
    [UIView transitionWithView:level.view duration:.6 options:UIViewAnimationOptionCurveEaseOut animations:^{
        level.view.transform = CGAffineTransformIdentity;
        _scrollView.alpha = 0.3;
        //self.devicesScrollView.backgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
    }completion:^(BOOL finished){
        [level didMoveToParentViewController:self];
        _scrollView.userInteractionEnabled= NO;
    }];
    level = nil;
}

-(void) dismissThirdLevel:(ThirdLevelViewController *) third{
	[UIView transitionWithView:_scrollView duration:0.4 options:UIViewAnimationOptionCurveLinear animations:^{
    	_scrollView.alpha = 1.0;
        third.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        third.view.alpha = 0.1;
		//self.devicesScrollView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
		
    }
					completion:^(BOOL finished){
						_scrollView.userInteractionEnabled = YES;
						[third removeFromParentViewController];
						[third.view removeFromSuperview];
					}];
}


#pragma mark Passcode

-(void) startPasscode{
	[self performSelector:@selector(checkPasscode) withObject:nil afterDelay:.5];
}

-(void) checkPasscode{
	if (!_stay.isSecure.boolValue || _passcodeVC) return;
	[self presentPasscode:_stay.passcode withType:PASSWORD_TYPE_CHECK];
}

-(void) presentPasscode:(NSString *) passcode withType:(PasswordType) type{
	if (_passcodeVC) return;
	_scrollView.userInteractionEnabled = NO;
	_passcodeVC = [[PasswordViewController alloc] initWithNibName:@"PasswordViewController" bundle:nil passcode:passcode type:type];
	//	_passcodeVC.view.bounds = self.view.bounds;
	_passcodeVC.delegate = self;
	_passcodeVC.view.alpha = 0.0;
	_passcodeVC.view.center = self.view.center;
	[self addChildViewController:_passcodeVC];
	[self.view addSubview:_passcodeVC.view];
	_passcodeVC.view.center = self.view.center;
	[UIView transitionWithView:_passcodeVC.view duration:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
		_passcodeVC.view.alpha = 1.0;
	} completion:^(BOOL finished) {
		[_passcodeVC didMoveToParentViewController:self];
	}];
}

-(void) passwordVCdidResolveCode:(PasswordViewController *)passwordVC withType:(PasswordType)type{
	[self dismissPasswordViewAndExit:NO];
	/*if (type == PASSWORD_TYPE_REMOVE) {
		Stay *st = (Stay *) _detailItem;
		st.isSecure = @NO;
		st.passcode = @"";
		[st.managedObjectContext save:nil];
	}
	if (type == PASSWORD_TYPE_ADD) {
		Stay *st = (Stay *) _detailItem;
		st.isSecure = @YES;
		st.passcode = [passwordVC getPasscode];
		[st.managedObjectContext save:nil];
	}*/
}

-(void) passwordVCdidCancelCode:(PasswordViewController *)passwordVC withType:(PasswordType)type{
	if (type == PASSWORD_TYPE_CHECK)[self dismissPasswordViewAndExit:YES];
	if (type == PASSWORD_TYPE_REMOVE)[self dismissPasswordViewAndExit:NO];
	if (type == PASSWORD_TYPE_ADD)[self dismissPasswordViewAndExit:NO];
	
}

-(void) dismissPasswordViewAndExit:(BOOL) exit{
	[_passcodeVC willMoveToParentViewController:nil];
	[_passcodeVC.view removeFromSuperview];
	[_passcodeVC removeFromParentViewController];
	_passcodeVC = nil;
	if (exit) {
		[self goBack];
	}
	else{
		_scrollView.userInteractionEnabled = YES;
	}
	
	/*	[UIView transitionFromView:_passcodeVC.view toView:self.view duration:.4 options:UIViewAnimationOptionTransitionCrossDissolve completion:^(BOOL finished) {
	 self.devicesScrollView.userInteractionEnabled = YES;
	 [_passcodeVC.view removeFromSuperview];
	 _passcodeVC = nil;
	 if (exit && !DEVICE_IS_IPAD)[self performSegueWithIdentifier:@"ExitDevices" sender:self];
	 }];*/
}

@end
