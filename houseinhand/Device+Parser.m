//
//  Device+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Device+Parser.h"
#import "Telegram+Parser.h"

#define KEY_NAME			@"name"
#define KEY_CODE			@"appCode"
#define KEY_STATUS			@"hasStatus"
#define KEY_SCENES			@"allowScenes"
#define KEY_TELEGRAMS	@"telegrams"
@implementation Device (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	return nil;
}
-(void) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	self.order = @(idx);
	self.name = (info[KEY_NAME] ? info[KEY_NAME]: self.name);
	self.code = (info[KEY_CODE] ? @([info[KEY_CODE] integerValue]): self.code);
	self.hasStatus = (info[KEY_STATUS] ? @([info[KEY_STATUS] integerValue]): self.hasStatus);
    self.allowEnvironment = (info[KEY_SCENES] ? @([info[KEY_SCENES] integerValue]): @YES);
        //self.allowEnvironment = @YES;
	self.isSynchronized = @NO;

	//add telegrams array
	if (info[KEY_TELEGRAMS] && [info[KEY_TELEGRAMS] count] >0){
		[info[KEY_TELEGRAMS] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			Telegram *tel = [Telegram insertWithInfo:obj intoManagedObjectContext:moc];
			[self addR_telegramObject:tel];
		}];
	}
}

-(Telegram *) getTelegramWithCode:(NSUInteger) code{
	if (!self.r_telegram || self.r_telegram.count == 0) return nil;
	NSSet *telegrams = [self.r_telegram filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"code == %d",code]];
	if (!telegrams || telegrams.count == 0) return nil;
	return [telegrams anyObject];
}

-(NSArray *) getTelegramsWithAddress:(NSNumber *) address{
	if (!self.r_telegram || self.r_telegram.count == 0) return nil;
	NSSet *telegrams = [self.r_telegram filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"address == %d",address.unsignedIntegerValue]];
	if (!telegrams || telegrams.count == 0) return nil;
	return [telegrams allObjects];
}
@end
