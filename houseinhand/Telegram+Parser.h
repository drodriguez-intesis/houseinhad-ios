//
//  Telegram+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Telegram.h"

@interface Telegram (Parser)
+(Telegram *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc;
+(NSData *) getDataFromInfo:(id) info dpt:(NSString *) dpt write:(BOOL) write;
+(NSUInteger) getSizeOfDpt:(NSString *) dpt;
-(void) setValueFromData:(NSData *) data;
@end
