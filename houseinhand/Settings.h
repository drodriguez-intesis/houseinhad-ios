//
//  Settings.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Config, Network, Preferences;

@interface Settings : NSManagedObject

@property (nonatomic, retain) NSString * version;
@property (nonatomic, retain) NSNumber * netMode;
@property (nonatomic, retain) Config *r_config;
@property (nonatomic, retain) NSSet *r_network;
@property (nonatomic, retain) Preferences *r_preferences;
@end

@interface Settings (CoreDataGeneratedAccessors)

- (void)addR_networkObject:(Network *)value;
- (void)removeR_networkObject:(Network *)value;
- (void)addR_network:(NSSet *)values;
- (void)removeR_network:(NSSet *)values;

@end
