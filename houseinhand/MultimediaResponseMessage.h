//
//  MultimediaResponseMessage.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/5/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

#define  IRTRANS						 		@"irtrans"
#define GLOBAL_CACHE					@"globalcache"

@interface MultimediaResponseMessage : NSObject
+ (id)sharedInstance;

-(void) setDeviceType:(NSString *) devType;
-(BOOL) didExecuteMessage:(NSString *) message;
-(BOOL) isKnownError:(NSString *) message;
-(BOOL) shouldWaitResponse;
@end
