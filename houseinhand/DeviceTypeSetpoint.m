//
//  DeviceTypeSetpoint.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeSetpoint.h"


@implementation DeviceTypeSetpoint

@dynamic decimals;
@dynamic image1;
@dynamic image2;
@dynamic maxValue;
@dynamic minValue;
@dynamic scale;
@dynamic step;
@dynamic units;

@end
