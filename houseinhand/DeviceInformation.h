//
//  DeviceInformation.h
//  houseinhand
//
//  Created by Isaac Lozano on 10/16/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceInformation : NSObject


+ (id)sharedInstance;

-(NSDictionary *) deviceInformationForCheckDevice;
-(NSDictionary *) deviceInformationForAddDevice;
-(NSDictionary *) deviceInformationForAddDeviceMacAddress:(NSString *) macAddress;

@end
