//
//  TypeVeluxViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeVeluxViewController.h"
#import "DeviceTypeVelux.h"


#define BUTTON_DOWN						0
#define BUTTON_STOP							1
#define BUTTON_UP								2

typedef 	NS_ENUM (NSUInteger,SceneStatus) {SCENE_STATUS_NONE,SCENE_STATUS_LEFT,SCENE_STATUS_CENTER,SCENE_STATUS_RIGHT};

@interface TypeVeluxViewController ()
@property (strong,nonatomic) Telegram *upTelegram;
@property (strong,nonatomic) Telegram *downTelegram;
@property (assign,nonatomic) CGFloat stopTime;
@property (assign,nonatomic) SceneStatus scStatus;
@end

@implementation TypeVeluxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeVelux *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:(Device *)newDevice];
    if (self) {
        // Custom initialization
        _stopTime = [[newDevice valueForKey:@"stopTime"] floatValue];
        [self readControlStatus];

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeVelux*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
        _stopTime = [[newDevice valueForKey:@"stopTime"] floatValue];
		[self readControlStatus];
		_scStatus = (BOOL)[self readSceneStatus];
    }
    return self;
}


-(NSInteger) readControlStatus{
    NSUInteger status= 0;
	_downTelegram = [super.device getTelegramWithCode:0];
	_upTelegram = [super.device getTelegramWithCode:1];
    return status;
}

-(CGFloat) readSceneStatus{
	SceneStatus scStatus= SCENE_STATUS_NONE;
	SceneTelegram *down = [super.scene getTelegramWithAddress:_downTelegram.address.integerValue];
	SceneTelegram *up = [super.scene getTelegramWithAddress:_upTelegram.address.integerValue];
	super.widgetSceneActive = YES;
	if (up && down) {
		scStatus= SCENE_STATUS_CENTER;
	}
	else if (down){
		scStatus= SCENE_STATUS_LEFT;
	}
	else if (up){
		scStatus= SCENE_STATUS_RIGHT;
	}
	else{
		scStatus= SCENE_STATUS_LEFT;
		super.widgetSceneActive = NO;
	}
	return scStatus;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];
	if (super.type == TYPE_SCENE) {
		[self markSelectedSceneButton];
		super.centerButton.hidden =YES;
		super.centerButton.userInteractionEnabled =NO;
	}
}

-(void) markSelectedSceneButton{
	super.centerButton.alpha = ALPHA_UNSELECTED;
	super.leftButton.alpha = ALPHA_UNSELECTED;
	super.rightButton.alpha = ALPHA_UNSELECTED;
	switch (_scStatus) {
		case SCENE_STATUS_LEFT:
			super.leftButton.alpha = 1.0;
			break;
		case SCENE_STATUS_CENTER:
			super.centerButton.alpha = 1.0;
			break;
		case SCENE_STATUS_RIGHT:
			super.rightButton.alpha = 1.0;
			break;
		default:
			break;
	}
}

- (void)viewDidUnload
{
    _upTelegram = nil;
    _downTelegram = nil;
	[super viewDidUnload];
}

-(void) setActions{
    [super.leftButton addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.rightButton addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.centerButton addTarget:self action:@selector(centerButtonPressed) forControlEvents:UIControlEventTouchDown];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void) leftButtonPressed{
	if (super.type == TYPE_SCENE) {
		[super enableSceneWidget];
		_scStatus = SCENE_STATUS_LEFT;
		[self markSelectedSceneButton];
	}
	else{
		[self actionVelux:BUTTON_DOWN];
	}
}

-(void) rightButtonPressed{
	if (super.type == TYPE_SCENE) {
		[super enableSceneWidget];
		_scStatus = SCENE_STATUS_RIGHT;
		[self markSelectedSceneButton];
	}
	else{
		[self actionVelux:BUTTON_UP];
	}
}

-(void) centerButtonPressed{
	if (super.type == TYPE_SCENE) {
		[super enableSceneWidget];
		_scStatus = SCENE_STATUS_CENTER;
		[self markSelectedSceneButton];
	}
	else{
		[self actionVelux:BUTTON_STOP];
	}
}

-(void) actionVelux:(NSUInteger) button{
    if (button == 0 || button == 1) {
        [super sendData:@1 address:_downTelegram.address dpt:_downTelegram.dpt type:_downTelegram.type delay:_downTelegram.delay];
        [super sendData:@0 address:_downTelegram.address dpt:_downTelegram.dpt type:_downTelegram.type delay:@(_stopTime)];
    }
    if (button == 2 || button == 1) {
        [super sendData:@1 address:_upTelegram.address dpt:_upTelegram.dpt type:_upTelegram.type delay:_upTelegram.delay];
        [super sendData:@0 address:_upTelegram.address dpt:_upTelegram.dpt type:_upTelegram.type delay:@(_stopTime)];
    }
}

-(NSArray *) getSceneStatus{
	NSArray *infoArray = nil;
	if ([super needSceneStatus]) {
		switch (_scStatus) {
			case SCENE_STATUS_LEFT:{
				infoArray = @[@{
					  @"address": _downTelegram.address,
	   @"value": @1U,
	   @"dpt": _downTelegram.dpt,
	   },
				  @{
					  @"address": _downTelegram.address,
	   @"value": @0U,
	   @"dpt": _downTelegram.dpt,
	   @"delay": @(_stopTime)
	   }
				  ];
			}
				break;
			case SCENE_STATUS_CENTER:{
				infoArray = @[@{
					  @"address": _downTelegram.address,
	   @"value": @1U,
	   @"dpt": _downTelegram.dpt,
	   },
				  @{
					  @"address": _upTelegram.address,
	   @"value": @1U,
	   @"dpt": _upTelegram.dpt,
	   },
				  @{
					  @"address": _downTelegram.address,
	   @"value": @0U,
	   @"dpt": _downTelegram.dpt,
	   @"delay": @(_stopTime)
	   },
				  @{
					  @"address": _upTelegram.address,
	   @"value": @0U,
	   @"dpt": _upTelegram.dpt,
	   @"delay": @(_stopTime)
	   }
				  ];
			}
				break;
			case SCENE_STATUS_RIGHT:{
				infoArray =  @[@{
					   @"address": _upTelegram.address,
		@"value": @1U,
		@"dpt": _upTelegram.dpt,
		},
				   @{
					   @"address": _upTelegram.address,
		@"value": @0U,
		@"dpt": _upTelegram.dpt,
		@"delay": @(_stopTime)
		}];
			}
				break;
			default:
				break;
		}
	}
	return infoArray;
}


@end
