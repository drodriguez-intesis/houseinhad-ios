//
//  DeviceTypeClima.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeClima : Device

@property (nonatomic, retain) NSNumber * hasLock;

@end
