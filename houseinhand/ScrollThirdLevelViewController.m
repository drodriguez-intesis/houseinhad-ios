//
//  ScrollThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/13/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//
#import "ScrollThirdLevelViewController.h"
#import "TwoLinesCell.h"

@interface ScrollThirdLevelViewController ()
@property (assign,nonatomic) NSArray * items;
@property (weak, nonatomic) IBOutlet UILabel *titleName;

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation ScrollThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil items:(NSArray *) items
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
		_items = items;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_titleName.text = NSLocalizedStringFromTableInBundle(@"KNX/IP gateways", nil, CURRENT_LANGUAGE_BUNDLE, @"title of uialertview style");
	[self resizeFromItemsNumber];
    [self registerNib];
}

-(void) resizeFromItemsNumber{
	CGRect frame = self.view.frame;
	switch ([_items count]) {
		case 1:
			frame.size.height -= 240;
			break;
		case 2:
			frame.size.height -= 160;
			break;
		case 3:
			frame.size.height -= 80;
			break;
		default:
			break;
	}
	self.view.frame = frame;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

-(void) registerNib{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    UINib *topNib = [UINib nibWithNibName:@"TwoLinesCell" bundle:classBundle];
    [[self tableView] registerNib:topNib forCellReuseIdentifier:@"TwoLines"];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"TwoLines";
    TwoLinesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[TwoLinesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
        }
    }
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundColor = [UIColor clearColor];
	}
}

- (void)configureCell:(TwoLinesCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    //NSLog(@"Object is:%@",[self.fetchedResultsController objectAtIndexPath:indexPath]);
	cell.name.text = (_items[indexPath.row])[@"name"];
	cell.ipPort.text = [NSString stringWithFormat:@"@IP: %@:%@",(_items[indexPath.row])[@"ip"],(_items[indexPath.row])[@"port"]];
	cell.mac.text = [NSString stringWithFormat:@"@MAC: %@",(_items[indexPath.row])[@"mac"]];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_items count];
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Network", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview") message:[NSString stringWithFormat:@"%@ %@:%@",NSLocalizedStringFromTableInBundle(@"Main network will be changed to:", nil, CURRENT_LANGUAGE_BUNDLE, @"message of UIalertview"),(_items[indexPath.row])[@"ip"],(_items[indexPath.row])[@"port"]] delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStyleDefault;
	[alert show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0) {
		[_delegateLevel scrollThirdLevel:self didSelectRowWithInfo:_items[_tableView.indexPathForSelectedRow.row]];
	}
}

@end
