//
//  ActionScrollViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 10/26/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#define Y_OPTIONS_VIEW_OFFSET 39

@class Stay;
@class ThirdLevelViewController;
@protocol ActionScrollViewControllerDelegate <NSObject>
//- (void)actionViewDidEndEditing:(NSDictionary *)info;
- (void)actionViewDidEndEditing:(NSDictionary *)info viewController:(ThirdLevelViewController *) level;

@end

typedef 	NS_ENUM (NSUInteger,ActionScrollOptions) {ACTION_HAS_NOTHING,ACTION_HAS_IMAGE_PSW,ACTION_HAS_IMAGE,ACTION_HAS_PSW,ACTION_DISABLE_TAKEIM,ACTION_DISABLE_TAKEIM_PSW,ACTION_DISABLE_ALLIMAGE,ACTION_DISABLE_ALLIMAGE_PSW,ACTION_DISABLE_ALLPSW,ACTION_DISABLE_ALLIMAGE__NOPSW,ACTION_HAS_IMAGE_NOPSW};

@interface ActionScrollViewController : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIAlertViewDelegate,UIPopoverControllerDelegate>
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id <ActionScrollViewControllerDelegate> delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil availableOptions:(ActionScrollOptions) options;

@end
