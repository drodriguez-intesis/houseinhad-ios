//
//  SceneTelegram.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/9/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Scene;

@interface SceneTelegram : NSManagedObject

@property (nonatomic, retain) NSNumber * address;
@property (nonatomic, retain) NSString * adValue;
@property (nonatomic, retain) NSNumber * delay;
@property (nonatomic, retain) NSString * dpt;
@property (nonatomic, retain) NSNumber * options;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) Scene *r_scene;

@end
