//
//  TypeTextViewerViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/23/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "ViewerViewController.h"

@interface TypeTextViewerViewController : ViewerViewController <DeviceStatusDelegate>

@end
