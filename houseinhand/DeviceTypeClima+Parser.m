//
//  DeviceTypeClima+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeClima+Parser.h"
#import "Device+Parser.h"

#define KEY_LOCK	@"hasLock"
@implementation DeviceTypeClima (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeClima *dev = (DeviceTypeClima *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.hasLock = (info[KEY_LOCK] ? @([info[KEY_LOCK] integerValue]): dev.hasLock);
	
	return dev;
}

@end
