//
//  TypeSetpointViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/6/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ViewerButtonsViewController.h"

@interface TypeSetpointViewController : ViewerButtonsViewController <DeviceStatusDelegate,DevicesScenesViewControllerDelegate>
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@property (assign,nonatomic) CGFloat maxValue;
@property (assign,nonatomic) CGFloat minValue;
@property (assign,nonatomic) CGFloat step;
-(void) updateLabel:(BOOL) animated;
-(void) updateButtons;
-(void) sendButtonData:(CGFloat) value;
@end
