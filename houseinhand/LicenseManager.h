//
//  LicenseManager.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/13/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>



typedef void (^LicenseResultBlock)( NSDictionary * result);

@interface LicenseManager : NSObject
@property (copy,nonatomic) LicenseResultBlock delegateBlock;


+ (id)sharedInstance;
-(void) remoteCheckWithStoredLoginResultBlock:(LicenseResultBlock) delegateBlock;
-(void) remoteCheckWithUsername:(NSString *)username password:(NSString *) password resultBlock:(LicenseResultBlock) delegateBlock;

-(void) remoteCheckDeviceStoredLoginResultBlock:(LicenseResultBlock) delegateBlock;
-(void) remoteAddDeviceStoredLoginMac:(NSString *) mac resultBlock:(LicenseResultBlock) delegateBlock;


- (NSString *) licenseVersion;
-(BOOL) hasValidLogin;
-(BOOL) hasStoredAccount;
- (NSDictionary *) credentials;
- (NSDictionary *) checkLicenseGetCredentials;
-(void) unassign;
@end
