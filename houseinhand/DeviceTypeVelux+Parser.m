//
//  DeviceTypeVelux+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeVelux+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1			@"image1"
#define KEY_IMAGE_2			@"image2"
#define KEY_IMAGE_3			@"image3"
#define KEY_STOP_TIME		@"stopTime"


@implementation DeviceTypeVelux (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeVelux *dev = (DeviceTypeVelux *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.image3 = (info[KEY_IMAGE_3] ? info[KEY_IMAGE_3]: dev.image3);
	dev.stopTime = (info[KEY_STOP_TIME] ? @([info[KEY_STOP_TIME] floatValue]): dev.stopTime);
	
	return dev;
}

@end
