//
//  ThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ThirdLevelViewController,Device;
@protocol ThirdLevelViewControllerDelegate <NSObject>

-(void) dismissThirdLevel:(ThirdLevelViewController *) third;

@end
@interface ThirdLevelViewController : UIViewController
@property (strong,nonatomic) IBOutlet UIImageView *backgroundImage;
@property (nonatomic, weak) id <ThirdLevelViewControllerDelegate> delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice;
-(IBAction)dismissView;
@end
