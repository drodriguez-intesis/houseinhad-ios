//
//  TypeRgbViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThreeButtonsViewController.h"
#import "RgbThirdLevelViewController.h"
@interface TypeRgbViewController : ThreeButtonsViewController <RgbThirdLevelViewControllerDelegate>
@property (strong,nonatomic) NSArray *sceneColors;
@end
