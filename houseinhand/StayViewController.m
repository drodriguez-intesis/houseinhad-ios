//
//  StayViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StayViewController.h"
#import "Stay.h"


#define VIEW_BACKGROUND_COLOR_POD			[UIColor colorWithWhite:0.2 alpha:1.0]
#define VIEW_BACKGROUND_COLOR_PAD			[UIColor colorWithWhite:1 alpha:.2]

@interface StayViewController ()
@property (strong,nonatomic) NSTimer *longTouch;
@property (assign,nonatomic) BOOL isActive;
@property (weak,nonatomic) IBOutlet UIImageView *lockedImage;
@end

@implementation StayViewController

-(void) setStay:(Stay *)stay{
    if (_stay != stay) {
        _stay = stay;
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self reloadInformationType:nil];
    self.bckgImage.layer.borderColor = [[UIColor orangeColor] CGColor];
	self.view.backgroundColor = [self getViewBackgroundColor];
	[_stay addObserver:self forKeyPath:@"useCustomImage" options:0 context:nil];
	[_stay addObserver:self forKeyPath:@"name" options:0 context:nil];
	[_stay addObserver:self forKeyPath:@"isSecure" options:0 context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[self reloadInformationType:keyPath];
}


-(void) dealloc{
	[_stay removeObserver:self forKeyPath:@"useCustomImage"];
	[_stay removeObserver:self forKeyPath:@"name"];
	[_stay removeObserver:self forKeyPath:@"isSecure"];
}

-(void) updateImage{
	if ([_stay.useCustomImage boolValue] == YES) {
		dispatch_queue_t someQueue = dispatch_queue_create("cell background queue", NULL);
		dispatch_async(someQueue, ^(void){
			UIImage *im = [UIImage imageWithData:_stay.thumbImage];
			dispatch_async(dispatch_get_main_queue(), ^(void){
				[self.bckgImage setImage:im];
			});
		});
		self.stayImage.hidden = YES;
	}
	else{
		self.stayImage.hidden = NO;
		[self.bckgImage setImage:nil];
		if ([[self.stay valueForKey:@"image"] isEqualToString:@"default"]) {
			[self.stayImage setImage:[UIImage imageNamed:@"stay_generic"]];
		}
		else{
			[self.stayImage setImage:[UIImage imageNamed:[self.stay valueForKey:@"image"]]];
		}
	}
}

-(void) updatePasscode{
	_lockedImage.hidden = (self.stay.isSecure.boolValue ? NO:YES);
}

-(void) updateName{
	self.name.text = [self.stay valueForKey:@"name"];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.stayImage = nil;
    self.bckgImage = nil;
    self.name = nil;
    self.stay = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(IBAction)stayPressed{
	if (DEVICE_IS_IPAD) [self selectStay];
	[self.delegate stayViewControllerDidTouched:self];
}

-(IBAction)stayWillBePressed{
	if (!DEVICE_IS_IPAD) [self selectStay];
}

-(void) showOptionsMenu{
	if (!DEVICE_IS_IPAD) {
		[self.delegate stayViewControllerDidOptionsMenu:self];
		_longTouch = nil;
	}
}

-(void) reloadInformationType:(NSString *) type{
	//[self performSelectorOnMainThread:@selector(setCustomCell) withObject:nil waitUntilDone:NO];
	if (!type || [type isEqualToString:@"useCustomImage"]) [self updateImage];
	if (!type || [type isEqualToString:@"name"]) [self updateName];
	if (!type || [type isEqualToString:@"isSecure"]) [self updatePasscode];
}

-(void) selectStay{
	self.view.backgroundColor = APP_COLOR_ORANGE_DARK;
	_isActive = YES;
}

-(void) unselectStay{
	if (_isActive) _isActive = NO;
	else{
		self.view.backgroundColor = [self getViewBackgroundColor];
	}
	if (!DEVICE_IS_IPAD) self.view.backgroundColor = [self getViewBackgroundColor];
}

-(void) unselectStayAnimated{
	[UIView animateWithDuration:.8 animations:^{
		[self unselectStay];
	}];
}

-(UIColor *) getViewBackgroundColor{
	return (DEVICE_IS_IPAD ? VIEW_BACKGROUND_COLOR_PAD : VIEW_BACKGROUND_COLOR_POD);
}
@end
