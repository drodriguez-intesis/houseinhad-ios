//
//  ServerHandler.h
//  licenseTest
//
//  Created by Isaac Lozano on 8/30/13.
//  Copyright (c) 2013 Intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef 	NS_ENUM (NSUInteger,SHandlerMethod) {METHOD_VERIFY,METHOD_CHECK,METHOD_ADD};

typedef void (^ServerHandlerBlock)( NSDictionary * json);


@interface ServerHandler : NSObject


@property (copy,nonatomic) ServerHandlerBlock delegateBlock;


+ (id)sharedInstance;
-(void) startMethod:(SHandlerMethod) method withInfo:(NSDictionary *) info usingBlock:(ServerHandlerBlock)delegate;

@end
