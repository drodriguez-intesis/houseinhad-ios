//
//  Device.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Stay, Telegram;

@interface Device : NSManagedObject

@property (nonatomic, retain) NSNumber * allowEnvironment;
@property (nonatomic, retain) NSNumber * code;
@property (nonatomic, retain) NSNumber * hasStatus;
@property (nonatomic, retain) NSNumber * isSynchronized;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) Stay *r_stay;
@property (nonatomic, retain) NSSet *r_telegram;
@end

@interface Device (CoreDataGeneratedAccessors)

- (void)addR_telegramObject:(Telegram *)value;
- (void)removeR_telegramObject:(Telegram *)value;
- (void)addR_telegram:(NSSet *)values;
- (void)removeR_telegram:(NSSet *)values;

@end
