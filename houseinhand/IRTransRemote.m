//
//  IRTransRemote.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/5/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "IRTransRemote.h"


#define  OK_MESSAGE 		@"**00018 RESULT OK"
#define  KO_MESSAGE_1 	@"**00047 RESULT Error: Remote Control not found"
#define  KO_MESSAGE_2 	@"**00047 RESULT Error: Remote Command not found"

@implementation IRTransRemote

-(BOOL) didExecutedMessage:(NSString *) message{
	if ([message isEqualToString:OK_MESSAGE]) return YES;
	return NO;
}

-(BOOL) shouldWaitResponse{
	return YES;
}

@end
