//
//  KNXCommunication.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/16/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "KNXCommunication.h"
#import "NSManagedObjectContext+Fetch.h"
#import "Reachability.h"
#import "AppDelegate.h"
#import "Config+Parser.h"
#import "Telegram+Parser.h"
#import "Device.h"
#import "Network.h"
#import "Settings.h"
#import "LicenseManager.h"
#if DEBUG
#define SHOW_STATUS_CHANGES			0
#else
#define SHOW_STATUS_CHANGES			0
#endif


@interface KNXCommunication ()

@property (nonatomic,strong) NSManagedObjectContext *moc;
@property (nonatomic,strong) Config *config;
@property (nonatomic,strong) NSOperationQueue *coreDataQueue;
@property (nonatomic,strong) KNXSocket *socketKNX;

@end

@implementation KNXCommunication

+ (id)sharedInstance {
    static KNXCommunication *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)setParentContext:(NSManagedObjectContext *) parentContext{
	_moc = [[NSManagedObjectContext alloc] init];
	[_moc setParentContext:parentContext];
}

-(BOOL) setActiveConfig{
	_config = nil;
	if (_moc) _config = [Config getActiveConfigOfContext:_moc];
	return  (_config ? YES :NO);
}

-(void) initVars{
	_coreDataQueue = [NSOperationQueue new];
	_coreDataQueue.maxConcurrentOperationCount = 1;
}

-(void) finVars{
	[_coreDataQueue cancelAllOperations];
	_coreDataQueue = nil;
	_socketKNX = nil;
}

#pragma mark public methods

+(void) startCommunication{
	[[self sharedInstance] startCommunication];
}

+(void) stopCommunication{
	[[self sharedInstance] stopCommunication];
}

-(void) startCommunication{
	if ([[LicenseManager sharedInstance] hasStoredAccount]) {
		[self initVars];
		[_coreDataQueue addOperationWithBlock:^{
			[self setupCommunication];
		}];
	}
}

-(void) stopCommunication{
	[self tearDownCommunication];
	//[self finVars];
}

-(void) setupCommunication{
	if ([self setActiveConfig]) {
		if (_config.isDemo.boolValue) {
			[self stopCommunication];
			[self knxSocket:nil didChangeConnectionStatus:STATE_CONNECTION_CONNECTED];
		}
		else{
			if (_socketKNX.connectionStatus == STATE_CONNECTION_CONNECTED) {
				[self stopCommunication];
			}
			_socketKNX = [[KNXSocket alloc] init];
			_socketKNX.delegate = self;
			[_socketKNX startCommunicationWithNetworks:[self getActiveNetworks]];
			[self resetSynchronizedDevices];
		}
    }
}

-(void) knxSocketUnexpectedClosed:(KNXSocket *)tcpServerCom{
    [_coreDataQueue addOperationWithBlock:^{
        [self transparentReconnection];
    }];
}

-(void) transparentReconnection{
    [_socketKNX stopCommunication];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [_coreDataQueue addOperationWithBlock:^{
            _socketKNX = [[KNXSocket alloc] init];
			_socketKNX.delegate = self;
			[_socketKNX startCommunicationWithNetworks:[self getActiveNetworks]];
		}];
    });
}

-(void) tearDownCommunication{
	if (_config && (_socketKNX.connectionStatus == STATE_CONNECTION_CONNECTED || _socketKNX.connectionStatus == STATE_CONNECTION_CONNECTING)) {
		[_socketKNX stopCommunication];
		if (!_config.isDemo.boolValue) {
			dispatch_async(dispatch_get_main_queue(), ^{
				[[NSNotificationCenter defaultCenter] postNotificationName:NOT_CONNECTION_STATUS
																	object:@(STATE_CONNECTION_DISCONNECTED)];
			});
		}
	}
}

-(NSArray *) getActiveNetworks{
	Network *net;
	NSMutableArray *array = [NSMutableArray array];
	if (_config.r_settings.netMode.unsignedIntegerValue == 1 || _config.r_settings.netMode.unsignedIntegerValue == 0) {
		net = [_config getNetworkOfType:CONNECT_TO_PRIVATE_IP];
		if (net)[array addObject:@{@"nat": net.nat,@"ip": net.ip,@"port": net.port}];
		net = nil;
	}
	if (_config.r_settings.netMode.unsignedIntegerValue == 2 || _config.r_settings.netMode.unsignedIntegerValue == 0){
		net = [_config getNetworkOfType:CONNECT_TO_PUBLIC_IP];
		if (net)[array addObject:@{@"nat": net.nat,@"ip": net.ip,@"port": net.port}];
		net = nil;
	}
	return array;
}

-(NSString *) getConnectedIp{
	if (_socketKNX && _socketKNX.connectionStatus == STATE_CONNECTION_CONNECTED) return [_socketKNX getConnectedAddress];
	NSArray *networks = [self getActiveNetworks];
	if (!networks || networks.count == 0) return nil;
	return networks[0][@"ip"];
}

-(NSNumber *) getConnectedPort{
	if (_socketKNX && _socketKNX.connectionStatus == STATE_CONNECTION_CONNECTED) return [_socketKNX getConnectedPort];
	NSArray *networks = [self getActiveNetworks];
	if (!networks || networks.count == 0) return nil;
	return networks[0][@"port"];
}

-(BOOL) connectionIsLocal{
	if (_config.r_settings.netMode.unsignedIntegerValue == 1) return YES;
	if (_config.r_settings.netMode.unsignedIntegerValue == 2) return NO;
	if (_socketKNX && _socketKNX.connectionStatus == STATE_CONNECTION_CONNECTED) return [_socketKNX isLocalConnection];
	return YES;
}

-(NSString *) getRemoteIp{
	Network *net = [_config getNetworkOfType:CONNECT_TO_PUBLIC_IP];
	if (net) return net.ip;
	return nil;
}

-(NSString *) getMacAddress{
	if (_socketKNX) {
		return _socketKNX.getConnectedMac;
	}
	return nil;
}

-(void) resetSynchronizedDevices{
	[_coreDataQueue addOperationWithBlock:^{
		NSArray *devices = [_moc executeFetchRequest:[_moc.persistentStoreCoordinator.managedObjectModel fetchRequestTemplateForName:@"fetchSynchronizedDevices"] error:nil];
		if ([devices count] > 0) {
			[devices makeObjectsPerformSelector:@selector(setIsSynchronized:) withObject:@NO];
			[_moc save:nil];
		}
	}];
}

#pragma mark Delegates

-(void) knxSocket:(KNXSocket *)tcpServerCom didChangeConnectionStatus:(KNXStatus)connectionStatus{
	dispatch_async(dispatch_get_main_queue(), ^{
		[[NSNotificationCenter defaultCenter] postNotificationName:NOT_CONNECTION_STATUS
															object:@(connectionStatus)];
	});
	if (connectionStatus == STATE_CONNECTION_CONNECTED && !_config.isDemo.boolValue) {
		[[LicenseManager sharedInstance] remoteCheckDeviceStoredLoginResultBlock:^(NSDictionary *result) {
			if (result && result[@"error"]) {
				[self stopCommunication];
				NSString *errorCode = (result[@"error"][@"code"] ? result[@"error"][@"code"] : @"-1");
				[self showAlertWitTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title for AlertView") ,errorCode] message:NSLocalizedStringFromTableInBundle(result[@"error"][@"message"], nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")];
			}
			else if (result && result[@"result"]) {
				if ([result[@"result"] isKindOfClass:[NSDictionary class]] && [result[@"result"][@"licenseType"] isEqualToString:@"0"]) {
					[_coreDataQueue addOperationWithBlock:^{
						if ([self hasMoreThanMaxWidgets:result[@"result"][@"maxWidgets"]]) {
							[self stopCommunication];
							dispatch_async(dispatch_get_main_queue(), ^{
								[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 19",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:[NSString stringWithFormat:NSLocalizedStringFromTableInBundle(@"Profiles have more than %@ devices", nil, CURRENT_LANGUAGE_BUNDLE, @"message for AlertView"),result[@"result"][@"maxWidgets"]]];
							});
						}
					}];
				}
				else if (result && [result[@"result"] isKindOfClass:[NSString class]]) {
					if ([result[@"result"] isEqualToString:@"offline"]) {

					}
					else if ([result[@"result"] isEqualToString:@"ko"]){
						[self stopCommunication];
						dispatch_async(dispatch_get_main_queue(), ^{
							[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 17",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Could not verify the license", nil, CURRENT_LANGUAGE_BUNDLE, @"message for AlertView")];
						});
					}
					
				}
			}
			else{
				[self stopCommunication];
				[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 5",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Unknown error", nil, CURRENT_LANGUAGE_BUNDLE, @"message for AlertView")];

			}
		}];
	}
}

-(BOOL) hasMoreThanMaxWidgets:(NSString *) maxWidgets{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(r_stay.r_config.isDemo == %@) OR (r_stay.r_prevStay.r_config.isDemo == %@)",@NO,@NO];
	NSArray * maxWidgetArray = [_moc searchObject:@"Device" limit:0 predicatePredicate:predicate sorted:nil resultsType:NSCountResultType];
	if (maxWidgetArray && maxWidgetArray[0] && [maxWidgetArray[0] integerValue] <= [maxWidgets integerValue]) return NO;
	return YES;
}

-(void) knxSocket:(KNXSocket *)tcpServerCom didReceiveData:(NSData *)data address:(NSData *)address{
	__block NSUInteger intAddress = 0;
	char buffer[2];
	[address getBytes:&buffer length:2];
	unsigned int lowAdr = 0x00FF & buffer[1];
	unsigned int highAdr = 0xFF00 & (buffer[0]<<8);
	intAddress = highAdr | lowAdr;
	__block NSData * info = data;
	[_coreDataQueue addOperationWithBlock:^{
		[self exchangeValueStatus:info Address:intAddress];
	}];
}

-(void) exchangeValueStatus:(NSData *)data Address:(NSUInteger) address{
	@try {
#if SHOW_STATUS_CHANGES
		NSLog(@"RX -----> Status changing");
#endif
		NSArray *fetchedTelegrams = [_config getTelegramsWithAddress:@(address) ofContext:_moc];
		if ( fetchedTelegrams && [fetchedTelegrams count] > 0) {
			for (Telegram *knx in fetchedTelegrams) {
				[knx setValueFromData:data];
				knx.r_device.isSynchronized = @YES;
				[_moc save:nil];
#if SHOW_STATUS_CHANGES
				NSLog(@"EXECUTED Status Address:%@ ---- New Value is:%@",@(address),knx.value);
#endif
			}
		}
	}
	@catch (NSException *exception) {
		//NSLog(@"ERROR executing Status Address:%@",@(address));
	}
}

#pragma mark - Connection Status
-(void) changeConnectionStatus:(NSUInteger) status{
 /*   if (status != _connectionStatus) {
        _connectionStatus = status;
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        app.connectionStatus = _connectionStatus;
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NOT_CONNECTION_STATUS
                                                                object:@(_connectionStatus)];
        });
    }*/
}

#pragma mark - Send Data to KNX

-(void) sendInfoToKnx:(NSData *) info address:(NSNumber *) address objectId:(NSManagedObjectID *) objectId{
	if (_socketKNX){
		[_socketKNX sendInfoToKnx:info address:address];
	}
	@try {
		[_coreDataQueue addOperationWithBlock:^{
			Device *dev = nil;
			dev = (Device *)[_moc objectRegisteredForID:objectId];
            [self unSynchronizeDevice:dev];
			dev = nil;
		}];
	}
	@catch (NSException *exception) {
		//NSLog(@"Exception is:%@",exception);
	}
}

-(void) unSynchronizeDevice:(Device *) dev{
	if (!dev || !dev.managedObjectContext || dev.objectID) return;
	dev.isSynchronized = @NO;
	[_moc save:nil];

}

-(void) discoverKNXGatewaysUsingBlock:(KNXDiscoverBlock)delegateBlock{
	_delegateBlock = delegateBlock;
	KNXDiscover *discover = [[KNXDiscover alloc] init];
	discover.delegate = self;
	[discover sendDiscoverRequest];
}

-(void) getMacInstallationIp:(NSString *) ip port:(NSUInteger) port usingBlock:(KNXDescriptBlock)delegateBlock{
	KNXDescription *description = [[KNXDescription alloc] init];
	__block KNXDescriptBlock descBlock = delegateBlock;
	[description startDescriptionRequestIp:ip port:port usingBlock:^(NSDictionary *foundItems) {
		if (descBlock) {
			dispatch_async(dispatch_get_main_queue(), ^{
				descBlock(foundItems);
			});
		}
	}];
}

-(void) knxSocket:(KNXDiscover *) tcpServerCom didEndDiscoverRequest:(NSArray *) foundItems{
	if (foundItems && _delegateBlock) {
		dispatch_async(dispatch_get_main_queue(), ^{
			_delegateBlock(foundItems);
			[tcpServerCom endDiscover];
		});
	}
	
}

-(void) showAlertWitTitle:(NSString *) title message:(NSString *) message{
	dispatch_async(dispatch_get_main_queue(), ^{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Dismiss", nil, CURRENT_LANGUAGE_BUNDLE, @"Button for AlertView") otherButtonTitles: nil];
		[alert show];
	});
}



@end
