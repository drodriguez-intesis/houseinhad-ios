//
//  UISwipeViewGestureRecognizer.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#define INITAL_VIEW_POSITION 480.0
#define CENTER_POINT 210
#define VELOCITY_TRANSITION 1000.0

#import "UISwipeViewGestureRecognizer.h"
#import "UIMovingView.h"
#import "InitialViewController.h"
@interface UISwipeViewGestureRecognizer ()
@property (strong) UIMovingView *relevantView;
@property (assign) BOOL forceMove;
@property (assign) BOOL hasMoved;
@property (assign) CGFloat initialX;
@property (assign) CGFloat initialY;
@end

@implementation UISwipeViewGestureRecognizer


-(void)isMoving:(id)sender {
    UIPanGestureRecognizer *pan = (UIPanGestureRecognizer*)sender;
    if(pan.state == UIGestureRecognizerStateBegan) {
       
        self.forceMove = NO;
        NSUInteger index= 0;
        for (UIMovingView *vw in self.viewsArray) {
            vw.initialPos = CGPointMake(vw.center.x, vw.center.y);
            if (vw.center.x == vw.window.center.x) {
                self.relevantView = vw;
                self.relevantView.isLastLeftView = NO;
                self.relevantView.isLastRightView = NO;
                switch (index) {
                    case 0:
                        self.relevantView.isLastLeftView = YES;
                        break;
                    case 2:
                        self.relevantView.isLastRightView = YES;
                        break;
                    default:
                        break;
                }

            }
            index++;
        }
    }
    
    if(pan.state == UIGestureRecognizerStateChanged) {
        CGFloat velocityX = [sender velocityInView:[[sender view] superview]].x;
        self.swipeToRight = YES;
        if (velocityX > 0) {
            self.swipeToRight = NO;
        }
        if ((self.relevantView.isLastRightView && self.swipeToRight) || (self.relevantView.isLastLeftView && !self.swipeToRight)) {
			[self moveViewsToPoint:sender];
         //   [pan setState:UIGestureRecognizerStateCancelled];
        }
        else{
            if (abs(velocityX) > VELOCITY_TRANSITION && self.forceMove == NO) {
                if ((!self.swipeToRight && !self.relevantView.isLastLeftView) || (self.swipeToRight && !self.relevantView.isLastRightView)) {
                    self.forceMove = YES;
                    [pan setState:UIGestureRecognizerStateEnded];
                }
            }
            else {
                [self moveViewsToPoint:sender];
            }
        }
    }
    else if(pan.state == UIGestureRecognizerStateEnded ) {
        
        CGFloat offset = 0;
        if (self.swipeToRight) {
            if ((self.relevantView.center.x <110 || self.forceMove) && !self.relevantView.isLastRightView){
                offset = -self.relevantView.bounds.size.width;
                [self.delegate viewDidChanged:sender position:1];
            }
        }
        
        else {
            if ((self.relevantView.center.x >210 || self.forceMove) && !self.relevantView.isLastLeftView){
                offset = self.relevantView.bounds.size.width;
                [self.delegate viewDidChanged:sender position:0];
            }
        }
        self.forceMove = NO;
        [UIView animateWithDuration:.25 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
            for (UIMovingView *view in self.viewsArray) {
                CGPoint finalPoint = CGPointMake(view.initialPos.x +offset, view.initialPos.y);
                [view setCenter:finalPoint];
            }
        } completion:^(BOOL finished){
        }];
    }
}

-(void) moveViewsToPoint:(id) sender{
    for (UIMovingView *viw in self.viewsArray) {
        CGPoint translatedPoint = [(UIPanGestureRecognizer*)sender translationInView:[[sender view] superview]];
        translatedPoint = CGPointMake(viw.initialPos.x+translatedPoint.x, viw.initialPos.y);
        [viw setCenter:translatedPoint];
    }
}



@end
