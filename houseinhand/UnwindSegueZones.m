//
//  UnwindSegueZones.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/26/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "UnwindSegueZones.h"

@implementation UnwindSegueZones

- (void)perform
{
	UIViewController *source = self.sourceViewController;
	UIViewController *destination = self.destinationViewController;
	UIView *sourceView = source.view;
	UIView *destinationView = [(destination.childViewControllers)[0] view];
	
	//destinationView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	destinationView.alpha = 1.0;
	destinationView.hidden = NO;

	[UIView animateWithDuration:0.5f
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^(void)
	 {
		 // Move the views to their endpoints and make the dimView darker.
	//	 destinationView.alpha = 1.0;
		// destinationView.transform = CGAffineTransformIdentity;
		 sourceView.alpha = 0.0;
		 sourceView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	 }
					 completion: ^(BOOL done)
	 {
		 // The black view is no longer needed now, so remove it.
		 // [dimView removeFromSuperview];
		 [sourceView removeFromSuperview];
		 [source removeFromParentViewController];
		 // Properly present the new screen.
		 // [source presentViewController:destination animated:NO completion:nil];
		 //[source presentViewController:destination animated:NO completion:nil];
	 }];
}

@end
