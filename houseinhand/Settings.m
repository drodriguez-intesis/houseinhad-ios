//
//  Settings.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Settings.h"
#import "Config.h"
#import "Network.h"
#import "Preferences.h"


@implementation Settings

@dynamic version;
@dynamic netMode;
@dynamic r_config;
@dynamic r_network;
@dynamic r_preferences;

@end
