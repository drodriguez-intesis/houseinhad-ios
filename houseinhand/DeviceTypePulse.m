//
//  DeviceTypePulse.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/7/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypePulse.h"


@implementation DeviceTypePulse

@dynamic image1;
@dynamic pulseWidth;
@dynamic valueOn;
@dynamic valueOff;

@end
