//
//  MultimediaResponseMessage.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/5/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MultimediaResponseMessage.h"

#define  IRTRANS						 		@"irtrans"
#define GLOBAL_CACHE					@"globalcache"

#define  IRTRANS_OK_MESSAGE 					@"**00018 RESULT OK\n"
#define  GLOBAL_CACHE_OK_MESSAGE 		@"completeir"


@interface MultimediaResponseMessage ()
@property (strong,nonatomic) NSString *devType;
@end
@implementation MultimediaResponseMessage

+ (id)sharedInstance {
    static MultimediaResponseMessage *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(BOOL) didExecuteMessage:(NSString *) message{
	if ([_devType isEqualToString:IRTRANS] && [message isEqualToString:IRTRANS_OK_MESSAGE]) return YES;
	if ([_devType isEqualToString:GLOBAL_CACHE] && [message hasPrefix:GLOBAL_CACHE_OK_MESSAGE]) return YES;
	return NO;
}

-(BOOL) isKnownError:(NSString *) message{
	if (message && [_devType isEqualToString:IRTRANS]) return YES;
	if (message && [_devType isEqualToString:GLOBAL_CACHE]) return YES;
	return NO;
}

-(void) setDeviceType:(NSString *) devType{
	_devType = [NSString stringWithString:devType];
}

-(BOOL) shouldWaitResponse{
	if ([_devType isEqualToString:IRTRANS]) return YES;
	if ([_devType isEqualToString:GLOBAL_CACHE]) return YES;
	return NO;
}

@end
