//
//  ConfigMigrate.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/10/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "ConfigMigrate.h"
@interface ConfigMigrate ()
@property (strong,nonatomic) const NSDictionary *transformStayImage;
@end
@implementation ConfigMigrate


-(NSDictionary *) migrateConfigFile:(NSArray *) oldCfg withName:(NSString *) name{
	__block NSDictionary *newCfgFile;
	[self initTransformStayImage];

	newCfgFile = @{
				@"platform": @"ios",
	@"osVersion":[[UIDevice currentDevice] systemVersion],
	@"appVersion":[NSString stringWithFormat:@"%@",(NSString*)kCFBundleVersionKey],
	@"date": [NSDate date],
	@"config": @[@{
				  @"name": (name ? name :@"Home"),
					@"validated": @"1",
	  				@"settings": @{
			  			@"network": @{
				@"items": @[@{@"nat": @"0", @"ip":([[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_IP_LOCAL] ? [[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_IP_LOCAL] : @""),@"port":([[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_PORT_LOCAL] ? [[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_PORT_LOCAL] : @"3671")},@{@"nat": @"1", @"ip":([[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_IP_REMOTE] ? [[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_IP_REMOTE] : @""),@"port":([[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_PORT_REMOTE] ? [[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_OLD_PORT_REMOTE] : @"3671")}],
						},
						@"preferences": @{@"pin": @"",@"web":@"",@"email":@""}
	 				},
	  				@"stays": [self getStaysFromArray:oldCfg]
	  				}]
	};

	return newCfgFile;
}

-(void) initTransformStayImage{
	_transformStayImage = @{
		@"Bathroom" : @"stay_bathtub",
	   @"Living Room" : @"stay_chair",
	   @"Ps3" : @"stay_ps3",
	   @"Osito" : @"stay_bear",
	   @"Tumbona" : @"stay_takesun",
	   @"Temperatura" : @"stay_temperature",
	   @"Dining Room" : @"stay_dinning",
	   @"Parking" : @"stay_garage",
	   @"Vestidor" : @"stay_wardrobe",
	   @"Neteja" : @"stay_cleaning",
	   @"Billar" : @"stay_pool",
	   @"Persianes" : @"stay_blind",
	   @"Double Bedroom" : @"stay_dualbed",
	   @"Study" : @"stay_study",
	   @"Generic" : @"stay_generic",
	   @"Poker" : @"stay_poker",
	   @"Massapizza" : @"stay_eggs",
	   @"Musica" : @"stay_music",
	   @"Exit" : @"stay_exit",
	   @"Weather" : @"stay_meteocloud",
	   @"Pastadents" : @"stay_cleanteeth",
	   @"Biberon" : @"stay_babybottle",
	   @"Biblio" : @"stay_books",
	   @"Llum" : @"stay_lights",
	   @"Scenes" : @"stay_scene",
	   @"Single Bedroom" : @"stay_singlebed",
	   @"Claus" : @"stay_key",
	   @"Chupete" : @"stay_babylollipop",
	   @"Pingpong" : @"stay_pingpong",
	   @"Camara" : @"stay_roofprojector",
	   @"Garden" : @"stay_flour",
	   @"Piscina" : @"stay_swimmingpool",
	   @"iMac" : @"stay_desktop",
	   @"Cafe" : @"stay_coffee",
	   @"Gym" : @"stay_gym",
	   @"Altaveu" : @"stay_music",
	   @"Kitchen" : @"stay_chef",
	   @"Stairs" : @"stay_stairs",
	   @"Paraigues" : @"stay_umbrella",
	   @"Bodega" : @"stay_wine",
	   @"Llapissos" : @"stay_pencils"
	   };
}

-(NSArray *) getStaysFromArray:(NSArray *) array{
	if (!array || array.count == 0) return @[];
	NSMutableArray *stays = [NSMutableArray array];
	[array enumerateObjectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, array.count-1)] options:0 usingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		NSDictionary *stay = [self stayFromStay:obj];
		if (stay) {
			[stays addObject:stay];
		}
	}];
	return stays;
}

-(NSDictionary *) stayFromStay:(NSDictionary*) stay{
	return @{
		@"name": (stay[@"name"] ? stay[@"name"] : @"no_name"),
  		@"image": (_transformStayImage[stay[@"image"]] ? _transformStayImage[stay[@"image"]] : @"stay_generic"),
  		@"locked": (stay[@"isSecure"] ? stay[@"isSecure"] : @"0"),
  		@"passcode": (stay[@"pin"] ? stay[@"pin"] : @"0"),
  		@"devices": (stay[@"deviceList"] ? [self getDevicesFromArray:stay[@"deviceList"] ]: @[]),
	};
}

-(NSArray *) getDevicesFromArray:(NSArray *) array{
	if (!array || array.count == 0) return @[];
	__block NSMutableArray *devices = [NSMutableArray array];
	[array enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
		NSDictionary *dev = [self deviceFromDevice:obj];
		if (dev) {
			[devices addObject:dev];
		}
	}];
	return devices;
}

-(NSDictionary *) deviceFromDevice:(NSDictionary *) device{
	NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
	NSString * type = device[@"type"];
	if (!type) return nil;
	if ([type isEqualToString:@"light"]) {
		[dictionary setValue:@"1" forKey:@"idFamily"];
		[dictionary setValue:@"1" forKey:@"code"];
		[dictionary setValue:@"0" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_lamp1" forKey:@"image1"];
		[dictionary setValue:@"device_lamp1_" forKey:@"image2"];
		if (device[@"status_light"] && device[@"switch"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_light"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
				@"address": [self toIntAddress:device[@"switch"]],
				@"delay":  @"0.0",
				@"dpt": @"1.001",
				@"type": @"1",
				@"code": @"1"
			}
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"dimmer"]) {
		[dictionary setValue:@"1" forKey:@"idFamily"];
		[dictionary setValue:@"2" forKey:@"code"];
		[dictionary setValue:@"1" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_lightbulb" forKey:@"image1"];
		[dictionary setValue:@"device_lightbulb_" forKey:@"image2"];
		[dictionary setValue:@"0" forKey:@"minValue"];
		[dictionary setValue:@"255" forKey:@"maxValue"];
		[dictionary setValue:@"1.0" forKey:@"step"];
		if (device[@"status_dimmer"] && device[@"position"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_dimmer"]],
			 @"delay": @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"position"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"rgb"]) {
		[dictionary setValue:@"1" forKey:@"idFamily"];
		[dictionary setValue:@"3" forKey:@"code"];
		[dictionary setValue:@"9" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_lightbulb" forKey:@"image1"];
		[dictionary setValue:@"device_lightbulb_" forKey:@"image2"];
		[dictionary setValue:@"device_palette" forKey:@"image3"];
		if (device[@"r_position"] && device[@"r_status"] && device[@"g_position"] && device[@"g_status"] && device[@"b_position"] && device[@"b_status"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"r_status"]],
			 @"delay": @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"g_status"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"1"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"b_status"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"2"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"r_position"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"3"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"g_position"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"4"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"b_position"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"5"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"blind"]) {
		[dictionary setValue:@"2" forKey:@"idFamily"];
		[dictionary setValue:@"4" forKey:@"code"];
		[dictionary setValue:@"5" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_blind" forKey:@"image1"];
		[dictionary setValue:@"device_blind_" forKey:@"image2"];
		if (device[@"stop"] && device[@"direction"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"stop"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"direction"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"reg_blind"]) {
		[dictionary setValue:@"2" forKey:@"idFamily"];
		[dictionary setValue:@"5" forKey:@"code"];
		[dictionary setValue:@"6" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_blind" forKey:@"image1"];
		[dictionary setValue:@"device_blind_" forKey:@"image2"];
		if (device[@"status_blind"] && device[@"position"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_blind"]],
			 @"delay": @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"position"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"grad_blind"]) {
		[dictionary setValue:@"2" forKey:@"idFamily"];
		[dictionary setValue:@"6" forKey:@"code"];
		[dictionary setValue:@"7" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_gradhermetic" forKey:@"image1"];
		[dictionary setValue:@"device_gradhermetic_" forKey:@"image2"];
		[dictionary setValue:@"device_gradhermetic_center" forKey:@"image3"];
		[dictionary setValue:@"0.6" forKey:@"stopTime"];
		[dictionary setValue:@"6.1" forKey:@"exitTime"];
		[dictionary setValue:@"3.1" forKey:@"lamasTime"];
		if (device[@"close_dir"] && device[@"open_dir"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"close_dir"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"open_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"velux_blind"]) {
		[dictionary setValue:@"2" forKey:@"idFamily"];
		[dictionary setValue:@"7" forKey:@"code"];
		[dictionary setValue:@"8" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_blind" forKey:@"image1"];
		[dictionary setValue:@"device_blind_" forKey:@"image2"];
		[dictionary setValue:@"device_stop" forKey:@"image3"];
		[dictionary setValue:@"0.6" forKey:@"stopTime"];
		if (device[@"close_dir"] && device[@"open_dir"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"close_dir"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"open_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"clima_control_onebit"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"9" forKey:@"code"];
		[dictionary setValue:@"11" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_clima" forKey:@"image1"];
		[dictionary setValue:@"0" forKey:@"hasLock"];
		if (device[@"status_dir"] && device[@"confort_dir"] && device[@"standby_dir"] && device[@"night_dir"] && device[@"extremes_dir"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_dir"]],
			 @"delay": @"0.0",
			 @"dpt": @"XX.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"confort_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"standby_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"3"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"night_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"4"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"extremes_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"5"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"clima_control"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"10" forKey:@"code"];
		[dictionary setValue:@"12" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_clima" forKey:@"image1"];
		[dictionary setValue:@"0" forKey:@"hasLock"];
		if (device[@"status_dir"] && device[@"mode_dir"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_dir"]],
			 @"delay": @"0.0",
			 @"dpt": @"XX.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"mode_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"cont_celsius"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"11" forKey:@"code"];
		[dictionary setValue:@"3" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_setpoint" forKey:@"image1"];
		[dictionary setValue:@"device_setpoint_" forKey:@"image2"];
		[dictionary setValue:@"ºC" forKey:@"units"];
		[dictionary setValue:@"1" forKey:@"step"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		[dictionary setValue:@"1" forKey:@"scale"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"0" : @"16") forKey:@"minValue"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"70" : @"30") forKey:@"maxValue"];
		if (device[@"status_celsius"] && device[@"act_celsius"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_celsius"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"act_celsius"]],
			 @"delay":  @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"cont_celsius_jung"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"12" forKey:@"code"];
		[dictionary setValue:@"20" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_setpoint" forKey:@"image1"];
		[dictionary setValue:@"device_setpoint_" forKey:@"image2"];
		[dictionary setValue:@"ºC" forKey:@"units"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"0" : @"16") forKey:@"minValue"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"70" : @"30") forKey:@"maxValue"];
		[dictionary setValue:@"1" forKey:@"step"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		[dictionary setValue:@"1" forKey:@"scale"];
		if (device[@"status_celsius"] && device[@"setpoint_celsius"] && device[@"act_celsius"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_celsius"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"setpoint_celsius"]],
			 @"delay":  @"0.0",
			 @"dpt": @"6.010",
			 @"type": @"0",
			 @"code": @"1"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"act_celsius"]],
			 @"delay":  @"0.0",
			 @"dpt": @"6.010",
			 @"type": @"1",
			 @"code": @"2"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"cont_berker"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"13" forKey:@"code"];
		[dictionary setValue:@"21" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_setpoint" forKey:@"image1"];
		[dictionary setValue:@"device_setpoint_" forKey:@"image2"];
		[dictionary setValue:@"ºC" forKey:@"units"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"0" : @"16") forKey:@"minValue"];
		[dictionary setValue:((device[@"range_extended"] && [device[@"range_extended"] boolValue]) ?@"70" : @"30") forKey:@"maxValue"];
		[dictionary setValue:@"1" forKey:@"step"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		[dictionary setValue:@"1" forKey:@"scale"];
		if (device[@"status_celsius"] && device[@"act_celsius"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_celsius"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"act_celsius"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"heat_cool_control"]) {
		[dictionary setValue:@"3" forKey:@"idFamily"];
		[dictionary setValue:@"8" forKey:@"code"];
		[dictionary setValue:@"0" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_heatcool" forKey:@"image1"];
		[dictionary setValue:@"device_heatcool_" forKey:@"image2"];
		if (device[@"status_switch"] && device[@"switch"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_switch"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"switch"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_celsius"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"14" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"ºC" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_celsius"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_celsius"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_kelvin"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"15" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"K" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_kelvin"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_kelvin"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_fahrenheit"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"16" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"ºF" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_fahrenheit"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_fahrenheit"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_humidity"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"17" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"%" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_humidity"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_humidity"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_pressure"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"18" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"Pa" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_pressure"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_pressure"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_aquality"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"19" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"ppm" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_aquality"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_aquality"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_power"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"20" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"kW" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_power"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_power"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_volt"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"21" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"mV" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_volt"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_volt"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_curr"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"22" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"mA" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_curr"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_curr"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_kmh"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"23" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"Km/h" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_kmh"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_kmh"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_wind"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"24" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"m/s" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_wind"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_wind"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_light"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"25" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"klux" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_light"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_light"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_rain"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"26" forKey:@"code"];
		[dictionary setValue:@"15" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"Yes" forKey:@"textYes"];
		[dictionary setValue:@"No" forKey:@"textNo"];
		[dictionary setValue:@"No" forKey:@"textDefault"];
		[dictionary setValue:@"1" forKey:@"valueYes"];
		[dictionary setValue:@"0" forKey:@"valueNo"];
		if (device[@"status_rain"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_rain"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"visu_twilight"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"16" forKey:@"code"];
		[dictionary setValue:@"4" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"lux" forKey:@"units"];
		[dictionary setValue:@"1.0" forKey:@"scale"];
		[dictionary setValue:@"1" forKey:@"decimal"];
		if (device[@"status_twilight"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_twilight"]],
			 @"delay": @"0.0",
			 @"dpt": @"9.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"bool_control"]) {
		[dictionary setValue:@"5" forKey:@"idFamily"];
		[dictionary setValue:@"33" forKey:@"code"];
		[dictionary setValue:@"0" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_power" forKey:@"image1"];
		[dictionary setValue:@"device_power_" forKey:@"image2"];
		if (device[@"status_switch"] && device[@"switch"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_switch"]],
			 @"delay": @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"switch"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"gen_onebyte"]) {
		[dictionary setValue:@"5" forKey:@"idFamily"];
		[dictionary setValue:@"37" forKey:@"code"];
		[dictionary setValue:@"10" forKey:@"appCode"];
		[dictionary setValue:(device[@"read_enabled"] ? device[@"read_enabled"] : @"0") forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_segmented" forKey:@"image1"];
		if (device[@"generic_status"] || device[@"generic_adr"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"generic_status"]],
			 @"delay": @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"0",
			 @"code": @"0"
			 },
			 @{
			 @"address": [self toIntAddress:device[@"generic_adr"]],
			 @"delay":  @"0.0",
			 @"dpt": @"5.004",
			 @"type": @"1",
			 @"code": @"1"
			 }
			 ] forKey:@"telegrams"];
		}
		
		//calculate num segments
		
		[dictionary setValue:device[@"first_text"] forKey:@"firstText"];
		[dictionary setValue:device[@"second_text"] forKey:@"secondText"];
		[dictionary setValue:device[@"third_text"] forKey:@"thirdText"];
		[dictionary setValue:device[@"fourth_text"] forKey:@"fourthText"];
		[dictionary setValue:device[@"fifth_text"] forKey:@"fifthText"];

		[dictionary setValue:device[@"first_value"] forKey:@"firstValue"];
		[dictionary setValue:device[@"second_value"] forKey:@"secondValue"];
		[dictionary setValue:device[@"third_value"] forKey:@"thirdValue"];
		[dictionary setValue:device[@"fourth_value"] forKey:@"fourthValue"];
		[dictionary setValue:device[@"fifth_value"] forKey:@"fifthValue"];
		
		NSUInteger numSegments =5;
		if (device[@"fifth_value"]  && [device[@"fifth_value"] integerValue] != 1000) {
			numSegments = 5;
		}
		else if (device[@"fourth_value"]  && [device[@"fourth_value"] integerValue] != 1000) {
			numSegments = 4;
		}
		else if (device[@"third_value"]  && [device[@"third_value"] integerValue] != 1000) {
			numSegments = 3;
		}
		else if (device[@"second_value"]  && [device[@"second_value"] integerValue] != 1000) {
			numSegments = 2;
		}
		else if (device[@"first_value"]  && [device[@"first_value"] integerValue] != 1000) {
			numSegments = 1;
		}
		[dictionary setValue:[NSString stringWithFormat:@"%d",numSegments] forKey:@"numSegments"];
	}
	
	else if ([type isEqualToString:@"television"] || [type isEqualToString:@"multimedia"]) {
		[dictionary setValue:@"8" forKey:@"idFamily"];
		[dictionary setValue:([device[@"moduleTYPE"] isEqualToString:@"IRTrans"] ? @"39" : @"40") forKey:@"code"];
		[dictionary setValue:([device[@"moduleTYPE"] isEqualToString:@"IRTrans"] ? @"irtrans" : @"globalcache") forKey:@"moduleType"];
		[dictionary setValue:@"18" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_tv" forKey:@"image1"];
		[dictionary setValue:device[@"destinationIP"] forKey:@"localIp"];
		[dictionary setValue:device[@"destinationPORT"] forKey:@"localPort"];
		[dictionary setValue:device[@"destinationExternalPORT"] forKey:@"remotePort"];
		[dictionary setValue:([type isEqualToString:@"television"]  ? @"0" : @"2") forKey:@"remoteType"];
		
		//commands
		[dictionary setValue:[self extractCommandsFromDevice:device isIrtrans:([device[@"moduleTYPE"] isEqualToString:@"IRTrans"] ? YES:NO) isMacro:NO] forKey:@"commands"];
	}
	
	else if ([type isEqualToString:@"multi_macro"]) {
		[dictionary setValue:@"8" forKey:@"idFamily"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"irtrans"] ? @"43" : @"44") forKey:@"code"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"irtrans"] ? @"irtrans" : @"globalcache") forKey:@"moduleType"];
		[dictionary setValue:@"19" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_tv" forKey:@"image1"];
		[dictionary setValue:device[@"destinationIP"] forKey:@"localIp"];
		[dictionary setValue:device[@"destinationPORT"] forKey:@"localPort"];
		[dictionary setValue:device[@"destinationExternalPORT"] forKey:@"remotePort"];
		
		//commands
		[dictionary setValue:[self extractCommandsFromDevice:device isIrtrans:([device[@"subtype"] isEqualToString:@"irtrans"] ? YES:NO) isMacro:YES] forKey:@"commands"];
	}
	
	else if ([type isEqualToString:@"ip_camera"]) {
		[dictionary setValue:@"9" forKey:@"idFamily"];
		[dictionary setValue:@"13" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_camera" forKey:@"image1"];
		[dictionary setValue:device[@"destinationIP"] forKey:@"localIp"];
		[dictionary setValue:device[@"externalPORT"] forKey:@"remotePort"];
		[dictionary setValue:device[@"username"] forKey:@"username"];
		[dictionary setValue:device[@"password"] forKey:@"password"];
		[dictionary setValue:device[@"0"] forKey:@"isSecure"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis"] ? @"46" : [device[@"subtype"] isEqualToString:@"mobotix"] ? @"47" : @"45") forKey:@"code"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis"] ? @"1" : [device[@"subtype"] isEqualToString:@"mobotix"] ? @"2" : @"0") forKey:@"cameraType"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis"] ? @"axis-cgi/mjpg/video.cgi?resolution=480x360&compression=30&fps=15" : [device[@"subtype"] isEqualToString:@"mobotix"] ? @"cgi-bin/faststream.jpg?stream=full&fps=0.0" : @"") forKey:@"lowParam"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis"] ? @"axis-cgi/mjpg/video.cgi?resolution=640x360&compression=60&fps=15" : [device[@"subtype"] isEqualToString:@"mobotix"] ? @"cgi-bin/faststream.jpg?stream=full&fps=0.0" : @"") forKey:@"highParam"];
	}
	
	else if ([type isEqualToString:@"generic_intercom"]) {
		[dictionary setValue:@"10" forKey:@"idFamily"];
		[dictionary setValue:@"17" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"device_intercom" forKey:@"image1"];
		[dictionary setValue:@"device_intercom" forKey:@"image2"];
		[dictionary setValue:device[@"destinationIP"] forKey:@"localIp"];
		[dictionary setValue:device[@"externalPORT"] forKey:@"remotePort"];
		[dictionary setValue:device[@"username"] forKey:@"username"];
		[dictionary setValue:device[@"password"] forKey:@"password"];
		[dictionary setValue:device[@"0"] forKey:@"isSecure"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis-knx"] ? @"48" : @"49") forKey:@"code"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis-knx"] ? @"1" : @"2") forKey:@"cameraType"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis-knx"] ? @"axis-cgi/mjpg/video.cgi?resolution=480x360&compression=30&fps=15" : @"cgi-bin/faststream.jpg?stream=full&fps=0.0") forKey:@"lowParam"];
		[dictionary setValue:([device[@"subtype"] isEqualToString:@"axis-knx"] ? @"axis-cgi/mjpg/video.cgi?resolution=640x360&compression=60&fps=15" : @"cgi-bin/faststream.jpg?stream=full&fps=0.0") forKey:@"highParam"];
		
		[dictionary setValue:@"1" forKey:@"valueOn"];
		[dictionary setValue:@"0" forKey:@"valueOff"];
		[dictionary setValue:@"2" forKey:@"pulseWidth"];
		
		if (device[@"open_door"]) {
			[dictionary setValue:@[
			 @{
			 @"address": [self toIntAddress:device[@"open_door"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	
	else if ([type isEqualToString:@"fc_exit"] || [type isEqualToString:@"fc_blind"] || [type isEqualToString:@"fc_sim"]) {
		[dictionary setValue:@"6" forKey:@"idFamily"];
		[dictionary setValue:@"32" forKey:@"code"];
		[dictionary setValue:@"2" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_switch" forKey:@"image1"];
		[dictionary setValue:device[@"general_val"]  forKey:@"sendValue"];
		NSString *address = ([type isEqualToString:@"fc_sim"] ? device[@"general_sim"]: device[@"general_off"]);
		if (address) {
			[dictionary setValue:@[
			 @{
			 @"address": [self toIntAddress:address],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	else if ([type isEqualToString:@"knx_scene"]) {
		[dictionary setValue:@"6" forKey:@"idFamily"];
		[dictionary setValue:@"31" forKey:@"code"];
		[dictionary setValue:@"2" forKey:@"appCode"];
		[dictionary setValue:@"0" forKey:@"hasStatus"];
		[dictionary setValue:@"1" forKey:@"allowScenes"];
		[dictionary setValue:@"device_scene" forKey:@"image1"];
		[dictionary setValue:[NSString stringWithFormat:@"%d",([device[@"scene_num"] integerValue] > 0 ? [device[@"scene_num"] integerValue] - 1 : 0)]  forKey:@"sendValue"];
		if (device[@"scene_dir"]) {
			[dictionary setValue:@[
			 @{
			 @"address": [self toIntAddress:device[@"scene_dir"]],
			 @"delay":  @"0.0",
			 @"dpt": @"1.001",
			 @"type": @"1",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	
	else if ([type isEqualToString:@"technical_alarm"]) {
		[dictionary setValue:@"4" forKey:@"idFamily"];
		[dictionary setValue:@"28" forKey:@"code"];
		[dictionary setValue:@"16" forKey:@"appCode"];
		[dictionary setValue:@"1" forKey:@"hasStatus"];
		[dictionary setValue:@"0" forKey:@"allowScenes"];
		[dictionary setValue:@"No alarm" forKey:@"textDefault"];
		if (device[@"status_alarm"]) {
			[dictionary setValue:@[@{
			 @"address": [self toIntAddress:device[@"status_alarm"]],
			 @"delay": @"0.0",
			 @"dpt": @"16.001",
			 @"type": @"0",
			 @"code": @"0"
			 }
			 ] forKey:@"telegrams"];
		}
	}
	
	else if ([type isEqualToString:@"tkm"]) {
		dictionary = nil; //TODO: Add tkm
	}

	
	else{
		dictionary = nil;
	}
	
	if (dictionary) [dictionary setValue:(device[@"name"] ? device[@"name"] : @"no_name") forKey:@"name"];

	
	return dictionary;
}


-(NSString *) toIntAddress:(NSString *) address{
	//TODO try exceptions
	NSArray *subStrings = [address componentsSeparatedByString:@"/"];
	NSUInteger main=0,middle=0,low =0;
	main = ([subStrings[0] integerValue]<<11)&0x7800;
	middle = ([subStrings[1] integerValue]<<8)&0x0700;
	low = 0x00FF&[subStrings[2] integerValue];
	NSUInteger value =main|middle|low;
	return [NSString stringWithFormat:@"%d",value];
}

-(NSArray *) extractCommandsFromDevice:(NSDictionary *) device isIrtrans:(BOOL) irtrans isMacro:(BOOL) isMacro{
	if (isMacro) {
		return @[
		   @{@"delay":@"0", @"command" : (device[@"first_frame"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"first_frame"],device[@"destinationLED"]] : device[@"first_frame"]) : @"")},
	 @{@"delay":device[@"first_delay"] , @"command" : (device[@"second_frame"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"second_frame"],device[@"destinationLED"]] : device[@"second_frame"]) : @"")},
	 @{@"delay":device[@"second_delay"] , @"command" : (device[@"third_frame"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"third_frame"],device[@"destinationLED"]] : device[@"third_frame"]) : @"")},
	 ];

	}
	return @[
	@{@"tag":@"power", @"command" : (device[@"guide"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"power"],device[@"destinationLED"]] : device[@"power"]) : @"")},
	@{@"tag":@"leftOption",@"txt":@"GUIDE", @"command" : (device[@"guide"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"guide"],device[@"destinationLED"]] : device[@"guide"]) : @"")},
 	@{@"tag":@"rightOption",@"txt":@"TXT", @"command" : (device[@"return"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"return"],device[@"destinationLED"]] : device[@"return"]) : @"")},
 	@{@"tag":@"av", @"command" : (device[@"av"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"av"],device[@"destinationLED"]] : device[@"av"]) : @"")},
	@{@"tag":@"one", @"command" : (device[@"one"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"one"],device[@"destinationLED"]] : device[@"one"]) : @"")},
	@{@"tag":@"two", @"command" : (device[@"two"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"two"],device[@"destinationLED"]] : device[@"two"]) : @"")},
	@{@"tag":@"three", @"command" : (device[@"three"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"three"],device[@"destinationLED"]] : device[@"three"]) : @"")},
	@{@"tag":@"four", @"command" : (device[@"four"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"four"],device[@"destinationLED"]] : device[@"four"]) : @"")},
	@{@"tag":@"five", @"command" : (device[@"five"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"five"],device[@"destinationLED"]] : device[@"five"]) : @"")},
	@{@"tag":@"six", @"command" : (device[@"six"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"six"],device[@"destinationLED"]] : device[@"six"]) : @"")},
	@{@"tag":@"seven", @"command" : (device[@"seven"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"seven"],device[@"destinationLED"]] : device[@"seven"]) : @"")},
	@{@"tag":@"eight", @"command" : (device[@"eight"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"eight"],device[@"destinationLED"]] : device[@"eight"]) : @"")},
	@{@"tag":@"nine", @"command" : (device[@"nine"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"nine"],device[@"destinationLED"]] : device[@"nine"]) : @"")},
	@{@"tag":@"zero", @"command" : (device[@"zero"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"zero"],device[@"destinationLED"]] : device[@"zero"]) : @"")},
	@{@"tag":@"chUp", @"command" : (device[@"channelUP"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"channelUP"],device[@"destinationLED"]] : device[@"channelUP"]) : @"")},
	@{@"tag":@"chDown", @"command" : (device[@"channelDW"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"channelDW"],device[@"destinationLED"]] : device[@"channelDW"]) : @"")},
	@{@"tag":@"mute", @"command" : (device[@"mute"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"mute"],device[@"destinationLED"]] : device[@"mute"]) : @"")},
	@{@"tag":@"eject", @"command" : (device[@"eject"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"eject"],device[@"destinationLED"]] : device[@"eject"]) : @"")},
	@{@"tag":@"volUp", @"command" : (device[@"volumeUP"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"volumeUP"],device[@"destinationLED"]] : device[@"volumeUP"]) : @"")},
	@{@"tag":@"volDown", @"command" : (device[@"volumeDW"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"volumeDW"],device[@"destinationLED"]] : device[@"volumeDW"]) : @"")},
	@{@"tag":@"info", @"command" : (device[@"info"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"info"],device[@"destinationLED"]] : device[@"info"]) : @"")},
	@{@"tag":@"ok", @"command" : (device[@"ok"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"ok"],device[@"destinationLED"]] : device[@"ok"]) : @"")},
	@{@"tag":@"up", @"command" : (device[@"up"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"up"],device[@"destinationLED"]] : device[@"up"]) : @"")},
	@{@"tag":@"down", @"command" : (device[@"down"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"down"],device[@"destinationLED"]] : device[@"down"]) : @"")},
	@{@"tag":@"left", @"command" : (device[@"left"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"left"],device[@"destinationLED"]] : device[@"left"]) : @"")},
	@{@"tag":@"right", @"command" : (device[@"right"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"right"],device[@"destinationLED"]] : device[@"right"]) : @"")},
	@{@"tag":@"red", @"command" : (device[@"red"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"red"],device[@"destinationLED"]] : device[@"red"]) : @"")},
	@{@"tag":@"green", @"command" : (device[@"green"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"green"],device[@"destinationLED"]] : device[@"green"]) : @"")},
	@{@"tag":@"blue", @"command" : (device[@"blue"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"blue"],device[@"destinationLED"]] : device[@"blue"]) : @"")},
	@{@"tag":@"yellow", @"command" : (device[@"yellow"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"yellow"],device[@"destinationLED"]] : device[@"yellow"]) : @"")},
	@{@"tag":@"play", @"command" : (device[@"play"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"play"],device[@"destinationLED"]] : device[@"play"]) : @"")},
	@{@"tag":@"pause", @"command" : (device[@"pause"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"pause"],device[@"destinationLED"]] : device[@"pause"]) : @"")},
	@{@"tag":@"stop", @"command" : (device[@"stop"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"stop"],device[@"destinationLED"]] : device[@"stop"]) : @"")},
 	@{@"tag":@"rec", @"command" : (device[@"rec"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"rec"],device[@"destinationLED"]] : device[@"rec"]) : @"")},
 	@{@"tag":@"backward", @"command" : (device[@"backward"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"backward"],device[@"destinationLED"]] : device[@"backward"]) : @"")},
 	@{@"tag":@"forward", @"command" : (device[@"forward"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"forward"],device[@"destinationLED"]] : device[@"forward"]) : @"")},
 	@{@"tag":@"skipRight", @"command" : (device[@"skipR"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"skipR"],device[@"destinationLED"]] : device[@"skipR"]) : @"")},
 	@{@"tag":@"skipLeft", @"command" : (device[@"skipL"] ? (irtrans ? [NSString stringWithFormat:@"Asnd %@,%@,l%@",device[@"remote"],device[@"skipL"],device[@"destinationLED"]] : device[@"skipL"]) : @"")},
	];
}

@end
