//
//  ProfilesThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "Config.h"
@class ProfilesThirdLevelViewController;
@protocol ProfilesThirdLevelViewControllerDelegate <NSObject>

-(void) didEndChoosingProfile:(ProfilesThirdLevelViewController *) level;
-(void) didEndDeleting:(ProfilesThirdLevelViewController *) level config:(Config *) config;

@end
@interface ProfilesThirdLevelViewController : ThirdLevelViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id <ProfilesThirdLevelViewControllerDelegate> delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title profiles:(NSArray *) configs remove:(BOOL) remove;
@end
