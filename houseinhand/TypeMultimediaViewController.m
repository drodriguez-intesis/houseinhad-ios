//
//  TypeMultimediaViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/26/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeMultimediaViewController.h"
#import "DeviceTypeMultimedia+Parser.h"
#import "MultiCommand+Parser.h"

@interface TypeMultimediaViewController ()
@property (strong,nonatomic) DeviceTypeMultimedia *dev;

@end

@implementation TypeMultimediaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeMultimedia *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
    	_dev = newDevice;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setActions];
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchDown];
}

-(void) buttonPressed{
    [self.delegate deviceViewControllerNextLevel:self];
}


@end
