//
//  AppDelegate.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/30/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "AppDelegate.h"
#import "RootViewController.h"
#import "KNXCommunication.h"
#import "Config+Parser.h"
#import "KNXSocket.h"
#import "FileManagement.h"
#import "MigrationViewController.h"
#import "LicenseManager.h"
#import "AFAppDotNetAPIClient.h"
#import "NSMutableArray+MoveMutableArray.h"
#import "TSMessage.h"
#define NUMBER_OF_RGB						9
#define TIME_TO_SHOW_STATUS_COM	.5

@interface AppDelegate ()
@property (strong,nonatomic) NSTimer * statusComTimer;
@property (assign,nonatomic) BOOL restartComms;
@end





@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[self registerUserDefaults];
    [self registerDefaultLanguage];
	[self customizeClassesAppearance];
	
	_restartComms = YES;
	
	if ([application respondsToSelector:@selector(setStatusBarStyle:)]) [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
	
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    	
    self.window.backgroundColor = [UIColor colorWithWhite:0.15 alpha:1.0];
	if (![[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_DEMO_LOADED] boolValue]) {
		[[FileManagement sharedInstance] loadDemoDataIntoContext:self.managedObjectContext];
		[[NSUserDefaults standardUserDefaults] setValue:@YES forKey:USER_DEFAULTS_DEMO_LOADED];
	}
    RootViewController *root = (RootViewController *) self.window.rootViewController;
    root.managedObjectContext = self.managedObjectContext;
    return YES;
}

-(void) registerDefaultLanguage{
    NSArray *availableLanguages = @[@"en",@"fr",@"de",@"zh",@"nl",@"it",@"es",@"hi",@"pt",@"da",@"nb",@"sv",@"ru",@"pl",@"tr",@"ar",@"cs",@"el",@"ro",@"ca",@"fa"];
    NSArray *defaultLanguages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    if ([defaultLanguages isEqualToArray:availableLanguages]);
    else{
        NSString *firstLang = defaultLanguages.firstObject;
        if ([availableLanguages containsObject:firstLang]) {
            NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:availableLanguages];
            [mutableArray moveObjectFromIndex:[availableLanguages indexOfObject:firstLang] toIndex:0];
            [[NSUserDefaults standardUserDefaults] setObject:mutableArray forKey:@"AppleLanguages"];
        }
        else{
            [[NSUserDefaults standardUserDefaults] setObject:availableLanguages forKey:@"AppleLanguages"];
        }
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(void) showInfoUpgradeToController:(UIViewController *) controller{
	MigrationViewController *mig = [[MigrationViewController alloc] initWithNibName:[NSString stringWithFormat:@"MigrationViewController%@",(DEVICE_IS_IPAD ? @"_ipad" :@"")] bundle:nil];
	if (DEVICE_IS_IPAD) {
		mig.modalPresentationStyle = UIModalPresentationFormSheet;
	}
	[controller presentViewController:mig animated:YES completion:nil];
}
-(BOOL) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
	[[FileManagement sharedInstance] showUrl:url];
	return YES;
}

-(void) forceChangeConfigFile{
	if ([self.window.rootViewController isKindOfClass:[RootViewController class]]) {
		RootViewController * root = (RootViewController *)self.window.rootViewController;
		[KNXCommunication stopCommunication];
		[root changeConfigFile];
		[KNXCommunication startCommunication];
	}
		
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOT_CONNECTION_STATUS object:nil];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	[[AFAppDotNetAPIClient sharedClient	] cancelAllHTTPOperationsWithMethod:API_URL path:API_PATH];
	[KNXCommunication stopCommunication];
    [self saveContext];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	_restartComms = YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	if (_restartComms) {
		_restartComms = NO;
		[[KNXCommunication sharedInstance] setParentContext:self.managedObjectContext];
		if ([[LicenseManager sharedInstance] hasStoredAccount]) [KNXCommunication startCommunication];
		[[FileManagement sharedInstance] checkFileSharing];
		[[NSUserDefaults standardUserDefaults] setObject:@0 forKey:USER_DEFAULTS_CONNECTION];
		[[NSNotificationCenter defaultCenter] postNotificationName:NOT_DID_REALLY_APP_BECOME_ACTIVE object:nil];
	}
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notifChangeStatusBar:) name:NOT_CONNECTION_STATUS object:nil];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	[self saveContext];
}

-(void) notifChangeStatusBar:(NSNotification *) notification {
    [self updateStatusBar:[[notification object] integerValue] errorCode:0];
}

-(void) registerUserDefaults{
	if ([[NSUserDefaults standardUserDefaults] valueForKey:@"kAppiraterCurrentVersion"]) {
		NSString *localIp = [[NSUserDefaults standardUserDefaults] valueForKey:@"manualIP_preference"];
		NSString *remoteIp = [[NSUserDefaults standardUserDefaults] valueForKey:@"manual2IP_preference"];
		NSUInteger localPort = [[[NSUserDefaults standardUserDefaults] valueForKey:@"manualPort_preference"] integerValue];
		NSUInteger remotePort = [[[NSUserDefaults standardUserDefaults] valueForKey:@"manual2Port_preference"] integerValue];
		[self cleanUserDefaults];
		[[NSUserDefaults standardUserDefaults] setValue:(localIp ? localIp : @"") forKey:USER_DEFAULTS_OLD_IP_LOCAL];
		[[NSUserDefaults standardUserDefaults] setValue:(remoteIp ? remoteIp : @"")  forKey:USER_DEFAULTS_OLD_IP_REMOTE];
		[[NSUserDefaults standardUserDefaults] setValue:@((localPort != 0 ? localPort : 3671)) forKey:USER_DEFAULTS_OLD_PORT_LOCAL];
		[[NSUserDefaults standardUserDefaults] setValue:@((remotePort != 0 ? remotePort : 3671)) forKey:USER_DEFAULTS_OLD_PORT_REMOTE];
	}
	[self registerAccount];
	[self registerDemoData];
	[self registerRgb];
	[self registerConnection];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) cleanUserDefaults{
	NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
	[[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

-(void) registerAccount{
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_SECRET: @""}];
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_LOGGED: @NO}];
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_SHOW_MIGRATION: @YES}];
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_CONFIG_USER: @"NO_USER"}];
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_OFFLINE_MODE: [NSData data]}];
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_EXPIRATION_DATE: @"-"}];
}
-(void) registerDemoData{
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_DEMO_LOADED: @NO}];
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"webview_ip": @"http://houseinhand.com"}];

}
-(void) registerConnection{
	[[NSUserDefaults standardUserDefaults] registerDefaults:@{USER_DEFAULTS_CONNECTION: @NO}];
}

-(void) registerRgb{
	for (NSUInteger i=0;i<NUMBER_OF_RGB;i++) {
		NSDictionary * info = @{@"red": @1.0f,@"green": @1.0f,@"blue": @1.0f};
		[[NSUserDefaults standardUserDefaults] registerDefaults:@{[NSString stringWithFormat:@"%@_%d",USER_DEFAULTS_RGB,i+1]: info}];
	}
}
-(void) updateStatusBar:(NSUInteger) state errorCode:(NSUInteger) err{
    [TSMessage dismissActiveNotification];
	[_statusComTimer invalidate];
	_statusComTimer = [NSTimer scheduledTimerWithTimeInterval:TIME_TO_SHOW_STATUS_COM target:self selector:@selector(showStatusBarOverlay:) userInfo:@(state) repeats:NO];
}

-(void) showStatusBarOverlay:(NSTimer *) timer{
	NSNumber *type = (NSNumber *) [timer userInfo];
	switch ([type integerValue]) {
		case STATE_CONNECTION_CONNECTING:{
            [TSMessage showNotificationInViewController:self.window.rootViewController title:NSLocalizedStringFromTableInBundle(@"Connecting ...", nil, CURRENT_LANGUAGE_BUNDLE, @"toast view title") subtitle:nil image:nil type:TSMessageNotificationTypeWarning duration:TSMessageNotificationDurationAutomatic callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
			[[NSUserDefaults standardUserDefaults] setObject:@NO forKey:USER_DEFAULTS_CONNECTION];
            break;
		}
        case STATE_CONNECTION_DISCONNECTED:{
            [TSMessage showNotificationInViewController:self.window.rootViewController title:NSLocalizedStringFromTableInBundle(@"Disconnected", nil, CURRENT_LANGUAGE_BUNDLE, @"toast view title") subtitle:nil image:nil type:TSMessageNotificationTypeError duration:TSMessageNotificationDurationAutomatic callback:^{
                [KNXCommunication startCommunication];
            } buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
			[[NSUserDefaults standardUserDefaults] setObject:@NO forKey:USER_DEFAULTS_CONNECTION];
            break;
		}
		case STATE_CONNECTION_CONNECTED:{
            [TSMessage showNotificationInViewController:self.window.rootViewController title:NSLocalizedStringFromTableInBundle(@"Connected", nil, CURRENT_LANGUAGE_BUNDLE, @"toast view title") subtitle:nil image:nil type:TSMessageNotificationTypeSuccess duration:TSMessageNotificationDurationAutomatic callback:nil buttonTitle:nil buttonCallback:nil atPosition:TSMessageNotificationPositionTop canBeDismissedByUser:YES];
			[[NSUserDefaults standardUserDefaults] setObject:@YES forKey:USER_DEFAULTS_CONNECTION];
            break;
		}
		case STATE_CONNECTION_DISCONNECTING:{
			[[NSUserDefaults standardUserDefaults] setObject:@NO forKey:USER_DEFAULTS_CONNECTION];
            break;
		}
	}
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"houseinhand" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    NSURL *storeURL = [[self applicationLibraryDirectory] URLByAppendingPathComponent:@"houseinhand.sqlite"];
    NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES,NSInferMappingModelAutomaticallyOption: @YES};
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
        //NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil];
    }
    
    return _persistentStoreCoordinator;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)applicationLibraryDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSLibraryDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void) customizeClassesAppearance{
	//segmented
    UIImage *segmentSelected = [[UIImage imageNamed:@"segmented_selected.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *segmentUnselected = [[UIImage imageNamed:@"segmented_frame.png"] stretchableImageWithLeftCapWidth:5 topCapHeight:0];
    UIImage *segmentedDivider = [UIImage imageNamed:@"segmented_divider.png"];
    
    [[UISegmentedControl appearance] setBackgroundImage:segmentUnselected forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
    [[UISegmentedControl appearance] setBackgroundImage:segmentSelected forState:UIControlStateSelected barMetrics:UIBarMetricsDefault];
    
    [[UISegmentedControl appearance] setDividerImage:segmentedDivider forLeftSegmentState:(UIControlStateNormal | UIControlStateSelected)
                                   rightSegmentState:(UIControlStateNormal | UIControlStateSelected) barMetrics:UIBarMetricsDefault];
	[[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeTextShadowColor:[UIColor clearColor]} forState:UIControlStateNormal];
	[[UISegmentedControl appearance] setTitleTextAttributes:@{UITextAttributeTextColor: [UIColor whiteColor],UITextAttributeTextShadowColor:[UIColor clearColor]} forState:UIControlStateSelected];
	
    // slider
    UIImage *minImage = [[UIImage imageNamed: @"slider_low.png"] stretchableImageWithLeftCapWidth:3 topCapHeight: 0];
    UIImage *maxImage = [[UIImage imageNamed: @"slider_high2.png"] stretchableImageWithLeftCapWidth: 3 topCapHeight: 0];
    UIImage *thumbImage = [[UIImage imageNamed: @"slider_ball.png"] stretchableImageWithLeftCapWidth:6 topCapHeight: 6];
    [[UISlider appearance] setMinimumTrackImage:minImage forState:UIControlStateNormal];
    [[UISlider appearance]  setMaximumTrackImage:maxImage forState:UIControlStateNormal];
    [[UISlider appearance]  setThumbImage:thumbImage forState:UIControlStateNormal];
	[[UISlider appearance] setThumbImage:thumbImage forState:UIControlStateHighlighted];

	
	//nav bar
	[[UINavigationBar appearance] setTitleTextAttributes:@{UITextAttributeTextShadowColor: [UIColor clearColor],UITextAttributeFont : [UIFont fontWithName:@"HelveticaNeue" size:20.0],UITextAttributeTextColor : [UIColor whiteColor]}];
    if([UINavigationBar conformsToProtocol:@protocol(UIAppearanceContainer)]) {
        [UINavigationBar appearance].tintColor = [UIColor whiteColor];
    }
	if ([[UITextField appearance] respondsToSelector:@selector(setTintColor:)]) {
		[[UITextField appearance] setTintColor:APP_COLOR_ORANGE_DARK];
	}
}

@end
