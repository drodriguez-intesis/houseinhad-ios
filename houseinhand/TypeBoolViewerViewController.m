//
//  TypeBoolViewerViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeBoolViewerViewController.h"
#import "DeviceTypeBoolViewer.h"

@interface TypeBoolViewerViewController ()
@property (strong,nonatomic) Telegram *statusTelegram;
@property (strong,nonatomic) NSString *textYes;
@property (strong,nonatomic) NSString *textNo;
@property (strong,nonatomic) NSString *textDefault;
@property (assign,nonatomic) NSInteger valueYes;
@property (assign,nonatomic) NSInteger valueNo;
@property (assign,nonatomic) NSInteger currentVal;
@end

@implementation TypeBoolViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeBoolViewer *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _textNo = newDevice.textNo;
		_textYes = newDevice.textYes;
		_textDefault = newDevice.textDefault;
		_valueNo = newDevice.valueNo.integerValue;
		_valueYes = newDevice.valueYes.integerValue;
        _currentVal = [self readControlStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self updateLabel:NO];
	[self checkStatus];
	if ([super.device.hasStatus boolValue]) {
		[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		Telegram * tel = (Telegram *) object;
		_currentVal = tel.value.integerValue;
		[self updateLabel:ANIMATIONS_ENABLED];
	}
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
	}
}
	
-(CGFloat) readControlStatus{
	NSUInteger status=0;
	_statusTelegram = [super.device getTelegramWithCode:0];
	status = _statusTelegram.value.floatValue;
	return status;
}

-(void) updateLabel:(BOOL) animated{
	if (animated) {
		[super animationUpdate:super.centerLabel];
	}
	NSString * text = _textDefault;
	if (_currentVal == _valueYes) {
		text = _textYes;
	}
	else if (_currentVal == _valueNo){
		text = _textNo;
	}
	super.centerLabel.text  = text;
}

- (void)viewDidUnload
{
	_statusTelegram = nil;
	_textYes = nil;
	_textNo = nil;
    [super viewDidUnload];
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
