//
//  SegueToDevices.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/17/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegueToDevices.h"
#import "DevicesViewControllerViewController.h"
#import "StaysViewController.h"
#import "RootViewController.h"
#import "InitialViewController.h"
#import "ZonesViewController.h"

@implementation SegueToDevices
- (void)perform
{
	UIViewController *vw = self.sourceViewController;
	InitialViewController *initial = (InitialViewController *)vw.parentViewController.parentViewController;
	
	DevicesViewControllerViewController *devices = (DevicesViewControllerViewController *)self.destinationViewController;
	UIView *sourceView = initial.view;
	UIView *destinationView = devices.view;
	
	// Add the destination view to the source view.
	destinationView.frame = sourceView.bounds;
	[sourceView addSubview:destinationView];
	
	// Calculate the endpoint for the destination view. Note: this view is
	// now a subview of the source view and the source view will be moving
	// too, hence the strange coordinates.
	CGPoint destEndPoint = destinationView.center;
	destEndPoint.x += destinationView.bounds.size.width / 2.0f;
	
	// Move the destination view outside the visible area so that it will
	// appear to slide in from the right.
	CGPoint destStartPoint = destinationView.center;
	destStartPoint.x += destinationView.bounds.size.width;
	destinationView.center = destStartPoint;
	
	// Calculate the endpoint for the source view. It will slide off to the
	// left but only moves half the width of the screen, while the destination
	// view slides the entire width of the screen. This gives the animation a
	// nice parallax effect.
	CGPoint sourceEndPoint = sourceView.center;
	sourceEndPoint.x -= sourceView.bounds.size.width / 2.0f;
	
	// Start the animation.
	[UIView animateWithDuration:0.3f
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^(void)
	 {
		 // Move the views to their endpoints and make the dimView darker.
		 destinationView.center = destEndPoint;
		 sourceView.center = sourceEndPoint;
	 }
					 completion: ^(BOOL done)
	 {
		 
		 // Properly present the new screen.
		 [initial presentViewController:devices animated:NO completion:nil];
	 }];
}

@end
