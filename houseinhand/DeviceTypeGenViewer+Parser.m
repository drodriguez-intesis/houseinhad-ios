//
//  DeviceTypeGenViewer+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeGenViewer+Parser.h"
#import "Device+Parser.h"

#define KEY_ON_TEXT			@"textOn"
#define KEY_OFF_TEXT			@"textOff"
#define KEY_LOGIC				@"logic"

@implementation DeviceTypeGenViewer (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeGenViewer *dev = (DeviceTypeGenViewer *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.textYes = (info[KEY_ON_TEXT] ? info[KEY_ON_TEXT]: dev.textYes);
	dev.textNo = (info[KEY_OFF_TEXT] ? info[KEY_OFF_TEXT]: dev.textNo);
	dev.logic = (info[KEY_LOGIC] ? info[KEY_LOGIC]: dev.logic);
	return dev;
}


@end
