//
//  DeviceTypeLight.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeLight.h"


@implementation DeviceTypeLight

@dynamic image1;
@dynamic image2;
@dynamic image3;
@dynamic sendSceneValue;

@end
