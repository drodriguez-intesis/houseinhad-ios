//
//  SegueToDevices.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/17/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayViewController.h"

@interface SegueToDevices : UIStoryboardSegue
@property (strong,nonatomic) StayViewController *stay;

@end
