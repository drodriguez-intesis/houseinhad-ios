//
//  DeviceType3Buttons.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/01/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import "DeviceType3Buttons.h"


@implementation DeviceType3Buttons

@dynamic image1;
@dynamic image2;
@dynamic image3;
@dynamic numButtons;
@dynamic firstValue;
@dynamic secondValue;
@dynamic thirdValue;

@end
