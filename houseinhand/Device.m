//
//  Device.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Device.h"
#import "Stay.h"
#import "Telegram.h"


@implementation Device

@dynamic allowEnvironment;
@dynamic code;
@dynamic hasStatus;
@dynamic isSynchronized;
@dynamic name;
@dynamic order;
@dynamic r_stay;
@dynamic r_telegram;

@end
