//
//  Scene+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 4/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Scene.h"

@interface Scene (Parser)

-(SceneTelegram *) getTelegramWithAddress:(NSUInteger) address;
@end
