//
//  ObjectsAnimations.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ObjectsAnimations <NSObject>
-(IBAction) animationFinPressed:(id) sender;
-(IBAction) animationInitPressed:(id) sender;
-(IBAction) animationInitFinPressed:(id) sender;
-(void) animationUpdate:(id) sender;
@end
