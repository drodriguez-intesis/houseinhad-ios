//
//  ScrollThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/13/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"


@class ScrollThirdLevelViewController;
@protocol ScrollThirdLevelViewControllerDelegate <NSObject>
-(void) scrollThirdLevel:(ScrollThirdLevelViewController *) thirdLevel didSelectRowWithInfo:(NSDictionary *) info;
@end

@interface ScrollThirdLevelViewController : ThirdLevelViewController <UITableViewDataSource,UITableViewDelegate>
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id <ScrollThirdLevelViewControllerDelegate> delegateLevel;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil items:(NSArray *) items;
@end
