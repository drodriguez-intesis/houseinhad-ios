//
//  DeviceTypeSetpoint.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeSetpoint : Device

@property (nonatomic, retain) NSNumber * decimals;
@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSNumber * maxValue;
@property (nonatomic, retain) NSNumber * minValue;
@property (nonatomic, retain) NSNumber * scale;
@property (nonatomic, retain) NSNumber * step;
@property (nonatomic, retain) NSString * units;

@end
