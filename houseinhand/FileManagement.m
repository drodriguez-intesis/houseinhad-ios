//
//  FileManagement.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/7/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "FileManagement.h"
#import "Config+Parser.h"
#import "AppDelegate.h"
#import "NSData+AES256.h"
#import "ConfigMigrate.h"
#define DEMO_ENCRIPTION_ENABLED		YES
#define ENCRIPTION_ENABLED					YES
#define ENCRIPTION_KEY							@"b5YleYzTyAaxWnWx0ZVhoFt64NgLjVgm"
#define CONFIG_FILE_EXTENSION				@".hcf"
#define CONFIG_OLD_FILE_EXTENSION		@".cfg"
#define LICENSE_OLD_FILE_EXTENSION		@".hih"

#define RANDOM_INIT_SIZE						16


#define IMPORT_TESTING_FILE					NO


@implementation FileManagement

+ (id)sharedInstance {
    static FileManagement *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void) deleteAllConfigurationsInContext:(NSManagedObjectContext *) moc{
	[Config deleteAllInContext:moc];
	[moc save:nil];
}

-(void) checkFileSharing{
	[self loadFilesOfFileSharing];
    
    if (IMPORT_TESTING_FILE){
        NSURL *path = [[NSBundle bundleForClass:self.class] URLForResource:@"TestUnlocked" withExtension:CONFIG_FILE_EXTENSION];
        NSData *demoData = [NSData dataWithContentsOfURL:path];
        NSError *error;
        NSDictionary *configDemo = [NSJSONSerialization JSONObjectWithData:demoData options:kNilOptions error:&error];
        if (error || !configDemo) return;
        AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [self insertNewConfigurationFIle:configDemo withName:@"TestUnlocked" intoMoc:app.managedObjectContext];
    }
}



-(void) insertNewConfigurationFIle:(NSDictionary *) file withName:(NSString *) name intoMoc:(NSManagedObjectContext *) moc{
	@try {
		if (file[@"config"] && [file[@"config"] count] > 0) {
			for (NSDictionary *cfg in file[@"config"]) {
				[Config insertConfig:cfg isActive:NO context:moc];
			}
		}
		[self showAlertViewWithTitle:NSLocalizedStringFromTableInBundle(@"Imported file", nil, CURRENT_LANGUAGE_BUNDLE, @"title of uialertview") message:[NSString stringWithFormat:@"%@ %@",name,NSLocalizedStringFromTableInBundle(@"successfuly imported", nil, CURRENT_LANGUAGE_BUNDLE, @"message of uialertview")]];
	}
	@catch (NSException *exception) {
		
		//NSLog(@"Exception is: %@",exception);
		if ([exception.name isEqualToString:RAISE_EXCEPTION_INVALID_CONFIG]) {
			[self showAlertViewWithTitle:NSLocalizedStringFromTableInBundle(@"Imported file", nil, CURRENT_LANGUAGE_BUNDLE, @"title of uialertview") message:[NSString stringWithFormat:@"%@ %@",name,NSLocalizedStringFromTableInBundle(@"validation failed", nil, CURRENT_LANGUAGE_BUNDLE, @"message of uialertview")]];
		}
		else{
			//delete config if added
			[self showAlertViewWithTitle:NSLocalizedStringFromTableInBundle(@"Imported file", nil, CURRENT_LANGUAGE_BUNDLE, @"title of uialertview") message:[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"Unable to import file:", nil, CURRENT_LANGUAGE_BUNDLE, @"message of uialertview"),name]];
		}
	}
}

-(void) loadDemoDataIntoContext:(NSManagedObjectContext *) moc{
	[Config insertConfig:[self getDemoData:NSLocalizedStringFromTableInBundle(@"en_demo", nil, CURRENT_LANGUAGE_BUNDLE, @"don't translate please!") isDemo:YES] isActive:YES context:moc];

}

-(NSDictionary *) getDemoData:(NSString *) name isDemo:(BOOL) isDemo{
	NSURL *path = [[NSBundle bundleForClass:self.class] URLForResource:name withExtension:CONFIG_FILE_EXTENSION];
	NSData *demoData = [NSData dataWithContentsOfURL:path];
	NSData *unData;
	if ( (!isDemo && ENCRIPTION_ENABLED) || (isDemo && DEMO_ENCRIPTION_ENABLED)) {
		NSData *decData =  [demoData AES256DecryptWithKey:ENCRIPTION_KEY];

		NSData *tempData = [NSData dataWithData:[decData subdataWithRange:NSMakeRange(RANDOM_INIT_SIZE, decData.length - RANDOM_INIT_SIZE)]];
        
        NSString *stringTemp = [NSString stringWithUTF8String:[tempData bytes]];
	
        unData = [stringTemp dataUsingEncoding:NSUTF8StringEncoding];
       
	}
	else{
		unData = [NSData dataWithData:demoData];
	}
    NSError *error;
    NSDictionary *configDemo = [NSJSONSerialization JSONObjectWithData:unData
                                                                 options:kNilOptions
                                                                   error:&error];
	if (error || !configDemo) return nil;
	[[NSUserDefaults standardUserDefaults] setValue:@YES forKey:USER_DEFAULTS_DEMO_LOADED];
	[[NSUserDefaults standardUserDefaults] synchronize];
	return configDemo[@"config"][0];
}

-(void) loadFileName:(NSString *) name intoContext:(NSManagedObjectContext *) moc{
	[Config insertConfig:[self getDemoData:name isDemo:NO] isActive:NO context:moc];
}

-(void) showUrl:(NSURL *) url{
	[self storeFileReceived:url];
	[self loadFilesOfFileSharing];
}

-(void) loadFilesOfFileSharing{
	NSArray *path_array = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *publicDocumentsDir = path_array[0];
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:publicDocumentsDir error:nil];
	if (!files || files.count == 0) return;
	[files enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if ([obj hasSuffix:CONFIG_FILE_EXTENSION]) {
			NSString *path = [publicDocumentsDir stringByAppendingPathComponent:obj];
			NSData *data = [NSData dataWithContentsOfFile:path];
			NSDictionary *config;
			NSError *error;
			if (data) {
				if (ENCRIPTION_ENABLED) {
					NSData *unData =  [data AES256DecryptWithKey:ENCRIPTION_KEY];
					NSMutableData *trimedData = [[NSData dataWithData:[unData subdataWithRange:NSMakeRange(RANDOM_INIT_SIZE, unData.length - (RANDOM_INIT_SIZE))]] mutableCopy];
                    NSString *stringTemp = [NSString stringWithUTF8String:[trimedData bytes]];
                    NSData *finalData = [stringTemp dataUsingEncoding:NSUTF8StringEncoding];
					config = [NSJSONSerialization JSONObjectWithData:finalData options:kNilOptions error:&error];
				}
				else{
					config = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
				}
			}
			if (config && !error) {
				dispatch_async(dispatch_get_main_queue(), ^{
					AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
					[self insertNewConfigurationFIle:config withName:[obj stringByDeletingPathExtension] intoMoc:app.managedObjectContext];
				});
			}
			[self deleteFileAtPath:path];
		}
		else if ([obj hasSuffix:CONFIG_OLD_FILE_EXTENSION]){
			NSString *path = [publicDocumentsDir stringByAppendingPathComponent:obj];
			NSArray* infoArray = [NSArray arrayWithContentsOfFile:path];
			ConfigMigrate *migrate = [[ConfigMigrate alloc] init];
			NSDictionary *newConfig = [migrate migrateConfigFile:infoArray withName:[obj stringByDeletingPathExtension]];
			if (newConfig) {
				NSString *path2 = [publicDocumentsDir stringByAppendingPathComponent:@"Old"];
				[[NSFileManager defaultManager]  createDirectoryAtPath:path2 withIntermediateDirectories:YES attributes:nil error:nil];
				NSError *err;
				[[NSFileManager defaultManager] moveItemAtPath:path toPath:[publicDocumentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Old/%@",[obj lastPathComponent]]] error:&err];
				if (err) [[NSFileManager defaultManager] removeItemAtPath:path error:nil];

				dispatch_async(dispatch_get_main_queue(), ^{
					AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
					[self insertNewConfigurationFIle:newConfig withName:[obj stringByDeletingPathExtension] intoMoc:app.managedObjectContext];
				});
			}
		}
		else if ([obj hasSuffix:LICENSE_OLD_FILE_EXTENSION]){
			NSString *path = [publicDocumentsDir stringByAppendingPathComponent:obj];
			NSString *path2 = [publicDocumentsDir stringByAppendingPathComponent:@"Old"];
			[[NSFileManager defaultManager]  createDirectoryAtPath:path2 withIntermediateDirectories:YES attributes:nil error:nil];
			NSError *err;
			[[NSFileManager defaultManager] moveItemAtPath:path toPath:[publicDocumentsDir stringByAppendingPathComponent:[NSString stringWithFormat:@"Old/%@",[obj lastPathComponent]]] error:&err];
			if (err) [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
		}
	}];
}


-(void) deleteFileAtPath:(NSString *) path{
	[[NSFileManager defaultManager]  removeItemAtPath:path error:nil];
}

-(void) storeFileReceived:(NSURL *) url{
    NSString * fullPathToFile;
    NSString * fullPathToErase;
    NSScanner * scan = [NSScanner scannerWithString:[url relativePath]];
    [scan scanUpToString:@"Inbox/" intoString:&fullPathToErase];    
    NSString * fileName = [NSString stringWithString:[url lastPathComponent]];
    
    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString* documentsDirectory = [paths objectAtIndex:0];
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
	if (files == nil) {
		//NSLog(@"Error reading contents of documents directory: %@", [error localizedDescription]);
	}
	
	else {
		for (NSString *file in files) {
            //NSLog(@"file is:%@",file);
			if ([file hasSuffix:CONFIG_FILE_EXTENSION] && [fileName hasSuffix:CONFIG_FILE_EXTENSION]) {
                NSFileManager *fileManager = [NSFileManager defaultManager];
                [fileManager removeItemAtPath:[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",file]]  error:NULL];
            }
		}
	}	
    fullPathToFile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@",fileName]];
    NSFileManager *defaultManager;
    defaultManager = [NSFileManager defaultManager];	
    [defaultManager moveItemAtPath:[url relativePath] toPath:fullPathToFile error:nil];
    [defaultManager removeItemAtPath:[NSString stringWithFormat:@"%@/Inbox/",fullPathToErase] error:nil];
}



- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void) showAlertViewWithTitle:(NSString *) title message:(NSString *) message{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Return", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of uialertview") otherButtonTitles: nil];
	[alert show];
}

-(void) showAlertViewWithTitle:(NSString *) title message:(NSString *) message buttonTitle:(NSString *) buttonTitle{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Return", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of uialertview") otherButtonTitles: buttonTitle,nil];
	[alert show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex == 0) return;
	AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
	[app forceChangeConfigFile];
}

@end
