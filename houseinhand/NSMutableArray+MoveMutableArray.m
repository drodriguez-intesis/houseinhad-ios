//
//  NSMutableArray+MoveMutableArray.m
//  IntesisHome
//
//  Created by Isaac Lozano on 1/31/13.
//
//

#import "NSMutableArray+MoveMutableArray.h"

@implementation NSMutableArray (MoveMutableArray)

- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to
{
	@synchronized (self){
		if (to != from) {
			id obj = self[from];
			[self removeObjectAtIndex:from];
			if (to >= [self count]) {
				[self addObject:obj];
			} else {
				[self insertObject:obj atIndex:to];
			}
		}

	}
}

@end
