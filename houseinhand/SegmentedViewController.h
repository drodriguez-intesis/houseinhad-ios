//
//  SegmentedViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"

@interface SegmentedViewController : DevicesViewController
@property (strong,nonatomic) IBOutlet UISegmentedControl *centralSegmented;
@end
