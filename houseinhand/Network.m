//
//  Network.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Network.h"
#import "Settings.h"


@implementation Network

@dynamic ip;
@dynamic nat;
@dynamic port;
@dynamic r_settings;

@end
