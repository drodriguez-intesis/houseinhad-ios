//
//  ZonesViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZonesViewController.h"
#import "InitialViewController.h"

#import "Stay.h"

@interface ZonesViewController ()
@property (strong,nonatomic) NewActionController *actionSheetController;
@end

@implementation ZonesViewController

-(void) viewDidLoad{
    [super viewDidLoad];
    if (DEVICE_IS_IPAD) {
         [self addToScrollView];
    }
        //  [super addToScrollView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)showOptionsMenu:(UIButton *)button{
	NSUInteger avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_DISABLE_ALLIMAGE__NOPSW : ACTION_DISABLE_ALLPSW);
	Stay *st = (Stay *) super.detailItem;
	if (st.useCustomImage.boolValue) avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_HAS_IMAGE_NOPSW : ACTION_HAS_IMAGE_NOPSW);
	_actionSheetController = [[NewActionController alloc] initActionSheetWithOptions:avOptions originViewController:self.parentViewController withFrame:button.frame setDelegate:self];
}

#pragma mark ActionView delegate
-(void)actionController:(NewActionController *)action didEndEditing:(NSDictionary *)info{
	if (!info) return;
	Stay *st = (Stay *) super.detailItem;
	if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_IMAGE"]) {
		UIImage *thumbImage = (UIImage *) [info valueForKey:@"thumbImage"];
		NSData *imageData = UIImagePNGRepresentation (thumbImage);
		st.thumbImage = imageData;
		st.useCustomImage = @YES;
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"DELETE_IMAGE"]){
		st.thumbImage = nil;
		st.useCustomImage = @NO;
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_NAME"]){
		st.name = [info valueForKey:@"name"];
		super.configName.text =  [super.detailItem name];
	}
}

-(void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (_actionSheetController) {
		Stay *st = (Stay *) super.detailItem;
		[_actionSheetController showModalControllerFromOption:buttonIndex actionSheet:actionSheet inViewController:self withButtonFrame:CGRectMake(0, 0, 0, 0) stayName:st.name];
	}
}
@end
