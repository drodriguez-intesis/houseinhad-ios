//
//  FileManagement.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/7/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FileManagement : NSObject <UIAlertViewDelegate>

+ (id)sharedInstance;
-(void) showUrl:(NSURL *) url;
-(void) checkFileSharing;
-(void) loadDemoDataIntoContext:(NSManagedObjectContext *) demoData;
-(void) loadFileName:(NSString *) name intoContext:(NSManagedObjectContext *) moc;
@end
