//
//  TypeBlindViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeBlindViewController.h"
#import "DeviceTypeLight.h"
#import <AudioToolbox/AudioServices.h>

@interface TypeBlindViewController ()
@property (strong,nonatomic) Telegram *movementTelegram;
@property (strong,nonatomic) Telegram *stopTelegram;
@property (strong,nonatomic) NSTimer *timerLongPressing;
@property (assign,nonatomic) BOOL longPressing;
@property (assign,nonatomic) BOOL scenePressedLeft;
@end

@implementation TypeBlindViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        [self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self readControlStatus];
        _scenePressedLeft = [self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setActions];
	if (super.type == TYPE_SCENE) {
		[self updateAlphaButtons:_scenePressedLeft];
	}
}

-(void) viewDidUnload{
    [super viewDidUnload];
    _movementTelegram = nil;
    [_timerLongPressing invalidate];
    _timerLongPressing = nil;
}

-(void) setActions{
    [super.leftButton addTarget:self action:@selector(initLeftButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.rightButton addTarget:self action:@selector(initRightButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.leftButton addTarget:self action:@selector(finLeftButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [super.rightButton addTarget:self action:@selector(finRightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
}

-(NSUInteger) readControlStatus{
    NSUInteger status= 0;
	_stopTelegram = [super.device getTelegramWithCode:0];
	_movementTelegram = [super.device getTelegramWithCode:1];
   
    return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=NO;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_movementTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) initLeftButtonPressed{
	if (super.type == TYPE_CONTROL) {
		[self createTimer:YES];
	}
	else if (super.type == TYPE_SCENE){
		[self updateAlphaButtons:YES];
	}

}

-(void) initRightButtonPressed{
	if (super.type == TYPE_CONTROL) {
    	[self createTimer:NO];
	}
	else if (super.type == TYPE_SCENE){
		[self updateAlphaButtons:NO];
	}
}

-(void) updateAlphaButtons:(BOOL) left{
	_scenePressedLeft = left;
	if (left) {
		super.rightButton.alpha = ALPHA_UNSELECTED;
		super.leftButton.alpha = 1.0;
	}
	else{
		super.leftButton.alpha = ALPHA_UNSELECTED;
		super.rightButton.alpha = 1.0;
	}
}

-(void) createTimer:(BOOL) leftButton{
    [_timerLongPressing invalidate];
    _longPressing = NO;
    _timerLongPressing = [NSTimer scheduledTimerWithTimeInterval:LONG_TOUCH target:self selector:@selector(timerFired:) userInfo:@(leftButton) repeats:NO];
}

-(void) timerFired:(NSTimer *) timer{
    NSNumber * info= [timer userInfo];
    _longPressing = YES;
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [super sendData:info address:_movementTelegram.address dpt:_movementTelegram.dpt type:_movementTelegram.type delay:_movementTelegram.delay];
}


-(void) finLeftButtonPressed{
    [_timerLongPressing invalidate];
    if (!_longPressing) {
        [super sendData:@1U address:_stopTelegram.address dpt:_stopTelegram.dpt type:_stopTelegram.type delay:_stopTelegram.delay];
    }
}

-(void) finRightButtonPressed{
    [_timerLongPressing invalidate];
    if (!_longPressing) {
        [super sendData:@0U address:_stopTelegram.address dpt:_stopTelegram.dpt type:_stopTelegram.type delay:_stopTelegram.delay];
    }
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSNumber *info = @0U;
		if (super.leftButton.alpha == 1) {
			info = @1U;
		}
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_movementTelegram.address,@"address",info,@"value",_movementTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
