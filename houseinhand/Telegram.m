//
//  Telegram.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Telegram.h"
#import "Device.h"


@implementation Telegram

@dynamic address;
@dynamic code;
@dynamic delay;
@dynamic dpt;
@dynamic stringValue;
@dynamic type;
@dynamic value;
@dynamic r_device;

@end
