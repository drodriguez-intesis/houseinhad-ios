//
//  ActionScrollViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 10/26/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ActionScrollViewController.h"
#import "OverlayCameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AVCamCaptureManager.h"
#import "AppDelegate.h"
#import "StayViewController.h"
#import "InitialViewController.h"
#import "UIImage+Resize.h"
#define CELL_CHANGE_NAME		0
#define CELL_TAKE_IMAGE			1
#define CELL_SELECT_IMAGE			2
#define CELL_REMOVE_IMAGE		3
#define CELL_ADD_PSW				4
#define CELL_CHANGE_PSW			6
#define CELL_REMOVE_PSW			5

#define IMAGE_CROP_OFFSET_X			26
#define IMAGE_CROP_OFFSET_Y			16

#define TYPE_CAPTURE_PHOTO			0
#define TYPE_CAPTURE_VIDEO				1
@class AVCamCaptureManager, AVCamPreviewView, AVCaptureVideoPreviewLayer;

@interface ActionScrollViewController () <AVCamCaptureManagerDelegate>
@property (strong,nonatomic) UIImagePickerController *imagePicker;
@property (nonatomic,retain) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (strong,nonatomic) const NSString * options;
@property (strong,nonatomic) UIButton *takeImageButton;
@property (nonatomic,retain) AVCamCaptureManager *captureManager;
@property (strong,nonatomic) UIPopoverController *popover;
@property (assign,nonatomic) NSUInteger actionOptions;
@end

@implementation ActionScrollViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil availableOptions:(ActionScrollOptions) options
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_actionOptions = options;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self.view setBounds:CGRectMake(self.view.bounds.origin.x, self.view.bounds.origin.y, self.view.bounds.size.width, self.tableView.rowHeight*[self.tableView numberOfRowsInSection:0])];
    // Do any additional setup after loading the view from its nib.
	//self.view.layer.cornerRadius = 6;
	//self.view.layer.borderWidth = 1;
	self.view.layer.shadowOpacity = 0.8;
	self.view.layer.shadowColor = [[UIColor colorWithWhite:0.1 alpha:1.0] CGColor];
	self.view.layer.shadowOffset = CGSizeMake(0, 2);
	//self.view.alpha = 0.9;
	[NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(createImagePicker) userInfo:nil repeats:NO];
	//[self performSelectorInBackground:@selector(createImagePicker) withObject:nil];

}

-(void) viewDidUnload{
	_imagePicker = nil;
	_takeImageButton = nil;
    
	[super viewDidUnload];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"CellId";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    }
	[self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.selectedBackgroundView.backgroundColor = [UIColor orangeColor];
	}
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
	const NSArray *titles = @[NSLocalizedStringFromTableInBundle(@"Rename Stay", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Take Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Select Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Remove Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Add Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Remove Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")];
	cell.textLabel.textColor = [UIColor lightGrayColor];
	cell.textLabel.font = [UIFont systemFontOfSize:16];
	cell.textLabel.textColor = [UIColor colorWithWhite:0.85 alpha:1.0];
	cell.textLabel.highlightedTextColor = [UIColor colorWithWhite:0.2 alpha:1.0];
	cell.textLabel.backgroundColor = [UIColor clearColor];
	cell.textLabel.textAlignment = NSTextAlignmentLeft;
	
	NSUInteger index = indexPath.row;
	if (_actionOptions == ACTION_HAS_NOTHING && indexPath.row == 3) {
		index++;
	}
	else if (_actionOptions == ACTION_HAS_IMAGE_PSW && indexPath.row >= 4) {
		index++;
	}
	else if (_actionOptions == ACTION_HAS_PSW && indexPath.row >= 3){
		index = index +2;
	}
	else if (_actionOptions == ACTION_DISABLE_TAKEIM && indexPath.row >= 1){
		index = index +2;
	}
	else if (_actionOptions == ACTION_DISABLE_TAKEIM_PSW){
		if (indexPath.row == 1) index = index +2;
		else if (indexPath.row > 1) index = index +3;
	}
	else if (_actionOptions == ACTION_DISABLE_ALLIMAGE){
		if (indexPath.row == 1) index = index +3;
	}
	else if (_actionOptions == ACTION_DISABLE_ALLIMAGE_PSW){
		if (indexPath.row >= 1) index = index +4;
	}
	else if (_actionOptions == ACTION_HAS_IMAGE_NOPSW){
		if (indexPath.row >= 1) index = index +2;
	}
	cell.textLabel.text = titles[index];
	cell.tag = index;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
	if (_actionOptions == ACTION_HAS_IMAGE_PSW) return 5;
	else if ((_actionOptions == ACTION_HAS_NOTHING) || (_actionOptions == ACTION_DISABLE_TAKEIM_PSW)) return 4;
	else if (_actionOptions == ACTION_DISABLE_TAKEIM) return 3;
	else if (_actionOptions == ACTION_DISABLE_ALLIMAGE_PSW) return 2;
	else if (_actionOptions == ACTION_DISABLE_ALLIMAGE) return 2;
	else if (_actionOptions == ACTION_HAS_IMAGE) return 5;
	else if (_actionOptions == ACTION_DISABLE_ALLPSW) return 3;
	else if (_actionOptions == ACTION_DISABLE_ALLIMAGE__NOPSW) return 1;
	else if (_actionOptions == ACTION_HAS_IMAGE_NOPSW) return 2;

	return 4;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	UITableViewCell* cell = [tableView cellForRowAtIndexPath:indexPath];
	switch (cell.tag) {
		case CELL_CHANGE_NAME:
			[self changeName];
			break;
		case CELL_TAKE_IMAGE:
            if (DEVICE_IS_IPAD) {
                [self startAndOverlayCamera];
				self.view.alpha = 0.0;
            }else{
                [self presentViewController:_imagePicker animated:YES completion:nil];
            }
			break;
		case CELL_SELECT_IMAGE:
			[self createLibraryPickerFromRect:cell.frame];
			break;
		case CELL_REMOVE_IMAGE:
			[self deleteImage];
			break;
		case CELL_ADD_PSW:
			[self.delegate actionViewDidEndEditing:@{@"type": @"ADD_PASSWORD"} viewController:(ThirdLevelViewController *)self];
			break;
		case CELL_REMOVE_PSW:
			[self.delegate actionViewDidEndEditing:@{@"type": @"REMOVE_PASSWORD"} viewController:(ThirdLevelViewController *)self];
			break;
		default:
			break;
	}
}

#pragma mark ImagePicker

-(void) createImagePicker{
	if (_imagePicker == nil && [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
		_imagePicker =[[UIImagePickerController alloc] init];
		_imagePicker.delegate = self;
		_imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
		_imagePicker.showsCameraControls = YES;
		_imagePicker.allowsEditing = YES;
	}
}

-(void) createLibraryPickerFromRect:(CGRect) rect{
	UIImagePickerController *picker =[[UIImagePickerController alloc] init];
	picker.delegate = self;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
	picker.allowsEditing = YES;
	if (DEVICE_IS_IPAD) {
		if ( _popover && _popover.isPopoverVisible) {
			[_popover dismissPopoverAnimated:YES];
		}
		else{
			_popover= [[UIPopoverController alloc]
					   initWithContentViewController: picker];
			_popover.delegate = self;
			[_popover presentPopoverFromRect:rect	inView:self.view permittedArrowDirections:UIPopoverArrowDirectionAny
									animated:YES];
		}
	}
	else{
		[self presentViewController:picker animated:YES completion:nil];
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
	UIImage *resizedImage = [info valueForKey:UIImagePickerControllerEditedImage];
	if (resizedImage == nil) {
		resizedImage = [info valueForKey:UIImagePickerControllerOriginalImage];
	}
	[self resizeImage:resizedImage type:TYPE_CAPTURE_PHOTO];
	if (DEVICE_IS_IPAD) {
		[_popover dismissPopoverAnimated:YES];
	}else{
		[picker dismissViewControllerAnimated:YES completion:nil];
	}
}

-(void) resizeImage:(UIImage *) im type:(NSUInteger) type{

	UIImage *bckImage;
	
	
	UIImage *thumbTmp = [im resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(IMAGE_CROP_SIZE_W, IMAGE_CROP_SIZE_W) interpolationQuality:kCGInterpolationDefault];
	UIImage *thumb = [thumbTmp croppedImage:CGRectMake(thumbTmp.size.width/2 - IMAGE_CROP_SIZE_W/2, thumbTmp.size.height/2 - IMAGE_CROP_SIZE_H/2, IMAGE_CROP_SIZE_W, IMAGE_CROP_SIZE_H)];
	
	if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_SAVE_IMAGES] boolValue]) 		UIImageWriteToSavedPhotosAlbum(im, nil, nil, nil);

	
	if (DEVICE_IS_IPAD) {
		CGFloat realHeight = (self.view.window.rootViewController.view.frame.size.width - 40) * [[UIScreen mainScreen] scale];
		CGFloat realWidth = (self.view.window.rootViewController.view.frame.size.height) * [[UIScreen mainScreen] scale];
			
		UIImage *bckTmp = [im resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:CGSizeMake(realWidth, realWidth) interpolationQuality:kCGInterpolationDefault];
			bckImage= [bckTmp croppedImage:CGRectMake(bckTmp.size.width/2 - realWidth/2, bckTmp.size.height/2 - realHeight/2, realWidth, realHeight)];

		[self.delegate actionViewDidEndEditing:@{@"type": @"CHANGE_IMAGE",@"thumbImage": thumb,@"backgroundImage":bckImage} viewController:(ThirdLevelViewController *)self];
	}
	else{
		[self.delegate actionViewDidEndEditing:@{@"type": @"CHANGE_IMAGE",@"thumbImage": thumb} viewController:(ThirdLevelViewController *)self];
	}	
}

-(void) deleteImage{
	[self.delegate actionViewDidEndEditing:@{@"type": @"DELETE_IMAGE"} viewController:(ThirdLevelViewController *)self];
}

-(void) changeName{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"New Stay Name", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	alert.tag = CELL_CHANGE_NAME;
	[alert show];
	
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0 && alertView.tag == CELL_CHANGE_NAME) {
		NSString *newName = [[alertView textFieldAtIndex:0] text];
		[self.delegate actionViewDidEndEditing:@{@"type": @"CHANGE_NAME",@"name": newName} viewController:(ThirdLevelViewController *)self];
	}
	else{
		[self.delegate actionViewDidEndEditing:nil viewController:(ThirdLevelViewController *)self];

	}
}

#pragma mark Video Capture session

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    if (_captureVideoPreviewLayer) {
        if (toInterfaceOrientation==UIInterfaceOrientationPortrait || toInterfaceOrientation==UIInterfaceOrientationPortraitUpsideDown) {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[[_captureManager session] stopRunning];
			});
			[_takeImageButton removeFromSuperview];
			[_captureVideoPreviewLayer removeFromSuperlayer];
			if (_popover && _popover.isPopoverVisible) [_popover dismissPopoverAnimated:NO];
			[self.delegate actionViewDidEndEditing:nil viewController:(ThirdLevelViewController *)self];
		}
		else{
			_captureVideoPreviewLayer.frame = self.parentViewController.parentViewController.view.bounds;
		//	_takeImageButton.center = CGPointMake(self.parentViewController.view.bounds.size.width - 40, self.parentViewController.view.center.y);
			[_captureVideoPreviewLayer.connection setVideoOrientation:toInterfaceOrientation];
		}
	}
	else{
		if (_popover && _popover.isPopoverVisible) [_popover dismissPopoverAnimated:NO];
		[self.delegate actionViewDidEndEditing:nil viewController:(ThirdLevelViewController *)self];
	}
}

-(void) startAndOverlayCamera{
	//self.view.alpha = 0.0;;
	
	if (_captureManager == nil) {
		_captureManager = [[AVCamCaptureManager alloc] init];
		[self setCaptureManager:_captureManager];
		if ([[self captureManager] setupSession]) {
			_captureManager.delegate = self;
            // Create video preview layer and add it to the UI
			_captureVideoPreviewLayer= [[AVCaptureVideoPreviewLayer alloc] initWithSession:[_captureManager session]];
			InitialViewController * initial = (InitialViewController *) self.parentViewController.parentViewController;
			UIImageView *view = initial.backgroundImageView;
			initial.backgroundImageView.image = nil;
			initial.backgroundImageView.alpha = 1.0;
			initial.backgroundImageView.hidden = NO;
			CALayer *viewLayer = view.layer;
			[viewLayer setMasksToBounds:YES];
			CGRect bounds = [view bounds];
			[_captureVideoPreviewLayer setFrame:bounds];
			[_captureVideoPreviewLayer removeAllAnimations];
		//	[_captureVideoPreviewLayer.connection.supportsVideoOrientation:(AVCaptureVideoOrientationLandscapeLeft | AVCaptureVideoOrientationLandscapeRight)];
			_captureVideoPreviewLayer.connection.videoOrientation = [[UIDevice currentDevice] orientation];
			
			[_captureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];

			[viewLayer insertSublayer:_captureVideoPreviewLayer below:[[viewLayer sublayers] objectAtIndex:0]];
			//[viewLayer insertSublayer:_captureVideoPreviewLayer atIndex:[viewLayer.sublayers count]];

			[self setCaptureVideoPreviewLayer:_captureVideoPreviewLayer];
			
            // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[[[self captureManager] session] startRunning];
			});
			
			//add stop button
			_takeImageButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
			_takeImageButton.backgroundColor = [UIColor orangeColor];
			_takeImageButton.layer.cornerRadius = 20.0;
			_takeImageButton.layer.shadowRadius = 20.0;
			_takeImageButton.layer.shadowOpacity = 1.0;
			_takeImageButton.layer.shouldRasterize = YES;
			[_takeImageButton.layer setShadowPath:[UIBezierPath bezierPathWithRect:CGRectMake(-20, -20, 80, 80)].CGPath];
			_takeImageButton.layer.rasterizationScale = [UIScreen mainScreen].scale;
			_takeImageButton.layer.shadowColor = [[UIColor blackColor] CGColor];
			_takeImageButton.center = CGPointMake(self.parentViewController.parentViewController.view.bounds.size.width - 40, self.parentViewController.parentViewController.view.center.y);
			_takeImageButton.autoresizingMask = (UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin);
			[_takeImageButton addTarget:self action:@selector(captureSingleImage:) forControlEvents:UIControlEventTouchUpInside];
			[self.parentViewController.parentViewController.view addSubview:_takeImageButton];
		}
	}
}

- (void)captureSingleImage:(id)sender
{
    // Capture a still image
    // Flash the screen white and fade it out to give UI feedback that a still image was taken
    UIView *flashView = [[UIView alloc] initWithFrame:self.view.window.frame];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.4f
                     animations:^{
                         [flashView setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
						 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
							 [[_captureManager session] stopRunning];
						 });
						 [_captureManager captureStillImage];

                     }
     ];
}

- (void)captureManagerStillImageCaptured:(UIImage *)image
{
	[self resizeImage:image type:TYPE_CAPTURE_VIDEO];
	[_takeImageButton removeFromSuperview];
	[_captureVideoPreviewLayer removeFromSuperlayer];
}

@end
