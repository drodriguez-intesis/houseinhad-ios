//
//  WebViewViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 20/11/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import "WebViewViewController.h"

@interface WebViewViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (strong,nonatomic) NSString * stringUrl;
@end

@implementation WebViewViewController

-(void) setUrlString:(NSString *) stringUrl{
    if (![stringUrl hasPrefix:@"http"]) {
        _stringUrl = stringUrl;
    }
    else{
        _stringUrl = [NSString stringWithFormat:@"http://%@",stringUrl];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _webView.scalesPageToFit = YES;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:(_stringUrl ? _stringUrl : @"http://houseinhand.com")]];
    [_webView loadRequest:request];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)dismiss:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
