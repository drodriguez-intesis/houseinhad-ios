//
//  Scene.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Scene.h"
#import "Config.h"
#import "SceneTelegram.h"


@implementation Scene

@dynamic isActive;
@dynamic name;
@dynamic order;
@dynamic r_config;
@dynamic r_sceneTelegram;

@end
