//
//  WebViewViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 20/11/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebViewViewController : UIViewController

-(void) setUrlString:(NSString *) stringUrl;
@end
