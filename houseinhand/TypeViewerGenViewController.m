//
//  TypeViewerGenViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/12/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeViewerGenViewController.h"
#import "DeviceTypeGenViewer.h"
@interface TypeViewerGenViewController ()
@property (strong,nonatomic) Telegram *aTelegram;
@property (strong,nonatomic) Telegram *bTelegram;
@property (strong,nonatomic) Telegram *cTelegram;
@property (assign,nonatomic) NSUInteger logic;
@property (strong,nonatomic) NSString *textYes;
@property (strong,nonatomic) NSString *textNo;
@end

@implementation TypeViewerGenViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeGenViewer *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _textNo = newDevice.textNo;
		_textYes = newDevice.textYes;
		_logic = [newDevice.logic integerValue];
        [self readControlStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self updateLabel:NO];
	[self checkStatus];
	if ([super.device.hasStatus boolValue]) {
		[_aTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		[_bTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		[_cTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self updateLabel:ANIMATIONS_ENABLED];
	}
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_aTelegram.address dpt:_aTelegram.dpt type:_aTelegram.type delay:_aTelegram.delay];
		 [super sendData:@0 address:_bTelegram.address dpt:_bTelegram.dpt type:_bTelegram.type delay:_bTelegram.delay];
		 [super sendData:@0 address:_cTelegram.address dpt:_cTelegram.dpt type:_cTelegram.type delay:_cTelegram.delay];
    }
}

-(CGFloat) readControlStatus{
    NSUInteger status=0;
	_aTelegram = [super.device getTelegramWithCode:0];
	_bTelegram = [super.device getTelegramWithCode:1];
	_cTelegram = [super.device getTelegramWithCode:2];

    return status;
}

-(void) updateLabel:(BOOL) animated{
	if (animated) {
		[super animationUpdate:super.centerLabel];
	}
    switch (_logic) {
        case 0:
			//((a&b)|c)
			if (([self checkCondition:_aTelegram] && [self checkCondition:_bTelegram]) || [self checkCondition:_cTelegram]) {
				super.centerLabel.text  = _textYes;
				break;
			}
			super.centerLabel.text  = _textNo;
            break;
        case 1:
			//((a|b)&c)
            if (([self checkCondition:_aTelegram] || [self checkCondition:_bTelegram]) && [self checkCondition:_cTelegram]) {
				super.centerLabel.text  = _textYes;
				break;
			}
			super.centerLabel.text  = _textNo;
            break;
        case 2:
			//((a&b)&c)
            if (([self checkCondition:_aTelegram] && [self checkCondition:_bTelegram]) && [self checkCondition:_cTelegram]) {
				super.centerLabel.text  = _textYes;
				break;
			}
			super.centerLabel.text  = _textNo;
            break;
		case 3:
			//((a|b)|c)
            if (([self checkCondition:_aTelegram] || [self checkCondition:_bTelegram]) || [self checkCondition:_cTelegram]) {
				super.centerLabel.text  = _textYes;
				break;
			}
			super.centerLabel.text  = _textNo;
            break;
        default:
            break;
    }
}

-(BOOL) checkCondition:(Telegram *) tel{
/*	switch ([tel.operator integerValue]) {
		case 0:
			//=
			if (tel.auxValue == tel.value) {
				return YES;
			}
			break;
		case 1:
			//<
			if (tel.auxValue < tel.value) {
				return YES;
			}
			break;
		case 2:
			//+
			if (tel.auxValue > tel.value) {
				return YES;
			}
			break;
		default:
			break;
	}*/
	return NO;
}
- (void)viewDidUnload
{
	_aTelegram = nil;
	_bTelegram = nil;
	_cTelegram = nil;
	_textYes = nil;
	_textNo = nil;
    [super viewDidUnload];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) dealloc{
	if ([super.device.hasStatus boolValue]) {
		[_aTelegram removeObserver:self forKeyPath:@"value"];
		[_bTelegram removeObserver:self forKeyPath:@"value"];
		[_cTelegram removeObserver:self forKeyPath:@"value"];

	}
	
}

@end
