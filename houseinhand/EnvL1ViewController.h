//
//  EnvL1ViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 12/7/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Scene.h"
#import "EnvDevViewController.h"
@class EnvDevViewController;
@class EnvL1ViewController;
@protocol EnvL1ViewControllerDelegate <NSObject>
-(void) didEndEditingScene:(EnvL1ViewController *) level saveChanges:(BOOL) save;
@end

@interface EnvL1ViewController : UIViewController  <UITableViewDataSource,UITableViewDelegate,EnvDevViewControllerDelegate,UIAlertViewDelegate>
@property (weak,nonatomic) id <EnvL1ViewControllerDelegate> delegate;
@property (assign,nonatomic) BOOL isNew;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *) context stays:(NSArray *) stays scene:(Scene *) scene level:(NSUInteger) level;

@end
