//
//  MultimediaThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/26/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MultimediaThirdLevelViewController.h"
#import "DeviceTypeMultimedia+Parser.h"
#import "MultiCommand+Parser.h"
#import <AudioToolbox/AudioToolbox.h>
#import "MultimediaResponseMessage.h"
#import "MultimediaResponseMessage.h"
#define X_SCROLL_OFFSET 20

typedef 	NS_ENUM (NSUInteger,CMD_TAG) {
	CMD_POWER = 10,	CMD_ONE = 11,	CMD_TWO = 12,	CMD_THREE = 13,	CMD_FOUR = 14,	CMD_FIVE = 15,	CMD_SIX = 16,	CMD_SEVEN = 17,	CMD_EIGHT = 18,	CMD_NINE = 19,	CMD_LOPTION = 20,	CMD_ZERO = 21,	CMD_ROPTION = 22,	CMD_RED = 23,	CMD_GREEN = 24,	CMD_YELLOW = 25,	CMD_BLUE = 26,	CMD_CHUP = 27,	CMD_UP = 28,	CMD_VOLUP = 29,	CMD_LEFT = 30,	CMD_OK = 31,	CMD_RIGHT = 32,	CMD_CHDOWN = 33,	CMD_DOWN = 34,	CMD_VOLDOWN = 35,	CMD_REC = 36,	CMD_PAUSE = 37,	CMD_PLAY = 38,	CMD_STOP = 39,	CMD_SKIPL = 40,	CMD_BACKWARD = 41,	CMD_FORWARD = 42,	CMD_SKIPR = 43,	CMD_EJECT = 44,	CMD_AV = 45,	CMD_INFO = 46,	CMD_MUTE = 47};


@interface MultimediaThirdLevelViewController ()
@property (strong,nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong,nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong,nonatomic) IBOutlet UIButton * leftOption;
@property (strong,nonatomic) IBOutlet UIButton * rightOption;
@property (strong,nonatomic) IBOutlet UIButton * powerButton;
@property (strong,nonatomic) const NSDictionary * equivalences;
@property (strong,nonatomic) IBOutletCollection(UIButton) NSArray *buttons;
@property (strong, nonatomic) DeviceTypeMultimedia * device;
@property (strong,nonatomic) TCPSocketCommunication *com;
@property (strong,nonatomic) IBOutlet UIButton * statusButton;
@end

@implementation MultimediaThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeMultimedia*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		[self initEquivalences];
		_device = newDevice;
		[[MultimediaResponseMessage sharedInstance] setDeviceType:_device.type];
    }
    return self;
}

-(void) initEquivalences{
	_equivalences = @{
									@(CMD_POWER) : @"power",
									@(CMD_ONE) : @"one",
									@(CMD_TWO) : @"two",
									@(CMD_THREE) : @"three",
									@(CMD_FOUR) : @"four",
									@(CMD_FIVE) : @"five",
									@(CMD_SIX) : @"six",
									@(CMD_SEVEN) : @"seven",
									@(CMD_EIGHT) : @"eight",
									@(CMD_NINE) : @"nine",
									@(CMD_ZERO) : @"zero",
									@(CMD_LOPTION) : @"leftOption",
									@(CMD_ROPTION) : @"rightOption",
									@(CMD_CHUP) : @"chUp",
									@(CMD_CHDOWN) : @"chDown",
									@(CMD_VOLUP) : @"volUp",
									@(CMD_VOLDOWN) : @"volDown",
									@(CMD_UP) : @"up",
									@(CMD_DOWN) : @"down",
									@(CMD_LEFT) : @"left",
									@(CMD_RIGHT) : @"right",
									@(CMD_OK) : @"ok",
									@(CMD_REC) : @"rec",
									@(CMD_STOP) : @"stop",
			 						@(CMD_PLAY) : @"play",
			 						@(CMD_PAUSE) : @"pause",
			 						@(CMD_SKIPL) : @"skipLeft",
			 						@(CMD_SKIPR) : @"skipRight",
									@(CMD_BACKWARD) : @"backward",
			 						@(CMD_FORWARD) : @"forward",
			 						@(CMD_EJECT) : @"eject",
			 						@(CMD_AV) : @"av",
			 						@(CMD_INFO) : @"info",
		 							@(CMD_MUTE) : @"mute",
		 							@(CMD_RED) : @"red",
		 							@(CMD_GREEN) : @"green",
		 							@(CMD_YELLOW) : @"yellow",
		 							@(CMD_BLUE) : @"blue",
									   };
}

-(void) connectToHost{
	if (!_com) {
		_com = [[TCPSocketCommunication alloc] init];
		_com.delegate = self;
	}
	[_com connectToDevice:_device];
}

- (void)viewDidLoad
{
	
    [super viewDidLoad];
	
	[[NSNotificationCenter defaultCenter] addObserver:_com selector:@selector(disconnect) name:UIApplicationWillResignActiveNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectToHost) name:UIApplicationDidBecomeActiveNotification object:nil];

	[self initStatusButtonState];
	
	[self connectToHost];
	
	
	[self initEquivalences];
	[self configurePages];
	[self configureButtonsText];
	[self addActionsButtons];
	[self checkExistingCommands];

}

-(void) checkExistingCommands{
	[_buttons enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop) {
		if (![self commandExistsAtIndex:obj.tag]) {
			obj.userInteractionEnabled = NO;
			obj.alpha = .3;
		}
	}];
}

-(BOOL) commandExistsAtIndex:(NSUInteger) tag{
	MultiCommand *cmd = [_device getCommandWithTag:_equivalences[@(tag)]];
	if (cmd) return YES;
	return NO;
}
-(void) initStatusButtonState{
	_statusButton.layer.cornerRadius = _statusButton.bounds.size.width/2;
	_statusButton.layer.borderColor = [[UIColor whiteColor] CGColor];
	_statusButton.layer.borderWidth = 1.0;
	_statusButton.backgroundColor = [UIColor redColor];
}

-(void) setStatusButtonStateAdapter:(NSNumber * )stateNumber{
	TcpSocketStatus state = stateNumber.unsignedIntegerValue;
	[self setStatusButtonState:state];
}
-(void) setStatusButtonState:(TcpSocketStatus) state{
	switch (state) {
		case STATUS_DISCONNECTED:
			_statusButton.backgroundColor = [UIColor redColor];
			break;
		case STATUS_CONNECTED:
			_statusButton.backgroundColor = [UIColor greenColor];
			break;
		case STATUS_CONNECTING:
			_statusButton.backgroundColor = [UIColor yellowColor];
			break;
		default:
			break;
	}
}

-(void) configurePages{
	_pageControl.numberOfPages = (_device.remoteType.unsignedIntegerValue == REMOTE_TYPE_FULL ? 2 : 1);
	_scrollView.scrollEnabled = (DEVICE_IS_IPAD ? NO : (_device.remoteType.unsignedIntegerValue == REMOTE_TYPE_FULL ? YES : NO));
	_pageControl.hidden = (_scrollView.scrollEnabled ? NO : YES);
	self.view.frame = CGRectMake(0, 0, (DEVICE_IS_IPAD && _pageControl.numberOfPages == 2 ? 580 : 300), self.view.frame.size.height);
	[_scrollView setFrame:CGRectMake(_scrollView.frame.origin.x, _scrollView.frame.origin.y, self.view.frame.size.width - X_SCROLL_OFFSET, _scrollView.frame.size.height)];
	[_scrollView setContentSize:CGSizeMake(_pageControl.numberOfPages*_scrollView.frame.size.width, _scrollView.contentSize.height)];
	[_scrollView setContentOffset:CGPointMake((_pageControl.numberOfPages == 1 && _device.remoteType.unsignedIntegerValue == REMOTE_TYPE_MULTI ? _scrollView.contentSize.width: 0), 0)];
	[_powerButton setCenter:CGPointMake(self.view.center.x, _powerButton.center.y)];
}

-(void) addActionsButtons{
	[_buttons enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop) {
		[obj addTarget:self action:@selector(didPressedButton:) forControlEvents:UIControlEventTouchDown];
	}];
}

-(void) configureButtonsText{
	MultiCommand *lCommand = [_device getCommandWithTag:_equivalences[@(CMD_LOPTION)]];
	if (lCommand) [_leftOption setTitle:lCommand.text forState:UIControlStateNormal];
	MultiCommand *rCommand = [_device getCommandWithTag:_equivalences[@(CMD_ROPTION)]];
	if (rCommand) [_rightOption setTitle:rCommand.text forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)pageControlTaped:(UIPageControl *)sender{
	[_scrollView setContentOffset:CGPointMake(sender.currentPage * _scrollView.bounds.size.width, 0) animated:YES];
}

-(void) scrollViewDidScroll:(UIScrollView *)scrollView{
	[_pageControl setCurrentPage:MIN((NSUInteger)scrollView.contentOffset.x/_scrollView.bounds.size.width, _pageControl.numberOfPages-1) ];
}

-(IBAction)didPressedButton:(UIButton *) button{
	MultiCommand *cmd = [_device getCommandWithTag:_equivalences[@(button.tag)]];
	if (cmd) {
		if ([[MultimediaResponseMessage sharedInstance] shouldWaitResponse]) [self setStatusButtonState:STATUS_CONNECTING];
		AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
		if ([_device.type isEqualToString:GLOBAL_CACHE]) {
			[_com sendInfo:[NSString stringWithFormat:@"%@\r",cmd.command]];
		}
		else{
			[_com sendInfo:cmd.command];
		}
	}
}

-(IBAction)dismissView{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[_com disconnect];
	[super dismissView];
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
}

#pragma mark Delegate TCPSocket

-(void) tcpSocketCommunication:(TCPSocketCommunication *)tcpSocket didChangeConnectionStatus:(TcpSocketStatus)status{
	[self performSelectorOnMainThread:@selector(setStatusButtonStateAdapter:) withObject:@(status) waitUntilDone:NO];
}

-(void) tcpSocketCommunication:(TCPSocketCommunication *)tcpSocket didReadStringResponse:(NSString *)response{
	
	if ([[MultimediaResponseMessage sharedInstance] shouldWaitResponse]){
		if ([[MultimediaResponseMessage sharedInstance] didExecuteMessage:response]) [self performSelectorOnMainThread:@selector(setStatusButtonStateAdapter:) withObject:@(STATUS_CONNECTED) waitUntilDone:NO];
		else if ([[MultimediaResponseMessage sharedInstance] isKnownError:response]){
			[self performSelectorOnMainThread:@selector(showAlertWithMessage:) withObject:response waitUntilDone:NO];
			
		}
		else{
			[self performSelectorOnMainThread:@selector(setStatusButtonStateAdapter:) withObject:@(STATUS_DISCONNECTED) waitUntilDone:NO];
		}
	}
}

-(void) showAlertWithMessage:(NSString *) response{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") message:response delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Dismiss", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles: nil];
	alert.delegate = self;
	[alert show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	[self performSelectorOnMainThread:@selector(setStatusButtonStateAdapter:) withObject:@(STATUS_CONNECTED) waitUntilDone:NO];
}

@end
