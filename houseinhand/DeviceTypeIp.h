//
//  DeviceTypeIp.h
//  houseinhand
//
//  Created by Isaac Lozano on 20/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeIp : Device

@property (nonatomic, retain) NSString * highParam;
@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSNumber * isSecure;
@property (nonatomic, retain) NSString * localIp;
@property (nonatomic, retain) NSString * lowParam;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * pulseWidth;
@property (nonatomic, retain) NSNumber * remotePort;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSNumber * valueOff;
@property (nonatomic, retain) NSNumber * valueOn;
@property (nonatomic, retain) NSString * doorPath;

@end
