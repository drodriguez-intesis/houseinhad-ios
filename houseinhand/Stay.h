//
//  Stay.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BackgroundStayImage, Config, Device, Stay;

@interface Stay : NSManagedObject

@property (nonatomic, retain) NSString * bckgColor;
@property (nonatomic, retain) NSNumber * isSecure;
@property (nonatomic, retain) NSNumber * level;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) NSString * passcode;
@property (nonatomic, retain) NSData * thumbImage;
@property (nonatomic, retain) NSNumber * useCustomImage;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) Config *r_config;
@property (nonatomic, retain) NSSet *r_device;
@property (nonatomic, retain) NSSet *r_nextStay;
@property (nonatomic, retain) Stay *r_prevStay;
@property (nonatomic, retain) BackgroundStayImage *r_backgroundImage;
@end

@interface Stay (CoreDataGeneratedAccessors)

- (void)addR_deviceObject:(Device *)value;
- (void)removeR_deviceObject:(Device *)value;
- (void)addR_device:(NSSet *)values;
- (void)removeR_device:(NSSet *)values;

- (void)addR_nextStayObject:(Stay *)value;
- (void)removeR_nextStayObject:(Stay *)value;
- (void)addR_nextStay:(NSSet *)values;
- (void)removeR_nextStay:(NSSet *)values;

@end
