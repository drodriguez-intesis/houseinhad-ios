//
//  NSMutableArray+MoveMutableArray.h
//  IntesisHome
//
//  Created by Isaac Lozano on 1/31/13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableArray (MoveMutableArray)
- (void)moveObjectFromIndex:(NSUInteger)from toIndex:(NSUInteger)to;
@end
