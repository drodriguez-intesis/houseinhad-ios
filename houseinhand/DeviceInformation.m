//
//  DeviceInformation.m
//  houseinhand
//
//  Created by Isaac Lozano on 10/16/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceInformation.h"
#import "KeychainItemWrapper.h"
#import "KNXCommunication.h"
#include <sys/types.h>
#include <sys/sysctl.h>

@interface DeviceInformation ()
@property (strong,nonatomic) NSString * devResolution;
@property (strong,nonatomic) NSString * isGod;
@property (strong,nonatomic) NSString * appVersion;
@property (strong,nonatomic) NSString * deviceCode;
@property (strong,nonatomic) NSString * deviceAdCode;
@property (strong,nonatomic) NSString * deviceModel;

@end

@implementation DeviceInformation


+ (id)sharedInstance {
    static DeviceInformation *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
		[sharedInstance createBasicInfo];
    });
    return sharedInstance;
}

-(void) createBasicInfo{
	float scaleFactor = [[UIScreen mainScreen] scale];
	CGRect screen = [[UIScreen mainScreen] bounds];
	NSUInteger widthInPixel = screen.size.width * scaleFactor;
	NSUInteger heightInPixel = screen.size.height * scaleFactor;
	_devResolution = [NSString stringWithFormat:@"%dx%d",widthInPixel,heightInPixel];
	_isGod = [NSString stringWithFormat:@"%d",[self isJailbroken]];
	_appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
	NSString *platform = [NSString stringWithString:[self platform]];
	_deviceModel = (platform ? platform : [[UIDevice currentDevice] model]);
}
-(NSDictionary *) deviceInformationForCheckDevice{
	return [self getCommonInfo];
}


-(NSDictionary *) deviceInformationForAddDevice{
	return [self deviceInformationForAddDeviceMacAddress:@"0"];
}

-(NSDictionary *) deviceInformationForAddDeviceMacAddress:(NSString *)macAddress{
	NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:[self getCommonInfo]];
	
	[dict setObject:@"Apple" forKey:@"deviceBrand"];
	[dict setObject:_devResolution forKey:@"deviceResolution"];
	[dict setObject:(DEVICE_IS_IPAD ? @"Tablet" : @"Phone") forKey:@"deviceInterface"];
	[dict setObject:[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_CONFIG_USER] forKey:@"configUser"];
	[dict setObject:macAddress.uppercaseString forKey:@"instCode"];

	return dict;
}

-(NSDictionary *) getCommonInfo{
	_deviceCode =  [NSString stringWithFormat:@"%@",[[UIDevice currentDevice] identifierForVendor].UUIDString];
	KeychainItemWrapper *keychain = [[KeychainItemWrapper alloc] initWithIdentifier:@"HIH_CODES" accessGroup:nil];
	_deviceAdCode = [keychain objectForKey:(__bridge id)kSecAttrAccount];
	if	(!_deviceAdCode || [_deviceAdCode isEqualToString:@""]){
		_deviceAdCode = _deviceCode;
		[keychain setObject:_deviceCode forKey:(__bridge id)kSecAttrAccount];
	}
	NSString *macAddress = [[KNXCommunication sharedInstance] getMacAddress];
	if (!macAddress) macAddress = @"1";
	
	return @{
			@"isGod" : _isGod,
			@"appVersion" : _appVersion,
			@"osType" : @"ios",
			@"osVersion" :  [[UIDevice currentDevice] systemVersion] ,
			@"instCode" : macAddress.uppercaseString,
			@"deviceCode" : _deviceCode ,
			@"deviceAdCode" : _deviceAdCode,
			@"deviceName" : [[UIDevice currentDevice] name],
			@"deviceModel" : _deviceModel
			 };
}

- (NSString *) platform {
    size_t size;
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    char *machine = malloc(size);
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    NSString *platform = [NSString stringWithUTF8String:machine];
    free(machine);
    return platform;
}

- (BOOL) isJailbroken {
	
#if TARGET_IPHONE_SIMULATOR
	return NO;
	
#else
	BOOL isJailbroken = NO;
	
	BOOL cydiaInstalled = [[NSFileManager defaultManager] fileExistsAtPath:@"/Applications/Cydia.app"];
	
	FILE *f = fopen("/bin/bash", "r");
	
	if (!(errno == ENOENT) && cydiaInstalled) {
		
		//Device is jailbroken
		isJailbroken = YES;
	}
	fclose(f);
	return isJailbroken;
#endif
}

@end
