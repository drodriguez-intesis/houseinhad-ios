//
//  NSManagedObjectContext+Fetch.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface NSManagedObjectContext (Fetch)
-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicate:(NSString *) predicate sorted:(NSString *) sort;
-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicatePredicate:(NSPredicate *) predicate sorted:(NSString *) sort;
-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicate:(NSString *) predicate sorted:(NSString *) sort prefetchedRelationship:(NSArray *) prefetch;
-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicatePredicate:(NSPredicate *) predicate sorted:(NSString *) sort resultsType:(NSFetchRequestResultType) resultType;
-(NSArray *) fetchTelegramsWithAddress:(NSNumber *) address;
@end
