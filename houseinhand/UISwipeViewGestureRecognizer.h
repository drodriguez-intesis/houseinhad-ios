//
//  UISwipeViewGestureRecognizer.h
//  
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIKit/UIGestureRecognizerSubclass.h>
@class UISwipeViewGestureRecognizer;


@protocol UISwipeViewGestureRecognizerDelegate <NSObject>

- (void)viewDidChanged:(UISwipeViewGestureRecognizer *)controller position:(NSUInteger) position;
@end


@interface UISwipeViewGestureRecognizer : UIPanGestureRecognizer 
@property (nonatomic, weak) id <UISwipeViewGestureRecognizerDelegate> delegate;
@property (strong,nonatomic)  NSArray *viewsArray;
@property (assign) BOOL swipeToRight;
@end
