//
//  DeviceTypeLight+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeLight+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1			@"image1"
#define KEY_IMAGE_2			@"image2"
#define KEY_IMAGE_3			@"image3"
#define KEY_SCENE_VAL		@"sendValue"

@implementation DeviceTypeLight (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeLight *dev = (DeviceTypeLight *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.image3 = (info[KEY_IMAGE_3] ? info[KEY_IMAGE_3]: dev.image3);
	dev.sendSceneValue = (info[KEY_SCENE_VAL] ? @([info[KEY_SCENE_VAL] integerValue]): dev.sendSceneValue);
	return dev;
}

@end
