//
//  PasswordViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "PasswordViewController.h"
@interface PasswordViewController ()
@property (strong,nonatomic) NSString * passcode;
@property (assign,nonatomic) NSUInteger actionType;
@property (assign,nonatomic) BOOL showAlert;
@property (assign,nonatomic) BOOL fromAddPassword;
@end

@implementation PasswordViewController

-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil passcode:(NSString *) passcode type:(PasswordType) type;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_passcode = passcode;
		_actionType = type;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}


-(void) showPasscodeView{
	//[self showPasswordAlert];
}
-(void) showPasswordAlert{
	NSString *title = NSLocalizedStringFromTableInBundle(@"Passcode Required", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView");
	if (_actionType == PASSWORD_TYPE_ADD) title = (_fromAddPassword ? NSLocalizedStringFromTableInBundle(@"Confirm passcode", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") : NSLocalizedStringFromTableInBundle(@"Enter passcode", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView"));
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[[alert textFieldAtIndex:0] setSecureTextEntry:YES];
	[alert show];
}

-(void) didMoveToParentViewController:(UIViewController *)parent{
	if (parent) {
		if (PASSWORD_TYPE_CHECK || PASSWORD_TYPE_REMOVE) {
			[self performSelector:@selector(showPasswordAlert) withObject:nil afterDelay:.0];
		}

	}
}
-(void) alertView:(UIAlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0) {
		if (_actionType == PASSWORD_TYPE_CHECK || _actionType == PASSWORD_TYPE_REMOVE) {
			[_delegate passwordVCdidResolveCode:self withType:_actionType];
		}
		else if (_actionType == PASSWORD_TYPE_ADD){
			if (!_fromAddPassword) {
				_fromAddPassword = YES;
				_passcode = [NSString stringWithString:[[alertView textFieldAtIndex:0] text]];
				[self showPasswordAlert];
			}
			else{
				_fromAddPassword = NO;
				[_delegate passwordVCdidResolveCode:self withType:_actionType];
			}
		}
	}
	else{
		[_delegate passwordVCdidCancelCode:self withType:_actionType];
	}
}
/*
-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0) {
		if (_actionType == PASSWORD_TYPE_CHECK || _actionType == PASSWORD_TYPE_REMOVE) {
			[_delegate passwordVCdidResolveCode:self withType:_actionType];
		}
		else if (_actionType == PASSWORD_TYPE_ADD){
			if (!_fromAddPassword) {
				_fromAddPassword = YES;
				_passcode = [NSString stringWithString:[[alertView textFieldAtIndex:0] text]];
				[self showPasswordAlert];
			}
			else{
				_fromAddPassword = NO;
				[_delegate passwordVCdidResolveCode:self withType:_actionType];
			}
		}
	}
	else{
		[_delegate passwordVCdidCancelCode:self withType:_actionType];
	}
}*/

- (BOOL)alertViewShouldEnableFirstOtherButton:(UIAlertView *)alertView
{
	UITextField *textField = [alertView textFieldAtIndex:0];

	if (_actionType == PASSWORD_TYPE_ADD && !_fromAddPassword && textField.text.length >0) return YES;
	if ([textField.text isEqualToString:_passcode])
	{
		return YES;
	}
	return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString *) getPasscode{
	if (_actionType == PASSWORD_TYPE_ADD) return _passcode;
	return nil;
}

@end
