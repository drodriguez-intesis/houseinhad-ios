//
//  IPCameraThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//


#define ANIMATION_FULL_SCREEN			.4
#import "IPCameraThirdLevelViewController.h"
#import "KNXCommunication.h"
@interface IPCameraThirdLevelViewController ()
@property (strong,nonatomic) IBOutlet MjpegImageView *cameraView;
@property (strong,nonatomic) UIView *overlayFullScreenView;
@property (strong,nonatomic) NSString *username;
@property (strong,nonatomic) NSString *password;
@property (assign,nonatomic) CGPoint originalCenter;
@end

@implementation IPCameraThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeIp*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_device = newDevice;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_cameraView.delegate = self;
	_cameraView.allowSelfSignedCertificates = YES;
	_originalCenter = _cameraView.center;
	_cameraView.backgroundColor = self.view.backgroundColor;
	[self loadStreamCameraPortrait:YES];
}

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[[NSNotificationCenter defaultCenter] addObserver:self  selector:@selector(orientationChanged:)  name:UIDeviceOrientationDidChangeNotification  object:nil];

}

-(void) viewWillDisappear:(BOOL)animated{
	[[NSNotificationCenter defaultCenter] removeObserver:self];
	[super viewWillDisappear:animated];
}

-(void) orientationChanged:(NSNotification *) not{
	if (DEVICE_IS_IPAD) return;
	UIDeviceOrientation deviceOrientation = [[UIDevice currentDevice] orientation];
	[_cameraView pause];
	if (UIDeviceOrientationIsPortrait(deviceOrientation)) {
		//portrait
		[self transformCameraToPortrait];
	}
	else{
		[self transformCameraToLandscape:deviceOrientation];
	}

}

-(IBAction)dismissView{
     [_cameraView stop];
    _cameraView = nil;
    [super dismissView];
}

-(void) transformCameraToPortrait{
	
	[self loadStreamCameraPortrait:YES];
	_cameraView.backgroundColor = self.view.backgroundColor;
	[_cameraView setBounds:CGRectMake(_cameraView.bounds.origin.x, _cameraView.bounds.origin.y, 288, 216)];
	[_overlayFullScreenView removeFromSuperview];
	[self.view addSubview:_cameraView];
	[UIView animateWithDuration:ANIMATION_FULL_SCREEN animations:^{
		_cameraView.transform = CGAffineTransformIdentity;
		[_cameraView setCenter:_originalCenter];
	} completion:^(BOOL finished) {
		[[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationSlide];
	}];
}

-(void) transformCameraToLandscape:(UIDeviceOrientation) devOrientation{
	[self loadStreamCameraPortrait:NO];
	[self initOverlayView];
	[self.view.window addSubview:_overlayFullScreenView];
	[self.view.window addSubview:_cameraView];
	_cameraView.backgroundColor = [UIColor blackColor];
	[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
	CGAffineTransform rotation = CGAffineTransformMakeRotation((devOrientation == UIDeviceOrientationLandscapeLeft ? M_PI_2 : -M_PI_2));
	[_cameraView setBounds:CGRectMake(_cameraView.bounds.origin.x, _cameraView.bounds.origin.y, 400, 225)];
	CGFloat scaleX = self.view.window.bounds.size.width/(_cameraView.bounds.size.height);
	CGFloat scaleY = self.view.window.bounds.size.width/(_cameraView.bounds.size.height);
	CGAffineTransform scale = CGAffineTransformMakeScale(scaleX,scaleY);
	CGAffineTransform transf = CGAffineTransformConcat(rotation, scale);
	[UIView animateWithDuration:ANIMATION_FULL_SCREEN animations:^{
		_cameraView.transform = CGAffineTransformConcat(transf, CGAffineTransformMakeTranslation(0,0));
		[_cameraView setCenter:CGPointMake(self.view.window.center.x, self.view.window.center.y)];
		_overlayFullScreenView.alpha = 1.0;
	}];
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
}

-(void) initOverlayView{
	if (!_overlayFullScreenView) {
		_overlayFullScreenView = [[UIView alloc] initWithFrame:CGRectMake(self.view.window.frame.origin.x, self.view.window.frame.origin.y, self.view.window.frame.size.width, self.view.window.frame.size.height)];
		_overlayFullScreenView.backgroundColor = [UIColor blackColor];
	}
	_overlayFullScreenView.alpha = 0.0;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(void) loadStreamCameraPortrait:(BOOL) portrait{
	NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@/%@",(_device.isSecure.boolValue ? @"https" : @"http"),_device.localIp,_device.lowParam]];
	if (!portrait) url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@/%@",(_device.isSecure.boolValue ? @"https" : @"http"),_device.localIp,_device.highParam]];
	
	if (![[KNXCommunication sharedInstance] connectionIsLocal]) {
		NSString *ip = [[KNXCommunication sharedInstance] getRemoteIp];
		url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@:%@/%@",(_device.isSecure.boolValue ? @"http" : @"http"),ip,_device.remotePort,_device.lowParam]];
		if (!portrait) url = [NSURL URLWithString:[NSString stringWithFormat:@"%@://%@:%@/%@",(_device.isSecure.boolValue ? @"http" : @"http"),ip,_device.remotePort,_device.highParam]];
	}
	_cameraView.url = url;
	[_cameraView play];
}

-(void) mjpegImageView:(MjpegImageView *)mjpegImageView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge{
	if	([challenge previousFailureCount] > 0 || !_device.username || !_device.password){
		[[challenge sender] cancelAuthenticationChallenge:challenge];
		if (mjpegImageView) [mjpegImageView cleanupConnection];
	}
	else{
		[[challenge sender] useCredential:[NSURLCredential credentialWithUser:_device.username password:_device.password persistence:NSURLCredentialPersistenceForSession] forAuthenticationChallenge:challenge];
	}
}
@end
