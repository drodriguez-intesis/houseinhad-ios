//
//  DeviceType3Buttons+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceType3Buttons+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1								@"image1"
#define KEY_IMAGE_2								@"image2"
#define KEY_IMAGE_3								@"image3"
#define KEY_BUTTONS_NUM						@"buttonsNumber"
#define KEY_FIRST_VALUE							@"firstValue"
#define KEY_SECOND_VALUE					@"secondValue"
#define KEY_THIRD_VALUE						@"thirdValue"

@implementation DeviceType3Buttons (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceType3Buttons *dev = (DeviceType3Buttons *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
    dev.image3 = (info[KEY_IMAGE_3] ? info[KEY_IMAGE_3]: dev.image3);

	dev.numButtons = (info[KEY_BUTTONS_NUM] ? @([info[KEY_BUTTONS_NUM] integerValue]): dev.numButtons);
    
    dev.firstValue = (info[KEY_FIRST_VALUE] ? @([info[KEY_FIRST_VALUE] integerValue]): dev.firstValue);
	dev.secondValue = (info[KEY_SECOND_VALUE] ? @([info[KEY_SECOND_VALUE] integerValue]): dev.secondValue);
	dev.thirdValue = (info[KEY_THIRD_VALUE] ? @([info[KEY_THIRD_VALUE] integerValue]): dev.thirdValue);
	return dev;
}
@end
