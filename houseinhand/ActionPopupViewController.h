//
//  ActionPopupViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 11/30/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "ActionScrollViewController.h"

@interface ActionPopupViewController : ThirdLevelViewController <UITableViewDataSource,UITableViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIAlertViewDelegate,UIPopoverControllerDelegate>
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id <ActionScrollViewControllerDelegate> delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title items:(NSArray *) items;
@end
