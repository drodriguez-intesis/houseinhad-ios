//
//  DeviceTypeSlider.h
//  houseinhand
//
//  Created by Isaac Lozano on 20/01/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeSlider : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSNumber * maxValue;
@property (nonatomic, retain) NSNumber * minValue;
@property (nonatomic, retain) NSNumber * step;
@property (nonatomic, retain) NSNumber * hasStop;

@end
