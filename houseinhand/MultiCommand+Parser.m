//
//  MultiCommand+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MultiCommand+Parser.h"

#define KEY_COMMAND		@"command"
#define KEY_TAG					@"tag"
#define KEY_DELAY				@"delay"
#define KEY_TEXT					@"txt"

@implementation MultiCommand (Parser)


+(MultiCommand *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc{
	MultiCommand *command = (MultiCommand *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	//FIXME tag must be a string
	command.tag = (info[KEY_TAG] ? info[KEY_TAG] : command.tag);
	//command.tag = (info[KEY_TAG] ? [NSString stringWithFormat:@"%@",info[KEY_TAG]] : command.tag);
	command.command = (info[KEY_COMMAND] ? info[KEY_COMMAND] : command.command);
	command.delay = (info[KEY_DELAY] ? @([info[KEY_DELAY] floatValue]): command.delay);
	command.text = (info[KEY_TEXT] ? info[KEY_TEXT] : command.text);

	return command;
}


@end
