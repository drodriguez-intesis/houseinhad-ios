//
//  Network+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Network+Parser.h"

#define KEY_NAT				@"nat"
#define KEY_IP					@"ip"
#define KEY_PORT			@"port"

@implementation Network (Parser)
+(Network *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc{
	Network *network = (Network *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	network.nat = (info[KEY_NAT] ? @([info[KEY_NAT] integerValue]): network.nat);
	network.ip = (info[KEY_IP] ? info[KEY_IP]: network.ip);
	network.port = (info[KEY_PORT] ? @([info[KEY_PORT] integerValue]): network.port);
	return network;
}
@end
