//
//  BackgroundStayImage.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Stay;

@interface BackgroundStayImage : NSManagedObject

@property (nonatomic, retain) NSData * backgroundImage;
@property (nonatomic, retain) Stay *r_stay;

@end
