//
//  NewActionController.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "NewActionController.h"
#import "ActionScrollViewController.h"
#import "OverlayCameraViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "AVCamCaptureManager.h"
#import "AppDelegate.h"
#import "StayViewController.h"
#import "InitialViewController.h"
#import "UIImage+Resize.h"

#define TYPE_CAPTURE_PHOTO			0
#define TYPE_CAPTURE_VIDEO				1

@interface NewActionController () <AVCamCaptureManagerDelegate>
@property (assign,nonatomic) ActionScrollOptions actionOptions;
@property (strong,nonatomic) UIImagePickerController *imagePicker;
@property (nonatomic,retain) AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;
@property (strong,nonatomic) const NSString * options;
@property (strong,nonatomic) UIButton *takeImageButton;
@property (nonatomic,retain) AVCamCaptureManager *captureManager;
@property (strong,nonatomic) UIPopoverController *popover;
@property (strong,nonatomic) UIActionSheet *actionSheet;
@end

@interface UIImagePickerController(Nonrotating)
- (NSUInteger)supportedInterfaceOrientations;
@end

@implementation UIImagePickerController(Nonrotating)

- (NSUInteger)supportedInterfaceOrientations{
	if (DEVICE_IS_IPAD) {
		return UIInterfaceOrientationMaskLandscape;
	}
	return UIInterfaceOrientationMaskPortrait;
}

@end

@interface UIPopoverController(Nonrotating)
- (NSUInteger)supportedInterfaceOrientations;
@end

@implementation NewActionController


-(instancetype) initActionSheetWithOptions:(ActionScrollOptions) options originViewController:(id <UIActionSheetDelegate,NewActionControllerDelegate>) originViewController withFrame:(CGRect) frame setDelegate:(id <NewActionControllerDelegate,UIActionSheetDelegate>) delegate
{
	self = [super init];
    if (self) {
		_actionOptions = options;
		_actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:delegate cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
		self.delegate = delegate;
		[_actionSheet setActionSheetStyle:UIActionSheetStyleBlackTranslucent];
		[self addOptionsToSheet:_actionSheet];
		if (originViewController && _actionSheet && _delegate) {
			UIViewController *vc = (UIViewController *) originViewController;
			[_actionSheet showFromRect:frame inView:vc.view animated:YES];
		}
    }
    return self;
}

-(void) addOptionsToSheet:(UIActionSheet *)actionSheet{
	const NSArray *titles = @[NSLocalizedStringFromTableInBundle(@"Rename Stay", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Take Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Select Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Remove Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Add Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Remove Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")];
	
	switch (_actionOptions) {
		case ACTION_HAS_NOTHING:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[1]];
			[actionSheet addButtonWithTitle:titles[2]];
			[actionSheet addButtonWithTitle:titles[4]];
			break;

		case ACTION_HAS_IMAGE_PSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[1]];
			[actionSheet addButtonWithTitle:titles[2]];
			[actionSheet addButtonWithTitle:titles[3]];
			[actionSheet addButtonWithTitle:titles[5]];
			break;
		case ACTION_HAS_IMAGE:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[1]];
			[actionSheet addButtonWithTitle:titles[2]];
			[actionSheet addButtonWithTitle:titles[3]];
			[actionSheet addButtonWithTitle:titles[4]];
			break;
		case ACTION_HAS_PSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[1]];
			[actionSheet addButtonWithTitle:titles[2]];
			[actionSheet addButtonWithTitle:titles[5]];
			break;
		case ACTION_DISABLE_TAKEIM:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[3]];
			[actionSheet addButtonWithTitle:titles[4]];
			break;
		case ACTION_DISABLE_TAKEIM_PSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[3]];
			[actionSheet addButtonWithTitle:titles[5]];
			//
			break;
		case ACTION_DISABLE_ALLIMAGE:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[4]];
			//
			break;
		case ACTION_DISABLE_ALLIMAGE_PSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[5]];
			break;
		case ACTION_DISABLE_ALLPSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[1]];
			[actionSheet addButtonWithTitle:titles[2]];
			break;
		case ACTION_DISABLE_ALLIMAGE__NOPSW:
			[actionSheet addButtonWithTitle:titles[0]];
			break;
		case ACTION_HAS_IMAGE_NOPSW:
			[actionSheet addButtonWithTitle:titles[0]];
			[actionSheet addButtonWithTitle:titles[3]];
			break;
			
		default:
			break;
	}
	if (!DEVICE_IS_IPAD){
		[actionSheet addButtonWithTitle:@"Cancel"];
		[actionSheet setDestructiveButtonIndex:actionSheet.numberOfButtons-1];
	}
}



-(void) showModalControllerFromOption:(NSInteger) option actionSheet:(UIActionSheet *) actionSheet inViewController:(UIViewController *) viewController withButtonFrame:(CGRect) frame stayName:(NSString *) prevName{
	UIViewController * cont = viewController;
	if	(!DEVICE_IS_IPAD && actionSheet.numberOfButtons -1 == option) return;
	if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Rename Stay", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self changeName:prevName];
	}
	else if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Take Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self createImagePicker];
		if (DEVICE_IS_IPAD) {
			[viewController presentViewController:_imagePicker animated:YES completion:nil];
			//[self startAndOverlayCameraWithController:viewController];
		}else{
			[viewController presentViewController:_imagePicker animated:YES completion:nil];
		}
	}
	else if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Select Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self createLibraryPickerFromRect:frame inViewController:cont];
	}
	else if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Remove Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self deleteImage];

	}
	else if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Add Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self.delegate actionController:self didEndEditing:@{@"type": @"ADD_PASSWORD"}];

	}
	else if ([[actionSheet buttonTitleAtIndex:option] isEqualToString:NSLocalizedStringFromTableInBundle(@"Remove Password", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]) {
		[self.delegate actionController:self didEndEditing:@{@"type": @"REMOVE_PASSWORD"} ];
	}
		
}

-(void) dismissActionSheet{
	if (_actionSheet) {
		[_actionSheet dismissWithClickedButtonIndex:-1 animated:YES];
		_actionSheet = nil;
	}
}

#pragma mark ImagePicker

-(void) createImagePicker{
	if (_imagePicker == nil && [UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
		_imagePicker =[[UIImagePickerController alloc] init];
		_imagePicker.delegate = self;
		_imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
		_imagePicker.showsCameraControls = YES;
        _imagePicker.allowsEditing = (DEVICE_IS_IPAD ? NO : YES);
	}
}

-(void) createLibraryPickerFromRect:(CGRect) rect inViewController:(UIViewController *) viewController{
	UIImagePickerController *picker =[[UIImagePickerController alloc] init];
	picker.navigationBar.tintColor = APP_COLOR_ORANGE;
	picker.delegate = self;
	if (DEVICE_IS_IPAD) picker.navigationController.navigationItem.rightBarButtonItem = nil;
	picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.allowsEditing = (DEVICE_IS_IPAD ? NO : YES);
	if (DEVICE_IS_IPAD) {
		if ( _popover && _popover.isPopoverVisible) {
			[_popover dismissPopoverAnimated:YES];
			[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications	];
		}
		else{
			_popover= [[UIPopoverController alloc]
					   initWithContentViewController: picker];
			_popover.delegate = self;
			[[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
			[_popover presentPopoverFromRect:rect	inView:viewController.view permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
		}
	}
	else{
		[viewController presentViewController:picker animated:YES completion:nil];
	}
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications	];
	UIImage *resizedImage = [info valueForKey:UIImagePickerControllerEditedImage];
	if (resizedImage == nil) {
		resizedImage = [info valueForKey:UIImagePickerControllerOriginalImage];
	}
	[self resizeImage:resizedImage type:TYPE_CAPTURE_PHOTO];
	if (DEVICE_IS_IPAD && picker.sourceType != UIImagePickerControllerSourceTypeCamera) {
		[_popover dismissPopoverAnimated:YES];
	}else{
		[picker dismissViewControllerAnimated:YES completion:nil];
	}
}

-(void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController{
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications	];
}
-(void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
	[[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications	];
	if (DEVICE_IS_IPAD && picker.sourceType != UIImagePickerControllerSourceTypeCamera) {
		[_popover dismissPopoverAnimated:YES];
	}else{
		[picker dismissViewControllerAnimated:YES completion:nil];
	}
}

-(void) resizeImage:(UIImage *) im type:(NSUInteger) type{
	
	UIImage *bckImage;
	
	
	UIImage *thumbTmp = [im resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(IMAGE_CROP_SIZE_W, IMAGE_CROP_SIZE_W) interpolationQuality:kCGInterpolationDefault];
	UIImage *thumb = [thumbTmp croppedImage:CGRectMake(thumbTmp.size.width/2 - IMAGE_CROP_SIZE_W/2, thumbTmp.size.height/2 - IMAGE_CROP_SIZE_H/2, IMAGE_CROP_SIZE_W, IMAGE_CROP_SIZE_H)];
	
	if ([[[NSUserDefaults standardUserDefaults] valueForKey:USER_DEFAULTS_SAVE_IMAGES] boolValue]) 		UIImageWriteToSavedPhotosAlbum(im, nil, nil, nil);
	
	
	if (DEVICE_IS_IPAD) {
		AppDelegate *app = [[UIApplication sharedApplication] delegate];
		CGFloat realHeight = (app.window.rootViewController.view.frame.size.width - 40) * [[UIScreen mainScreen] scale];
		CGFloat realWidth = (app.window.rootViewController.view.frame.size.height) * [[UIScreen mainScreen] scale];
		
		UIImage *bckTmp = [im resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:CGSizeMake(realWidth, realWidth) interpolationQuality:kCGInterpolationDefault];
		bckImage= [bckTmp croppedImage:CGRectMake(bckTmp.size.width/2 - realWidth/2, bckTmp.size.height/2 - realHeight/2, realWidth, realHeight)];
		[self.delegate actionController:self didEndEditing:@{@"type": @"CHANGE_IMAGE",@"thumbImage": thumb,@"backgroundImage":bckImage}];
	}
	else{
		[self.delegate actionController:self didEndEditing:@{@"type": @"CHANGE_IMAGE",@"thumbImage": thumb}];
	}
}

-(void) deleteImage{
	[self.delegate actionController:self didEndEditing:@{@"type": @"DELETE_IMAGE"} ];
}

-(void) changeName:(NSString *) prevName{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"New Stay Name", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[[alert textFieldAtIndex:0] setClearButtonMode:UITextFieldViewModeWhileEditing];
	[[alert textFieldAtIndex:0] setText:prevName];
	alert.tag = 12;
	[alert show];
	
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex > 0 && alertView.tag == 12) {
		NSString *newName = [[alertView textFieldAtIndex:0] text];
		[self.delegate actionController:self didEndEditing:@{@"type": @"CHANGE_NAME",@"name": newName}];
	}
	else{
		[self.delegate actionController:self didEndEditing:nil];
		
	}
}

@end
