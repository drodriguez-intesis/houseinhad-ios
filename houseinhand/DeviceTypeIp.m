//
//  DeviceTypeIp.m
//  houseinhand
//
//  Created by Isaac Lozano on 20/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeIp.h"


@implementation DeviceTypeIp

@dynamic highParam;
@dynamic image1;
@dynamic image2;
@dynamic isSecure;
@dynamic localIp;
@dynamic lowParam;
@dynamic password;
@dynamic pulseWidth;
@dynamic remotePort;
@dynamic type;
@dynamic username;
@dynamic valueOff;
@dynamic valueOn;
@dynamic doorPath;

@end
