//
//  DeviceTypeBoolViewer+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeBoolViewer+Parser.h"
#import "Device+Parser.h"

#define KEY_YES_TEXT						@"textYes"
#define KEY_NO_TEXT						@"textNo"
#define KEY_YES_VALUE					@"valueYes"
#define KEY_NO_VALUE					@"valueNo"
#define KEY_DEFAULT_TEXT				@"textDefault"

@implementation DeviceTypeBoolViewer (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeBoolViewer *dev = (DeviceTypeBoolViewer *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.textYes = (info[KEY_YES_TEXT] ? info[KEY_YES_TEXT]: dev.textYes);
	dev.textNo = (info[KEY_NO_TEXT] ? info[KEY_NO_TEXT]: dev.textNo);
	dev.valueYes = (info[KEY_YES_VALUE] ? @([info[KEY_YES_VALUE] integerValue]): dev.valueYes);
	dev.valueNo = (info[KEY_NO_VALUE] ? @([info[KEY_NO_VALUE] integerValue]): dev.valueNo);
	dev.textDefault = (info[KEY_DEFAULT_TEXT] ? info[KEY_DEFAULT_TEXT]: dev.textDefault);
	return dev;
}

@end
