//
//  TCPSocketCommunication.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncSocket.h"
#import "DeviceTypeMultimedia+Parser.h"

typedef NS_ENUM(NSUInteger, TcpSocketStatus){STATUS_DISCONNECTED,STATUS_CONNECTING,STATUS_CONNECTED};


@class TCPSocketCommunication;
@protocol TCPSocketCommunicationDelegate <NSObject>
-(void) tcpSocketCommunication:(TCPSocketCommunication *) tcpSocket didChangeConnectionStatus:(TcpSocketStatus) status;
-(void) tcpSocketCommunication:(TCPSocketCommunication *) tcpSocket didReadStringResponse:(NSString *) response;
@end

@interface TCPSocketCommunication : NSObject <GCDAsyncSocketDelegate>
@property (weak,nonatomic) id<TCPSocketCommunicationDelegate> delegate;
-(void) connectToHost:(NSString *) ip onPort:(NSUInteger) port;
-(void) connectToDevice:(DeviceTypeMultimedia *) device;
-(void) disconnect;
-(void) sendInfo:(NSString *) info;
-(BOOL) isConnected;
@end
