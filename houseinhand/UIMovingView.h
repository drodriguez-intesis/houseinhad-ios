//
//  UIMovingView.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIMovingView : UIView
@property (assign) CGPoint initialPos;
@property (assign) BOOL isLastRightView;
@property (assign) BOOL isLastLeftView;

@end
