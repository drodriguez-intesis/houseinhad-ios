//
//  DeviceTypeIpButton.h
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeIpButton : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSNumber * pulseWidth;
@property (nonatomic, retain) NSString * offPressIp;
@property (nonatomic, retain) NSString * onPressIp;

@end
