//
//  TypeSceneViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SimpleButtonViewController.h"

@interface TypeSceneViewController : SimpleButtonViewController <DevicesScenesViewControllerDelegate>

@end
