//
//  DeviceTypeBoolViewer.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeBoolViewer.h"


@implementation DeviceTypeBoolViewer

@dynamic textDefault;
@dynamic textNo;
@dynamic textYes;
@dynamic valueNo;
@dynamic valueYes;

@end
