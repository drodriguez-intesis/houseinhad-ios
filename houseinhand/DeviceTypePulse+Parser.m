//
//  DeviceTypePulse+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/7/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypePulse+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1					@"image1"
#define KEY_PULSE_WIDTH			@"pulseWidth"
#define KEY_VALUE_ON				@"valueOn"
#define KEY_VALUE_OFF				@"valueOff"

@implementation DeviceTypePulse (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypePulse *dev = (DeviceTypePulse *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.pulseWidth = (info[KEY_PULSE_WIDTH] ? @([info[KEY_PULSE_WIDTH] floatValue]): dev.pulseWidth);
	dev.valueOff = (info[KEY_VALUE_OFF] ? @([info[KEY_VALUE_OFF] integerValue]): dev.valueOff);
	dev.valueOn = (info[KEY_VALUE_ON] ? @([info[KEY_VALUE_ON] integerValue]): dev.valueOn);
	return dev;
}
@end