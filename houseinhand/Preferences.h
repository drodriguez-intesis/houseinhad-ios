//
//  Preferences.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Settings;

@interface Preferences : NSManagedObject

@property (nonatomic, retain) NSNumber * locked;
@property (nonatomic, retain) NSNumber * passcodeValue;
@property (nonatomic, retain) NSNumber * saveImages;
@property (nonatomic, retain) NSNumber * screenRest;
@property (nonatomic, retain) NSNumber * sounds;
@property (nonatomic, retain) NSNumber * vibrate;
@property (nonatomic, retain) Settings *r_settings;

@end
