//
//  Config+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Config+Parser.h"
#import "Settings+Parser.h"
#import "Stay+Parser.h"
#import "Device.h"
#import "NSManagedObjectContext+Fetch.h"

#define KEY_NAME					@"name"
#define KEY_ACTIVE					@"isActive"
#define KEY_DEMO					@"isDemo"
#define KEY_VALID					@"validated"

#define KEY_STAY						@"stays"
#define KEY_SETTINGS				@"settings"
#define KEY_SCENES					@"scenes"

@implementation Config (Parser)

+(void) deleteAllInContext:(NSManagedObjectContext *) moc{
	NSArray *objects = [moc searchObject:NSStringFromClass([self class]) limit:0 predicate:nil sorted:nil];
	if (!objects) return;
	@synchronized (moc){
		[objects enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			[moc deleteObject:obj];
		}];
	}
}

+(Config *) insertConfig:(NSDictionary *) info isActive:(BOOL) isActive context:(NSManagedObjectContext *) moc{
	if (!info[KEY_VALID] || [info[KEY_VALID] integerValue] == 0){
		[NSException raise:RAISE_EXCEPTION_INVALID_CONFIG format:@"Configuration File not Validated"];
		return nil;
	};
	Config *config = (Config *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	config.name = (info[KEY_NAME] ? info[KEY_NAME]: config.name);
	config.isDemo = (info[KEY_DEMO] ? @([info[KEY_DEMO] integerValue]): config.isDemo);
	config.isActive = @(isActive);

	//add stays array
	if (info[KEY_STAY] && [info[KEY_STAY] count] >0){
		[info[KEY_STAY] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			Stay *st = [Stay insertWithInfo:obj order:idx intoManagedObjectContext:moc];
			[config addR_nextStayObject:st];
		}];
	}
	
	//add settings
	if (info[KEY_SETTINGS] && [info[KEY_SETTINGS] count] >0){
		Settings *stt = [Settings insertWithInfo:info[KEY_SETTINGS] intoManagedObjectContext:moc];
		config.r_settings = stt;
	}
	
	//TODO: add scenes
	if (info[KEY_SCENES] && [info[KEY_SCENES] count] >0){
	}
	NSError *err;
	[moc save:&err];
	return config;
}

+(Config *) getActiveConfigOfContext:(NSManagedObjectContext *) moc{
	NSArray *objects = [moc searchObject:NSStringFromClass([self class]) limit:1 predicate:@"isActive == 1" sorted:nil];
	if (objects && objects.count > 0) return objects[0];
	return nil;
}


+(Config *) getDemoConfigOfContext:(NSManagedObjectContext *) moc{
	NSArray *objects = [moc searchObject:NSStringFromClass([self class]) limit:1 predicate:@"isDemo == 1" sorted:nil];
	if (objects && objects.count > 0) return objects[0];
	return nil;
}

-(NSNumber *) getDevicesCount{
	__weak Config * cfg = self;
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(r_stay.r_config == %@) OR (r_stay.r_prevStay.r_config == %@)",self,self];
	//NSString *predicate = [NSString stringWithFormat:@"(r_stay.r_config == %@) OR (r_stay.r_prevStay.r_config == %@)",self,self];
	NSArray *objects = [self.managedObjectContext searchObject:NSStringFromClass([Device class]) limit:0 predicatePredicate:predicate sorted:nil];
	if (objects && objects.count >0) return @(objects.count);
	return nil;
}

-(NSArray *) getOrderedStays{
	if (!self.r_nextStay || self.r_nextStay.count == 0) return nil;
	return [[self.r_nextStay allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
}

-(Network *) getNetworkOfType:(NSUInteger) type{
	NSArray * array = [[self.r_settings.r_network allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"nat == %d",type]];
	if (!array || array.count == 0) return nil;
	return array[0];
}

-(NSArray *) getTelegramsWithAddress:(NSNumber *) address{
	if (!self.r_nextStay || self.r_nextStay.count == 0) return nil;
	__block NSNumber * adr= address;
	__block NSMutableArray *array = [NSMutableArray array];
	[[self.r_nextStay allObjects] enumerateObjectsUsingBlock:^(Stay * obj, NSUInteger idx, BOOL *stop) {
		NSArray * telegrams = [obj getTelegramsWithAddress:adr];
		if (telegrams && telegrams.count>0) [array addObjectsFromArray:telegrams];
	}];
	return array;
}

-(NSArray *) getTelegramsWithAddress:(NSNumber *) address ofContext:(NSManagedObjectContext *) moc{
	return [moc fetchTelegramsWithAddress:address];
}

@end
