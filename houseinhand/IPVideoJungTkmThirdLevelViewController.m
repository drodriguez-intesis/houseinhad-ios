//
//  IPVideoJungTkmThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 20/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "IPVideoJungTkmThirdLevelViewController.h"
#import "DeviceTypeIp.h"
#import "AFHTTPRequestOperation.h"
#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.05

@interface IPVideoJungTkmThirdLevelViewController ()
@property (strong,nonatomic) UIImage *image;
@property (strong,nonatomic) IBOutlet UIButton *doorButton;
@property (strong,nonatomic) NSString *doorPath;

@end

@implementation IPVideoJungTkmThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeIp*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _image = [UIImage imageNamed:newDevice.image2];
		_doorPath = newDevice.doorPath;
    }
    return self;
}

-(void) viewDidLoad{
	[super viewDidLoad];
	[_doorButton setImage:_image forState:UIControlStateNormal];
    [_doorButton setImage:_image forState:UIControlStateHighlighted];
}

-(IBAction)touchDownButton:(id)sender{
        // send door open
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",super.device.localIp,_doorPath]]]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    }
     ];
    [operation start];
}

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}

-(void) orientationChanged:(NSNotification *) not{
	return;
}

@end
