//
//  TypeTextViewerViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/23/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeTextViewerViewController.h"
#import "DeviceTypeLabel.h"

@interface TypeTextViewerViewController ()
@property (strong,nonatomic) Telegram *statusTelegram;
@property (strong,nonatomic) NSString * currentText;
@end

@implementation TypeTextViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLabel *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_currentText = (NSString *)[self readControlStringStatus];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
	[self updateLabel];
    [self checkStatus];
	if ([super.device.hasStatus boolValue]) {
		[_statusTelegram addObserver:self forKeyPath:@"stringValue" options:0 context:nil];
	}
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
		//NSLog(@"Status telegram:%@",_statusTelegram);
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

-(NSString *) readControlStringStatus{
    NSString * status= @"No text";
	_statusTelegram = [super.device getTelegramWithCode:0];
	status = _statusTelegram.stringValue;
	return status;
}


- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (![_currentText isEqualToString:_statusTelegram.stringValue]) {
		[self updateLabel];
		if (animated) [super animationUpdate:super.centerLabel];
	}
}

-(void) updateLabel{
	_currentText = _statusTelegram.stringValue;
	super.centerLabel.text = _currentText;
}

-(void) dealloc{
	if ([super.device.hasStatus boolValue]) {
		[_statusTelegram removeObserver:self forKeyPath:@"stringValue"];
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _currentText = nil;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
