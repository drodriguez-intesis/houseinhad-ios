//
//  Settings+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Settings+Parser.h"
#import "Network+Parser.h"
#import "Preferences+Parser.h"

#define KEY_NETWORK				@"network"
#define KEY_ITEMS					@"items"
#define KEY_PREFERENCES		@"preferences"

@implementation Settings (Parser)

+(Settings *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc{
	Settings *settings = (Settings *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	settings.version = (__bridge NSString *)(kCFBundleVersionKey);
	
	//add network settings
	if (info[KEY_NETWORK] && [info[KEY_NETWORK] count] >0){
		[info[KEY_NETWORK][KEY_ITEMS] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			Network *nt = [Network insertWithInfo:obj intoManagedObjectContext:moc];
			[settings addR_networkObject:nt];
		}];
	}
	
	//add preferences
	if (info[KEY_PREFERENCES] && [info[KEY_PREFERENCES] count] >0){
		Preferences *pref = [Preferences insertWithInfo:info[KEY_PREFERENCES] intoManagedObjectContext:moc];
		settings.r_preferences = pref;
	}
	return settings;
}
@end
