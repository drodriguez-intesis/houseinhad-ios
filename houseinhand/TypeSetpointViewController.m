//
//  TypeSetpointViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/6/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeSetpointViewController.h"
#import "DeviceTypeSetpoint.h"
@interface TypeSetpointViewController ()
@property (strong,nonatomic) NSString *units;
@property (assign,nonatomic) NSUInteger decimals;
@property (assign,nonatomic) CGFloat scale;
@end

@implementation TypeSetpointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSetpoint *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self initVars:newDevice];
        super.currentValue = (CGFloat)[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSetpoint*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self initVars:newDevice];
		[self readControlStatus];
        super.currentValue = (CGFloat)[self readSceneStatus];
		
    }
    return self;
}

-(void) initVars:(DeviceTypeSetpoint *) newDevice{
	_units = newDevice.units;
	_scale = [newDevice.scale floatValue];
	_minValue = [newDevice.minValue floatValue];
	_maxValue = [newDevice.maxValue floatValue];
	_step = [newDevice.step floatValue];
	_decimals = [newDevice.decimals integerValue];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setActions];
    [self updateLabel:NO];
	[self updateButtons];
	if (super.type == TYPE_CONTROL) {
    	[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (super.currentValue !=	_statusTelegram.value.floatValue && super.type == TYPE_CONTROL) {
		super.currentValue = _statusTelegram.value.floatValue;
		[self updateLabel:YES];
		[self updateButtons];
	}
	else if (super.type == TYPE_SCENE){
		[self updateLabel:YES];
		[self updateButtons];
	}
}


-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    _units = nil;
}



-(void) setActions{
    [super.leftButton addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.rightButton addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchDown];
}

-(void)leftButtonPressed{
    if ((super.currentValue - _step)>= _minValue) {
        super.currentValue = super.currentValue - _step;
		[self updateLabel:NO];
		[self sendButtonData:super.currentValue];
    }
	[self updateButtons];
}

-(void)rightButtonPressed{
    if ((super.currentValue + _step)<= _maxValue) {
        super.currentValue = super.currentValue + _step;
		[self updateLabel:NO];
		[self sendButtonData:super.currentValue];
    }
	[self updateButtons];
}

-(void) updateLabel:(BOOL) animated{
	if (animated) {
		[super animationUpdate:super.centerLabel];
	}
	[self createValue];
}

-(void) createValue{
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
	[numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[numberFormatter setMaximumFractionDigits:_decimals];
	NSString * trimmed = [numberFormatter stringFromNumber:@(_scale*super.currentValue)];
    super.centerLabel.text =  [NSString stringWithFormat:@"%@ %@",trimmed,_units];
}

-(void) updateButtons{
	if ((super.currentValue - _step)>= _minValue) {
		super.leftButton.userInteractionEnabled = YES;
		super.leftButton.alpha = 1.0;
	}
	else{
		super.leftButton.userInteractionEnabled = NO;
		super.leftButton.alpha = ALPHA_UNSELECTED;
	}
    if ((super.currentValue + _step)<= _maxValue) {
		super.rightButton.userInteractionEnabled = YES;
		super.rightButton.alpha = 1.0;
	}
	else{
		super.rightButton.userInteractionEnabled = NO;
		super.rightButton.alpha = ALPHA_UNSELECTED;
	}
}

-(CGFloat) readControlStatus{
    CGFloat status=22.0;
	_statusTelegram = [super.device getTelegramWithCode:0];
	status =[_statusTelegram.value floatValue];
	_actionTelegram = [super.device getTelegramWithCode:1];
	return status;
}

-(CGFloat) readSceneStatus{
    CGFloat status=22.0;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.floatValue];
	if (scTel) {
		status =[scTel.value floatValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

-(void) sendButtonData:(CGFloat) value{
    [super sendData:@(value) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_actionTelegram.address,@"address",@(super.currentValue),@"value",_actionTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
