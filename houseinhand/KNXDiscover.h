//
//  KNXDiscover.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

@class KNXDiscover;
@protocol KNXDiscoverDelegate <NSObject>
-(void) knxSocket:(KNXDiscover *) tcpServerCom didEndDiscoverRequest:(NSArray *) foundItems;
@end



@interface KNXDiscover : NSObject
-(void) sendDiscoverRequest;
-(void) endDiscover;
@property (weak,nonatomic)  id<KNXDiscoverDelegate> delegate;
@end
