//
//  AdditionalViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class InitialViewController;
@interface AdditionalViewController : UIViewController
@property (strong,nonatomic) InitialViewController *mainController;

@end
