//
//  YoutubeViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 01/04/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#define HIH_URL_OVERVIEW			@"X6ZlnLTx-e4"
#define HIH_URL_DESIGNER			@"X6ZlnLTx-e4"
#import "YoutubeViewController.h"

@interface YoutubeViewController ()
@property (weak,nonatomic) IBOutlet UIWebView *youtubeVideo;
@end

@implementation YoutubeViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self embedYouTube:@""];
}

- (void)embedYouTube:(NSString *)urlString
{
    
    NSString *html = [NSString stringWithFormat:@"<html><head><style type='text/css'>body {background-color: transparent;color: white;-webkit-transform-origin: left top;-webkit-transform: scale(0.5);}</style></head><body style='margin:0'><iframe width='%f' height='%f' src='%@' frameborder='0' allowfullscreen></iframe></body></html>", _youtubeVideo.frame.size.width*2, _youtubeVideo.frame.size.height*2, HIH_URL_OVERVIEW];
    [_youtubeVideo sizeToFit];
    [_youtubeVideo loadHTMLString:html baseURL:[NSURL URLWithString:@"http://www.youtube.com/embed/"]];
}


- (IBAction)dismissView {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)gotoHouseinhand:(id)sender {
    NSString *urlStr = @"http://houseinhand.com";
	NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
	if ([language isEqualToString:@"es"] || [language isEqualToString:@"ca"]) {
		urlStr = @"http://www.houseinhand.com";
	}
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}

@end
