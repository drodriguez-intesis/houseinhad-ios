//
//  TypeIpWebViewViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 16/12/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import "TypeIpWebViewViewController.h"
#import "WebViewViewController.h"
@interface TypeIpWebViewViewController ()

@end

@implementation TypeIpWebViewViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) buttonPressed{
    WebViewViewController *web = [[WebViewViewController alloc] initWithNibName:@"WebView" bundle:nil];
    [web setUrlString:super.onAction];
    [self.parentViewController presentViewController:web animated:YES completion:nil];
}

-(void) buttonReleased{
}

@end
