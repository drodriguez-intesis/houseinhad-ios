//
//  TypeClimaViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeClimaViewController.h"
#import "DeviceTypeClima.h"

#define SEGMENTED_SELECTED_COMFORT		0
#define SEGMENTED_SELECTED_STANDBY			1
#define SEGMENTED_SELECTED_NIGHT				2
#define SEGMENTED_SELECTED_EXTREMES		3
#define SEGMENTED_SELECTED_LOCK				4

@interface TypeClimaViewController ()
@property (assign,nonatomic) NSUInteger currentValue;
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@property (strong,nonatomic) Telegram *lockTelegram;
@end

@implementation TypeClimaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self readControlStatus];
		_currentValue = _statusTelegram.value.integerValue;
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
        [self readControlStatus];
		_currentValue = [self readSceneStatus];
		
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setActions];
	if (super.type == TYPE_CONTROL) {
		[self updateCurrentImage:NO];
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
	else{
		[self updateSegmentedScenes];
	}
}

-(void) updateSegmentedScenes{
	super.leftImage.hidden = YES;
	super.rightImage.hidden = YES;
	[super.centralSegmented setSelectedSegmentIndex:_currentValue-1];
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (_currentValue !=	_statusTelegram.value.integerValue) {
		_currentValue = _statusTelegram.value.integerValue;
		 [self updateCurrentImage:animated];
	 }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
	_actionTelegram = nil;
	_statusTelegram = nil;
	_lockTelegram = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) setActions{
    [super.centralSegmented addTarget:self action:@selector(segmentedPressed:) forControlEvents:UIControlEventValueChanged];
}

-(void)segmentedPressed:(UISegmentedControl *)sender{
	//[super playButtonSound];
	if (sender.selectedSegmentIndex < 4 && sender.selectedSegmentIndex !=UISegmentedControlNoSegment) {
		//unlock controller
		if (super.hasLock) {
			[super sendData:@(0) address:_lockTelegram.address dpt:_lockTelegram.dpt type:_lockTelegram.type delay:_lockTelegram.delay];
		}
		[super sendData:@(sender.selectedSegmentIndex+1) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:@([_actionTelegram.delay floatValue]+ .1)];
	}
	else{
		//lock controller
		 [super sendData:@(1) address:_lockTelegram.address dpt:_lockTelegram.dpt type:_lockTelegram.type delay:_lockTelegram.delay];
	}
}

-(void) readControlStatus{
	_statusTelegram = [super.device getTelegramWithCode:0];
	_actionTelegram = [super.device getTelegramWithCode:1];
	_lockTelegram = [super.device getTelegramWithCode:2];
}

-(NSUInteger) readSceneStatus{
    NSUInteger status=1;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =scTel.value.unsignedIntegerValue;
		super.widgetSceneActive = YES;
	}
	else{
		scTel = [super.scene getTelegramWithAddress:_lockTelegram.address.integerValue];
		if (scTel) {
			status =5;
			super.widgetSceneActive = YES;
		}
	}
	return status;
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

-(void) updateCurrentImage:(BOOL) animated{
	if (animated) {
	 	[super animationUpdate:super.leftImage];
	 }
	[self setLeftImage];
	[self setRightImage];

}

-(void) setRightImage{
	NSString *im = @"device_mode_heat";
	if ((_currentValue & 0x0020) == 32){
		//cold
		im = @"clima_mode_cool";
	}
	[super.rightImage setImage:[UIImage imageNamed:im]];
}

-(void) setLeftImage{
	NSString *im = @"device_mode_comfort";
	if ((_currentValue & 0x0010) == 16){
		//bloqueado
		im = @"device_mode_locked";
		if ([[super.device valueForKey:@"hasLock"] boolValue] == YES) [super.centralSegmented setSelectedSegmentIndex:4];
	}
	else if ((_currentValue & 0x0011) == 1){
		//confort
		im = @"device_mode_comfort";
		[super.centralSegmented setSelectedSegmentIndex:0];
	}else if ((_currentValue & 0x0013) == 2){
		//stby
		im = @"device_mode_standby";
		[super.centralSegmented setSelectedSegmentIndex:1];
	}else if ((_currentValue & 0x0017) == 4){
		//nocturno
		im = @"device_mode_night";
		[super.centralSegmented setSelectedSegmentIndex:2];
	}
	else if ((_currentValue & 0x001F) == 8){
		//extremos
		im = @"device_mode_extremes";
		[super.centralSegmented setSelectedSegmentIndex:3];
	}
	[super.leftImage setImage:[UIImage imageNamed:im]];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info,*info2;
		if (super.hasLock) {
			if (super.centralSegmented.selectedSegmentIndex < 4 && super.centralSegmented.selectedSegmentIndex !=UISegmentedControlNoSegment) {
				info = @{
			 		@"address": _lockTelegram.address,
					@"dpt": _lockTelegram.dpt,
	 				@"value": @0,
	 			};
				info2 = @{
					@"address": _actionTelegram.address,
					@"dpt": _actionTelegram.dpt,
					@"value": @(super.centralSegmented.selectedSegmentIndex+1) ,
					@"delay": @.1F
				};
			}
			else{
				info = @{
			 		@"address": _lockTelegram.address,
					@"dpt": _lockTelegram.dpt,
					@"value": @1
				};
			}
		}
		else{
			info = @{
			 @"address": _actionTelegram.address,
			@"dpt": _actionTelegram.dpt,
			@"value": @(super.centralSegmented.selectedSegmentIndex+1) ,
		};
		}
		if	(info2) return @[info,info2];
		else return @[info];
	}
	return nil;
}

@end
