//
//  UIMovingView.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UIMovingView.h"

@implementation UIMovingView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.isLastLeftView = NO;
        self.isLastRightView = NO;
    }
    return self;
}

@end
