//
//  MultimediaThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/26/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "TCPSocketCommunication.h"

@interface MultimediaThirdLevelViewController : ThirdLevelViewController <UIScrollViewDelegate,TCPSocketCommunicationDelegate,UIAlertViewDelegate>

@end
