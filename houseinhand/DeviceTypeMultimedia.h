//
//  DeviceTypeMultimedia.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"

@class MultiCommand;

@interface DeviceTypeMultimedia : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * localIp;
@property (nonatomic, retain) NSNumber * localPort;
@property (nonatomic, retain) NSNumber * remotePort;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSNumber * remoteType;
@property (nonatomic, retain) NSSet *r_command;
@end

@interface DeviceTypeMultimedia (CoreDataGeneratedAccessors)

- (void)addR_commandObject:(MultiCommand *)value;
- (void)removeR_commandObject:(MultiCommand *)value;
- (void)addR_command:(NSSet *)values;
- (void)removeR_command:(NSSet *)values;

@end
