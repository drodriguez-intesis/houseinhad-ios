//
//  NSManagedObjectContext+Fetch.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "NSManagedObjectContext+Fetch.h"
#define KEY_CONFIG			@"config"

#define KEY_FETCH_TELEGRAMS_ADDRESS			@"fetchTelegramsWithAddress"

@implementation NSManagedObjectContext (Fetch)

-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicate:(NSString *) predicate sorted:(NSString *) sort{
    @synchronized(self){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *act = [NSEntityDescription entityForName:type inManagedObjectContext:self];
        [fetchRequest setEntity:act];
        if (limit > 0) {
            [fetchRequest setFetchLimit:limit];
        }
        if (predicate) {
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:predicate]];
        }
		if (sort) {
			NSSortDescriptor *descSort = [NSSortDescriptor sortDescriptorWithKey:sort ascending:YES];
			[fetchRequest setSortDescriptors:@[descSort]];
		}
        NSArray *fetchedObjects = [self executeFetchRequest:fetchRequest error:nil];
        return fetchedObjects;
	}
}

-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicatePredicate:(NSPredicate *) predicate sorted:(NSString *) sort{
    @synchronized(self){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *act = [NSEntityDescription entityForName:type inManagedObjectContext:self];
        [fetchRequest setEntity:act];
        if (limit > 0) {
            [fetchRequest setFetchLimit:limit];
        }
        if (predicate) {
            [fetchRequest setPredicate:predicate];
        }
		if (sort) {
			NSSortDescriptor *descSort = [NSSortDescriptor sortDescriptorWithKey:sort ascending:YES];
			[fetchRequest setSortDescriptors:@[descSort]];
		}
        NSArray *fetchedObjects = [self executeFetchRequest:fetchRequest error:nil];
        return fetchedObjects;
	}
}


-(NSArray *) searchObject:(NSString *) type limit:(NSUInteger) limit predicatePredicate:(NSPredicate *) predicate sorted:(NSString *) sort resultsType:(NSFetchRequestResultType) resultType{
    @synchronized(self){
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *act = [NSEntityDescription entityForName:type inManagedObjectContext:self];
        [fetchRequest setEntity:act];
        if (limit > 0) {
            [fetchRequest setFetchLimit:limit];
        }
        if (predicate) {
            [fetchRequest setPredicate:predicate];
        }
		if (sort) {
			NSSortDescriptor *descSort = [NSSortDescriptor sortDescriptorWithKey:sort ascending:YES];
			[fetchRequest setSortDescriptors:@[descSort]];
		}
		[fetchRequest setResultType:resultType];
		NSError *err;
        NSArray * fetchedObjects = [self executeFetchRequest:fetchRequest error:&err];
        return fetchedObjects;
	}
}

-(NSArray *) fetchTelegramsWithAddress:(NSNumber *) address{
	NSArray *array = [self executeFetchRequest:[self.persistentStoreCoordinator.managedObjectModel fetchRequestFromTemplateWithName:KEY_FETCH_TELEGRAMS_ADDRESS substitutionVariables:@{@"VAR_1":address}] error:nil];
	return array;
}
@end
