//
//  TypeIpButtonViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeIpButtonViewController.h"
#import "DeviceTypeIpButton+Parser.h"
#import "AFHTTPRequestOperation.h"

@interface TypeIpButtonViewController ()
@property (strong,nonatomic) NSString *offAction;
@property (strong,nonatomic) NSNumber *pulseWidth;

@end

@implementation TypeIpButtonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeIpButton *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _onAction = newDevice.onPressIp;
        _offAction = newDevice.offPressIp;
        _pulseWidth = newDevice.pulseWidth;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchDown];
    [super.centerButton addTarget:self action:@selector(buttonReleased) forControlEvents:UIControlEventTouchUpInside];
}

-(void) buttonPressed{
	[self startOperationWithUrl:[NSURL URLWithString:_onAction]];
}

-(void) buttonReleased{
    [self performSelector:@selector(startOperationWithUrl:) withObject:[NSURL URLWithString:_offAction] afterDelay:_pulseWidth.floatValue];
}

-(void) startOperationWithUrl:(NSURL *) url{
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc]
                                         initWithRequest:[NSURLRequest requestWithURL:url]];
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation
                                               , id responseObject) {
     
            //NSLog(@"OK");
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            //NSLog(@"KO");
    }
     ];
    [operation start];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
