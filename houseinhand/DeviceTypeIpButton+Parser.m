//
//  DeviceTypeIpButton+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeIpButton+Parser.h"
#import "Device+Parser.h"


#define KEY_IMAGE_1				@"image1"
#define KEY_PULSE_WIDTH		@"pulseWidth"
#define KEY_VALUE_ON			@"onPressIp"
#define KEY_VALUE_OFF			@"offPressIp"

@implementation DeviceTypeIpButton (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeIpButton *dev = (DeviceTypeIpButton *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.pulseWidth = (info[KEY_PULSE_WIDTH] ? @([info[KEY_PULSE_WIDTH] floatValue]): dev.pulseWidth);
	  
    dev.onPressIp = (info[KEY_VALUE_ON] ? info[KEY_VALUE_ON]: dev.onPressIp);
	dev.offPressIp = (info[KEY_VALUE_OFF] ? info[KEY_VALUE_OFF]: dev.offPressIp);

	return dev;
}

@end
