//
//  MigrationViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/12/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>

@interface MigrationViewController : UIViewController <MFMailComposeViewControllerDelegate,UIScrollViewDelegate>

@end
