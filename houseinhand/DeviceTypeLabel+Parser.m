//
//  DeviceTypeLabel+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeLabel+Parser.h"
#import "Device+Parser.h"

#define KEY_UNITS				@"units"
#define KEY_SCALE				@"scale"
#define KEY_DECIMALS			@"decimals"
@implementation DeviceTypeLabel (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeLabel *dev = (DeviceTypeLabel *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.units = (info[KEY_UNITS] ? info[KEY_UNITS]: dev.units);
	dev.scale = (info[KEY_SCALE] ? @([info[KEY_SCALE] floatValue]): dev.scale);
	dev.decimals = (info[KEY_DECIMALS] ? @([info[KEY_DECIMALS] integerValue]): dev.decimals);
	
	return dev;
}
@end
