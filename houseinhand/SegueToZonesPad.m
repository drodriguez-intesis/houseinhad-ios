//
//  SegueToZonesPad.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/31/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegueToZonesPad.h"
#import "InitialViewController.h"

@implementation SegueToZonesPad
- (void)perform
{
	UIViewController *source = self.sourceViewController;
	UIViewController *destination = self.destinationViewController;
	InitialViewController * initial = (InitialViewController *)source;
	[initial.devicesView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    initial.zonesView.hidden = NO;
    initial.zonesView.alpha = 0.8;
	initial.zonesView.transform = CGAffineTransformMakeScale(.9, .9);
    initial.staysView.userInteractionEnabled = NO;
    [[[initial getStaysViewController] view] setUserInteractionEnabled:NO];
    [initial.zonesView addSubview:destination.view];
    destination.view.frame = initial.zonesView.bounds;
    [source addChildViewController:destination];
    [UIView transitionWithView:initial.zonesView duration:.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
		initial.zonesView.transform = CGAffineTransformIdentity;
        initial.zonesView.alpha = 1.0;
		[initial.staysView setCenter:CGPointMake(initial.staysView.center.x -144, initial.staysView.center.y)];
    } completion:^(BOOL finished) {
        [destination didMoveToParentViewController:source];
        initial.staysView.userInteractionEnabled = NO;
    }];
}
@end
