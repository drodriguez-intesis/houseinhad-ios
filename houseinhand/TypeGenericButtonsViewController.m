//
//  TypeGenericButtonsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeGenericButtonsViewController.h"
#import "DeviceType3Buttons+Parser.h"

typedef 	NS_ENUM (NSUInteger,NumberButtons) {BUTTONS_NONE=0,BUTTONS_NUMBER_ONE=1,BUTTONS_NUMBER_TWO=2,BUTTONS_NUMBER_THREE=3};

@interface TypeGenericButtonsViewController ()
@property (strong,nonatomic) Telegram *firstTelegram;
@property (strong,nonatomic) Telegram *secondTelegram;
@property (strong,nonatomic) Telegram *thirdTelegram;
@property (assign,nonatomic) NSUInteger numButtons;
@property (assign,nonatomic) NumberButtons pressedButton;

@property (assign,nonatomic) NSInteger firstValue;
@property (assign,nonatomic) NSInteger secondValue;
@property (assign,nonatomic) NSInteger thirdValue;

@end




@implementation TypeGenericButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceType3Buttons *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self configureDevice:newDevice];
		[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceType3Buttons*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self configureDevice:newDevice];
		[self readControlStatus];
        _pressedButton = [self readSceneStatus];
    }
    return self;
}

-(void) configureDevice:(DeviceType3Buttons *) newDevice{
    _numButtons = newDevice.numButtons.unsignedIntegerValue;
	_firstValue = [newDevice.firstValue integerValue];
	_secondValue = [newDevice.secondValue integerValue];
	_thirdValue = [newDevice.thirdValue integerValue];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self configureButtons];
    [self setActions];
    if (super.type == TYPE_SCENE) {
		[self updateAlphaButtons];
	}
}

-(void) updateAlphaButtons{
    switch (_pressedButton) {
        case BUTTONS_NUMBER_TWO:
            super.leftButton.alpha = ALPHA_UNSELECTED;
            if (_numButtons == BUTTONS_NUMBER_TWO) {
                super.centerButton.alpha = ALPHA_UNSELECTED;
                super.rightButton.alpha = 1.0;
            }
            else{
                super.centerButton.alpha = 1.0;
                super.rightButton.alpha = ALPHA_UNSELECTED;
            }
            break;
        case BUTTONS_NUMBER_THREE:
            super.leftButton.alpha = ALPHA_UNSELECTED;
            super.centerButton.alpha = ALPHA_UNSELECTED;
            super.rightButton.alpha = 1.0;
            break;
        default:
            super.leftButton.alpha = 1.0;
            super.centerButton.alpha = ALPHA_UNSELECTED;
            super.rightButton.alpha = ALPHA_UNSELECTED;
            break;
    }
}

-(void) setActions{
    [super.leftButton addTarget:self action:@selector(firstButtonPressed) forControlEvents:UIControlEventTouchDown];
    switch (_numButtons) {
        case BUTTONS_NUMBER_TWO:
            [super.rightButton addTarget:self action:@selector(secondButtonPressed) forControlEvents:UIControlEventTouchDown];
            break;
        case BUTTONS_NUMBER_THREE:
            [super.centerButton addTarget:self action:@selector(secondButtonPressed) forControlEvents:UIControlEventTouchDown];
            [super.rightButton addTarget:self action:@selector(thirdButtonPressed) forControlEvents:UIControlEventTouchDown];
            break;
        default:
            break;
    }
}

-(void) firstButtonPressed{
    if (_firstTelegram) [self sendDataTelegram:_firstTelegram value:_firstValue];
    if (super.type == TYPE_SCENE){
        _pressedButton = BUTTONS_NUMBER_ONE;
		[self updateAlphaButtons];
    }

}

-(void) secondButtonPressed{
    if (_secondTelegram) [self sendDataTelegram:_secondTelegram value:_secondValue];
    if (super.type == TYPE_SCENE){
        _pressedButton = BUTTONS_NUMBER_TWO;
        [self updateAlphaButtons];
    }

}

-(void) thirdButtonPressed{
    if (_thirdTelegram) [self sendDataTelegram:_thirdTelegram value:_thirdValue];
    if (super.type == TYPE_SCENE){
        _pressedButton = BUTTONS_NUMBER_THREE;
        [self updateAlphaButtons];
    }
}

-(void) sendDataTelegram:(Telegram *) telegram value:(NSInteger) value{
    [super sendData:@(value) address:telegram.address dpt:telegram.dpt type:telegram.type delay:telegram.delay];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) readControlStatus{
	_firstTelegram = [super.device getTelegramWithCode:0];
	_secondTelegram = [super.device getTelegramWithCode:1];
    _thirdTelegram = [super.device getTelegramWithCode:2];
    
    
}

-(NumberButtons) readSceneStatus{
    NumberButtons selectedButton = BUTTONS_NONE;
    SceneTelegram *scTel1,*scTel2,*scTel3;
    NSInteger storedVal1,storedVal2,storedVal3;
    if (_firstTelegram){
        scTel1 = [super.scene getTelegramWithAddress:_firstTelegram.address.integerValue];
        storedVal1 = scTel1.value.integerValue;
    }
    
    if (_secondTelegram){
        scTel2 = [super.scene getTelegramWithAddress:_secondTelegram.address.integerValue];
        storedVal2 = scTel2.value.integerValue;
    }
    if (_thirdTelegram){
        scTel3 = [super.scene getTelegramWithAddress:_thirdTelegram.address.integerValue];
        storedVal3 = scTel3.value.integerValue;
    }

	if (scTel1 && storedVal1 == _firstValue) {
        selectedButton = BUTTONS_NUMBER_ONE;
        super.widgetSceneActive = YES;
    }
    else if (scTel2 && storedVal2 == _secondValue) {
        selectedButton = BUTTONS_NUMBER_TWO;
        super.widgetSceneActive = YES;
    }
    else if (scTel3 && storedVal3 == _thirdValue) {
        selectedButton = BUTTONS_NUMBER_THREE;
        super.widgetSceneActive = YES;
    }
    
    return selectedButton;
}

-(void) configureButtons{
    switch (_numButtons) {
        case BUTTONS_NUMBER_ONE:
            [super.leftButton setCenter:CGPointMake(super.centerButton.center.x, super.centerButton.center.y)];
            
            [super.centerButton removeFromSuperview];
            [super.rightButton removeFromSuperview];
            break;
        case BUTTONS_NUMBER_TWO:
            [super.leftButton setCenter:CGPointMake(super.leftButton.center.x + (super.centerButton.center.x - super.leftButton.center.x)/2, super.centerButton.center.y)];
            [super.rightButton setCenter:CGPointMake(super.centerButton.center.x + (super.rightButton.center.x - super.centerButton.center.x)/2, super.centerButton.center.y)];
            
            [super.centerButton removeFromSuperview];
            break;
        default:
            break;
    }
}

-(NSArray *) getSceneStatus{
    NSArray *telegramArray = nil;
	if ([super needSceneStatus]) {
        switch (_pressedButton) {
            case BUTTONS_NUMBER_ONE:
                if (_firstTelegram) telegramArray = @[@{@"address": _firstTelegram.address,@"value": @(_firstValue),@"dpt": _firstTelegram.dpt,@"delay": _firstTelegram.delay}];
                break;
            case BUTTONS_NUMBER_TWO:
                if (_secondTelegram) telegramArray = @[@{@"address": _secondTelegram.address,@"value": @(_secondValue),@"dpt": _secondTelegram.dpt,@"delay": _secondTelegram.delay}];
                break;
            case BUTTONS_NUMBER_THREE:
                if (_thirdTelegram) telegramArray = @[@{@"address": _thirdTelegram.address,@"value": @(_thirdValue),@"dpt": _thirdTelegram.dpt,@"delay": _thirdTelegram.delay}];
                break;
            default:
                break;
        }
    }
	return telegramArray;
}

@end
