//
//  SettingsViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AdditionalViewController.h"
#import "ThirdLevelViewController.h"
#import "ProfilesThirdLevelViewController.h"
#import "NetworkSettingsViewController.h"	
#import "ScrollThirdLevelViewController.h"
#import "LanguagesThirdLevelViewController.h"
@protocol SettingsControllerDelegate <NSObject>

- (void)settingsControllerDidChangeConfig;

@end
@interface SettingsViewController : AdditionalViewController <UITextFieldDelegate,ThirdLevelViewControllerDelegate,ObjectsAnimations,ProfilesThirdLevelViewControllerDelegate,UIAlertViewDelegate,NetworkSettingsViewControllerDelegate,ScrollThirdLevelViewControllerDelegate,LanguagesThirdLevelViewControllerDelegate>
@property (strong)  IBOutlet UIScrollView * scrollView;
@property (nonatomic, weak) IBOutlet id <SettingsControllerDelegate> delegate;
@end
