//
//  DevicesCompatibility.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//


// This class defines the compatibility matrix of widgets in houseinhand project



#import <Foundation/Foundation.h>



//devices types with appCode

#define TYPE_OBJECT_LIGHT                           			0
#define TYPE_OBJECT_DIMMER                          		1
#define TYPE_OBJECT_KNX_SCENE                   			2
#define TYPE_OBJECT_SETPOINT                        		3
#define TYPE_OBJECT_VIEWER                          			4
#define TYPE_OBJECT_BLIND                           			5
#define TYPE_OBJECT_REG_BLIND                   			6
#define TYPE_OBJECT_GRAD_BLIND                  			7
#define TYPE_OBJECT_VELUX_BLIND                 			8
#define TYPE_OBJECT_RGB                             			9
#define TYPE_OBJECT_GENERIC_SEGMENTED				10
#define TYPE_OBJECT_CLIMA_1b								11
#define TYPE_OBJECT_CLIMA_1B								12
#define TYPE_OBJECT_IP_CAMERA								13
#define TYPE_OBJECT_GEN_VIEWER							14
#define TYPE_OBJECT_BOOL_VIEWER							15
#define TYPE_OBJECT_TEXT_VIEWER							16
#define TYPE_OBJECT_IP_VIDEO_KNX							17
#define TYPE_OBJECT_MULTIMEDIA							18
#define TYPE_OBJECT_MULTIMEDIA_MACRO				19
#define TYPE_OBJECT_JUNG_SETPOINT						20
#define TYPE_OBJECT_BERKER_SETPOINT					21
#define TYPE_OBJECT_PULSE_BUTTON						22
#define TYPE_OBJECT_3_BUTTONS							23
#define TYPE_OBJECT_EIS_SOUND								24
#define TYPE_OBJECT_IP_BUTTON								25
#define TYPE_OBJECT_JUNG_TKM								26
#define TYPE_OBJECT_MOBOTIX_T24						27
#define TYPE_OBJECT_IP_WEBVIEW_BUTTON				30

@interface DevicesCompatibility : NSObject

+(const NSArray *) getAvailableWidgetDevices;
+(const NSDictionary *) getAvailableDataEntityDevices;
@end
