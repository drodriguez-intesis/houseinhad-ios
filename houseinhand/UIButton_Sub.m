//
//  UIButton_Sub.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "UIButton_Sub.h"
#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_MIN_SIZE            0.9
#define ANIM_UPDATE_MAX_SIZE            1.1

#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_2          1.2

#define ANIM_UPDATE_MAX_SIZE_3          1.05

#define ANIM_INIT_PRESSED_DURATION      0.1
#define ANIM_INIT_PRESSED_MAX_SIZE      1.1

#define ANIM_FIN_PRESSED_DURATION       0.3
@implementation UIButton_Sub

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void) animationUpdateMaster{
    [UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE,ANIM_UPDATE_MAX_SIZE);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MIN_SIZE,ANIM_UPDATE_MIN_SIZE);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformIdentity;}];}];}];
}

-(void) animationInitPressedMaster{
    [UIView animateWithDuration:ANIM_INIT_PRESSED_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformMakeScale(ANIM_INIT_PRESSED_MAX_SIZE,ANIM_INIT_PRESSED_MAX_SIZE);} completion:nil];
}

-(void) animationFinPressedMaster{
    [UIView animateWithDuration:ANIM_FIN_PRESSED_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformIdentity;} completion:nil];
}


-(void) animationUpdateDetail{
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_2,ANIM_UPDATE_MAX_SIZE_2);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{self.transform=CGAffineTransformIdentity;} completion:nil];}];
    
}

-(void) animationInitPressedDetail{
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(void) animationFinPressedDetail {
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformIdentity;} completion:nil];
}

@end
