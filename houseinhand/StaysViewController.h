//
//  StaysViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayViewController.h"
#import "ThirdLevelViewController.h"
#import "ActionPopupViewController.h"
@class InitialViewController;
@interface StaysViewController : UIViewController <StayViewControllerDelegate,ThirdLevelViewControllerDelegate,ActionScrollViewControllerDelegate>
@property (strong,nonatomic) IBOutlet UIScrollView *staysScrollView;
@property (strong,nonatomic) IBOutlet UILabel *configName;
@property (assign) BOOL isZone;
@property (strong,nonatomic) id detailItem;

-(IBAction)resetScrollOffset;
-(void) addToScrollView;
@end
