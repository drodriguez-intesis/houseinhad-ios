//
//  SegueToDevicesPad.m
//  houseinhand
//
//  Created by Isaac Lozano on 10/16/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegueToDevicesPad.h"
#import "InitialViewController.h"
#import "DevicesViewControllerViewController.h"
@implementation SegueToDevicesPad

- (void)perform
{
	InitialViewController * initial = (InitialViewController *)self.sourceViewController;
	DevicesViewControllerViewController * devices = (DevicesViewControllerViewController *)self.destinationViewController;
	
	[initial.childViewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
		if ([vc isKindOfClass:[DevicesViewControllerViewController class]]){
			[vc willMoveToParentViewController:nil];
			[vc.view removeFromSuperview];
			[vc removeFromParentViewController];
		}
	}];
	initial.staysView.userInteractionEnabled = NO;
	devices.view.frame = initial.devicesView.bounds;
	[initial addChildViewController:devices];
	initial.devicesView.alpha = 0.0;
	initial.devicesView.transform = CGAffineTransformMakeScale(.9,.9);
	[initial.devicesView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

	[devices reloadDetailItem];
	[initial.devicesView addSubview:devices.view];
	[UIView transitionWithView:initial.view duration:.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
		initial.devicesView.transform = CGAffineTransformIdentity;
		initial.devicesView.alpha = 1.0;
	} completion:^(BOOL finished) {
		[devices didMoveToParentViewController:initial];
		initial.staysView.userInteractionEnabled = YES;
	}];	

}
@end
