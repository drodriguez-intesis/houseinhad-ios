//
//  TypeRegBlindViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeRegBlindViewController.h"
#import "DeviceTypeSlider.h"
#import <AudioToolbox/AudioServices.h>

@interface TypeRegBlindViewController ()
@property (strong,nonatomic) UIImage *leftSelectedImage;
@property (strong,nonatomic) UIImage *rightSelectedImage;
@property (assign,nonatomic) NSUInteger currentValue;
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@property (strong,nonatomic) Telegram *movementTelegram;
@property (strong,nonatomic) Telegram *stopTelegram;
@property (strong,nonatomic) NSTimer *timerLongPressing;
@property (assign,nonatomic) BOOL longPressing;
@property (assign,nonatomic) BOOL hasStopTelegram;

@end

@implementation TypeRegBlindViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _hasStopTelegram = newDevice.hasStop.boolValue;
        _currentValue = (NSUInteger)[self readControlStatus];
        
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self readControlStatus];
        _currentValue = [self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self setActions];
	[super.slider setValue:(255 - _currentValue) animated:NO];
	if (super.type == TYPE_CONTROL) {
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
}

-(void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (_currentValue !=	_statusTelegram.value.integerValue) {
		_currentValue = _statusTelegram.value.integerValue;
		[super.slider setValue:(255 - _currentValue) animated:YES];
	}
}

-(void) setActions{
     [super.leftButton addTarget:self action:@selector(initLeftButtonPressed) forControlEvents:UIControlEventTouchDown];
     [super.rightButton addTarget:self action:@selector(initRightButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.slider addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventValueChanged];
    
    if (_hasStopTelegram) {
        [super.leftButton addTarget:self action:@selector(finLeftButtonPressed) forControlEvents:UIControlEventTouchUpInside];
        [super.rightButton addTarget:self action:@selector(finRightButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    }
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

-(CGFloat) readControlStatus{
    NSUInteger status= 0;
	_statusTelegram = [super.device getTelegramWithCode:0];
	_actionTelegram = [super.device getTelegramWithCode:1];
    _stopTelegram = [super.device getTelegramWithCode:2];
	_movementTelegram = [super.device getTelegramWithCode:3];
	status =[_statusTelegram.value integerValue];
    return status;
}

-(CGFloat) readSceneStatus{
    NSUInteger status=0;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) valueChanged{
    _currentValue = (NSUInteger) super.slider.value;
     [super sendData:@(255 - _currentValue) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];
}

-(void) initLeftButtonPressed{
    if (super.type == TYPE_CONTROL && _hasStopTelegram) {
		[self createTimer:YES];
	}
	else if (super.type == TYPE_SCENE || !_hasStopTelegram){
        [super.slider setValue:0 animated:YES];
        [self valueChanged];
	}

    
}

-(void) initRightButtonPressed{
    if (super.type == TYPE_CONTROL && _hasStopTelegram) {
    	[self createTimer:NO];
	}
	else if (super.type == TYPE_SCENE || !_hasStopTelegram){
        [super.slider setValue:255 animated:YES];
        [self valueChanged];
    }
}

-(void) createTimer:(BOOL) leftButton{
    [_timerLongPressing invalidate];
    _longPressing = NO;
    _timerLongPressing = [NSTimer scheduledTimerWithTimeInterval:LONG_TOUCH target:self selector:@selector(timerFired:) userInfo:@(leftButton) repeats:NO];
}

-(void) timerFired:(NSTimer *) timer{
    NSNumber * info= [timer userInfo];
    _longPressing = YES;
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);
    [super sendData:info address:_movementTelegram.address dpt:_movementTelegram.dpt type:_movementTelegram.type delay:_movementTelegram.delay];
}

-(void) finLeftButtonPressed{
    [_timerLongPressing invalidate];
    if (!_longPressing) {
        [super sendData:@1U address:_stopTelegram.address dpt:_stopTelegram.dpt type:_stopTelegram.type delay:_stopTelegram.delay];
    }
}

-(void) finRightButtonPressed{
    [_timerLongPressing invalidate];
    if (!_longPressing) {
        [super sendData:@0U address:_stopTelegram.address dpt:_stopTelegram.dpt type:_stopTelegram.type delay:_stopTelegram.delay];
    }
}

- (void)viewDidUnload
{
    _actionTelegram = nil;
    _statusTelegram = nil;
	[super viewDidUnload];
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_actionTelegram.address,@"address",@(255 - _currentValue),@"value",_actionTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
