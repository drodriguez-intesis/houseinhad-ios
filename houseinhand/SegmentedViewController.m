//
//  SegmentedViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegmentedViewController.h"

@interface SegmentedViewController ()
@property (assign,nonatomic) BOOL segmentedMomentary;
@end

@implementation SegmentedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        // Custom initialization
		_segmentedMomentary = ![newDevice.hasStatus boolValue];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _centralSegmented.apportionsSegmentWidthsByContent = NO;
	_centralSegmented.momentary = _segmentedMomentary;
    // Do any additional setup after loading the view from its nib.
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    _centralSegmented = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
