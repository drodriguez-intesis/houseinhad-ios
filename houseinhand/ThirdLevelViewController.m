//
//  ThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "Device.h"
#define CORNER_RADIUS			12
#define CORNER_WIDTH			2
#define MARGIN_BACK				4
#define SIZE_BACK					35
@interface ThirdLevelViewController ()
@property (strong,nonatomic) IBOutlet UILabel *titleLabel;
@property (strong,nonatomic) NSString *customTitle;
@end

@implementation ThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_customTitle = newDevice.name;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	if (_customTitle) _titleLabel.text = _customTitle;
    [self createCorners];
	[self addBackButton];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(IBAction)dismissView{
    [self.delegate dismissThirdLevel:self];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	if (UIInterfaceOrientationIsLandscape(toInterfaceOrientation)) {
		[self.view setCenter:CGPointMake(self.view.window.center.y, self.view.window.center.x)];
	}
	else{
		[self.view setCenter:self.view.window.center];
	}
}

-(void) createCorners{
    _backgroundImage.layer.masksToBounds = YES;
    self.view.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1.0];
    self.view.layer.borderColor = [[UIColor colorWithWhite:0.25 alpha:1.0] CGColor];
    self.view.layer.borderWidth = 1.0;
    self.view.layer.masksToBounds = YES;
}

-(void) removeCorners{
	self.view.layer.borderWidth = 0.0;
}

-(void) addBackButton{
    UIButton *back = [UIButton buttonWithType:UIButtonTypeCustom];
    [back setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateNormal];
    [back setImage:[UIImage imageNamed:@"cancel"] forState:UIControlStateHighlighted];
	back.autoresizingMask = (UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin);
	[back setFrame:CGRectMake(self.view.bounds.size.width - SIZE_BACK - MARGIN_BACK, self.view.bounds.origin.y + MARGIN_BACK, SIZE_BACK, SIZE_BACK)];
    [back addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:back];
    back = nil;
}
@end
