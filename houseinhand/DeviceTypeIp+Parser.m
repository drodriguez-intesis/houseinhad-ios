//
//  DeviceTypeIp+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeIp+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1				@"image1"
#define KEY_IMAGE_2				@"image2"
#define KEY_TYPE						@"cameraType"
#define KEY_LOW_PARAM			@"lowParam"
#define KEY_HIGH_PARAM		@"highParam"
#define KEY_LOCAL_IP				@"localIp"
#define KEY_REMOTE_PORT		@"remotePort"
#define KEY_USERNAME			@"username"
#define KEY_PASSWORD			@"password"
#define KEY_SECURE					@"isSecure"
#define KEY_PULSE_WIDTH		@"pulseWidth"
#define KEY_VALUE_ON			@"valueOn"
#define KEY_VALUE_OFF			@"valueOff"
#define KEY_DOOR_PATH			@"doorPath" //jung tkm

@implementation DeviceTypeIp (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeIp *dev = (DeviceTypeIp *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.type = (info[KEY_TYPE] ? @([info[KEY_TYPE] integerValue]): dev.type);
	dev.username = (info[KEY_USERNAME] ? info[KEY_USERNAME]: dev.username);
	dev.password = (info[KEY_PASSWORD] ? info[KEY_PASSWORD]: dev.password);
	dev.localIp = (info[KEY_LOCAL_IP] ? info[KEY_LOCAL_IP]: dev.localIp);
	dev.remotePort = (info[KEY_REMOTE_PORT] ? @([info[KEY_REMOTE_PORT] integerValue]): dev.remotePort);
	dev.lowParam = (info[KEY_LOW_PARAM] ? info[KEY_LOW_PARAM]: dev.lowParam);
	dev.highParam = (info[KEY_HIGH_PARAM] ? info[KEY_HIGH_PARAM]: dev.highParam);
	dev.isSecure = (info[KEY_SECURE] ? @([info[KEY_SECURE] integerValue]): dev.isSecure);
	dev.pulseWidth = (info[KEY_PULSE_WIDTH] ? @([info[KEY_PULSE_WIDTH] floatValue]): dev.pulseWidth);
	dev.valueOff = (info[KEY_VALUE_OFF] ? @([info[KEY_VALUE_OFF] integerValue]): dev.valueOff);
	dev.valueOn = (info[KEY_VALUE_ON] ? @([info[KEY_VALUE_ON] integerValue]): dev.valueOn);
	dev.doorPath = (info[KEY_DOOR_PATH] ? info[KEY_DOOR_PATH]: dev.doorPath);

	return dev;
}

@end
