//
//  DeviceTypeMultimedia+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeMultimedia+Parser.h"
#import "Device+Parser.h"
#import "MultiCommand+Parser.h"

#define KEY_IMAGE_1				@"image1"
#define KEY_TYPE						@"moduleType"
#define KEY_LOCAL_IP				@"localIp"
#define KEY_LOCAL_PORT			@"localPort"
#define KEY_REMOTE_PORT		@"remotePort"
#define KEY_REMOTE_TYPE		@"remoteType"
#define KEY_COMMANDS			@"commands"

@implementation DeviceTypeMultimedia (Parser)

+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeMultimedia *dev = (DeviceTypeMultimedia *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.type = (info[KEY_TYPE] ? info[KEY_TYPE]: dev.type);
	dev.localIp = (info[KEY_LOCAL_IP] ? info[KEY_LOCAL_IP]: dev.localIp);
	dev.localPort = (info[KEY_LOCAL_PORT] ? @([info[KEY_LOCAL_PORT] integerValue]): dev.localPort);
	dev.remotePort = (info[KEY_REMOTE_PORT] ? @([info[KEY_REMOTE_PORT] integerValue]): dev.remotePort);
	dev.remoteType = (info[KEY_REMOTE_TYPE] ? @([info[KEY_REMOTE_TYPE] integerValue]): dev.remoteType);

	//add commands array
	if (info[KEY_COMMANDS] && [info[KEY_COMMANDS] count] >0){
		[info[KEY_COMMANDS] enumerateObjectsUsingBlock:^(NSDictionary * obj, NSUInteger idx, BOOL *stop) {
			MultiCommand *tel = [MultiCommand insertWithInfo:obj intoManagedObjectContext:moc];
			[dev addR_commandObject:tel];
		}];
	}
	
	return dev;
}

-(NSArray *) getOrderedCommands{
	if (!self.r_command || self.r_command.count == 0) return nil;
	return [[self.r_command allObjects] sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"delay" ascending:YES]]];
}

-(MultiCommand *) getCommandWithTag:(NSString *) tag{
	if (!self.r_command || self.r_command.count == 0) return nil;
	NSSet *filtered = [self.r_command filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"tag == %@",tag]];
	return [filtered anyObject];

}
@end
