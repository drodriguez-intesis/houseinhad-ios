//
//  DeviceType3Buttons.h
//  houseinhand
//
//  Created by Isaac Lozano on 21/01/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceType3Buttons : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSString * image3;
@property (nonatomic, retain) NSNumber * numButtons;
@property (nonatomic, retain) NSNumber * firstValue;
@property (nonatomic, retain) NSNumber * secondValue;
@property (nonatomic, retain) NSNumber * thirdValue;

@end
