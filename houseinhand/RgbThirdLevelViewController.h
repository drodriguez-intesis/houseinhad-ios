//
//  RgbThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/13/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "RSColorPickerView.h"
#import "RSBrightnessSlider.h"
#import "Device.h"

@class RgbThirdLevelViewController;
@protocol RgbThirdLevelViewControllerDelegate <NSObject>
-(void) rgbDidEndChoosingColorScene:(RgbThirdLevelViewController *) rgbThirdLevel colors:(NSArray *) colors;
@end

@interface RgbThirdLevelViewController : ThirdLevelViewController <UIScrollViewDelegate,RSColorPickerViewDelegate,ObjectsAnimations,UIGestureRecognizerDelegate>
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIView *colorView;
@property (nonatomic,strong) IBOutlet UIButton *paletteButton;
@property (nonatomic,strong) IBOutlet UIButton *favouriteButton;
@property (weak,nonatomic) id<RgbThirdLevelViewControllerDelegate>  delegateScene;
@property (nonatomic,strong) IBOutletCollection(UIButton) NSArray * predefinedColors;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice sceneColors:(NSArray *) colors;

@end
