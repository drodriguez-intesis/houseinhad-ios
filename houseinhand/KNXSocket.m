//
//  KNXSocket.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "KNXSocket.h"
#import "Config+Parser.h"
#import "Reachability.h"


#if DEBUG
#define SHOW_ALL_MESSAGES					0
#if  SHOW_ALL_MESSAGES
#define SHOW_PROTOCOL					0
#define SHOW_TX									1
#define SHOW_RX									0
#define SHOW_OTHERS							0
#define SHOW_TX_TUNNELLING			0
#define SHOW_RX_TUNNELLING			1
#define SHOW_STATUS_CHANGES			0
#define SHOW_DESCRIPTION_PROCESS	0
#else
#define SHOW_PROTOCOL					0
#define SHOW_TX									0
#define SHOW_RX									0
#define SHOW_OTHERS							0
#define SHOW_TX_TUNNELLING			0
#define SHOW_RX_TUNNELLING			0
#define SHOW_STATUS_CHANGES			0
#define SHOW_DESCRIPTION_PROCESS	0

#endif
#endif

#define MAX_UNRESPONDED_CON_REQUEST					3

#define TAG_SOCKET_DEFAULT									13
#define TAG_SOCKET_AUX_LOCAL								17
#define TAG_SOCKET_AUX_PUBLIC								18

#define MAX_TUNNELLING_PER_SEQ_NUMBER				1
#define MAX_DESCRIPTION_INTENTS								1

#define TIMER_TIME_NOW												0
#define TIMER_TIME_TO_RECEIVE_T_ACK						5
#define TIMER_CONNECTION_REQUEST							6
#define TIMER_CONNECTION_STATE_REQUEST				10
#define TIMER_TIME_TO_CLOSE									0.3
#define TIMER_TIME_TO_RESTART								5
#define TIMER_MIN_TIME_TO_RESTART							1
#define TIMER_TIME_TO_DESCRIPTION							10
#define TIMER_TIME_TO_CHANGE_IP								3
#define TIMER_TIME_TO_CONNECT_HOST						10
#define TIMER_DELAY_NEW_MESSAGE							.05
#define TIMER_TUNNELLING_TIMEOUT							4

#define HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO	0x0610
#define HPAI_LENGTH													0x08
#define IPV4_UDP															0x01

#define MESSAGE_DESCRIPTION_REQUEST  					100
#define CODE_DESCRIPTION_REQUEST 							0x0203
#define CODE_DESCRIPTION_RESPONSE						1026
#define LENGTH_DESCRIPTION_REQUEST 						0x000E

#define MESSAGE_CONNECT_REQUEST  						101
#define CODE_CONNECT_REQUEST 								0x0205
#define CODE_CONNECT_RESPONSE								1538
#define LENGTH_CONNECT_REQUEST 							0x001A

#define MESSAGE_CONNECTION_STATE_REQUEST			102
#define CODE_CONNECTION_STATE_REQUEST 				0x0207
#define CODE_CONNECTION_STATE_RESPONSE				2050
#define LENGTH_CONNECTION_STATE_REQUEST			0x0010

#define MESSAGE_DISCONNECT_REQUEST  					103
#define CODE_DISCONNECT_REQUEST 							0x0209
#define CODE_DISCONNECT_REQUEST_RESPONSE			2562
#define LENGTH_DISCONNECT_REQUEST 						0x0010

#define MESSAGE_DISCONNECT_RESPONSE  					104
#define CODE_DISCONNECT_RESPONSE  						0x020A
#define LENGTH_DISCONNECT_RESPONSE  					0x0008

#define MESSAGE_TUNNELLING_REQUEST	 					105
#define CODE_TUNNELLING_REQUEST  							0x0420
#define CODE_TUNNELLING_REQUEST_ACK					8196
#define LENGTH_BEFORE_CEMI_T_REQUEST					20

#define HEADER_LENGTH_TUNNELLING							0x04

#define MESSAGE_TUNNELLING_ACK			 					106
#define CODE_TUNNELLING_ACK		  							0x0421
#define CODE_TUNNELLING_ACK_REQUEST					8452
#define LENGTH_TUNNELLING_ACK								0x000A

#define CODE_TUNNELLING_DATA_REQ						0x11
#define CODE_TUNNELLING_DATA_CON						0x2E
#define CODE_TUNNELLING_DATA_IND							0x29

#define L_DATA_MESSAGE_CONTROL_FIELD					0xBCE0



@interface KNXSocket ()
typedef 	NS_ENUM (NSUInteger,ConnectionType) {CONNECTION_TYPE_REMOTE = 1,CONNECTION_TYPE_LOCAL = 0};

@property (strong,nonatomic) dispatch_queue_t rxQueue;
@property (nonatomic,strong) NSOperationQueue *tunnelQueue;
@property (nonatomic,strong) NSOperationQueue *controlQueue;
@property (nonatomic,strong) NSOperationQueue *retryQueue;

@property (nonatomic,strong) GCDAsyncUdpSocket *connectionSocket;
@property (nonatomic,assign) NSUInteger seqNumber;
@property (nonatomic,assign) NSUInteger descriptionCount;
@property (nonatomic,assign) BOOL descriptionRead;
@property (nonatomic,assign) BOOL hostConnected;
@property (nonatomic,assign) NSUInteger conStateReadCount;
@property (nonatomic,assign) BOOL natConnection;
@property (nonatomic,strong) NSMutableArray * auxSockets;
@property (nonatomic,strong) NSData * comChannel;
@property (nonatomic,strong) NSString * macAddress;
@property (nonatomic,strong) dispatch_source_t timerConnectionRequest;
@property (nonatomic,strong) dispatch_source_t timerConnectionStateRequest;
@end
@implementation KNXSocket

-(void) startCommunicationWithNetworks:(NSArray *) netArray{
	if (!netArray) return;
	[self startQueues];
	__block NSArray *netArr = netArray;
	[_controlQueue addOperationWithBlock:^{
		[self initVars];
		[self startCommNetworks:(NSArray *) netArr];
	}];
}

-(void) stopCommunication{
	[_controlQueue addOperationWithBlock:^{
		[self stopCommNetwork];
		[self finVars];
	}];
}

-(void) startQueues{
	if (_rxQueue == nil) {
		_rxQueue = dispatch_queue_create("rxQueue", NULL);
	}
	_controlQueue = [NSOperationQueue new];
    [_controlQueue setMaxConcurrentOperationCount:1];
	_tunnelQueue = [NSOperationQueue new];
    [_tunnelQueue setMaxConcurrentOperationCount:1];
    _retryQueue = [NSOperationQueue new];
    [_retryQueue setMaxConcurrentOperationCount:1];
}

-(void) stopQueues{
	_rxQueue =nil;
	[_tunnelQueue cancelAllOperations];
	_tunnelQueue = nil;
	[_controlQueue cancelAllOperations];
	_controlQueue = nil;
    [_retryQueue cancelAllOperations];
	_retryQueue = nil;
}

dispatch_source_t CreateDispatchTimer(uint64_t interval,
									  uint64_t leeway,
									  dispatch_queue_t queue,
									  dispatch_block_t block)
{
	dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER,
                                                     0, 0, queue);
	if (timer)
	{
		dispatch_source_set_timer(timer, dispatch_walltime(NULL, 0), interval, leeway);
		dispatch_source_set_event_handler(timer, block);
		dispatch_resume(timer);
	}
	return timer;
}

-(NSString *) getConnectedAddress{
	if (!_connectionSocket) return nil;
	return _connectionSocket.connectedHost;
}

-(NSNumber *) getConnectedPort{
	if (!_connectionSocket) return nil;
	return @(_connectionSocket.connectedPort);
}

-(NSString *) getConnectedMac{
	if (!_connectionSocket) return nil;
	return _macAddress;
}

-(BOOL) isLocalConnection{
	return !_natConnection;
}
-(void) initVars{
	_connectionSocket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
    [_connectionSocket setUserData:@{@"tag": @(TAG_SOCKET_DEFAULT)}];
	[_connectionSocket setIPv4Enabled:YES];
	[_connectionSocket setIPv6Enabled:NO];
	_auxSockets = [NSMutableArray array];
    _seqNumber = 0;
	_descriptionCount = 0;
    _natConnection = NO;
    _descriptionRead = NO;
	_hostConnected = NO;
	_conStateReadCount = 0;
	_connectionStatus = STATE_CONNECTION_DISCONNECTED;
	[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
}

-(void) stopTimers{
	if (_timerConnectionRequest != nil) dispatch_source_cancel(_timerConnectionRequest);
    if (_timerConnectionStateRequest != nil)  dispatch_source_cancel(_timerConnectionStateRequest);

}
-(void) finVars{
	_connectionStatus = STATE_CONNECTION_DISCONNECTED;
	_auxSockets = nil;
	_comChannel = nil;
    _macAddress = nil;
	_timerConnectionRequest = nil;
    _timerConnectionStateRequest = nil;
}

-(void) startCommNetworks:(NSArray *) netArray{
	NSArray *info = [netArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"nat == %d",CONNECTION_TYPE_REMOTE]];
	if (info && info.count >0) [self connectToHost:info[0]];
	NSArray *info2 = [netArray filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"nat == %d",CONNECTION_TYPE_LOCAL]];
	if (info2 && info2.count >0) [self connectToHost:info2[0]];
	_connectionStatus = STATE_CONNECTION_CONNECTING;
	[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];

}

-(void) stopCommNetwork{
    [_tunnelQueue cancelAllOperations];
	[self sendDisconnectRequest];
}

-(void) connectToHost:(NSDictionary *) network{
		NSUInteger tag = ([network[@"nat"] unsignedIntegerValue] == CONNECTION_TYPE_REMOTE ? TAG_SOCKET_AUX_PUBLIC : TAG_SOCKET_AUX_LOCAL);
		NSString * ip = network[@"ip"];
		NSUInteger port = [network[@"port"] unsignedIntegerValue];
		GCDAsyncUdpSocket *sock = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
		[sock setUserData:@{@"tag": @(tag)}];
		[sock setIPv4Enabled:YES];
		[sock setIPv6Enabled:NO];
		[sock connectToHost:ip onPort:port error:nil];
		[_auxSockets addObject:sock];
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TIMER_TIME_TO_CONNECT_HOST * NSEC_PER_SEC), _rxQueue, ^{
			if(!_hostConnected){
				_connectionStatus = STATE_CONNECTION_DISCONNECTED;
				[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
			}
	});
}

#pragma mark - Socket Delegates
- (void)udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address{
# if SHOW_OTHERS || SHOW_DESCRIPTION_PROCESS
    Reachability * reacheability = [Reachability reachabilityWithHostname:sock.connectedHost];
    NSLog(@"-----ST Connected to:%@ -- Reachability:%d",sock.connectedHost,reacheability.isReachableViaWiFi);
#endif
    if ((NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_DEFAULT && [address isEqualToData:sock.connectedAddress]) {
  	    [_connectionSocket beginReceiving:nil];
		[self sendConnectionRequest];
	}
    else if ((NSUInteger)([[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_AUX_LOCAL || [[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_AUX_PUBLIC) && [address isEqualToData:sock.connectedAddress]){
		_hostConnected = YES;
		[sock receiveOnce:nil];
#if SHOW_DESCRIPTION_PROCESS
		NSLog(@"Connected Socket tag:%@ --- Ip:%@, Local port:%d",[sock.userData valueForKey:@"tag"],sock.connectedHost,sock.localPort);
#endif
		[self sendDescriptionRequest:(NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue] socket:sock];
		_descriptionCount++;
		dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TIMER_TIME_TO_DESCRIPTION * NSEC_PER_SEC), _rxQueue, ^{
			if(!_descriptionRead){
				if (_descriptionCount < MAX_DESCRIPTION_INTENTS*2) {
					//TODO: REconnect if need
				//	[self restart:NO];
					//return;
				}
				_connectionStatus = STATE_CONNECTION_DISCONNECTED;
				[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
			}
		});
    }
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error	{
		//NSLog(@"Error is:%@",error);
}

-(void)udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
	if (tag == TAG_SOCKET_DEFAULT) {
		//NSLog(@"Error is:%@",error);
	}
}

- (void)udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext{
	
	[self parseReceivedData:data socket:sock];
	
    if ((NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_DEFAULT && [address isEqualToData:sock.connectedAddress]) {
# if SHOW_RX
        NSLog(@"RX -----> Received data:%@",data);
#endif
	}
    else if ((NSUInteger)([[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_AUX_LOCAL || [[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_AUX_PUBLIC) && [address isEqualToData:sock.connectedAddress]){
# if SHOW_RX
        NSLog(@"RX -----> Received description data:%@",data);
#endif
    }
}

-(void)udpSocketDidClose:(GCDAsyncUdpSocket *)sock withError:(NSError *)error{
	if ((NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue]== TAG_SOCKET_DEFAULT && [sock.connectedAddress isEqualToData:sock.connectedAddress]) {
		[self finVars];
		[self stopTimers];
		[self stopQueues];
	}
}

#pragma mark Parse Data

-(void) parseReceivedData:(NSData *) data socket:(GCDAsyncUdpSocket *) sock{
    if (data.length> 2) {
        char initialCode [2];
        initialCode[0] = (char) (HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO >>8);
        initialCode [1] = (char) HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO;
        NSData *subdata = [data subdataWithRange:NSMakeRange(0,2)];
        if ([subdata isEqualToData:[NSData dataWithBytesNoCopy:&initialCode length:sizeof(initialCode) freeWhenDone:NO]]) {
            [self decodeMessageType:[data subdataWithRange:NSMakeRange(2, data.length -2)] socket:sock];
        }
        subdata = nil;
    }
}

-(void) decodeMessageType:(NSData *) data socket:(GCDAsyncUdpSocket *) sock{
    if (data.length> 2) {
        NSUInteger messageCode = 0;
        [data getBytes:&messageCode length:2];
       	switch (messageCode) {
		case CODE_DESCRIPTION_RESPONSE:
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Description Response");
#endif
                if (!_descriptionRead) {
                    _descriptionRead = YES;
					_descriptionCount = 0;
					NSUInteger tag = (NSUInteger)[[sock.userData valueForKey:@"tag"] integerValue];
    				if (tag == TAG_SOCKET_AUX_PUBLIC) {
                        _natConnection = YES;
                    }
                    [self parseDescriptionResponse:[data subdataWithRange:NSMakeRange(4, data.length -4)]];
                    [self prepareConnectionRequest:sock.connectedHost port:sock.connectedPort];
					[_auxSockets enumerateObjectsUsingBlock:^(GCDAsyncUdpSocket * sock, NSUInteger idx, BOOL *stop) {
						[sock close];
						sock = nil;
					}];
                }
				break;
            case CODE_CONNECT_RESPONSE:
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Connect Response");
#endif
                if (_connectionStatus == STATE_CONNECTION_CONNECTING) {
                    [self parseConnectResponse:[data subdataWithRange:NSMakeRange(4, data.length -4)]];
                }
                break;
            case CODE_CONNECTION_STATE_RESPONSE:
# if SHOW_PROTOCOL
                NSLog(@"RX -----> ConnectionState Response");
#endif
				_conStateReadCount = 0;
                [self parseConnectionStateResponse:[data subdataWithRange:NSMakeRange(4, data.length -4)]];
				break;
            case CODE_DISCONNECT_REQUEST:
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Disconnect Request");
#endif
				[self.delegate knxSocket:self didChangeConnectionStatus:STATE_CONNECTION_DISCONNECTING];
				[self sendDisconnectResponse];
                //TODO Stop com
				//[self stopCommunication:NO];
				break;
            case CODE_DISCONNECT_REQUEST_RESPONSE:
                // reconnect ?????
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Disconnect Response");
#endif
				[self.delegate knxSocket:self didChangeConnectionStatus:STATE_CONNECTION_DISCONNECTED];
				[self stopQueues];
				break;
            case CODE_TUNNELLING_REQUEST_ACK:
# if SHOW_RX_TUNNELLING
                NSLog(@"RX -----> Received data tunnelling: %@",data);
#endif
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Tunnelling Request:%@",data);
#endif
				
                [self parseTunnellingRequest:[data subdataWithRange:NSMakeRange(4, data.length -4)]];
                break;
            case CODE_TUNNELLING_ACK_REQUEST:{
# if SHOW_RX_TUNNELLING
                NSLog(@"RX -----> Received data ack: %@",data);
#endif
                NSData * seqNumber = [data subdataWithRange:NSMakeRange(data.length-2, 1)];
                NSUInteger seqCount = 0;
                [seqNumber getBytes:&seqCount length:1];
                [_retryQueue cancelAllOperations];
				dispatch_after(dispatch_time(DISPATCH_TIME_NOW, TIMER_DELAY_NEW_MESSAGE * NSEC_PER_SEC), _rxQueue, ^{
                    [self incrementSequenceNumberFromNumber:seqCount];
					[_tunnelQueue setSuspended:NO];
				});
# if SHOW_PROTOCOL
                NSLog(@"RX -----> Tunnelling Ack");
#endif
			}
                break;
            default:
                break;
        }
    }
}


#pragma mark - Parsing data

-(void) parseDescriptionResponse:(NSData *) data{
	_macAddress= [NSString stringWithString:[self getStringFromData:[data subdataWithRange:NSMakeRange(18, 6)]]];
}

-(NSString *) getStringFromData:(NSData *) data{
	NSString *string = [NSString stringWithFormat:@"%@",data];
    NSString *string_1 =[string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *string_2 =[string_1 stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *string_3 =[string_2 stringByReplacingOccurrencesOfString:@">" withString:@""];
	return string_3;
}

-(NSString *) getIPFromData:(NSData *) data{
	unsigned char ip [4];
	[data getBytes:&ip length:sizeof(ip)];
	return [NSString stringWithFormat:@"%d.%d.%d.%d",ip[0],ip[1],ip[2],ip[3]];
}

-(NSString *) getPortFromData:(NSData *) data{
	unsigned char char_port [2];
	[data getBytes:&char_port length:sizeof(char_port)];
	NSUInteger port = (char_port [0]<<8) | char_port[1];
	return [NSString stringWithFormat:@"%d",port];
}

-(void) parseConnectResponse:(NSData *) data{
    char statusCode;
    NSData *status = [data subdataWithRange:NSMakeRange(1, 1)];
    [status getBytes:&statusCode length:sizeof(statusCode)];
    switch (statusCode) {
        case 0x00:
			_connectionStatus = STATE_CONNECTION_CONNECTED;
			[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
            _comChannel = [NSData dataWithData:[data subdataWithRange:NSMakeRange(0, 1)]];
            dispatch_source_cancel(_timerConnectionRequest);
            [self prepareConnectionStateRequest];
            break;
        default:
			_connectionStatus = STATE_CONNECTION_DISCONNECTED;
			[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
            //NSLog(@"Error connecting:%d",statusCode);
            break;
    }
    
}

-(void) parseConnectionStateResponse:(NSData *) data{
    char statusCode;
    NSData *status = [data subdataWithRange:NSMakeRange(1, 1)];
    [status getBytes:&statusCode length:sizeof(statusCode)];
    switch (statusCode) {
        case 0x00:
            // no error
			if (_connectionStatus != STATE_CONNECTION_CONNECTED) {
				_connectionStatus = STATE_CONNECTION_CONNECTED;
				[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
			}
            break;
        default:
            //tractament d'errors -> reconnect ????
			_connectionStatus = STATE_CONNECTION_DISCONNECTED;
			[self.delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), _rxQueue, ^{
                [_delegate knxSocketUnexpectedClosed:self];
            });
			//      [self restart:YES];
            break;
    }
}

-(void) parseDisconnectRequest:(NSData *) data{
    
}

-(void) parseDisconnectResponse:(NSData *) data{
    
}

-(void) parseTunnellingRequest:(NSData *) data{
    NSData *channel = [data subdataWithRange:NSMakeRange(1, 1)];
    if ([channel isEqualToData:_comChannel]) {
        char seqCount;
        NSData *sequenceCount = [data subdataWithRange:NSMakeRange(2, 1)];
        [sequenceCount getBytes:&seqCount length:sizeof(seqCount)];
		[self sendAckToKnx:(NSUInteger) seqCount];
        NSData *subdata = [data subdataWithRange:NSMakeRange(4, data.length - 4)];
        [self parseLData:subdata seqnum:seqCount];
    }
    else{
        //NSLog(@"Not for me!!!");
    }
}

-(void) parseLData:(NSData *)data seqnum:(char) seqCount{
    char mCode;
    NSData *messageCode = [data subdataWithRange:NSMakeRange(0, 1)];
    [messageCode getBytes:&mCode length:sizeof(mCode)];
	switch (mCode) {
        case CODE_TUNNELLING_DATA_REQ:

            // nop
            break;
        case CODE_TUNNELLING_DATA_CON:
            // nop
            break;
        case CODE_TUNNELLING_DATA_IND:{
            // update status
			char dType;
			NSData *dataType = [data subdataWithRange:NSMakeRange(10, 1)];
			[dataType getBytes:&dType length:sizeof(mCode)];
			if (dType != 0x00) {
				NSData *subdata = [data subdataWithRange:NSMakeRange(6, data.length - 6)];
				[self parseIndData:subdata];
			}
			}
            break;
        default:
            break;
    }
}

-(void) parseIndData:(NSData *) data{
	NSData *address = [data subdataWithRange:NSMakeRange(0, 2)];
	NSData *info = [data subdataWithRange:NSMakeRange(4, data.length - 4)];
	[self.delegate knxSocket:self didReceiveData:info address:address];
}

#pragma mark Send data

#pragma mark - Message types


-(void) sendDescriptionRequest:(BOOL) natMode socket:(GCDAsyncUdpSocket *) socket{
	[_controlQueue addOperationWithBlock:^{
		[self communicationRequest:MESSAGE_DESCRIPTION_REQUEST nat:natMode socket:socket];
	}];
}

-(void) prepareConnectionRequest:(NSString *) ip port:(NSUInteger) port{
	//NSLog(@"IP:%@ -- port:%d",ip,port);
	if (_connectionSocket.isConnected) {
		[_connectionSocket close];
	}
	[_connectionSocket connectToHost:ip onPort:port error:nil];
    
}

-(void) sendConnectionRequest{
	_timerConnectionRequest = CreateDispatchTimer(TIMER_CONNECTION_REQUEST * NSEC_PER_SEC,
												  1ull * NSEC_PER_SEC,
												  _rxQueue,
												  ^{
													  [_controlQueue addOperationWithBlock:^{
														  [self communicationRequest:MESSAGE_CONNECT_REQUEST];
													  }];
												  });
}

-(void) prepareConnectionStateRequest{
	_timerConnectionStateRequest = CreateDispatchTimer(TIMER_CONNECTION_STATE_REQUEST * NSEC_PER_SEC,
													   1ull * NSEC_PER_SEC,
													   _rxQueue,
													   ^{
														   if (_connectionStatus == STATE_CONNECTION_CONNECTED) {
															   if (_conStateReadCount < MAX_UNRESPONDED_CON_REQUEST) {
																//   NSLog(@"Conn read count:%d",_conStateReadCount);
																   [self sendConnectionStateRequest];
																   _conStateReadCount ++;
															   }
															   else{
																   _connectionStatus = STATE_CONNECTION_DISCONNECTED;
																   [_delegate knxSocket:self didChangeConnectionStatus:_connectionStatus];
                                                                   [_delegate knxSocketUnexpectedClosed:self];
															   }
														   }
													   });
}

-(void) sendConnectionStateRequest{
	[_controlQueue addOperationWithBlock:^{
		[self communicationRequest:MESSAGE_CONNECTION_STATE_REQUEST];
	}];
}

-(void) sendDisconnectRequest{
	[self communicationRequest:MESSAGE_DISCONNECT_REQUEST];
}

-(void) sendDisconnectResponse{
	[_controlQueue addOperationWithBlock:^{
		[self communicationRequest:MESSAGE_DISCONNECT_RESPONSE];
	}];
}

-(void) sendInfoToKnx:(NSData *) info address:(NSNumber *) address{
	/*	if ([[info valueForKey:@"TYPE"] integerValue] == 0 ) {
	 return;
	 }*/
#if SHOW_TX_TUNNELLING
	NSLog(@"TX -----> Sending tunnelling data:%@",info);
#endif
	__block NSData *inf = info;
	__block NSNumber *adr = address;
	__block KNXStatus connectionSt = _connectionStatus;
	[_tunnelQueue addOperationWithBlock:^{
		//TODO remove while (use setSuspended instead)
		while (connectionSt != STATE_CONNECTION_CONNECTED);
		BOOL sent = NO;
		sent = [self tunnellingRequestInfo:inf address:adr.unsignedIntegerValue  size:inf.length seqNum:_seqNumber];
		if (sent){
            NSBlockOperation *operation = [[NSBlockOperation alloc] init];
            __weak NSBlockOperation *weakOperation = operation;
            [operation addExecutionBlock: ^ {
                sleep(3);
                if (!weakOperation.isCancelled) {
                    [_tunnelQueue setSuspended:NO];
                }
            }];
            [_retryQueue addOperation:weakOperation];
            [_tunnelQueue setSuspended:YES];
        }
	}];
}

-(void) sendAckToKnx:(NSUInteger) seqNum{
	[_controlQueue addOperationWithBlock:^{
		[self tunnellingAckWithSeqNumber:seqNum socket:_connectionSocket];
	}];
}

-(void) incrementSequenceNumberFromNumber:(NSUInteger) number{
    if (number < UCHAR_MAX) {
        _seqNumber = number +1;
    }
    else{
        _seqNumber = 0;
    }
}

#pragma mark -


-(void) communicationRequest:(NSUInteger) messageType{
	[self communicationRequest:messageType nat:0 socket:_connectionSocket];
}

-(void) communicationRequest:(NSUInteger) messageType nat:(NSUInteger) opt socket:(GCDAsyncUdpSocket *) sock{
	if ([self canSendToBus:messageType socket:sock]) {
        NSMutableData *data = [NSMutableData data];
        @synchronized (data){
        	[self createHeader:data type:messageType size:0];
        	[self createDataHeader:data type:messageType seqNum:0 forceNat:opt socket:sock];
#if SHOW_TX
			NSLog(@"TX -----> Sending data:%@",data);
#endif
			[sock sendData:data withTimeout:-1 tag:TAG_SOCKET_DEFAULT];
		}
        data= nil;
    }
}

-(void) tunnellingAckWithSeqNumber:(NSUInteger) seqNum socket:(GCDAsyncUdpSocket *) sock{
	if ([self canSendToBus:MESSAGE_TUNNELLING_ACK socket:sock]) {
        NSMutableData *data = [NSMutableData data];
        @synchronized (data){
        	[self createHeader:data type:MESSAGE_TUNNELLING_ACK size:0];
        	[self createDataHeader:data type:MESSAGE_TUNNELLING_ACK seqNum:seqNum forceNat:0 socket:sock];
#if SHOW_TX
			NSLog(@"TX -----> Sending ack:%@",data);
#endif
			[sock sendData:data withTimeout:-1 tag:MESSAGE_TUNNELLING_ACK];
		}
        data= nil;
    }
}

-(BOOL) tunnellingRequestInfo:(NSData *) info address:(NSUInteger) address size:(NSUInteger) size seqNum:(NSUInteger) seqNum{
	BOOL sent = NO;
    if ([self canSendToBus:MESSAGE_TUNNELLING_REQUEST socket:_connectionSocket]) {
        NSMutableData *data = [NSMutableData data];
        @synchronized (data){
        	[self createHeader:data type:MESSAGE_TUNNELLING_REQUEST size:info.length];
        	[self createDataHeader:data type:MESSAGE_TUNNELLING_REQUEST seqNum:seqNum forceNat:0 socket:_connectionSocket];
            [self createTunnelRequestBody:data info:info address:address];
			
#if SHOW_TX
			NSLog(@"TX -----> Sending tunreq:%@",data);
#endif
			[_connectionSocket sendData:data withTimeout:-1 tag:MESSAGE_TUNNELLING_REQUEST];
		}
        data= nil;
		sent = YES;
    }
	return sent;
}

#pragma mark Data headers

-(BOOL) canSendToBus:(NSUInteger) messageType socket:(GCDAsyncUdpSocket *) sock{
    if( sock.isConnected	&& (messageType != (MESSAGE_TUNNELLING_REQUEST | MESSAGE_TUNNELLING_ACK) || (messageType == (MESSAGE_TUNNELLING_REQUEST | MESSAGE_TUNNELLING_ACK) && _connectionStatus == STATE_CONNECTION_CONNECTED))) {
        return YES;
    }
    return NO;
}

-(void) createHeader:(NSMutableData *) data type:(NSUInteger) messageType size:(NSUInteger) size{
	char buffer [6];
    buffer[0] = (char) (HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO >>8);
    buffer [1] = (char) HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO;
    switch (messageType) {
		case MESSAGE_DESCRIPTION_REQUEST:
            buffer [2] = (char) (CODE_DESCRIPTION_REQUEST >>8);
            buffer [3] = (char) CODE_DESCRIPTION_REQUEST;
            buffer [4] = (char) (LENGTH_DESCRIPTION_REQUEST >>8);
            buffer [5] = (char) LENGTH_DESCRIPTION_REQUEST;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Description Request ");
#endif
            break;
        case MESSAGE_CONNECT_REQUEST:
            buffer [2] = (char) (CODE_CONNECT_REQUEST >>8);
            buffer [3] = (char) CODE_CONNECT_REQUEST;
            buffer [4] = (char) (LENGTH_CONNECT_REQUEST >>8);
            buffer [5] = (char) LENGTH_CONNECT_REQUEST;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Connect Request");
#endif
			break;
        case MESSAGE_CONNECTION_STATE_REQUEST:
            buffer [2] = (char) (CODE_CONNECTION_STATE_REQUEST >>8);
            buffer [3] = (char) CODE_CONNECTION_STATE_REQUEST;
            buffer [4] = (char) (LENGTH_CONNECTION_STATE_REQUEST >>8);
            buffer [5] = (char) LENGTH_CONNECTION_STATE_REQUEST;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> ConnectionState Request");
#endif
			break;
        case MESSAGE_DISCONNECT_REQUEST:
            buffer [2] = (char) (CODE_DISCONNECT_REQUEST >>8);
            buffer [3] = (char) CODE_DISCONNECT_REQUEST;
            buffer [4] = (char) (LENGTH_DISCONNECT_REQUEST >>8);
            buffer [5] = (char) LENGTH_DISCONNECT_REQUEST;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Disconnect Request");
#endif
			break;
        case MESSAGE_DISCONNECT_RESPONSE:
            buffer [2] = (char) (CODE_DISCONNECT_RESPONSE >>8);
            buffer [3] = (char) CODE_DISCONNECT_RESPONSE;
            buffer [4] = (char) (LENGTH_DISCONNECT_RESPONSE >>8);
            buffer [5] = (char) LENGTH_DISCONNECT_RESPONSE;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Disconnect Response");
#endif
			break;
        case MESSAGE_TUNNELLING_REQUEST:
            buffer [2] = (char) (CODE_TUNNELLING_REQUEST >>8);
            buffer [3] = (char) CODE_TUNNELLING_REQUEST;
            NSUInteger length = LENGTH_BEFORE_CEMI_T_REQUEST + size;
            buffer [4] = (char) (length >>8);
            buffer [5] = (char) length;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Tunnelling Request");
#endif
			break;
        case MESSAGE_TUNNELLING_ACK:
            buffer [2] = (char) (CODE_TUNNELLING_ACK >>8);
            buffer [3] = (char) CODE_TUNNELLING_ACK;
            buffer [4] = (char) (LENGTH_TUNNELLING_ACK >>8);
            buffer [5] = (char) LENGTH_TUNNELLING_ACK;
#if SHOW_PROTOCOL
            NSLog(@"TX -----> Tunnelling Ack");
#endif
			break;
        default:
            break;
    }
    [data appendBytes:buffer length:sizeof(buffer)];
}

-(void) createDataHeader:(NSMutableData *) data type:(NSUInteger) messageType seqNum:(NSUInteger) seqNum forceNat:(BOOL) forceNat socket:(GCDAsyncUdpSocket *) sock{
    char nop = 0x00;
    char  buffer [2];
    switch (messageType) {
		case MESSAGE_DESCRIPTION_REQUEST:
            buffer [0] = HPAI_LENGTH;
            buffer [1] = IPV4_UDP;
            [data appendBytes:buffer length:sizeof(buffer)];
			[self setControlEndpoint:data forceNat:forceNat socket:sock];
            break;
        case MESSAGE_CONNECT_REQUEST:
            buffer [0] = HPAI_LENGTH;
            buffer [1] = IPV4_UDP;
            [data appendBytes:buffer length:sizeof(buffer)];
			[self setControlEndpoint:data forceNat:NO socket:sock];
			buffer [0] = HPAI_LENGTH;
            buffer [1] = IPV4_UDP;
            [data appendBytes:buffer length:sizeof(buffer)];
			[self setControlEndpoint:data forceNat:NO socket:sock];
            [self setConnectionTypeCode:data];
            break;
        case MESSAGE_CONNECTION_STATE_REQUEST:
            [data appendData:_comChannel];
            [data appendBytes:&nop length:sizeof(nop)];
            buffer [0] = HPAI_LENGTH;
            buffer [1] = IPV4_UDP;
            [data appendBytes:buffer length:sizeof(buffer)];
			[self setControlEndpoint:data forceNat:NO socket:sock];
            break;
        case MESSAGE_DISCONNECT_REQUEST:
            [data appendData:_comChannel];
            [data appendBytes:&nop length:sizeof(nop)];
            buffer [0] = HPAI_LENGTH;
            buffer [1] = IPV4_UDP;
            [data appendBytes:buffer length:sizeof(buffer)];
			[self setControlEndpoint:data forceNat:NO socket:sock];
            break;
        case MESSAGE_DISCONNECT_RESPONSE:
            [data appendData:_comChannel];
            [data appendBytes:&nop length:sizeof(nop)];
            break;
        case MESSAGE_TUNNELLING_REQUEST:
            buffer [0] = HEADER_LENGTH_TUNNELLING;
            [data appendBytes:buffer length:1];
            [data appendData:_comChannel];
            buffer [0] = _seqNumber;
            [data appendBytes:buffer length:1];
            [data appendBytes:&nop length:sizeof(nop)];
            break;
        case MESSAGE_TUNNELLING_ACK:
            buffer [0] = HEADER_LENGTH_TUNNELLING;
            [data appendBytes:buffer length:1];
            [data appendData:_comChannel];
            buffer [0] = seqNum;
            [data appendBytes:buffer length:1];
            [data appendBytes:&nop length:sizeof(nop)];
            break;
        default:
            break;
    }
}

-(void) createTunnelRequestBody:(NSMutableData *) data info:(NSData *) info address:(NSUInteger) address{
    char nop = 0x00;
    char  buffer[2];
	buffer [0]= CODE_TUNNELLING_DATA_REQ;
	[data appendBytes:&buffer length:1];
	[data appendBytes:&nop length:sizeof(nop)];
	buffer [0] = (char) (L_DATA_MESSAGE_CONTROL_FIELD >>8);
	buffer [1] = (char) L_DATA_MESSAGE_CONTROL_FIELD;
	[data appendBytes:&buffer length:2];
	[data appendBytes:&nop length:sizeof(nop)];					//source address_high
	[data appendBytes:&nop length:sizeof(nop)];					//source address_low
	buffer [0] = (char) (address >>8);										//dest address_high
	buffer [1] = (char) address;												//dest address_low
	[data appendBytes:&buffer length:2];
	buffer [0] = (char) info.length;										            //npdu
	[data appendBytes:&buffer length:1];
	[data appendBytes:&nop length:sizeof(nop)];
	[data appendData:info];
#if SHOW_TX_TUNNELLING
	NSLog(@"TX -----> Sending data tunnelling:%@",data);
#endif
}

-(void) setConnectionTypeCode:(NSMutableData *) data{
    char buffer [4];
    buffer [0] = 0x04;
    buffer [1] = 0x04;
    buffer [2] = 0x02;
    buffer [3] = 0x00;
    [data appendBytes:buffer length:sizeof(buffer)];
	
}

-(void) setControlEndpoint:(NSMutableData *) data forceNat:(BOOL) nat socket:(GCDAsyncUdpSocket *) sock{
	uint16_t localPort = sock.localPort_IPv4;
	//NSLog(@"Local ip is:%@",sock.localHost_IPv4);
    char buffer [6];
    NSString * aux_ip = [self getIPAddress];
    if (nat || _natConnection) {
        buffer[0]=0x00;
        buffer[1]=0x00;
        buffer[2]=0x00;
        buffer[3]=0x00;
        buffer[4]=0x00;
        buffer[5]=0x00;
    }
    else {
# if SHOW_OTHERS
        NSLog(@"Using private IP");
#endif
        NSString *aux;
        NSScanner * scan = [NSScanner scannerWithString:aux_ip];
        [scan scanUpToString:@"." intoString:&aux];
        buffer[0] = [aux intValue];
        [scan setScanLocation:[scan scanLocation]+1];
        [scan scanUpToString:@"." intoString:&aux];
        buffer[1] = [aux intValue];
        [scan setScanLocation:[scan scanLocation]+1];
        [scan scanUpToString:@"." intoString:&aux];
        buffer[2] = [aux intValue];
        [scan setScanLocation:[scan scanLocation]+1];
        [scan scanUpToString:@"/0" intoString:&aux];
        buffer[3] = [aux intValue];
    	scan = nil;
        
        buffer [4] = (char)(localPort >> 8);
        buffer [5] = (char) localPort;
    }
    [data appendBytes:buffer length:sizeof(buffer)];
}

- (NSString *)getIPAddress
{
    NSString *address = @"0.0.0.0";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
    
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0)
    {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while(temp_addr != NULL)
        {
            if(temp_addr->ifa_addr->sa_family == AF_INET)
            {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if([@(temp_addr->ifa_name) isEqualToString:@"en0"])
                {
                    // Get NSString from C String
                    address = @(inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr));
                }
            }
            temp_addr = temp_addr->ifa_next;
        }
    }
    // Free memory
    freeifaddrs(interfaces);
    
    return address;
}
@end
