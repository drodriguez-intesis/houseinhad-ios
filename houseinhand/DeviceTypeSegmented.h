//
//  DeviceTypeSegmented.h
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeSegmented : Device

@property (nonatomic, retain) NSString * fifthText;
@property (nonatomic, retain) NSNumber * fifthValue;
@property (nonatomic, retain) NSString * firstText;
@property (nonatomic, retain) NSNumber * firstValue;
@property (nonatomic, retain) NSString * fourthText;
@property (nonatomic, retain) NSNumber * fourthValue;
@property (nonatomic, retain) NSNumber * numSegments;
@property (nonatomic, retain) NSString * secondText;
@property (nonatomic, retain) NSNumber * secondValue;
@property (nonatomic, retain) NSString * thirdText;
@property (nonatomic, retain) NSNumber * thirdValue;
@property (nonatomic, retain) NSNumber * readOnly;

@end
