//
//  TypePulseViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/29/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypePulseViewController.h"
#import "DeviceTypePulse+Parser.h"

@interface TypePulseViewController ()
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) NSNumber *valueOn;
@property (strong,nonatomic) NSNumber *valueOff;
@property (strong,nonatomic) NSNumber *pulseWidth;

@end

@implementation TypePulseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypePulse *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_valueOff = newDevice.valueOff;
		_valueOn = newDevice.valueOn;
		_pulseWidth = newDevice.pulseWidth;
    	[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypePulse*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		_valueOff = newDevice.valueOff;
		_valueOn = newDevice.valueOn;
		_pulseWidth = newDevice.pulseWidth;
		[self readControlStatus];
		[self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];
	
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _actionTelegram = nil;
}

-(BOOL) readControlStatus{
    BOOL status=NO;
	_actionTelegram = [super.device getTelegramWithCode:0];
	
    return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=NO;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
}

-(void) buttonPressed:(id) sender{
    // send value
   [super sendData:_valueOn address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:@0.0];
	 [super sendData:_valueOff address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_pulseWidth];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = @{
										  @"address": _actionTelegram.address,
			@"value": _valueOn,
			@"dpt": _actionTelegram.dpt,
			@"stopValue":_valueOff,
			@"stopDelay": _pulseWidth
			};
		NSDictionary *info2 = @{
						  @"address": _actionTelegram.address,
		@"value": _valueOff,
		@"dpt": _actionTelegram.dpt,
		@"delay": _pulseWidth
		};
		return [NSArray arrayWithObjects:info1,info2, nil];
	}
	return nil;
}

@end
