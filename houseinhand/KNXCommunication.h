//
//  KNXCommunication.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/16/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KNXSocket.h"
#import "KNXDiscover.h"
#import "KNXDescription.h"
typedef void (^KNXDiscoverBlock)( NSArray * foundItems);

@interface KNXCommunication : NSObject <KNXSocketDelegate,KNXDiscoverDelegate>
@property (copy,nonatomic) KNXDiscoverBlock delegateBlock;

+ (id)sharedInstance;
+(void) startCommunication;
+(void) stopCommunication;
- (void)setParentContext:(NSManagedObjectContext *) parentContext;
-(void) sendInfoToKnx:(NSData *) info address:(NSNumber *) address objectId:(NSManagedObjectID *) objectId;
-(void) discoverKNXGatewaysUsingBlock:(KNXDiscoverBlock)delegateBlock;
-(void) getMacInstallationIp:(NSString *) ip port:(NSUInteger) port usingBlock:(KNXDescriptBlock)delegateBlock;

-(NSString *) getConnectedIp;
-(NSString *) getRemoteIp;
-(NSString *) getMacAddress;
-(BOOL) connectionIsLocal;
@end
