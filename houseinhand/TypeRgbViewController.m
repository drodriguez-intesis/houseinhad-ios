//
//  TypeRgbViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeRgbViewController.h"
#import "DeviceTypeLight.h"
#import "SceneTelegram.h"
@interface TypeRgbViewController ()
@property (strong,nonatomic) Telegram *redActionTelegram;
@property (strong,nonatomic) Telegram *greenActionTelegram;
@property (strong,nonatomic) Telegram *blueActionTelegram;
@property (strong,nonatomic) SceneTelegram * redSceneTelegram,*greenSceneTelegram,*blueSceneTelegram;
@property (assign,nonatomic) CGFloat rCurrent,gCurrent,bCurrent;
@property (assign,nonatomic) BOOL thirdLevelShowed;
@end

@implementation TypeRgbViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		_thirdLevelShowed = NO;
		[self readControlStatus];
		[self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setActions];
	if (super.type == TYPE_SCENE) {
		super.leftButton.hidden = YES;
		super.leftButton.userInteractionEnabled = NO;
		super.rightButton.hidden = YES;
		super.rightButton.userInteractionEnabled = NO;
		[self startCurrentColors];
	}
}

-(void) startCurrentColors{
	_rCurrent = (_redSceneTelegram ? _redSceneTelegram.value.floatValue : 255.0);
	_gCurrent = (_greenSceneTelegram ? _greenSceneTelegram.value.floatValue : 255.0);
	_bCurrent =(_blueSceneTelegram ? _blueSceneTelegram.value.floatValue : 255.0);

}

- (void)viewDidUnload
{
	_redActionTelegram = nil;
    _greenActionTelegram = nil;
	_blueActionTelegram = nil;
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(CGFloat) readControlStatus{
	_redActionTelegram = [super.device getTelegramWithCode:3];
	_greenActionTelegram = [super.device getTelegramWithCode:4];
	_blueActionTelegram = [super.device getTelegramWithCode:5];
	return 1;
}

-(void) readSceneStatus{
	_redSceneTelegram= [super.scene getTelegramWithAddress:_redActionTelegram.address.integerValue];
	_greenSceneTelegram = [super.scene getTelegramWithAddress:_greenActionTelegram.address.integerValue];
	_blueSceneTelegram = [super.scene getTelegramWithAddress:_blueActionTelegram.address.integerValue];

	if (_redSceneTelegram && _greenSceneTelegram && _blueSceneTelegram) {
		super.widgetSceneActive = YES;
	}
}


-(void) setActions{
    [super.leftButton addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.rightButton addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.centerButton addTarget:self action:@selector(centerButtonPressed) forControlEvents:UIControlEventTouchDown];
}


-(void) leftButtonPressed{
	[self performSelector:@selector(sendOffTelegram:) withObject:_redActionTelegram afterDelay:0];
	[self performSelector:@selector(sendOffTelegram:) withObject:_greenActionTelegram afterDelay:.1];
	[self performSelector:@selector(sendOffTelegram:) withObject:_blueActionTelegram afterDelay:.2];
}

-(void) rightButtonPressed{
	[self performSelector:@selector(sendOnTelegram:) withObject:_redActionTelegram afterDelay:0];
	[self performSelector:@selector(sendOnTelegram:) withObject:_greenActionTelegram afterDelay:.1];
	[self performSelector:@selector(sendOnTelegram:) withObject:_blueActionTelegram afterDelay:.2];
}

-(void) sendOffTelegram:(Telegram *) tel{
	[self sendButtonData:0 ofTelegram:tel];
}

-(void) sendOnTelegram:(Telegram *) tel{
	[self sendButtonData:255 ofTelegram:tel];
}
-(void) sendButtonData:(NSUInteger) value ofTelegram:(Telegram *) telegram{
    [super sendData:@(value) address:telegram.address dpt:telegram.dpt type:telegram.type delay:telegram.delay];
}

-(void) centerButtonPressed{
	if (super.type == TYPE_SCENE) {
		_sceneColors = @[@(_rCurrent),@(_gCurrent),@(_bCurrent)];
		[self enableSceneWidget];
	}
    [self.delegate deviceViewControllerNextLevel:self];
}

-(void) loadHasStatus{
	// doesn't have status on this level
}

-(void) rgbDidEndChoosingColorScene:(RgbThirdLevelViewController *)rgbThirdLevel colors:(NSArray *)colors{
	_rCurrent = [colors[0] unsignedIntegerValue];
	_gCurrent = [colors[1] unsignedIntegerValue];
	_bCurrent = [colors[2] unsignedIntegerValue];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		return @[
		   @{@"address": _redActionTelegram.address,
	   			@"value": @(_rCurrent),
				@"dpt": _redActionTelegram.dpt},
	 		@{@"address": _greenActionTelegram.address,
				@"value": @(_gCurrent),
				@"dpt": _greenActionTelegram.dpt},
	 		@{@"address": _blueActionTelegram.address,
				@"value": @(_bCurrent),
				@"dpt": _blueActionTelegram.dpt}
			];
	}
	return nil;
}

@end
