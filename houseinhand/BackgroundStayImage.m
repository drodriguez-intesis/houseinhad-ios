//
//  BackgroundStayImage.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "BackgroundStayImage.h"
#import "Stay.h"


@implementation BackgroundStayImage

@dynamic backgroundImage;
@dynamic r_stay;

@end
