//
//  NetworkSettingsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/11/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "NetworkSettingsViewController.h"
#import "Network.h"
#import "Settings.h"
#import "Config+Parser.h"
#import "ScrollThirdLevelViewController.h"
#import "KNXCommunication.h"
#import "SettingsViewController.h"
#import "KNXCommunication.h"
#import "AppDelegate.h"

@interface NetworkSettingsViewController ()
@property (strong,nonatomic) MBProgressHUD * hud;
@property (strong,nonatomic) Config * activeConfig;
@property (weak, nonatomic) IBOutlet UILabel *searchLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *modeLabel;
@property (weak, nonatomic) IBOutlet UILabel *localNetLabel;
@property (weak, nonatomic) IBOutlet UILabel *remoteNetLabel;
@property (weak,nonatomic) UITextField *activeField;
-(IBAction)discoverGateways;
-(IBAction)storeLog:(UIButton *) button;
@end

@implementation NetworkSettingsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil activeConfig:(Config *) config
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_activeConfig = config;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	_needRestartStartCom = NO;
	[self setTranslations];
	[self setCommunicationValues];
    // Do any additional setup after loading the view from its nib.
}

-(void) setTranslations{
	_titleName.text = NSLocalizedStringFromTableInBundle(@"Network", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view");
	_searchLabel.text = NSLocalizedStringFromTableInBundle(@"Search KNX/IP Gateway", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view");
	_modeLabel.text = NSLocalizedStringFromTableInBundle(@"Mode", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view");
	[_modeSegmented setTitle:NSLocalizedStringFromTableInBundle(@"Auto", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view") forSegmentAtIndex:0];
	[_modeSegmented setTitle:NSLocalizedStringFromTableInBundle(@"Local", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view") forSegmentAtIndex:1];
	[_modeSegmented setTitle:NSLocalizedStringFromTableInBundle(@"Remote", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view") forSegmentAtIndex:2];
	_localNetLabel.text = NSLocalizedStringFromTableInBundle(@"Local Network", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view");
	_remoteNetLabel.text = NSLocalizedStringFromTableInBundle(@"Remote Network", nil, CURRENT_LANGUAGE_BUNDLE, @"network advanced view");
}
-(void) setCommunicationValues{
	[_modeSegmented setSelectedSegmentIndex:_activeConfig.r_settings.netMode.unsignedIntegerValue];
	Network *net = [_activeConfig getNetworkOfType:CONNECT_TO_PRIVATE_IP];
	if (net) {
		_localIp.text = net.ip;
		_localPort.text = [net.port stringValue];
		net = nil;
	}
	net = [_activeConfig getNetworkOfType:CONNECT_TO_PUBLIC_IP];
	if (net) {
		_publicIp.text = net.ip;
		_publicPort.text = [net.port stringValue];
	}
}

-(void)viewDidUnload{
	_hud = nil;
	_localIp = nil;
	_localPort	=nil;
	_publicIp	= nil;
	_publicPort	= nil;
	_activeConfig = nil;
	[super viewDidUnload];
}

-(void) textFieldDidBeginEditing:(UITextField *)textField{
	[self needsRestartAfterEnd];
	if (!_activeField) {
		[UIView animateWithDuration:0.3 animations:^{
			self.view.transform = CGAffineTransformMakeTranslation(0, -140);
		}];
	}
	_activeField = textField;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_activeField resignFirstResponder];
}

-(void) needsRestartAfterEnd{
	if (!_needRestartStartCom) {
		[self stopCommunication];
		_needRestartStartCom = YES;
	}
}

-(void) stopCommunication{
#if KNX_CONNECTION_ENABLED
	[KNXCommunication stopCommunication];
#endif
}


-(void) textFieldDidEndEditing:(UITextField *)textField{
	[UIView animateWithDuration:0.3 delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.transform = CGAffineTransformIdentity;

	} completion:nil];
	self.activeField = nil;
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)discoverGateways{
	[self showHud];
	[[KNXCommunication sharedInstance] discoverKNXGatewaysUsingBlock:^(NSArray *foundItems) {
		[self discoverFinished:foundItems];
	}];
}

-(IBAction)segmentedModeDidChangeValue{
	[self needsRestartAfterEnd];
}

-(void) showHud{
	_hud = [[MBProgressHUD alloc] initWithView:self.view.window];
    [self.view.window addSubview:_hud];
	_hud.mode = MBProgressHUDModeDeterminate;
    _hud.labelText = NSLocalizedStringFromTableInBundle(@"Loading", nil, CURRENT_LANGUAGE_BUNDLE, @"progress hud");
	_hud.color = [UIColor orangeColor];
	_hud.detailsLabelText = NSLocalizedStringFromTableInBundle(@"Searching KNX/IP gateways", nil, CURRENT_LANGUAGE_BUNDLE, @"progress hud");
    _hud.dimBackground = YES;
	self.parentViewController.view.userInteractionEnabled = NO;
	_hud.delegate = self;
	//[_hud show:YES];
	[_hud showWhileExecuting:@selector(progressTask) onTarget:self withObject:nil animated:YES];
}

- (void)hudWasHidden:(MBProgressHUD *)hud {
	self.parentViewController.view.userInteractionEnabled = YES;
	[_hud removeFromSuperview];
	_hud = nil;
}

-(void) discoverFinished:(NSArray *) info{
	[_hud hide:YES];
	
	if ([info count] >0) {
		[_networkDelegate networkSettings:self didReadDiscoverResults:info];
	}
	else{
		_hud = [[MBProgressHUD alloc] initWithView:self.view];
		[self.view.superview addSubview:_hud];
		_hud.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"37x-Checkmark.png"]];
		_hud.mode = MBProgressHUDModeCustomView;
		_hud.color = [UIColor orangeColor];
		//_hud.dimBackground = YES;
		_hud.detailsLabelText = NSLocalizedStringFromTableInBundle(@"No KNX/IP gateways found", nil, CURRENT_LANGUAGE_BUNDLE, @"progress hud");
		[_hud show:YES];
		[_hud hide:YES afterDelay:3];
	}
	
}
- (void)progressTask {
	// This just increases the progress indicator in a loop
	float progress = 0.0f;
	while (progress < 1.0f) {
		progress += 0.001f;
		_hud.progress = progress;
		usleep(3000);
	}
}

-(IBAction)storeLog:(UIButton *)button{
	
}
@end
