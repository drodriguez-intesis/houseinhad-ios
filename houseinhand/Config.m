//
//  Config.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Config.h"
#import "Scene.h"
#import "Settings.h"
#import "Stay.h"


@implementation Config

@dynamic isActive;
@dynamic isDemo;
@dynamic name;
@dynamic r_nextStay;
@dynamic r_scenes;
@dynamic r_settings;

@end
