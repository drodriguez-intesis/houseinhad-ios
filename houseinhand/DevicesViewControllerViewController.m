//
//  DevicesViewControllerViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "AppDelegate.h"
#import "DevicesViewControllerViewController.h"
#import "StaysViewController.h"
#import "ZonesViewController.h"
#import "InitialViewController.h"
#import "RootViewController.h"
#import "RgbThirdLevelViewController.h"
#import "Device.h"
#import "Stay+Parser.h"
#import "ActionScrollViewController.h"
#import "DevicesCompatibility.h"
#import "IPCameraThirdLevelViewController.h"
#import "IPVideoKNXThirdLevelViewController.h"
#import "IPVideoJungTkmThirdLevelViewController.h"
#import "MultimediaThirdLevelViewController.h"
#import "TypeRgbViewController.h"
#define NUM_OF_DEVICES  7
#define SPACE 10
#define Y_INITIAL_OFFSET 15
#define X_OFFSET 200


#define IPAD_RESIZE_THIRD_LEVEL							1.5

#define ANIMATION_TO_ORANGE								1
#define ANIMATION_TO_GRAY									2

@interface DevicesViewControllerViewController ()
@property (assign) BOOL isPinched;
@property (strong,nonatomic) PasswordViewController *passcodeVC;
@property (strong,nonatomic) NewActionController *actionSheetController;
//@property (strong,nonatomic) UITapGestureRecognizer *tapGestureRecognizer;
@end

@implementation DevicesViewControllerViewController

-(void) setDetailItem:(id)newDetailItem{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
		self.stayName.text =  [_detailItem name];
    }
}

-(void) reloadDetailItem{
	if	(self.devicesScrollView.subviews.count >0){
		[self.devicesScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}
	[self addToScrollView];
	[self checkBackgroundImage];
	[self startPasscode];
}

-(void) checkBackgroundImage{
	if (DEVICE_IS_IPAD) {
		Stay *st = (Stay *) _detailItem;
		if (st.useCustomImage.boolValue) {
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
				UIImage *im = [st retrieveBackgroundImage];
				dispatch_async(dispatch_get_main_queue(), ^{
					[self enableBackgroundImageWithImage:im];
				});
			});
		}
		else{
			[self disableBackgroundImage];
		}
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.isPinched = NO;
	_topOrangeImage.backgroundColor = APP_COLOR_ORANGE;
	self.stayName.text =  [_detailItem name];
	if (!DEVICE_IS_IPAD){
		[self addToScrollView];
		[self startPasscode];

	}
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startPasscode) name:NOT_DID_REALLY_APP_BECOME_ACTIVE object:nil];
}

-(void) checkPasscode{
	Stay *st = (Stay *) _detailItem;
	if (!st.isSecure.boolValue || _passcodeVC || !self.view.superview) return;
	[self presentPasscode:st.passcode withType:PASSWORD_TYPE_CHECK];
}
-(void) presentPasscode:(NSString *) passcode withType:(PasswordType) type{
	if (_passcodeVC) return;
	self.devicesScrollView.userInteractionEnabled = NO;
	_passcodeVC = [[PasswordViewController alloc] initWithNibName:@"PasswordViewController" bundle:nil passcode:passcode type:type];
//	_passcodeVC.view.bounds = self.view.bounds;
	_passcodeVC.delegate = self;
	_passcodeVC.view.alpha = 0.0;
	_passcodeVC.view.center = self.view.center;
	[self addChildViewController:_passcodeVC];
	[self.view addSubview:_passcodeVC.view];
	_passcodeVC.view.center = self.view.center;
	[UIView transitionWithView:_passcodeVC.view duration:.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
		_passcodeVC.view.alpha = 1.0;
	} completion:^(BOOL finished) {
		[_passcodeVC didMoveToParentViewController:self];
	}];
}

-(void) passwordVCdidResolveCode:(PasswordViewController *)passwordVC withType:(PasswordType)type{
	[self dismissPasswordViewAndExit:NO];
	if (type == PASSWORD_TYPE_REMOVE) {
		Stay *st = (Stay *) _detailItem;
		st.isSecure = @NO;
		st.passcode = @"";
		[st.managedObjectContext save:nil];
	}
	if (type == PASSWORD_TYPE_ADD) {
		Stay *st = (Stay *) _detailItem;
		st.isSecure = @YES;
		st.passcode = [passwordVC getPasscode];
		[st.managedObjectContext save:nil];
	}
}

-(void) passwordVCdidCancelCode:(PasswordViewController *)passwordVC withType:(PasswordType)type{
	if (type == PASSWORD_TYPE_CHECK)[self dismissPasswordViewAndExit:YES];
	if (type == PASSWORD_TYPE_REMOVE)[self dismissPasswordViewAndExit:NO];
	if (type == PASSWORD_TYPE_ADD)[self dismissPasswordViewAndExit:NO];

}

-(void) dismissPasswordViewAndExit:(BOOL) exit{
	[_passcodeVC willMoveToParentViewController:nil];
	[_passcodeVC.view removeFromSuperview];
	[_passcodeVC removeFromParentViewController];
	_passcodeVC = nil;
	if (exit) {
		if (!DEVICE_IS_IPAD) [self performSegueWithIdentifier:@"ExitDevices" sender:self];
		else 	[self.devicesScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
	}
	else{
		self.devicesScrollView.userInteractionEnabled = YES;
	}
	
}
- (void)viewDidUnload
{
    [super viewDidUnload];
	[[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

}

-(void) startPasscode{
	 [self performSelector:@selector(checkPasscode) withObject:nil afterDelay:.4];
}

-(void) viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
}

-(void) didTapView{
	[self showOptionsMenu:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[self recalculatePositions];
	[self centerScrollView];
	[self dismissActionSheet];
}


-(void) dismissActionSheet{
	if (DEVICE_IS_IPAD && _actionSheetController) {
		[_actionSheetController dismissActionSheet];
	}
}

-(IBAction)handlePinchRecognizer:(UIPinchGestureRecognizer *)recognizer{

}


-(void) addToScrollView{
    Device *device;
    NSMutableSet *devices = [(NSMutableSet *)self.detailItem mutableSetValueForKey:@"r_device"];
    NSArray *orderedS = [devices sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
	_devicesScrollView.autoresizesSubviews = NO;
    for (NSUInteger i=0; i<[devices count]; i++) {
		device = orderedS[i];
		[self addOneDevice:@(i) set:device];
    }
    [self centerScrollView];
	
}

-(void) centerScrollView{
	if (_devicesScrollView.contentSize.height <(self.view.bounds.size.height - _topOrangeImage.bounds.size.height)) {
		[_devicesScrollView setFrame:CGRectMake(_devicesScrollView.frame.origin.x, _devicesScrollView.frame.origin.y, _devicesScrollView.frame.size.width, _devicesScrollView.contentSize.height)];
		[_devicesScrollView setCenter:CGPointMake(_devicesScrollView.center.x, self.view.center.y+28)];
		_devicesScrollView.scrollEnabled = NO;
	}
}

-(void) addOneDevice:(NSNumber *) index set:(Device *) set{
	const NSArray *devicesType = [DevicesCompatibility getAvailableWidgetDevices];
	DevicesViewController *device;
	NSDictionary *dev = devicesType [[set.code integerValue]];
	Class klass = NSClassFromString(dev[@"class"]);
	device = [[klass alloc] initWithNibName:dev[@"interface"] bundle:nil device:set];
    if (device != nil) {
        device.delegate = self;
		BOOL hInc = [self calculateDevicePosition:device fromIndex:index.integerValue];
        [self addChildViewController:device];
        [self.devicesScrollView addSubview:device.view];
		CGFloat h = device.view.bounds.size.height;
		if (!hInc) {
			h = 0;
		}
        _devicesScrollView.contentSize=CGSizeMake(self.devicesScrollView.bounds.size.width, self.devicesScrollView.contentSize.height + h);
        [self didMoveToParentViewController:device];
        device = nil;
    }
}

-(void) recalculatePositions{
	_devicesScrollView.contentSize=CGSizeMake(0,0);
	[self.childViewControllers enumerateObjectsUsingBlock:^(UIViewController * dev, NSUInteger idx, BOOL *stop) {
		if ([dev isKindOfClass:[DevicesViewController class]]) {
			BOOL hInc = [self calculateDevicePosition:(DevicesViewController *) dev fromIndex:idx];
			CGFloat h = dev.view.bounds.size.height;
			if (!hInc) {
				h = 0;
			}
			_devicesScrollView.contentSize=CGSizeMake(self.devicesScrollView.bounds.size.width, self.devicesScrollView.contentSize.height + h);
		}
		else if ([dev isKindOfClass:[ThirdLevelViewController class]]) {
			[dev.view setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
		}
	}];
}
-(BOOL) calculateDevicePosition:(DevicesViewController *) devView fromIndex:(NSUInteger) i{
	BOOL hInc = YES;
	CGFloat x = self.view.center.x;
	CGFloat y = _devicesScrollView.contentSize.height + devView.view.bounds.size.height/2;
	if (DEVICE_IS_IPAD) {
		if (i % 2 && UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
			//parell
			x = self.devicesScrollView.center.x + X_OFFSET;
			if (y != 0.0) {
				y = y - devView.view.frame.size.height;
				hInc = NO;
			}
		}
		else{
			//senar
			if (UIInterfaceOrientationIsLandscape(self.interfaceOrientation)){
				x = self.devicesScrollView.center.x - X_OFFSET;
			}
		}
	}
	if (y == 0.0) {
		y = Y_INITIAL_OFFSET ;
	}
	devView.view.center = CGPointMake(x, y);
	return hInc;
}

-(IBAction)resetScrollOffset{
    [self.devicesScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}

-(void) toThirdLevel:(DevicesViewController *) device{
	ThirdLevelViewController *level;
	NSString *extension = @"_iPad";
	switch ([device.device.code integerValue]) {
		case TYPE_OBJECT_RGB:
			level = [[RgbThirdLevelViewController alloc] initWithNibName:[NSString stringWithFormat:@"RgbThirdLevelViewController%@",(DEVICE_IS_IPAD ? extension : @"")] bundle:nil device:device.device];
		
		{
			if (device.type == TYPE_CONTROL) {
				level = [[RgbThirdLevelViewController alloc] initWithNibName:[NSString stringWithFormat:@"RgbThirdLevelViewController%@",(DEVICE_IS_IPAD ? extension : @"")] bundle:nil device:device.device];
			}
			else{
				TypeRgbViewController *dev = (TypeRgbViewController *) device;
				level = [[RgbThirdLevelViewController alloc] initWithNibName:[NSString stringWithFormat:@"RgbThirdLevelViewController%@",(DEVICE_IS_IPAD ? extension : @"")] bundle:nil device:device.device sceneColors:dev.sceneColors];
			}
			break;
		}
        case TYPE_OBJECT_IP_CAMERA:
				level = [[IPCameraThirdLevelViewController alloc] initWithNibName:@"IPCameraThirdLevelViewController" bundle:nil device:device.device];
				if (DEVICE_IS_IPAD)[level.view setBounds:CGRectMake(level.view.bounds.origin.x, level.view.bounds.origin.y, level.view.bounds.size.width*IPAD_RESIZE_THIRD_LEVEL, level.view.bounds.size.height*IPAD_RESIZE_THIRD_LEVEL)];
        break;
        case TYPE_OBJECT_IP_VIDEO_KNX:
				level = [[IPVideoKNXThirdLevelViewController alloc] initWithNibName:@"IPVideoKNXThirdLevelViewController" bundle:nil device:device.device];
				if (DEVICE_IS_IPAD) [level.view setBounds:CGRectMake(level.view.bounds.origin.x, level.view.bounds.origin.y, level.view.bounds.size.width*IPAD_RESIZE_THIRD_LEVEL, level.view.bounds.size.height*IPAD_RESIZE_THIRD_LEVEL)];
        break;
        case TYPE_OBJECT_JUNG_TKM:
            level = [[IPVideoJungTkmThirdLevelViewController alloc] initWithNibName:@"IPVideoJungTkmThirdLevelViewController" bundle:nil device:device.device];
            if (DEVICE_IS_IPAD) [level.view setBounds:CGRectMake(level.view.bounds.origin.x, level.view.bounds.origin.y, level.view.bounds.size.width*IPAD_RESIZE_THIRD_LEVEL, level.view.bounds.size.height*IPAD_RESIZE_THIRD_LEVEL)];
            break;
        case TYPE_OBJECT_MOBOTIX_T24:
            level = [[IPVideoJungTkmThirdLevelViewController alloc] initWithNibName:@"IPVideoJungTkmThirdLevelViewController" bundle:nil device:device.device];
            if (DEVICE_IS_IPAD) [level.view setBounds:CGRectMake(level.view.bounds.origin.x, level.view.bounds.origin.y, level.view.bounds.size.width*IPAD_RESIZE_THIRD_LEVEL, level.view.bounds.size.height*IPAD_RESIZE_THIRD_LEVEL)];
            break;
		case TYPE_OBJECT_MULTIMEDIA:
			level = [[MultimediaThirdLevelViewController alloc] initWithNibName:@"MultimediaThirdLevelViewController" bundle:nil device:device.device];
			break;
        default:
			break;
	}
    if (level) {
        level.delegate =self;
        level.view.autoresizingMask = UIViewAutoresizingNone;
        [level.view setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2)];
        level.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        [self.view addSubview:level.view];
        [self addChildViewController:level];
        [UIView transitionWithView:level.view duration:.4 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            level.view.transform = CGAffineTransformIdentity;
            self.devicesScrollView.alpha = 0.3;
                //self.devicesScrollView.backgroundColor = [UIColor colorWithWhite:0.05 alpha:1.0];
        }completion:^(BOOL finished){
            [level didMoveToParentViewController:self];
            self.devicesScrollView.userInteractionEnabled= NO;
        }];
    }
}

- (void)deviceViewControllerNextLevel:(DevicesViewController *)controller{
	[self toThirdLevel:controller];
}

-(void) dismissThirdLevel:(ThirdLevelViewController *) third{
	[UIView transitionWithView:self.devicesScrollView duration:0.3 options:UIViewAnimationOptionCurveLinear animations:^{
    	self.devicesScrollView.alpha = 1.0;
        third.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        third.view.alpha = 0.1;
		//self.devicesScrollView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];

    }
    completion:^(BOOL finished){
        self.devicesScrollView.userInteractionEnabled = YES;
        [third removeFromParentViewController];
        [third.view removeFromSuperview];
    }];
}

-(IBAction)showOptionsMenu:(UIButton *)button{
	NSUInteger avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_DISABLE_ALLIMAGE : ACTION_HAS_NOTHING);;
	Stay *st = (Stay *) _detailItem;
	if (st.useCustomImage.boolValue && st.isSecure.boolValue) avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_DISABLE_TAKEIM_PSW : ACTION_HAS_IMAGE_PSW);
	else if (st.useCustomImage.boolValue) avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_DISABLE_TAKEIM : ACTION_HAS_IMAGE);
	else if (st.isSecure.boolValue) avOptions = ((DEVICE_IS_IPAD && UIInterfaceOrientationIsPortrait(self.interfaceOrientation))? ACTION_DISABLE_ALLIMAGE_PSW : ACTION_HAS_PSW);
	_actionSheetController = [[NewActionController alloc] initActionSheetWithOptions:avOptions originViewController:self withFrame:button.frame setDelegate:self];
}

#pragma mark ActionView delegate
-(void)actionController:(NewActionController *)action didEndEditing:(NSDictionary *)info{
	if (!info) return;
	Stay *st = (Stay *) _detailItem;
	if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_IMAGE"]) {
		UIImage *thumbImage = (UIImage *) [info valueForKey:@"thumbImage"];
		NSData *imageData = UIImagePNGRepresentation (thumbImage);
		st.thumbImage = imageData;
		st.useCustomImage = @YES;
		if (DEVICE_IS_IPAD){
			UIImage *bigImage = (UIImage *) [info valueForKey:@"backgroundImage"];
			if (UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])) [self enableBackgroundImageWithImage:bigImage];
			[st storeBackgroundImage:bigImage];
		}
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"DELETE_IMAGE"]){
		st.thumbImage = nil;
		st.useCustomImage = @NO;
		if (DEVICE_IS_IPAD) [self disableBackgroundImage];
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_NAME"]){
		st.name = [info valueForKey:@"name"];
		self.stayName.text =  [_detailItem name];
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"REMOVE_PASSWORD"]){
		[self presentPasscode:st.passcode withType:PASSWORD_TYPE_REMOVE];
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"ADD_PASSWORD"]){
		[self presentPasscode:nil withType:PASSWORD_TYPE_ADD];
	}
}

-(void) enableBackgroundImageWithImage:(UIImage *) image{
	InitialViewController *initial = (InitialViewController *) self.parentViewController;
	[initial setBackgroundImage:image];
}

-(void) disableBackgroundImage{
	InitialViewController *initial = (InitialViewController *) self.parentViewController;
	[initial removeBackgroundImage];
}

-(void) removeChildsControllers{
	[self.childViewControllers makeObjectsPerformSelector:@selector(willMoveToParentViewController:) withObject:nil];
	[self.childViewControllers makeObjectsPerformSelector:@selector(removeFromParentViewController)];
}

-(void) actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (buttonIndex >= 0 &&_actionSheetController) {
		Stay *st = (Stay *) _detailItem;
		[_actionSheetController showModalControllerFromOption:buttonIndex actionSheet:actionSheet inViewController:self withButtonFrame:_optionsButton.frame stayName:st.name];
	}
}

@end
