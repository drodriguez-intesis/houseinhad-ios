//
//  StayViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/26/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


#define IMAGE_CROP_SIZE_W				138*2
#define IMAGE_CROP_SIZE_H				90*2


@class StayViewController;
@class Stay;
@protocol StayViewControllerDelegate <NSObject>

- (void)stayViewControllerDidTouched:(StayViewController *)controller;
- (void)stayViewControllerDidOptionsMenu:(StayViewController *)controller;

@end

@interface StayViewController: UIViewController 

@property (strong,nonatomic) Stay * stay;

@property (nonatomic, weak) id <StayViewControllerDelegate> delegate;
@property (strong) IBOutlet UIImageView *stayImage;
@property (strong) IBOutlet UIImageView *bckgImage;
@property (strong) IBOutlet UILabel *name;
@property (assign) NSUInteger tag;
-(IBAction)stayPressed;
-(void) selectStay;
-(IBAction) unselectStay;
-(void) unselectStayAnimated;
@end
