//
//  Stay+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Stay.h"

@interface Stay (Parser)

+(Stay *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc;
-(NSArray *) getOrderedStays;
-(NSArray *) getOrderedDevices;
-(NSArray *) getTelegramsWithAddress:(NSNumber *) address;
-(void) storeBackgroundImage:(UIImage *) image;
-(UIImage *) retrieveBackgroundImage;
@end
