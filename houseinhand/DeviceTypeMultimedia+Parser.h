//
//  DeviceTypeMultimedia+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeMultimedia.h"


typedef 	NS_ENUM (NSUInteger,REMOTE_TYPE) {REMOTE_TYPE_FULL=0,REMOTE_TYPE_TV=1,REMOTE_TYPE_MULTI=2};
@interface DeviceTypeMultimedia (Parser)
-(NSArray *) getOrderedCommands;
-(MultiCommand *) getCommandWithTag:(NSString *) tag;
@end
