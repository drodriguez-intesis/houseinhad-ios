//
//  SimpleButtonViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SimpleButtonViewController.h"
#import "Device.h"
@interface SimpleButtonViewController ()
@property (strong,nonatomic) UIImage *image;
@end

@implementation SimpleButtonViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _image = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];
		
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
        _image = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setImages];
}

-(void) setImages{
    [_centerButton setImage:_image forState:UIControlStateNormal];
    [_centerButton setImage:_image forState:UIControlStateHighlighted];
}

- (void)viewDidUnload
{
    _centerButton = nil;
	_image = nil;
	[super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
