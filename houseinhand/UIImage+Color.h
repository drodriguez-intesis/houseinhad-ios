//
//  UIImage+Color.h
//  IntesisHome
//
//  Created by Isaac Lozano on 3/8/13.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)
+(UIImage *)imageWithColor:(UIColor *)color;
@end
