//
//  DeviceTypeIpButton.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeIpButton.h"


@implementation DeviceTypeIpButton

@dynamic image1;
@dynamic pulseWidth;
@dynamic offPressIp;
@dynamic onPressIp;

@end
