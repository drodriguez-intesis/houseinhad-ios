//
//  EnvL1ViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/7/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "EnvL1ViewController.h"
#import "ProfilesCell.h"
#import "Stay.h"

#define TAG_BADGE_NUMBER_VIEW 113
#define TAG_BADGE_LOCKED_VIEW 114

@interface EnvL1ViewController ()
@property (strong,nonatomic) IBOutlet UITableView *tableView;
@property (strong,nonatomic) NSArray *stays;
@property (strong,nonatomic) Scene *scene;
@property (strong,nonatomic) NSManagedObjectContext *context;
@property (assign,nonatomic) NSUInteger level;
@end

@implementation EnvL1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil context:(NSManagedObjectContext *) context stays:(NSArray *) stays scene:(Scene *) scene level:(NSUInteger) level{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_stays = stays;
		_scene = scene;
		_level = level;
		_context = context;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self registerNib];
	if (_level ==1) {
		self.navigationItem.title = _scene.name;
		UIButton* cancel =[UIButton buttonWithType:UIButtonTypeCustom];
		UIImage* buttonImage = [UIImage imageNamed:@"cancel"];
		[cancel addTarget:self action:@selector(cancel) forControlEvents:UIControlEventTouchUpInside];
		[cancel setImage:buttonImage forState:UIControlStateNormal];
		[cancel setImage:buttonImage  forState:UIControlStateHighlighted];
		cancel.frame = CGRectMake(0, 0, buttonImage.size.width, buttonImage.size.height);
		UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:cancel];
		
		UIButton* save =[UIButton buttonWithType:UIButtonTypeCustom];
		UIImage* buttonImage1 = [UIImage imageNamed:@"ok"];
		[save addTarget:self action:@selector(save) forControlEvents:UIControlEventTouchUpInside];
		[save setImage:buttonImage1 forState:UIControlStateNormal];
		[save setImage:buttonImage1  forState:UIControlStateHighlighted];
		save.frame = CGRectMake(0, 0, buttonImage1.size.width, buttonImage1.size.height);
		UIBarButtonItem *saveItem = [[UIBarButtonItem alloc]initWithCustomView:save];
		
		self.navigationItem.leftBarButtonItem = backItem;
		self.navigationItem.rightBarButtonItem = saveItem;
	}
	else{
		if ([_stays count] >0) {
			Stay * st = (Stay *) _stays[0];
			self.navigationItem.title = st.r_prevStay.name;
			//[self.navigationItem.backBarButtonItem setAction:@selector(saveWidgetsState)];
			/*UIButton* back =[UIButton buttonWithType:UIButtonTypeCustom];
			UIImage* buttonImage = [UIImage imageNamed:@"back"];
			[back addTarget:self.navigationController action:@selector(popViewControllerAnimated:) forControlEvents:UIControlEventTouchUpInside];
			[back setImage: [UIImage imageNamed:@"back"] forState:UIControlStateNormal];
			[back setImage: [UIImage imageNamed:@"back"] forState:UIControlStateHighlighted];
			back.frame = CGRectMake(-20, 0, buttonImage.size.width+20, buttonImage.size.height);
			UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithCustomView:back];
			self.navigationItem.leftBarButtonItem = backItem;*/
		}
	}
}

-(void) viewDidUnload{
	_stays = nil;
	_scene = nil;
	_tableView = nil;
	_context = nil;
	[super viewDidUnload];
}

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
	[_tableView deselectRowAtIndexPath:[_tableView indexPathForSelectedRow] animated:YES];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_stays count];
}

-(void) registerNib{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    UINib *topNib = [UINib nibWithNibName:@"ProfilesCell" bundle:classBundle];
    [[self tableView] registerNib:topNib forCellReuseIdentifier:@"Profiles"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"Profiles";
    ProfilesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if (cell == nil) {
        cell = [[ProfilesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
        }
    }
	Stay *stay = (Stay *) _stays[[indexPath row]];
	cell.name.text = stay.name;
	cell.name.textColor = [UIColor whiteColor];
	cell.name.highlightedTextColor = [UIColor whiteColor];

	//TODO add badge
	
	[self checkIfLockedStay:stay ofCell:cell];
	
	return cell;
}

/*
-(void) addBadgeNumber:(NSUInteger) badge toCell:(UITableViewCell *) cell{
	UIButton *circle = (UIButton *)[cell.contentView viewWithTag:TAG_BADGE_NUMBER_VIEW];
	if (circle == nil && badge >0) {
		circle = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 16, 16)];
		circle.layer.cornerRadius = circle.frame.size.width/2;
		circle.backgroundColor = [UIColor darkGrayColor];
		circle.userInteractionEnabled = NO;
		circle.tag = TAG_BADGE_NUMBER_VIEW;
		[circle setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
		circle.titleLabel.font = [UIFont boldSystemFontOfSize:10];
		[cell.contentView addSubview:circle];
	}
	if (badge == 0) {
		[circle removeFromSuperview];
	}
	else{
		[circle setCenter:CGPointMake(cell.bounds.size.width - 40, 30)];
		[circle setTitle:[NSString stringWithFormat:@"%d",badge] forState:UIControlStateNormal];
	}
}*/
-(void) checkIfLockedStay:(Stay *) stay ofCell:(UITableViewCell *) cell{
	UIImageView *locked = (UIImageView *)[cell.contentView viewWithTag:TAG_BADGE_LOCKED_VIEW];	
	if (stay.isSecure.boolValue) {
		if (!locked) {
			locked = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"locked_stay"]];
			[cell.contentView addSubview:locked];
		}
		[locked setCenter:CGPointMake(_tableView.bounds.size.width - 20, 30)];
	}
	else if (locked){
		[locked removeFromSuperview];
	}
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"rowUnselected_60"]];
		cell.backgroundColor = [UIColor clearColor];
		UIImage *image1 = [UIImage imageNamed:@"rowSettingsSelected"];
		cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[image1 resizableImageWithCapInsets:UIEdgeInsetsMake(0, 0, 0, 0)]];
		image1 = nil;
	}
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Stay *stay = (Stay *) _stays[[indexPath row]];
	NSMutableSet *set = [stay mutableSetValueForKey:@"r_nextStay"];
	if ([set count] > 0) {
		NSMutableSet *set = [stay mutableSetValueForKey:@"r_nextStay"];
		NSArray *orderedS = [set sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"order" ascending:YES]]];
		EnvL1ViewController *env = [[EnvL1ViewController alloc] initWithNibName:@"EnvL1ViewController" bundle:nil context:_context stays:orderedS scene:_scene level:2];
		[self.navigationController pushViewController:env animated:YES];
	}
	else{
		EnvDevViewController *dev = [[EnvDevViewController alloc] initWithNibName:@"EnvDevViewController" bundle:nil stay:stay scene:_scene];
		dev.delegate = self;
		[self.navigationController pushViewController:dev animated:YES];
	}
}

-(void) cancel{
	[self.delegate didEndEditingScene:self saveChanges:NO];
}

-(void) save{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Scene name", nil, CURRENT_LANGUAGE_BUNDLE, @"Title of UIAlertView") message:nil delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	alert.alertViewStyle = UIAlertViewStylePlainTextInput;
	[[alert textFieldAtIndex:0] setText:_scene.name];
	[[alert textFieldAtIndex:0] setClearButtonMode:UITextFieldViewModeAlways];
	[alert show];	
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex>0) {
		NSString *newName = [[alertView textFieldAtIndex:0] text];
		_scene.name = newName;
		[_context save:nil];
		[self.delegate didEndEditingScene:self saveChanges:YES];
	}
}
-(void) sceneDeviceDidSave:(EnvDevViewController *)controller actions:(NSArray *)actions fromStay:(Stay *) stay{
	// extract current telegrams from scene
	NSMutableSet *scSet = [_scene mutableSetValueForKey:@"r_sceneTelegram"];
	// extract available telegrams of edited stay
	NSMutableSet *devSet = [stay mutableSetValueForKey:@"r_device"];
	NSMutableSet *telSet = [NSMutableSet set];
	[[devSet allObjects] enumerateObjectsUsingBlock:^(Device *dev, NSUInteger idx, BOOL *stop) {
		if (dev.r_telegram != nil) {
			NSMutableSet *tels = [dev mutableSetValueForKey:@"r_telegram"];
			[telSet addObjectsFromArray:[tels allObjects]];
		}
	}];
	
	//check the intersection and remove from scene if necessary
	[[scSet allObjects] enumerateObjectsUsingBlock:^(SceneTelegram *scTel , NSUInteger idx, BOOL *stop) {
		NSArray *telToDelete = [[telSet allObjects] filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"address == %@",scTel.address]];
		if ([telToDelete count] >0) {
			@synchronized (_context){
				[_context deleteObject:scTel];
			}
		}
	}];
	//add new
	[actions enumerateObjectsUsingBlock:^(NSDictionary *info, NSUInteger idx, BOOL *stop) {
		SceneTelegram *newTel = [NSEntityDescription insertNewObjectForEntityForName:@"SceneTelegram" inManagedObjectContext:_context];
		newTel.address = info [@"address"];
		newTel.value = info [@"value"];
		newTel.dpt = info [@"dpt"];
		if	(info[@"delay"]) newTel.delay = info [@"delay"];

		[_scene addR_sceneTelegramObject:newTel];

	}];
}

@end
