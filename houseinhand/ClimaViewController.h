//
//  ClimaViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"

@interface ClimaViewController : DevicesViewController
@property (assign,nonatomic) BOOL hasLock;
@property (strong,nonatomic) IBOutlet UISegmentedControl *centralSegmented;
@property (strong,nonatomic) IBOutlet UIImageView *leftImage;
@property (strong,nonatomic) IBOutlet UIImageView *rightImage;
@end
