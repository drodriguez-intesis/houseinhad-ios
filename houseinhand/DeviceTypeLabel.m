//
//  DeviceTypeLabel.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeLabel.h"


@implementation DeviceTypeLabel

@dynamic decimals;
@dynamic scale;
@dynamic units;

@end
