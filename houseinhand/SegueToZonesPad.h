//
//  SegueToZonesPad.h
//  houseinhand
//
//  Created by Isaac Lozano on 12/31/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayViewController.h"

@interface SegueToZonesPad : UIStoryboardSegue
@property (strong,nonatomic) StayViewController *stay;

@end
