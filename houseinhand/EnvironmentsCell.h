//
//  EnvironmentsCell.h
//  houseinhand
//
//  Created by Isaac Lozano on 12/7/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnvironmentsCell : UITableViewCell
@property (weak,nonatomic) IBOutlet UILabel *name;
@end
