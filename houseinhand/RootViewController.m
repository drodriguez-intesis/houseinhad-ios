//
//  RootViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "RootViewController.h"
#import "InitialViewController.h"
#import "DevicesViewControllerViewController.h"
#import "StayViewController.h"
#import "StaysViewController.h"
#import "ZonesViewController.h"
#import "UnwindSegueDevices.h"
#import "UnwindSegueZones.h"
#import "UnwindSegueZonesPad.h"
#import "Config+Parser.h"
#define ANIMATION_TO_ORANGE								1
#define ANIMATION_TO_GRAY									2
@interface RootViewController ()
@property (strong,nonatomic) IBOutlet UIImageView *topOrangeImage;
@property (nonatomic,assign) NSUInteger tagLevel;
@property (assign) CGFloat previousYOffset;
@property (strong,nonatomic) Config * config;
@end

@implementation RootViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	_topOrangeImage.backgroundColor = APP_COLOR_ORANGE;
	[self loadActiveConfig];
}

-(void) loadActiveConfig{
	_config = [Config getActiveConfigOfContext:_managedObjectContext];
	if (_config)  [self addFirstLevel];
}

- (UIStoryboardSegue *)segueForUnwindingToViewController:(UIViewController *)toViewController fromViewController:(UIViewController *)fromViewController identifier:(NSString *)identifier
{
	if ([identifier isEqualToString:@"ExitDevices"])
		return [[UnwindSegueDevices alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
	else if ([identifier isEqualToString:@"ExitZones"])
		return [[UnwindSegueZones alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
	else if ([identifier isEqualToString:@"ExitZonesPad"])
		return [[UnwindSegueZonesPad alloc] initWithIdentifier:identifier source:fromViewController destination:toViewController];
	else
		return [super segueForUnwindingToViewController:toViewController fromViewController:fromViewController identifier:identifier];
}

-(void) addFirstLevel{
    InitialViewController *initial = (InitialViewController *) (self.childViewControllers)[0];
    self.previousYOffset = 0.0;
    initial.requestedLevel = 0;
    initial.previousYOffset = self.previousYOffset;
    initial.detailItem = _config;

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.managedObjectContext = nil;
    self.config = nil;
	[[NSNotificationCenter defaultCenter] removeObserver:self name:NSManagedObjectContextDidSaveNotification object:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) changeConfigFile{
	RootViewController *initial = [self.storyboard instantiateViewControllerWithIdentifier:@"RootViewController"];
	initial.managedObjectContext = _managedObjectContext;
	UIView *myView = initial.view;
	myView.alpha = 0.0;
	CALayer *layer = myView.layer;
	CATransform3D rotationAndPerspectiveTransform = CATransform3DIdentity;
	rotationAndPerspectiveTransform.m34 = 1.0 / -500;
	rotationAndPerspectiveTransform = CATransform3DRotate(rotationAndPerspectiveTransform, 45.0f * M_PI / 180.0f, 1.0f, 0.0f, 0.0f);
	layer.transform = rotationAndPerspectiveTransform;
	[self.view.window addSubview:initial.view];

	[UIView transitionWithView:initial.view duration:0.4 options:UIViewAnimationOptionCurveEaseIn animations:^{
		initial.view.alpha = 1.0;
		self.view.alpha = 0.0;
		initial.view.layer.transform = CATransform3DIdentity;
	}completion:^(BOOL finished) {
		self.view.window.rootViewController = initial;
		[self.view removeFromSuperview];
	}];
}

@end
