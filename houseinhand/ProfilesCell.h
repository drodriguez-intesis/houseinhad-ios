//
//  ProfilesCell.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfilesCell : UITableViewCell
@property (strong,nonatomic) IBOutlet UILabel *name;
@end
