//
//  DeviceTypeLight.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Device.h"


@interface DeviceTypeLight : Device

@property (nonatomic, retain) NSString * image1;
@property (nonatomic, retain) NSString * image2;
@property (nonatomic, retain) NSString * image3;
@property (nonatomic, retain) NSNumber * sendSceneValue;

@end
