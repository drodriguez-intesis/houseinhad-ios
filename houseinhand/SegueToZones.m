//
//  SegueToZones.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/17/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegueToZones.h"
#import "ZonesViewController.h"
#import "StaysViewController.h"
#import "InitialViewController.h"
@implementation SegueToZones

- (UIView *)findTopMostViewForViewController:(UIViewController *)viewController
{
	UIView *theView = viewController.view;
	UIViewController *parentViewController = viewController.parentViewController;
	while (parentViewController != nil)
	{
		theView = parentViewController.view;
		parentViewController = parentViewController.parentViewController;
	}
	return theView;
}

- (void)perform
{
	UIViewController *source = self.sourceViewController;
	UIViewController *destination = self.destinationViewController;
	
	UIView *sourceView = source.view;
	UIView *destinationView = destination.view;
	
	// Create a black view that covers the entire source view, but set it
	// fully transparent for now. We'll use this to make the source view
	// appear to recede into the background.
	UIView *dimView = [[UIView alloc] initWithFrame:sourceView.bounds];
	dimView.opaque = YES;
	dimView.alpha = 0.0f;
	dimView.backgroundColor = [UIColor blueColor];
	//	[sourceView addSubview:dimView];
	
	// Add the destination view to the source view.
	destinationView.frame = sourceView.bounds;
	[source.parentViewController addChildViewController:destination];
	[sourceView.superview addSubview:destinationView];
	
	destinationView.alpha = 0.0;
	destinationView.transform = CGAffineTransformMakeScale(0.1, 0.1);
	// Start the animation.
	[UIView animateWithDuration:0.5f
						  delay:0
						options:UIViewAnimationOptionCurveEaseOut
					 animations:^(void)
	 {
		 // Move the views to their endpoints and make the dimView darker.
		 destinationView.alpha = 1.0;
		 destinationView.transform = CGAffineTransformIdentity;
		 sourceView.alpha = 0.0;
		 dimView.alpha = 0.5f;
	 }
					 completion: ^(BOOL done)
	 {
		 // The black view is no longer needed now, so remove it.
		 // [dimView removeFromSuperview];
		 sourceView.hidden = YES;
		 // Properly present the new screen.
		 [destination didMoveToParentViewController:source];
		 // [source presentViewController:destination animated:NO completion:nil];
		 //[source presentViewController:destination animated:NO completion:nil];
	 }];
}

-(void) perform2{
    StaysViewController *zones = (StaysViewController *)self.destinationViewController;
    StaysViewController *stays = (StaysViewController *)self.sourceViewController;
    InitialViewController *initial = (InitialViewController *) stays.parentViewController;
    [initial addChildViewController:zones];
    CGPoint centerPoint = [[_stay.view superview] convertPoint:_stay.view.center toView:initial.view];
    CGAffineTransform t1 = CGAffineTransformMakeTranslation(centerPoint.x-stays.view.center.x, centerPoint.y-stays.view.center.y);
    CGFloat relY = _stay.view.bounds.size.height / zones.view.bounds.size.height;
    CGAffineTransform t2 = CGAffineTransformMakeScale(relY, relY);
    zones.view.transform = CGAffineTransformConcat(t2, t1);
    [initial transitionFromViewController:stays toViewController:zones duration:0.4 options:UIViewAnimationOptionTransitionNone animations:^{
        zones.view.transform = CGAffineTransformIdentity;
        stays.view.alpha = 0.0;
    }completion:^(BOOL finished){
        [zones didMoveToParentViewController:initial];
        [stays removeFromParentViewController];
    }];
}
@end
