//
//  IPVideoKNXThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/30/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "IPCameraThirdLevelViewController.h"

@interface IPVideoKNXThirdLevelViewController : IPCameraThirdLevelViewController <ObjectsAnimations>

@end
