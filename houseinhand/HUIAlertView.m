//
//  HUIAlertView.m
//  houseinhand
//
//  Created by Isaac Lozano on 11/23/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "HUIAlertView.h"

@implementation HUIAlertView

- (void)layoutSubviews
{
	self.backgroundColor = [UIColor colorWithWhite:0.2 alpha:1.0];
    self.layer.borderColor = [[UIColor colorWithWhite:0.4 alpha:1.0] CGColor];
    //self.view.layer.borderColor = [[UIColor orangeColor] CGColor];
    self.layer.borderWidth = 1.0;
    self.layer.masksToBounds = YES;
	for (UIView *subview in self.subviews){ //Fast Enumeration
		if ([subview isMemberOfClass:[UIImageView class]]) { //Find UIImageView Containing Blue Background
             ((UIImageView*)subview).image = nil;
		}
		if ([subview isMemberOfClass:[UILabel class]]) { //Find UIImageView Containing Blue Background
			UILabel * label = (UILabel *) subview;
			CGFloat fontSize = label.font.pointSize;
			label.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize];
		}
		if ([subview tag] == 1 || [subview tag] == 2 || [subview tag] == 3) {
			UIButton * button = (UIButton *) subview;
			CGFloat fontSize = button.titleLabel.font.pointSize;
			button.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:fontSize-2.0];
			[button setBackgroundImage:nil forState:UIControlStateNormal];
			[button setBackgroundImage:nil forState:UIControlStateHighlighted];
			button.backgroundColor = [UIColor colorWithWhite:0.1 alpha:1.0];
			
		}
        
	}
}

@end
