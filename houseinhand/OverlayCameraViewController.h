//
//  OverlayCameraViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 11/19/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>
@interface OverlayCameraViewController : UIViewController <UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (weak,nonatomic) IBOutlet UIView *overlayView;
@property (nonatomic, retain) UIImagePickerController *imagePickerController;

- (void)setupImagePicker:(UIImagePickerControllerSourceType)sourceType;

@end
