//
//  DeviceTypeGradhermetic+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeGradhermetic+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1			@"image1"
#define KEY_IMAGE_2			@"image2"
#define KEY_IMAGE_3			@"image3"
#define KEY_EXIT_TIME			@"exitTime"
#define KEY_STOP_TIME		@"stopTime"
#define KEY_LAMAS_TIME		@"lamasTime"


@implementation DeviceTypeGradhermetic (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeGradhermetic *dev = (DeviceTypeGradhermetic *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.image3 = (info[KEY_IMAGE_3] ? info[KEY_IMAGE_3]: dev.image3);
	dev.exitTime = (info[KEY_EXIT_TIME] ? @([info[KEY_EXIT_TIME] floatValue]): dev.exitTime);
	dev.stopTime = (info[KEY_STOP_TIME] ? @([info[KEY_STOP_TIME] floatValue]): dev.stopTime);
	dev.lamasTime = (info[KEY_LAMAS_TIME] ? @([info[KEY_LAMAS_TIME] floatValue]): dev.lamasTime);

	return dev;
}

@end
