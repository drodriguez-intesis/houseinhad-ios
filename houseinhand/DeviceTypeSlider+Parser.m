//
//  DeviceTypeSlider+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeSlider+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1			@"image1"
#define KEY_IMAGE_2			@"image2"
#define KEY_MAX_VALUE		@"maxValue"
#define KEY_MIN_VALUE		@"minValue"
#define KEY_STEP					@"step"
#define KEY_HAS_STOP			@"hasStop"

@implementation DeviceTypeSlider (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeSlider *dev = (DeviceTypeSlider *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.maxValue = (info[KEY_MAX_VALUE] ? @([info[KEY_MAX_VALUE] floatValue]): dev.maxValue);
	dev.minValue = (info[KEY_MIN_VALUE] ? @([info[KEY_MIN_VALUE] floatValue]): dev.minValue);
	dev.step = (info[KEY_STEP] ? @([info[KEY_STEP] floatValue]): dev.step);
	dev.hasStop = (info[KEY_HAS_STOP] ? @([info[KEY_HAS_STOP] boolValue]): dev.hasStop);

	return dev;
}
@end
