//
//  ThreeButtonsViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "DevicesViewController.h"
#import "UIButton_Sub.h"
@interface ThreeButtonsViewController : DevicesViewController
@property (weak,nonatomic) IBOutlet UIButton_Sub *leftButton;
@property (weak,nonatomic) IBOutlet UIButton_Sub *rightButton;
@property (weak,nonatomic) IBOutlet UIButton_Sub *centerButton;
@end
