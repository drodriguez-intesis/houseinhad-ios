//
//  UIImage+Tint.m
//
//  Created by Matt Gemmell on 04/07/2010.
//  Copyright 2010 Instinctive Code.
//

#import "UIImage+Tint.h"


@implementation UIImage (MGTint)

+(UIImage *) imageNamed:(NSString *) name withTintColor:(UIColor *) tintColor{
	UIImage *image = [UIImage imageNamed:name];
	if (image && tintColor) {
		return [image setTintColor:tintColor];
	}
	return image;
}
- (UIImage *) setTintColor: (UIColor *)color {
	if (color) {
		UIImage *image;		
		if ([UIScreen instancesRespondToSelector:@selector(scale)]) UIGraphicsBeginImageContextWithOptions([self size], NO, 0.f);
		else UIGraphicsBeginImageContext([self size]);
		CGRect rect = CGRectZero;
		rect.size = [self size];
		[color set];
		UIRectFill(rect);
		[self drawInRect:rect blendMode:kCGBlendModeDestinationIn alpha:1.0];
		image = UIGraphicsGetImageFromCurrentImageContext();
		UIGraphicsEndImageContext();
		
		return image;
	}
	return self;
}

@end
