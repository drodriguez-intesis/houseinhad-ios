//
//  InitialViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "InitialViewController.h"
#import "ZonesViewController.h"
#import "UISwipeViewGestureRecognizer.h"
#import "RootViewController.h"
#import "DevicesViewControllerViewController.h"
#import "Stay+Parser.h"

#define ADD_VIEW_IPAD_ANIMATION     0.25
#define TAB_BAR_TO_RIGHT 1
#define TAB_BAR_TO_LEFT 0
#define TAB_BAR_TO_SETTINGS 4
#define TAB_BAR_TO_HOME 2
#define TAB_BAR_TO_ENVIRONMENTS 3

@interface InitialViewController ()
@property (strong,nonatomic) NSArray *stayArray;
@property (strong,nonatomic) UITapGestureRecognizer *additionalTapView;
@property (weak,nonatomic) IBOutlet UILabel *titleLabelPad;
@property (weak,nonatomic) IBOutlet UIButton *homeControl;
@property (weak,nonatomic) IBOutlet UIButton *settingsControl;
@property (weak,nonatomic) IBOutlet UIButton *environmentControl;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

-(IBAction)showSettings;
-(IBAction)showEnvironments;
-(IBAction)showHome;
@end

@implementation InitialViewController

-(void) setDetailItem:(id)newDetailItem{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
		_titleLabelPad.text = [_detailItem valueForKey:@"name"];
        [self addRequestedLevel];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.homeControl setSelected:YES];
	if (DEVICE_IS_IPAD) {
		_additionalTapView = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissAdditionalView)];
		_additionalTapView.delegate = self;
        _additionalTapView.numberOfTapsRequired = 1;
        _additionalTapView.numberOfTouchesRequired = 1;
        _additionalTapView.enabled = NO;
        [self.view addGestureRecognizer:_additionalTapView];
	}
	else{
		UISwipeViewGestureRecognizer *swipeGesture = [[UISwipeViewGestureRecognizer alloc] init];
		[swipeGesture addTarget:swipeGesture action:@selector(isMoving:)];
		swipeGesture.minimumNumberOfTouches = 1;
		swipeGesture.maximumNumberOfTouches = 1;
		swipeGesture.viewsArray = @[self.environmentsView,self.staysView,self.settingsView];
		swipeGesture.delegate = self;
		[self.view addGestureRecognizer:swipeGesture];
	}
}

-(void) willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration{
	[UIView animateWithDuration:duration animations:^{
		[self checkBackgroundImageEnabled:toInterfaceOrientation];
	}];
}

-(BOOL) checkBackgroundImageEnabled:(UIInterfaceOrientation) interfaceOrientation{
	BOOL enabled = YES;
	if (UIInterfaceOrientationIsPortrait(interfaceOrientation)) {
		_backgroundImageView.alpha = 0.0;
		enabled = NO;
	}
	else{
		_backgroundImageView.alpha = 1.0;

	}
	return enabled;
}

-(void) addRequestedLevel{
    StaysViewController *stays = (StaysViewController *)(self.childViewControllers)[0];
	if (self.requestedLevel == 0) {
		stays.detailItem = self.detailItem;
		stays.isZone = NO;
	}
	else{
		NSArray *staysArray = [self.detailItem getOrderedStays];
		stays.detailItem = staysArray[self.previousLevel];
		stays.isZone = YES;
	}
    stays = nil;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
	if ([segue.identifier isEqualToString:@"segueToDevicesPad"]){
		DevicesViewControllerViewController *devices = segue.destinationViewController;
		StayViewController *stay = (StayViewController *) sender;
		devices.detailItem = stay.stay;
		_titleLabelPad.text = [NSString stringWithFormat:@"%@ - %@",[_detailItem valueForKey:@"name"],stay.stay.name];
		if (stay.stay.r_prevStay != nil) {
			_titleLabelPad.text = [NSString stringWithFormat:@"%@ - %@ - %@",[_detailItem valueForKey:@"name"],stay.stay.r_prevStay.name,stay.stay.name];
		}
	}
    else if ([segue.identifier isEqualToString:@"segueToZonesPad"]){
		ZonesViewController *zones = segue.destinationViewController;
		StayViewController *stay = (StayViewController *) sender;
        StaysViewController *stays = (StaysViewController *)(self.childViewControllers)[0];
        stays.view.userInteractionEnabled = NO;
        zones.isZone = YES;
		zones.detailItem = stay.stay;
		_titleLabelPad.text = [NSString stringWithFormat:@"%@ - %@",[_detailItem valueForKey:@"name"],stay.stay.name];
	}
}

-(StaysViewController *) getStaysViewController{
    return (self.childViewControllers)[0];
}

-(IBAction) exitDevices:(UIStoryboardSegue *) segue{
	
}

-(IBAction) exitZones:(UIStoryboardSegue *) segue{
	
}

-(IBAction) exitZonesPad:(UIStoryboardSegue *) segue{
	
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    [self.view removeGestureRecognizer:_additionalTapView];
    _additionalTapView = nil;
    self.staysView = nil;
    self.homeControl = nil;
    self.settingsControl = nil;
    self.environmentControl = nil;
    self.environmentsView = nil;
    self.settingsView = nil;
    self.stayArray = nil;
	_titleLabelPad = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) viewDidChanged:(UISwipeViewGestureRecognizer *)controller position:(NSUInteger)position{
	if (DEVICE_IS_IPAD) {
		switch (position) {
			case TAB_BAR_TO_ENVIRONMENTS:
				[self showEnvironments];
				break;
			case TAB_BAR_TO_SETTINGS:
				[self showSettings];
				break;
			default:
				[self dismissAdditionalView];
				break;
		}
	}
	else{
		[self changeTabBar:(NSUInteger)position];
	}
}

-(void) changeTabBar:(NSUInteger) position{
    if ((position == TAB_BAR_TO_LEFT || position == TAB_BAR_TO_RIGHT) ) {
        if (self.homeControl.userInteractionEnabled == NO) {
            position = position +3;
        }
        else {
            position = 2;
        }
    }
    self.settingsControl.userInteractionEnabled = YES;
    self.environmentControl.userInteractionEnabled = YES;
    self.homeControl.userInteractionEnabled = YES;
	
    if (position == TAB_BAR_TO_SETTINGS) {
        [self.settingsControl setSelected:YES];
        self.settingsControl.userInteractionEnabled = NO;
        [self.homeControl   setSelected:NO];
        [self.environmentControl setSelected:NO];
    }
    else if (position == TAB_BAR_TO_HOME){
        [self.settingsControl setSelected:NO];
        [self.environmentControl setSelected:NO];
        [self.homeControl   setSelected:YES];
        self.homeControl.userInteractionEnabled = NO;
    }
    else if (position == TAB_BAR_TO_ENVIRONMENTS){
        [self.settingsControl setSelected:NO];
        [self.homeControl   setSelected:NO];
        [self.environmentControl setSelected:YES];
        self.environmentControl.userInteractionEnabled = NO;
    }
	
}

-(IBAction)showSettings{
	if (DEVICE_IS_IPAD) {
		if (!_settingsView.userInteractionEnabled) {
			self.staysView.userInteractionEnabled = NO;
			self.zonesView.userInteractionEnabled = NO;
			self.devicesView.userInteractionEnabled = NO;
			[UIView animateWithDuration:ADD_VIEW_IPAD_ANIMATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
				[self moveIpadViews:NO];
				self.staysView.alpha = 0.4;
				self.zonesView.alpha = 0.4;
				self.devicesView.alpha = 0.4;
			} completion:^(BOOL finished) {
				_settingsView.userInteractionEnabled = YES;
                _additionalTapView.enabled = YES;
			}];
		}
	}
	else{
		BOOL repeat = NO;
		if (self.environmentControl.userInteractionEnabled == NO){
			repeat = YES;
		}
		[self changeTabBar:TAB_BAR_TO_SETTINGS];
		[self animateSwipeViews:YES repeat:repeat];

	}
}
-(IBAction)showEnvironments{
	if (DEVICE_IS_IPAD) {
		if (!_environmentsView.userInteractionEnabled) {
			self.staysView.userInteractionEnabled = NO;
			self.zonesView.userInteractionEnabled = NO;
			self.devicesView.userInteractionEnabled = NO;
			[UIView animateWithDuration:ADD_VIEW_IPAD_ANIMATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
				[self moveIpadViews:YES];
				self.staysView.alpha = 0.4;
				self.devicesView.alpha = 0.4;
				self.zonesView.alpha = 0.4;
			} completion:^(BOOL finished) {
				_environmentsView.userInteractionEnabled = YES;
                _additionalTapView.enabled = YES;
			}];
		}
	}
	else{
		BOOL repeat = NO;
		if (self.settingsControl.userInteractionEnabled == NO){
			repeat = YES;
		}
		[self changeTabBar:TAB_BAR_TO_ENVIRONMENTS];
		[self animateSwipeViews:NO repeat:repeat];
	}
 }

-(void) moveIpadViews:(BOOL) right{
	NSInteger value = 320;
	if (!right) {
		value = -320;
	}
	_settingsView.transform = CGAffineTransformMakeTranslation(value, 0);
	_environmentsView.transform = CGAffineTransformMakeTranslation(value, 0);
	_staysView.transform = CGAffineTransformMakeTranslation(value, 0);
	_zonesView.transform = CGAffineTransformMakeTranslation(value, 0);
	_devicesView.transform = CGAffineTransformMakeTranslation(value, 0);
	_titleLabelPad.transform = CGAffineTransformMakeTranslation(value, 0);
	_environmentControl.transform = CGAffineTransformMakeTranslation(value, 0);
	_settingsControl.transform = CGAffineTransformMakeTranslation(value, 0);
	_separatorView.transform = CGAffineTransformMakeTranslation(value, 0);

}

-(void) restoreIpadViews{
	_settingsView.transform = CGAffineTransformIdentity;
	_environmentsView.transform = CGAffineTransformIdentity;
	_staysView.transform = CGAffineTransformIdentity;
	_zonesView.transform = CGAffineTransformIdentity;
	_devicesView.transform = CGAffineTransformIdentity;
	_titleLabelPad.transform = CGAffineTransformIdentity;
	_environmentControl.transform = CGAffineTransformIdentity;
	_settingsControl.transform = CGAffineTransformIdentity;
	_separatorView.transform = CGAffineTransformIdentity;
}

-(IBAction)showHome{
    BOOL right = YES;
    if (self.settingsControl.userInteractionEnabled == NO){
        right = NO;
    }
    [self changeTabBar:TAB_BAR_TO_HOME];
	
    [self animateSwipeViews:right repeat:NO];
}

-(void) dismissAdditionalView{
    if (_environmentsView.userInteractionEnabled) {
        _additionalTapView.enabled = NO;
        _environmentsView.userInteractionEnabled = NO;
		[UIView animateWithDuration:ADD_VIEW_IPAD_ANIMATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            [self restoreIpadViews];
			self.staysView.alpha = 1.0;
			self.zonesView.alpha = 1.0;
            self.devicesView.alpha = 1.0;
		} completion:^(BOOL finished) {
			self.staysView.userInteractionEnabled = YES;
            self.devicesView.userInteractionEnabled = YES;
			self.zonesView.userInteractionEnabled = YES;
		}];
	}
    if (_settingsView.userInteractionEnabled) {
        _additionalTapView.enabled = NO;
        _settingsView.userInteractionEnabled = NO;
		[UIView animateWithDuration:ADD_VIEW_IPAD_ANIMATION delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
			[self restoreIpadViews];
            self.staysView.alpha = 1.0;
			self.view.transform = CGAffineTransformIdentity;
            self.devicesView.alpha = 1.0;
			self.zonesView.alpha = 1.0;
		} completion:^(BOOL finished) {
            self.staysView.userInteractionEnabled = YES;
            self.devicesView.userInteractionEnabled = YES;
			self.zonesView.userInteractionEnabled = YES;
		}];
	}
}

-(void) animateSwipeViews:(BOOL) right repeat:(BOOL) repeat{
    [UIView animateWithDuration:0.25 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        CGFloat offset = self.view.bounds.size.width;
        if (right) {
            offset = -self.view.bounds.size.width;
        }
		[self.settingsView setCenter:CGPointMake(self.settingsView.center.x + offset, self.staysView.center.y)];
		[self.environmentsView setCenter:CGPointMake(self.environmentsView.center.x+ offset, self.staysView.center.y)];
		[self.staysView setCenter:CGPointMake(self.staysView.center.x + offset, self.staysView.center.y)];
		
    } completion:^(BOOL finished){
        if (repeat == YES) {
            [self animateSwipeViews:right repeat:NO];
        }
    }];
}

-(void) changeConfigFile{
    [(RootViewController *)self.parentViewController changeConfigFile];
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    CGPoint pointInView = [touch locationInView:gestureRecognizer.view];
	
    if ( [gestureRecognizer isMemberOfClass:[UITapGestureRecognizer class]]){
		if (CGRectContainsPoint(_settingsView.frame, pointInView) || CGRectContainsPoint(_environmentsView.frame, pointInView)) {
			return NO;
		}
	}
    return YES;
}

#pragma mark BacgroundPhoto

-(void) setBackgroundImage:(UIImage *) image{
	if ([self checkBackgroundImageEnabled:[[UIApplication sharedApplication] statusBarOrientation]]) {
		_backgroundImageView.alpha = 0.7;
		[_backgroundImageView setImage:image];
		[UIView animateWithDuration:.6 animations:^{
			_backgroundImageView.alpha = .8;
		}];
	}
}

-(void) removeBackgroundImage {
	[_backgroundImageView setImage:nil];
}
@end