//
//  TypeRegBlindViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SliderButtonsViewController.h"

@interface TypeRegBlindViewController : SliderButtonsViewController <DeviceStatusDelegate>

@end
