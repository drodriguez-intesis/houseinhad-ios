//
//  DeviceTypeSegmented.m
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeSegmented.h"


@implementation DeviceTypeSegmented

@dynamic fifthText;
@dynamic fifthValue;
@dynamic firstText;
@dynamic firstValue;
@dynamic fourthText;
@dynamic fourthValue;
@dynamic numSegments;
@dynamic secondText;
@dynamic secondValue;
@dynamic thirdText;
@dynamic thirdValue;
@dynamic readOnly;

@end
