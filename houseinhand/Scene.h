//
//  Scene.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Config, SceneTelegram;

@interface Scene : NSManagedObject

@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * order;
@property (nonatomic, retain) Config *r_config;
@property (nonatomic, retain) NSSet *r_sceneTelegram;
@end

@interface Scene (CoreDataGeneratedAccessors)

- (void)addR_sceneTelegramObject:(SceneTelegram *)value;
- (void)removeR_sceneTelegramObject:(SceneTelegram *)value;
- (void)addR_sceneTelegram:(NSSet *)values;
- (void)removeR_sceneTelegram:(NSSet *)values;

@end
