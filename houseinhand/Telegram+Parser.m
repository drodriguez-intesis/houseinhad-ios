//
//  Telegram+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Telegram+Parser.h"


#define KEY_ADDRESS			@"address"
#define KEY_DPT					@"dpt"
#define KEY_DELAY				@"delay"
#define KEY_TYPE					@"type"
#define KEY_VALUE				@"value"
#define KEY_CODE				@"code"

#define DPT_TYPE_1_BIT						@"1.001"
#define DPT_TYPE_1_BYTE					@"5.004"
#define DPT_TYPE_1_BYTE_CLIMA			@"XX.001"			//readonly
#define DPT_TYPE_1_BYTE_PULSE			@"6.010"
#define DPT_TYPE_1_BYTE_SCALING		@"5.001"
#define DPT_TYPE_1_BYTE_ANGLE		@"5.003"

#define DPT_TYPE_2_BYTE					@"9.001" 				//readonly
#define DPT_TYPE_4_BYTE					@"13.01X" 			//readonly
#define DPT_TYPE_4_BYTE_POWER		@"14.056" 			//readonly
#define DPT_TYPE_14_BYTE					@"16.001" 			//readonly

@implementation Telegram (Parser)
+(Telegram *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc{
	Telegram *telegram = (Telegram *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	telegram.address = (info[KEY_ADDRESS] ? @([info[KEY_ADDRESS] integerValue]): telegram.address);
	telegram.dpt = (info[KEY_DPT] ? info[KEY_DPT]: telegram.dpt);
	telegram.delay = (info[KEY_DELAY] ? @([info[KEY_DELAY] floatValue]): telegram.delay);
	telegram.type = (info[KEY_TYPE] ? @([info[KEY_TYPE] integerValue]): telegram.type);
	if (info[KEY_VALUE]) {
		NSNumberFormatter * f = [[NSNumberFormatter alloc] init];
		[f setNumberStyle:NSNumberFormatterDecimalStyle];
		NSNumber *number = [f numberFromString:info[KEY_VALUE]];
		if (number) 	telegram.value = (info[KEY_VALUE] ? number: telegram.value);
		else 	telegram.stringValue = (info[KEY_VALUE] ? info[KEY_VALUE]: telegram.stringValue);
	}
	telegram.code = (info[KEY_CODE] ? @([info[KEY_CODE] integerValue]): telegram.code);
	
	return telegram;
}

-(void) setValueFromData:(NSData *) data{
	if (data.length ==[Telegram getSizeOfDpt:DPT_TYPE_1_BIT] && [self.dpt isEqualToString:DPT_TYPE_1_BIT]) {
		char dat;
		[data getBytes:&dat length:1];
		self.value = @(dat&0x01);
	}
	else if (data.length ==[Telegram getSizeOfDpt:DPT_TYPE_1_BYTE]){
        NSUInteger dat =0;
        [data getBytes:&dat range:NSMakeRange(1, 1)];
        if ([self.dpt isEqualToString:DPT_TYPE_1_BYTE] || [self.dpt isEqualToString:DPT_TYPE_1_BYTE_CLIMA] || [self.dpt isEqualToString:DPT_TYPE_1_BYTE_PULSE]) {
            self.value = @(dat);
        }
        else if ([self.dpt isEqualToString:DPT_TYPE_1_BYTE_SCALING]){
            CGFloat conversion = (CGFloat) dat*100/255;
            self.value = @(conversion);
        }
        else if ([self.dpt isEqualToString:DPT_TYPE_1_BYTE_ANGLE]){
            CGFloat conversion = (CGFloat) dat*360/255;
            self.value = @(conversion);
        }
	}
	else if (data.length ==[Telegram getSizeOfDpt:DPT_TYPE_2_BYTE] && [self.dpt isEqualToString:DPT_TYPE_2_BYTE]){
		self.value = [NSNumber numberWithFloat:[self create2ByteFromData:[data subdataWithRange:NSMakeRange(1, 2)]]];
	}
    else if (data.length ==[Telegram getSizeOfDpt:DPT_TYPE_4_BYTE]){
        if ([self.dpt isEqualToString:DPT_TYPE_4_BYTE]) {
            int value =0;
            NSData *subdata = [data subdataWithRange:NSMakeRange(1, 4)];
            value = CFSwapInt32BigToHost(*(int*)([subdata bytes]));
            self.value = @(value);
        }
        else if ([self.dpt isEqualToString:DPT_TYPE_4_BYTE_POWER]){
            float realValue = [Telegram floatAtOffset:1 inData:data];
            self.value = @(realValue);
        }
	}
	else if (data.length ==[Telegram getSizeOfDpt:DPT_TYPE_14_BYTE] && [self.dpt isEqualToString:DPT_TYPE_14_BYTE]){
		self.stringValue = [[NSString alloc] initWithData:[data subdataWithRange:NSMakeRange(1, 14)] encoding:NSASCIIStringEncoding];
	}
}

union intToFloat
{
    uint32_t i;
    float fp;
};

+(float)floatAtOffset:(NSUInteger)offset
               inData:(NSData*)data
{
    assert([data length] >= offset + sizeof(float));
    union intToFloat convert;
    
    const uint32_t* bytes = [data bytes] + offset;
    convert.i = CFSwapInt32BigToHost(*bytes);
    
    const float value = convert.fp;
    
    return value;
}


-(CGFloat) create2ByteFromData:(NSData *) data{
	unsigned char dat[2];
	int16_t mantisa =0;
	[data getBytes:&dat range:NSMakeRange(0, 2)];
	mantisa = (((dat[0]&0x07)&0x07)<<8)|dat[1];
	NSUInteger expo = (dat[0]>>3)&0x000F;
	if ((dat[0]>>7)&0x01) mantisa = ((mantisa)|0xFFFFF800);
	CGFloat info = (0.01*(CGFloat) mantisa * pow(2, expo));
	return info;	
}

+(NSData *) getDataFromInfo:(id) info dpt:(NSString *) dpt write:(BOOL) write{
	NSUInteger size = (write ?[Telegram getSizeOfDpt:dpt] : 1);
    if (size >0) {
        unsigned char array[size];
        if (!write || [dpt isEqualToString:DPT_TYPE_1_BIT]) {
            array[0] = (write ? (0x80 + [info unsignedIntegerValue]): 0);
        }
        else if ([dpt isEqualToString:DPT_TYPE_1_BYTE] || [dpt isEqualToString:DPT_TYPE_1_BYTE_CLIMA]){
            array[0] = 0x80;
            array[1] = (char) [info unsignedIntegerValue];
        }
        else if ([dpt isEqualToString:DPT_TYPE_1_BYTE_PULSE]){
            array[0] = 0x80;
            array[1] = (char) [info integerValue];
        }
        else if ([dpt isEqualToString:DPT_TYPE_1_BYTE_SCALING]){
            array[0] = 0x80;
            NSUInteger val = [info unsignedIntegerValue] *255/100;
            array[1] = (char) val;
        }
        else if ([dpt isEqualToString:DPT_TYPE_1_BYTE_ANGLE]){
            array[0] = 0x80;
            NSUInteger val = [info unsignedIntegerValue] *255/360;
            array[1] = (char) val;
        }
        else if ([dpt isEqualToString:DPT_TYPE_2_BYTE]){
            array[0] = 0x80;
            NSUInteger value = 0;
            value = ([info floatValue] > 40.0 ? (NSUInteger)( [info floatValue] / (0.01 * 4)) : (NSUInteger)( [info floatValue] / (0.01 * 2)));
            array[1] = ([info floatValue] > 40.0 ? (char) (((value>>8)|0x10)&0x17) : (char) (((value>>8)|0x08)&0x0F));
            array[2]= (char) value;
        }
        NSData* data = [NSData dataWithBytes:(const void *)array length:sizeof(unsigned char)*size];
        return data;
    }
	return nil;
}



+(NSUInteger) getSizeOfDpt:(NSString *) dpt{
	NSUInteger size = 0;
	if ([dpt isEqualToString:DPT_TYPE_1_BIT]) {
		size = 1;
	}
	else if ([dpt isEqualToString:DPT_TYPE_1_BYTE] || [dpt isEqualToString:DPT_TYPE_1_BYTE_PULSE] || [dpt isEqualToString:DPT_TYPE_1_BYTE_CLIMA] || [dpt isEqualToString:DPT_TYPE_1_BYTE_SCALING] || [dpt isEqualToString:DPT_TYPE_1_BYTE_ANGLE]){
		size = 2;
	}
	else if ([dpt isEqualToString:DPT_TYPE_2_BYTE]){
		size = 3;
	}
    else if ([dpt isEqualToString:DPT_TYPE_4_BYTE] || [dpt isEqualToString:DPT_TYPE_4_BYTE_POWER]){
		size = 5;
	}
	else if ([dpt isEqualToString:DPT_TYPE_14_BYTE]){
		size = 15;
	}
	return size;
}

@end
