//
//  TypeClima1bViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 12/12/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeClima1bViewController.h"
#import "DeviceTypeClima.h"

#define SEGMENTED_SELECTED_COMFORT		0
#define SEGMENTED_SELECTED_STANDBY			1
#define SEGMENTED_SELECTED_NIGHT				2
#define SEGMENTED_SELECTED_EXTREMES		3
#define SEGMENTED_SELECTED_LOCK				4

@interface TypeClima1bViewController ()
@property (assign,nonatomic) NSUInteger currentValue;
@property (strong,nonatomic) Telegram *comfortTelegram;
@property (strong,nonatomic) Telegram *standbyTelegram;
@property (strong,nonatomic) Telegram *nightTelegram;
@property (strong,nonatomic) Telegram *extremesTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@property (strong,nonatomic) Telegram *lockTelegram;
@end

@implementation TypeClima1bViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeClima*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self readControlStatus];
		_currentValue = [self readSceneStatus];

    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
	[self updateCurrentImage:NO];
	[self setActions];
	if (super.type == TYPE_CONTROL) {
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
	else{
		[self updateSegmentedScenes];
	}
}

-(void) updateSegmentedScenes{
	super.leftImage.hidden = YES;
	super.rightImage.hidden = YES;
	[super.centralSegmented setSelectedSegmentIndex:(_currentValue > 0 ? _currentValue-1 : 0)];
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (_currentValue !=	_statusTelegram.value.integerValue) {
		_currentValue = _statusTelegram.value.integerValue;
		[self updateCurrentImage:animated];
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
	_comfortTelegram = nil;
	_standbyTelegram = nil;
	_nightTelegram = nil;
	_extremesTelegram = nil;
	_statusTelegram = nil;
	_lockTelegram = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

-(void) setActions{
    [super.centralSegmented addTarget:self action:@selector(segmentedPressed:) forControlEvents:UIControlEventValueChanged];
}

-(void)segmentedPressed:(UISegmentedControl *)sender{
//	[super playButtonSound];
	if (sender.selectedSegmentIndex < SEGMENTED_SELECTED_LOCK && sender.selectedSegmentIndex != UISegmentedControlNoSegment) {
		//unlock controller
		if (super.hasLock) {
			[super sendData:@(0) address:_lockTelegram.address dpt:_lockTelegram.dpt type:_lockTelegram.type delay:_lockTelegram.delay];
		}
		[self sendOneMode:sender.selectedSegmentIndex];

	}
	else{
		//lock controller
		[super sendData:@(1) address:_lockTelegram.address dpt:_lockTelegram.dpt type:_lockTelegram.type delay:_lockTelegram.delay];
	}
}

-(void) sendOneMode:(NSInteger) index{
	switch (index) {
		case SEGMENTED_SELECTED_COMFORT:
			[super sendData:@(0) address:_extremesTelegram.address dpt:_extremesTelegram.dpt type:_extremesTelegram.type delay:_extremesTelegram.delay];
			[super sendData:@(1) address:_comfortTelegram.address dpt:_comfortTelegram.dpt type:_comfortTelegram.type delay:@([_comfortTelegram.delay floatValue] +.1)];
			break;
		case SEGMENTED_SELECTED_STANDBY:
			[super sendData:@(0) address:_extremesTelegram.address dpt:_extremesTelegram.dpt type:_extremesTelegram.type delay:_extremesTelegram.delay];
			[super sendData:@(0) address:_comfortTelegram.address dpt:_comfortTelegram.dpt type:_comfortTelegram.type delay:@([_comfortTelegram.delay floatValue] +.1)];
			[super sendData:@(1) address:_standbyTelegram.address dpt:_standbyTelegram.dpt type:_standbyTelegram.type delay:@([_standbyTelegram.delay floatValue] +.2)];
			break;
		case SEGMENTED_SELECTED_NIGHT:
			[super sendData:@(0) address:_extremesTelegram.address dpt:_extremesTelegram.dpt type:_extremesTelegram.type delay:_extremesTelegram.delay];
			[super sendData:@(0) address:_comfortTelegram.address dpt:_comfortTelegram.dpt type:_comfortTelegram.type delay:@([_comfortTelegram.delay floatValue] +.1)];
			[super sendData:@(0) address:_standbyTelegram.address dpt:_standbyTelegram.dpt type:_standbyTelegram.type delay:@([_standbyTelegram.delay floatValue] +.2)];
			[super sendData:@(1) address:_nightTelegram.address dpt:_nightTelegram.dpt type:_nightTelegram.type delay:@([_nightTelegram.delay floatValue] +.3)];

			break;
		case SEGMENTED_SELECTED_EXTREMES:
			[super sendData:@(1) address:_extremesTelegram.address dpt:_extremesTelegram.dpt type:_extremesTelegram.type delay:_extremesTelegram.delay];

			break;
		default:
			break;
	}
}

-(void) readControlStatus{
	_statusTelegram = [super.device getTelegramWithCode:0];
	_currentValue = _statusTelegram.value.integerValue;
	_comfortTelegram = [super.device getTelegramWithCode:1];
	_lockTelegram = [super.device getTelegramWithCode:2];
	_standbyTelegram = [super.device getTelegramWithCode:3];
	_nightTelegram = [super.device getTelegramWithCode:4];
	_extremesTelegram = [super.device getTelegramWithCode:5];
}



-(CGFloat) readSceneStatus{
    NSInteger status=0;
	SceneTelegram *comfort = [super.scene getTelegramWithAddress:_comfortTelegram.address.integerValue];
	SceneTelegram *lock = [super.scene getTelegramWithAddress:_lockTelegram.address.integerValue];
	SceneTelegram *standby = [super.scene getTelegramWithAddress:_standbyTelegram.address.integerValue];
	SceneTelegram *night = [super.scene getTelegramWithAddress:_nightTelegram.address.integerValue];
	SceneTelegram *extremes = [super.scene getTelegramWithAddress:_extremesTelegram.address.integerValue];
	if (comfort || lock || standby || night || extremes) {
		super.widgetSceneActive = YES;
		if (extremes.value.boolValue == YES) status = 4;
		else if (night.value.boolValue == YES) status = 3;
		else if (standby.value.boolValue == YES) status = 2;
		else if (comfort.value.boolValue == YES) status = 1;
		else if (lock.value.boolValue == YES) status = 5;
	}
	return status;
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

-(void) updateCurrentImage:(BOOL) animated{
	if (animated) {
	 	[super animationUpdate:super.leftImage];
	}
	[self setLeftImage];
	[self setRightImage];
	
}

-(void) setRightImage{
	NSString *im = @"device_mode_heat";
	if ((_currentValue & 0x0020) == 32){
		//cold
		im = @"device_mode_cool";
	}
	[super.rightImage setImage:[UIImage imageNamed:im]];
}

-(void) setLeftImage{
	NSString *im = @"device_mode_comfort";
	if ((_currentValue & 0x0010) == 16){
		//bloqueado
		im = @"device_mode_locked";
		if ([[super.device valueForKey:@"hasLock"] boolValue] == YES) [super.centralSegmented setSelectedSegmentIndex:4];
	}
	else if ((_currentValue & 0x0011) == 1){
		//confort
		im = @"device_mode_comfort";
		[super.centralSegmented setSelectedSegmentIndex:0];
	}else if ((_currentValue & 0x0013) == 2){
		//stby
		im = @"device_mode_standby";
		[super.centralSegmented setSelectedSegmentIndex:1];
	}else if ((_currentValue & 0x0017) == 4){
		//nocturno
		im = @"device_mode_night";
		[super.centralSegmented setSelectedSegmentIndex:2];
	}
	else if ((_currentValue & 0x001F) == 8){
		//extremos
		im = @"device_mode_extremes";
		[super.centralSegmented setSelectedSegmentIndex:3];
	}
	[super.leftImage setImage:[UIImage imageNamed:im]];
}

-(void) toRealValue:(NSUInteger) index{
	const NSArray *realValue = @[@23,@22,@21,@23,@24];
	_currentValue = [realValue[index] integerValue];;
}

-(NSArray *) getSceneStatus{
	NSMutableArray *sceneStatus;
	if ([super needSceneStatus]) {
		if (super.hasLock) {
			if (super.centralSegmented.selectedSegmentIndex < 4 && super.centralSegmented.selectedSegmentIndex !=UISegmentedControlNoSegment) {
				sceneStatus = [@[@{
							   @"address": _lockTelegram.address,
							   @"dpt": _lockTelegram.dpt,
							   @"value": @0}] mutableCopy];
			}
			else{
				sceneStatus = [@[@{
					@"address": _lockTelegram.address,
					@"dpt": _lockTelegram.dpt,
					@"value": @1}] mutableCopy];
			}
		}
		if (super.centralSegmented.selectedSegmentIndex < 4 && super.centralSegmented.selectedSegmentIndex !=UISegmentedControlNoSegment) {
			if (!sceneStatus) 	sceneStatus = [NSMutableArray array];
			switch (super.centralSegmented.selectedSegmentIndex) {
				case SEGMENTED_SELECTED_COMFORT:
					[sceneStatus addObject:@{
					 @"address": _extremesTelegram.address,
					 @"dpt": _extremesTelegram.dpt,
					 @"value": @0,
					 @"delay": @.1}];
					[sceneStatus addObject:@{
					 @"address": _comfortTelegram.address,
					 @"dpt": _comfortTelegram.dpt,
					 @"value": @1,
					 @"delay": @.2}];
					break;
				case SEGMENTED_SELECTED_STANDBY:
					[sceneStatus addObject:@{
					 @"address": _extremesTelegram.address,
					 @"dpt": _extremesTelegram.dpt,
					 @"value": @0,
					 @"delay": @.1}];
					[sceneStatus addObject:@{
					 @"address": _comfortTelegram.address,
					 @"dpt": _comfortTelegram.dpt,
					 @"value": @0,
					 @"delay": @.2}];
					[sceneStatus addObject:@{
					 @"address": _standbyTelegram.address,
					 @"dpt": _standbyTelegram.dpt,
					 @"value": @1,
					 @"delay": @.3}];
					break;
				case SEGMENTED_SELECTED_NIGHT:
					[sceneStatus addObject:@{
					 @"address": _extremesTelegram.address,
					 @"dpt": _extremesTelegram.dpt,
					 @"value": @0,
					 @"delay": @.1}];
					[sceneStatus addObject:@{
					 @"address": _comfortTelegram.address,
					 @"dpt": _comfortTelegram.dpt,
					 @"value": @0,
					 @"delay": @.2}];
					[sceneStatus addObject:@{
					 @"address": _standbyTelegram.address,
					 @"dpt": _standbyTelegram.dpt,
					 @"value": @0,
					 @"delay": @.3}];

					[sceneStatus addObject:@{
					 @"address": _nightTelegram.address,
					 @"dpt": _nightTelegram.dpt,
					 @"value": @1,
					 @"delay": @.4}];
					break;
				case SEGMENTED_SELECTED_EXTREMES:
					[sceneStatus addObject:@{
					 @"address": _extremesTelegram.address,
					 @"dpt": _extremesTelegram.dpt,
					 @"value": @1,
					 @"delay": @.1}];
					break;
				default:
					break;
			}
		}
	}
	return sceneStatus;
}
@end
