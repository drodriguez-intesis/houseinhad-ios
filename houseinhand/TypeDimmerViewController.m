//
//  TypeDimmerViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeDimmerViewController.h"
#import "DeviceTypeSlider.h"

@interface TypeDimmerViewController ()

@property (assign,nonatomic) NSUInteger currentValue;

@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) Telegram *statusTelegram;
@end

@implementation TypeDimmerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider*)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        // Custom initialization
        _currentValue = (NSUInteger)[self readControlStatus];

    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSlider*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		[self readControlStatus];
        _currentValue = (NSUInteger)[self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];
    [super.slider setValue:_currentValue animated:NO];
	if (super.type == TYPE_CONTROL) {
		[self checkStatus];
		if ([super.device.hasStatus boolValue]) {
			[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
		}
	}
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _statusTelegram = nil;
    _actionTelegram = nil;

}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (_currentValue !=	_statusTelegram.value.integerValue) {
		_currentValue = _statusTelegram.value.integerValue;
		[super.slider setValue:_currentValue animated:YES];
	}
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

-(CGFloat) readControlStatus{
    NSUInteger status= 0;
	_statusTelegram = [super.device getTelegramWithCode:0];
	status =[_statusTelegram.value integerValue];
	_actionTelegram = [super.device getTelegramWithCode:1];

	return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=NO;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}



-(void) setActions{
    [super.leftButton addTarget:self action:@selector(leftButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.rightButton addTarget:self action:@selector(rightButtonPressed) forControlEvents:UIControlEventTouchDown];
    [super.slider addTarget:self action:@selector(valueChanged) forControlEvents:UIControlEventValueChanged];
}

-(void) valueChanged{
    if (((NSUInteger)(super.slider.value - super.minValue) % (NSUInteger)self.step) == 0) {
        // es multiple!
        _currentValue = (NSUInteger) super.slider.value;
    }
    else{
        [super.slider setValue:super.slider.value - 1 animated:YES];
        [self valueChanged];
    }
       //Send
    [self sendButtonData:_currentValue];
}
-(void)leftButtonPressed{
    [super.slider setValue:super.slider.minimumValue animated:YES];
    [self valueChanged];
}

-(void)rightButtonPressed{
    [super.slider setValue:super.slider.maximumValue animated:YES];
    [self valueChanged];
}

-(void) sendButtonData:(NSUInteger) value{
    [super sendData:@(value) address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];
}

-(void) updateSliderValueAnimated:(BOOL) animated{
    [super.slider setValue:_currentValue animated:animated];
}

-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = [NSDictionary dictionaryWithObjectsAndKeys:_actionTelegram.address,@"address",@(_currentValue),@"value",_actionTelegram.dpt,@"dpt",nil];
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
