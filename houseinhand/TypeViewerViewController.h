//
//  TypeViewerViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/6/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ViewerViewController.h"

@interface TypeViewerViewController : ViewerViewController <DeviceStatusDelegate>

@end
