//
//  SettingsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "SettingsViewController.h"
#import "InitialViewController.h"
#import "RootViewController.h"
#import "ProfilesThirdLevelViewController.h"
#import "RgbThirdLevelViewController.h"
#import "AppDelegate.h"
#import "Config+Parser.h"
#import "Settings.h"
#import "Network.h"
#import "Preferences.h"
#import "KNXCommunication.h"
#import "AccountViewController.h"
#import "MigrationViewController.h"
#import "YoutubeViewController.h"

#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.1

@interface SettingsViewController ()
@property (strong,nonatomic) NSManagedObjectContext *settingsContext;
@property (strong,nonatomic) Config *activeConfig;
@property	 (strong,nonatomic) UIView *dimView;
@property (weak, nonatomic) IBOutlet UIImageView *topOrangeView;
@property (weak,nonatomic) IBOutlet UIButton *sounds;
@property (weak,nonatomic) IBOutlet UIButton *passcode;
@property (weak,nonatomic) IBOutlet UIButton *saveImages;
@property (weak,nonatomic) IBOutlet UIButton *screenRest;
@property (weak,nonatomic) IBOutlet UIButton *connectionStatus;
@property (weak,nonatomic) IBOutlet UIButton *advancedSettingsButton;
@property (weak, nonatomic) IBOutlet UIButton *accountStatus;
@property (weak,nonatomic) IBOutlet UILabel *advancedSettingsLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;
@property (weak, nonatomic) IBOutlet UILabel *screenRestLabel;
@property (weak, nonatomic) IBOutlet UILabel *languageLabel;
@property (weak, nonatomic) IBOutlet UILabel *soundsLabel;
@property (weak, nonatomic) IBOutlet UILabel *profileLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareProfileLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeProfileLabel;
@property (weak, nonatomic) IBOutlet UILabel *deleteProfileLabel;
@property (weak, nonatomic) IBOutlet UILabel *passcodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveImagesLabel;
@property (weak, nonatomic) IBOutlet UILabel *networkLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusNetworkLabel;
@property (weak, nonatomic) IBOutlet UILabel *accountLabel;
@property (weak, nonatomic) IBOutlet UILabel *settingsAccountLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusAccountLabel;
@property (weak, nonatomic) IBOutlet UIButton *licenseExpirationStatus;
@property (weak, nonatomic) IBOutlet UILabel *licenseExpiration;
@property (strong,nonatomic) ProfilesThirdLevelViewController *alertViewProfile;
@property (strong,nonatomic) Config *alertViewConfig;
@end

@implementation SettingsViewController

-(void) viewDidLoad{
    [super viewDidLoad];
	[[NSUserDefaults standardUserDefaults] addObserver:self forKeyPath:USER_DEFAULTS_CONNECTION options:NSKeyValueObservingOptionNew context:nil];
	[[NSUserDefaults standardUserDefaults] addObserver:self forKeyPath:USER_DEFAULTS_LOGGED options:NSKeyValueObservingOptionNew context:nil];
	[[NSUserDefaults standardUserDefaults] addObserver:self forKeyPath:USER_DEFAULTS_EXPIRATION_DATE options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTranslations) name:NOT_LANGUAGE_CHANGED object:nil];

        //    self.scrollView.translatesAutoresizingMaskIntoConstraints = YES;

        //   self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 730);
        //    [self.scrollView setContentSize:CGSizeMake(self.view.bounds.size.width, 1000)];
	_topOrangeView.backgroundColor = APP_COLOR_ORANGE;
    [self createContext];
	[self setTranslations];
    [self setCurrentValues];
	[self loadSpecificSettings];
}

-(void) setTranslations{
	_titleName.text = NSLocalizedStringFromTableInBundle(@"Settings", nil, CURRENT_LANGUAGE_BUNDLE, @"title of navigation bar style");
	_advancedSettingsLabel.text = NSLocalizedStringFromTableInBundle(@"Advanced", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_moreLabel.text = [NSLocalizedStringFromTableInBundle(@"More", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment") uppercaseString];
	_screenRestLabel.text = NSLocalizedStringFromTableInBundle(@"Automatic rest of screen", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_languageLabel.text = NSLocalizedStringFromTableInBundle(@"Language", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_soundsLabel.text = NSLocalizedStringFromTableInBundle(@"Sounds", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_profileLabel.text = NSLocalizedStringFromTableInBundle(@"Profile", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment").uppercaseString;
	_shareProfileLabel.text = NSLocalizedStringFromTableInBundle(@"Share", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_changeProfileLabel.text = NSLocalizedStringFromTableInBundle(@"Change", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_deleteProfileLabel.text = NSLocalizedStringFromTableInBundle(@"Delete", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_passcodeLabel.text = NSLocalizedStringFromTableInBundle(@"Protect profile", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_saveImagesLabel.text = NSLocalizedStringFromTableInBundle(@"Save taken images", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_networkLabel.text = NSLocalizedStringFromTableInBundle(@"Network", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment").uppercaseString;
	_statusNetworkLabel.text = NSLocalizedStringFromTableInBundle(@"Status", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_accountLabel.text = NSLocalizedStringFromTableInBundle(@"Account", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment").uppercaseString;
	_settingsAccountLabel.text = NSLocalizedStringFromTableInBundle(@"Settings", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_statusAccountLabel.text = NSLocalizedStringFromTableInBundle(@"Status", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
	_licenseExpiration.text = NSLocalizedStringFromTableInBundle(@"Expiration", nil, CURRENT_LANGUAGE_BUNDLE, @"option of settings fragment");
    
	[self changeAccountStatus:[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_LOGGED]];
	[self changeConnection:[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_CONNECTION]];
	[self changeLicenseStatus:[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_EXPIRATION_DATE]];
    
    [self changeButtonState:_passcode];
    [self changeButtonState:_screenRest];
    [self changeButtonState:_saveImages];
}

-(void) observeValueForKeyPath:(NSString *)keyPath ofObject:(NSUserDefaults *)object change:(NSDictionary *)change context:(void *)context{
	if ([keyPath isEqualToString:USER_DEFAULTS_CONNECTION]) {
		[self changeConnection:[object objectForKey:USER_DEFAULTS_CONNECTION]];
	}
	else if ([keyPath isEqualToString:USER_DEFAULTS_LOGGED]) {
		[self changeAccountStatus:[object objectForKey:USER_DEFAULTS_LOGGED]];
	}
	else if ([keyPath isEqualToString:USER_DEFAULTS_EXPIRATION_DATE]) {
		[self changeLicenseStatus:[object objectForKey:USER_DEFAULTS_EXPIRATION_DATE]];
	}
}

-(void) changeConnection:(NSNumber *) state{
	if ([state boolValue]) {
		[_connectionStatus setTitle:NSLocalizedStringFromTableInBundle(@"Connected", nil, CURRENT_LANGUAGE_BUNDLE, @"settings view") forState:UIControlStateNormal];
        [_connectionStatus setTitleColor:[UIColor colorWithRed:0.0/255.0 green:155.0/255.0 blue:0.0/255.0 alpha:0.8] forState:UIControlStateNormal];
	}
	else{
		[_connectionStatus setTitle:NSLocalizedStringFromTableInBundle(@"Disconnected", nil, CURRENT_LANGUAGE_BUNDLE, @"settings view") forState:UIControlStateNormal];
        [_connectionStatus setTitleColor:[UIColor colorWithRed:155.0/255.0 green:0.0/255.0 blue:0.0/255.0 alpha:0.8] forState:UIControlStateNormal];
	}
}

-(void) changeAccountStatus:(NSNumber *) state{
	if ([state boolValue]) {
		[_accountStatus setTitle:NSLocalizedStringFromTableInBundle(@"Logged", nil, CURRENT_LANGUAGE_BUNDLE, @"settings view") forState:UIControlStateNormal];
        [_accountStatus setTitleColor:APP_COLOR_ORANGE_DARK forState:UIControlStateNormal];
	}
	else{
		[_accountStatus setTitle:NSLocalizedStringFromTableInBundle(@"Not Logged", nil, CURRENT_LANGUAGE_BUNDLE, @"settings view") forState:UIControlStateNormal];
        [_accountStatus setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	}
}

-(void) changeLicenseStatus:(NSString *) date{
	dispatch_async(dispatch_get_main_queue(), ^{
		if (_activeConfig.isDemo.boolValue) {
			[_licenseExpirationStatus setTitle:@"-" forState:UIControlStateNormal];
		}
		else{
			[_licenseExpirationStatus setTitle:date forState:UIControlStateNormal];
		}
	});
}

-(void) dealloc{
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:USER_DEFAULTS_CONNECTION];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:USER_DEFAULTS_LOGGED];
	[[NSUserDefaults standardUserDefaults] removeObserver:self forKeyPath:USER_DEFAULTS_EXPIRATION_DATE];

}
-(void) viewDidUnload{
    _scrollView = nil;
    _settingsContext = nil;
	_sounds = nil;
	_passcode = nil;
	_saveImages = nil;
	_screenRest = nil;
	_connectionStatus = nil;
	_advancedSettingsButton = nil;
	_advancedSettingsLabel = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOT_LANGUAGE_CHANGED object:nil];
    [super viewDidUnload];
}

-(void) loadSpecificSettings{
	
	if (_activeConfig.isDemo.boolValue) {
		_advancedSettingsButton.userInteractionEnabled = NO;
		_advancedSettingsButton.alpha = ALPHA_UNSELECTED;
		_advancedSettingsLabel.alpha = ALPHA_UNSELECTED;
	}
	Preferences *pref = (Preferences *) _activeConfig.r_settings.r_preferences;
	_sounds.selected = [pref.sounds boolValue];
	[self changeButtonState:_sounds];
	_passcode.selected = [pref.locked boolValue];
	[self changeButtonState:_passcode];
	_saveImages.selected = [pref.saveImages boolValue];
	[self changeButtonState:_saveImages];
	_screenRest.selected = [pref.screenRest boolValue];
	[self changeButtonState:_screenRest];
	[[NSUserDefaults standardUserDefaults] setValue:pref.screenRest forKey:USER_DEFAULTS_SAVE_IDLE_TIMER];
	[[NSUserDefaults standardUserDefaults] setValue:pref.sounds forKey:USER_DEFAULTS_SOUNDS];
	[[NSUserDefaults standardUserDefaults] setValue:pref.saveImages forKey:USER_DEFAULTS_SAVE_IMAGES];
	[[NSUserDefaults standardUserDefaults] setValue:pref.locked forKey:USER_DEFAULTS_PASSCODE];
	
	[[UIApplication sharedApplication] setIdleTimerDisabled:!_screenRest.selected];
}

-(void) setSpecificSettings{
	Preferences *pref = (Preferences *) _activeConfig.r_settings.r_preferences;
	pref.sounds = @(_sounds.isSelected);
	pref.locked = @(_passcode.isSelected);
	pref.saveImages = @(_saveImages.isSelected);
	pref.screenRest = @(_screenRest.isSelected);
	[[UIApplication sharedApplication] setIdleTimerDisabled:!_screenRest.isSelected];
	[[NSUserDefaults standardUserDefaults] setValue:pref.screenRest forKey:USER_DEFAULTS_SAVE_IDLE_TIMER];
	[[NSUserDefaults standardUserDefaults] setValue:pref.sounds forKey:USER_DEFAULTS_SOUNDS];
	[[NSUserDefaults standardUserDefaults] setValue:pref.saveImages forKey:USER_DEFAULTS_SAVE_IMAGES];
	[[NSUserDefaults standardUserDefaults] setValue:pref.locked forKey:USER_DEFAULTS_PASSCODE];
	[self saveContext];

}

-(IBAction)changeSounds:(UIButton *)sender{
	[self invertPreference:sender];
	
}

-(IBAction)changePasscode:(UIButton *)sender{
	[self invertPreference:sender];

}

-(IBAction)changeSaveImages:(UIButton *)sender{
	[self invertPreference:sender];

}

-(IBAction)changeScreenRest:(UIButton *)sender{
	[self invertPreference:sender];

}

-(IBAction)netModeNetworkSettings{
	NetworkSettingsViewController *net = [[NetworkSettingsViewController alloc] initWithNibName:@"NetworkSettingsViewController" bundle:nil activeConfig:_activeConfig];
	net.networkDelegate = self;
	[self presentViewController:net];
}

-(IBAction)advancedNetworkSettings{
	NetworkSettingsViewController *net = [[NetworkSettingsViewController alloc] initWithNibName:@"NetworkSettingsViewController" bundle:nil activeConfig:_activeConfig];
	net.networkDelegate = self;
	[self presentViewController:net];
}

-(IBAction)advancedAccountSettings{
	AccountViewController *account = [[AccountViewController alloc] initWithNibName:@"AccountViewController" bundle:nil];
	[self presentViewController:account];
}


-(void) networkSettings:(ThirdLevelViewController *)thirdLevel didReadDiscoverResults:(NSArray *)results{
	[self dismissThirdLevel:thirdLevel];
	ScrollThirdLevelViewController *level = [[ScrollThirdLevelViewController alloc] initWithNibName:@"ScrollThirdLevelViewController" bundle:nil items:results];
	level.delegateLevel = self;
	[self performSelector:@selector(presentViewController:) withObject:level afterDelay:.3];
}

-(void) scrollThirdLevel:(ScrollThirdLevelViewController *)thirdLevel didSelectRowWithInfo:(NSDictionary *)info{
	[self dismissThirdLevel:thirdLevel];
	[self setCommunicationValue:info];
	[self stopCommunication];
	[self performSelector:@selector(startCommunication) withObject:nil afterDelay:1];
}

-(void) setCommunicationValue:(NSDictionary *) info{
	Network *net = [_activeConfig getNetworkOfType:CONNECT_TO_PRIVATE_IP];
	if (net) {
		net.ip = info[@"ip"];
		net.port = @([info[@"port"] integerValue]);
		net = nil;
	}
	[self saveContext];
}

-(IBAction)locationSettings{
	/*LocationViewController *loc = [[LocationViewController alloc] initWithNibName:@"LocationViewController" bundle:nil config:_activeConfig];
	UINavigationController *navi = [[UINavigationController alloc] initWithRootViewController:loc];
	[navi.navigationBar setBackgroundImage:[UIImage imageNamed:@"orangeBar"] forBarMetrics:UIBarMetricsDefault];
	[self presentViewController:navi animated:YES completion:^{
		//
	}];*/	
}
-(void) invertPreference:(UIButton *) but{
	BOOL isSel = but.isSelected;
	[but setSelected:!isSel];
	[self changeButtonState:but];
	[self setSpecificSettings];
}

-(void) createContext{
	AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    _settingsContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
	//_settingsContext = app.managedObjectContext;

	[_settingsContext setParentContext:app.managedObjectContext];
}

-(void) setCurrentValues{
	_activeConfig = [Config getActiveConfigOfContext:_settingsContext];
}



-(IBAction)shareConfigFile{
	//NSString *path = [[NSBundle mainBundle] pathForResource:@"Example_Config" ofType:@"plist"];
	//NSURL *fileUrl     = [NSURL fileURLWithPath:path];
	NSString *docsPath = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)[0];
	// The file extension is important so that some mime magic happens!
	NSString *filePath = [docsPath stringByAppendingPathComponent:@"1.6.1.cfg"];
	NSURL *fileUrl     = [NSURL fileURLWithPath:filePath];
	UIActivityViewController *activityViewController = [[UIActivityViewController alloc]
														initWithActivityItems:@[fileUrl]
														applicationActivities:nil];
	[self presentViewController:activityViewController animated:YES completion:nil];
}

-(void) chooseOptionDelete:(BOOL) delete{
	NSArray * configArray = [_settingsContext executeFetchRequest:[[[_settingsContext persistentStoreCoordinator] managedObjectModel] fetchRequestTemplateForName:@"fetchAllConfigs"] error:nil];
	switch ([configArray count]) {
		case 0:
			//error
			break;
		case 1:
			//alert view de nomes hi ha 1
			break;
		default:{
			if (delete) {
				[self deleteConfigFileWithItems:configArray];
			}
			else{
				[self changeConfigFileWithItems:configArray];
			}
		}
			break;
	}

}
-(IBAction) changeConfigFile {
	//[_settingsContext c];
	if (!_activeConfig.isDemo.boolValue) {
		[self stopCommunication];
	}
	[self chooseOptionDelete:NO];
}

-(IBAction) changeLanguage {
    LanguagesThirdLevelViewController *level = [[LanguagesThirdLevelViewController alloc] initWithNibName:@"ProfilesThirdLevelViewController" bundle:nil title:NSLocalizedStringFromTableInBundle(@"Language", nil, CURRENT_LANGUAGE_BUNDLE, @"style alert view title")];
    [self presentViewController:level];
}

-(IBAction) deleteConfigFile {
	if (!_activeConfig.isDemo) {
		[self stopCommunication];
	}
	[self chooseOptionDelete:YES];
}


-(void)changeConfigFileWithItems:(NSArray *) items{
	NSArray * filtered = [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isActive == NO"]]];
	NSArray * sorted = [filtered sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"isDemo" ascending:YES]]];
	if (!sorted || sorted.count == 0) return;
    ProfilesThirdLevelViewController *level = [[ProfilesThirdLevelViewController alloc] initWithNibName:@"ProfilesThirdLevelViewController" bundle:nil title:NSLocalizedStringFromTableInBundle(@"Select Profile", nil, CURRENT_LANGUAGE_BUNDLE, @"style alert view title") profiles:sorted remove:NO];
    [self presentViewController:level];
}

-(void)deleteConfigFileWithItems:(NSArray *) items{
	NSArray * filtered = [items filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:[NSString stringWithFormat:@"isDemo != YES"]]];
	if (!filtered || filtered.count == 0) return;
    ProfilesThirdLevelViewController *level = [[ProfilesThirdLevelViewController alloc] initWithNibName:@"ProfilesThirdLevelViewController" bundle:nil title:NSLocalizedStringFromTableInBundle(@"Profile to delete", nil, CURRENT_LANGUAGE_BUNDLE, @"style alert view title") profiles:filtered remove:YES];
    [self presentViewController:level];
}

-(void) finishChangeConfig{
	[self saveContext];
	[(InitialViewController *)self.parentViewController changeConfigFile];
	[self startCommunication];
}

-(void) stopCommunication{
#if KNX_CONNECTION_ENABLED
	[KNXCommunication stopCommunication];
#endif
}

-(void) startCommunication{
#if KNX_CONNECTION_ENABLED
    [[KNXCommunication sharedInstance] setParentContext:_settingsContext.parentContext];
	[KNXCommunication startCommunication];
#endif
}

-(void) presentViewController:(ThirdLevelViewController *) level{
    level.delegate = self;
    level.view.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
    [level.view setCenter:self.parentViewController.view.center];
    level.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
	[self.parentViewController.parentViewController addChildViewController:level];
	_dimView = [[UIView alloc] initWithFrame:self.parentViewController.view.bounds];
	_dimView.opaque = YES;
	_dimView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	_dimView.alpha = 0.0f;
	_dimView.backgroundColor = [UIColor blackColor];
	[self.parentViewController.parentViewController.view addSubview:_dimView];
	[self.parentViewController.parentViewController.view addSubview:level.view];
    [UIView transitionWithView:level.view duration:0.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        level.view.transform = CGAffineTransformIdentity;
		_dimView.alpha = 0.7f;
    }completion:^(BOOL finished){
        [level didMoveToParentViewController:self.parentViewController.parentViewController];
		self.parentViewController.view.userInteractionEnabled = NO;
    }];
    level = nil;
}

-(void) dismissThirdLevel:(ThirdLevelViewController *) third{
	[UIView transitionWithView:self.scrollView duration:0.4 options:UIViewAnimationOptionCurveLinear animations:^{
    	self.parentViewController.view.alpha = 1.0;
        third.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        third.view.alpha = 0.1;
		_dimView.alpha = 0.0f;

    }
    completion:^(BOOL finished){
        self.parentViewController.view.userInteractionEnabled = YES;
        [third removeFromParentViewController];
        [third.view removeFromSuperview];
		[_dimView removeFromSuperview];
		_dimView = nil;
		
    }];
	[self checkInfo:third];
}

-(void) checkInfo:(ThirdLevelViewController *) third{
	if ([third isKindOfClass:[NetworkSettingsViewController class]]) {
		NetworkSettingsViewController *net = (NetworkSettingsViewController *) third;
		if (net.needRestartStartCom) {
			[self setCommunicationValues:net];
			[self startCommunication];
		}
	}
}

-(void) setCommunicationValues:(NetworkSettingsViewController *) network{
	_activeConfig.r_settings.netMode = @(network.modeSegmented.selectedSegmentIndex);
	Network *net = [_activeConfig getNetworkOfType:CONNECT_TO_PRIVATE_IP];
	if (net) {
		net.ip = network.localIp.text;
		net.port = @([network.localPort.text integerValue]);
		net = nil;
	}
	net = [_activeConfig getNetworkOfType:CONNECT_TO_PUBLIC_IP];
	if (net) {
		net.ip = network.publicIp.text;
		net.port = @([network.publicPort.text integerValue]);
	}
	[self saveContext];
}

-(void) saveContext{
	@synchronized (_settingsContext){
		[_settingsContext save:nil];
	}
[_settingsContext.parentContext save:nil];
}

-(void) didEndChoosingProfile:(ProfilesThirdLevelViewController *)level{
	_activeConfig.isActive = @NO;
	[self dismissThirdLevel:level];
	[self finishChangeConfig];
}

-(void) didEndChoosingLanguage:(LanguagesThirdLevelViewController *)level{
	[self dismissThirdLevel:level];
}

-(void) didEndDeleting:(ProfilesThirdLevelViewController *)level config:(Config *) config{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[NSString stringWithFormat:@"%@\n%@",config.name,NSLocalizedStringFromTableInBundle(@"will be deleted", nil, CURRENT_LANGUAGE_BUNDLE, @"text of UIalertview")] delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Done", nil, CURRENT_LANGUAGE_BUNDLE, @"Button of UIalertview"), nil];
	_alertViewProfile = level;
	_alertViewConfig = config;
	[alert show];
}

-(void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
	if (buttonIndex>0 && _alertViewConfig != nil) {
		Config *config = (Config *) _alertViewConfig;
		[_settingsContext deleteObject:config];
		if ([config isEqual:_activeConfig]) {
			//select next config
			[self selectNewConfig];
			[self finishChangeConfig];
		}
		else{
			[self saveContext];
		}
		[self dismissThirdLevel:(ThirdLevelViewController *)_alertViewProfile];
	}
}

-(void) selectNewConfig{
	NSArray * configArray = [_settingsContext executeFetchRequest:[[[_settingsContext persistentStoreCoordinator] managedObjectModel] fetchRequestTemplateForName:@"fetchAllConfigs"] error:nil];
	NSArray * sorted = [configArray sortedArrayUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"isDemo" ascending:YES]]];
	if ([sorted count] > 0) {
		[sorted[0] setIsActive:@YES];
	}
}

-(void) changeButtonState:(UIButton *) button{
    if (!button.isSelected) {
        [button setTitle:NSLocalizedStringFromTableInBundle(@"off", nil, CURRENT_LANGUAGE_BUNDLE, @"text button").uppercaseString forState:UIControlStateNormal];
        [button setTitle:NSLocalizedStringFromTableInBundle(@"off", nil, CURRENT_LANGUAGE_BUNDLE, @"text button").uppercaseString forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor colorWithHue:0.0 saturation:0.0 brightness:0.7 alpha:1.0] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithHue:0.0 saturation:0.0 brightness:0.7 alpha:1.0] forState:UIControlStateHighlighted];

    }
    else{
        [button setTitle:NSLocalizedStringFromTableInBundle(@"on", nil, CURRENT_LANGUAGE_BUNDLE, @"text button").uppercaseString forState:UIControlStateNormal];
        [button setTitle:NSLocalizedStringFromTableInBundle(@"on", nil, CURRENT_LANGUAGE_BUNDLE, @"text button").uppercaseString forState:UIControlStateHighlighted];
        [button setTitleColor:[UIColor orangeColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor orangeColor] forState:UIControlStateHighlighted];
    }
}

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}

-(IBAction)animationInitFinPressed:(id)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:^(BOOL finished){
        [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
    }];
}
- (IBAction)showInfoView:(id)sender {
        //YoutubeViewController *mig = [[YoutubeViewController alloc] initWithNibName:@"YoutubeViewController" bundle:nil];
    MigrationViewController *mig = [[MigrationViewController alloc] initWithNibName:[NSString stringWithFormat:@"MigrationViewController%@",(DEVICE_IS_IPAD ? @"_ipad" :@"")] bundle:nil];
	if (DEVICE_IS_IPAD) {
		mig.modalPresentationStyle = UIModalPresentationFormSheet;
	}
	[self presentViewController:mig animated:YES completion:nil];

}

@end
