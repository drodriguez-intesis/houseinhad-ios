//
//  Device+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Device.h"
@class Telegram;
@interface Device (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc;
-(void) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc;
-(Telegram *) getTelegramWithCode:(NSUInteger) code;
-(NSArray *) getTelegramsWithAddress:(NSNumber *) address;

@end
