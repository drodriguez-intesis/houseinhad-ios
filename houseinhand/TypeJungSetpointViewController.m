//
//  TypeJungSetpointViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/29/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeJungSetpointViewController.h"
#import "DeviceTypeSetpoint.h"
@interface TypeJungSetpointViewController ()
@property (strong,nonatomic) Telegram *actionStatusTelegram;
@end

@implementation TypeJungSetpointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSetpoint*) newDevice scene:(Scene *) scene
{
    return nil;
}

-(CGFloat) readControlStatus{
    CGFloat status=22.0;
	super.statusTelegram = [super.device getTelegramWithCode:0];
	status =[super.statusTelegram.value floatValue];
	_actionStatusTelegram = [super.device getTelegramWithCode:1];
	super.actionTelegram = [super.device getTelegramWithCode:2];
	return status;
}

-(void) checkStatus{
	[super checkStatus];
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
        [super sendData:@0 address:_actionStatusTelegram.address dpt:_actionStatusTelegram.dpt type:_actionStatusTelegram.type delay:_actionStatusTelegram.delay];
    }
}

-(void) updateButtons{
	if (super.currentValue > super.minValue) {
		super.leftButton.userInteractionEnabled = YES;
		super.leftButton.alpha = 1.0;
	}
	else{
		super.leftButton.userInteractionEnabled = NO;
		super.leftButton.alpha = ALPHA_UNSELECTED;
	}
    if (super.currentValue< super.maxValue) {
		super.rightButton.userInteractionEnabled = YES;
		super.rightButton.alpha = 1.0;
	}
	else{
		super.rightButton.userInteractionEnabled = NO;
		super.rightButton.alpha = ALPHA_UNSELECTED;
	}
}


-(void)leftButtonPressed{
    if (super.currentValue> super.minValue) {
		[super sendButtonData:_actionStatusTelegram.value.integerValue -1];
    }
}

-(void)rightButtonPressed{
    if (super.currentValue< super.maxValue) {
		[super sendButtonData:_actionStatusTelegram.value.integerValue +1];
    }
}


-(NSArray *) getSceneStatus{
	return nil;
}
@end
