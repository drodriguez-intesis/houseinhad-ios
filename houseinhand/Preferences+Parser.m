//
//  Preferences+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Preferences+Parser.h"

#define KEY_PASSCODE						@"locked"
#define KEY_PASSCODE_VALUE			@"passcodeValue"
#define KEY_SOUNDS						@"sounds"
#define KEY_SAVE_IMAGES				@"saveImages"
#define KEY_SCREEN_REST				@"screenRest"
#define KEY_VIBRATE						@"vibrate"

@implementation Preferences (Parser)

+(Preferences *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc{
	Preferences *pref = (Preferences *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	pref.locked = (info[KEY_PASSCODE] ? @([info[KEY_PASSCODE] integerValue]): pref.locked);
	pref.passcodeValue = (info[KEY_PASSCODE_VALUE] ? @([info[KEY_PASSCODE_VALUE] integerValue]): pref.passcodeValue);
	pref.sounds = (info[KEY_SOUNDS] ? @([info[KEY_SOUNDS] integerValue]): pref.sounds);
	pref.saveImages = (info[KEY_SAVE_IMAGES] ? @([info[KEY_SAVE_IMAGES] integerValue]): pref.saveImages);
	pref.screenRest = (info[KEY_SCREEN_REST] ? @([info[KEY_SCREEN_REST] integerValue]): pref.screenRest);
	pref.vibrate = (info[KEY_VIBRATE] ? @([info[KEY_VIBRATE] integerValue]): pref.vibrate);

	return pref;
}
@end
