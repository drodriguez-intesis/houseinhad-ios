//
//  Config.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Scene, Settings, Stay;

@interface Config : NSManagedObject

@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isDemo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSSet *r_nextStay;
@property (nonatomic, retain) NSSet *r_scenes;
@property (nonatomic, retain) Settings *r_settings;
@end

@interface Config (CoreDataGeneratedAccessors)

- (void)addR_nextStayObject:(Stay *)value;
- (void)removeR_nextStayObject:(Stay *)value;
- (void)addR_nextStay:(NSSet *)values;
- (void)removeR_nextStay:(NSSet *)values;

- (void)addR_scenesObject:(Scene *)value;
- (void)removeR_scenesObject:(Scene *)value;
- (void)addR_scenes:(NSSet *)values;
- (void)removeR_scenes:(NSSet *)values;

@end
