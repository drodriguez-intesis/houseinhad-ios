//
//  ProfilesCell.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ProfilesCell.h"

@implementation ProfilesCell
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
