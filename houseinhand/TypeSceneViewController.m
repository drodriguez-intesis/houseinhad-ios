//
//  TypeSceneViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeSceneViewController.h"
#import "DeviceTypeLight.h"
@interface TypeSceneViewController ()
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) NSNumber *sendValue;

@end

@implementation TypeSceneViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_sendValue = (newDevice.sendSceneValue ? newDevice.sendSceneValue : @0);
    	[self readControlStatus];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLight*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
		_sendValue = (newDevice.sendSceneValue ? newDevice.sendSceneValue : @0);
		[self readControlStatus];
		[self readSceneStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _actionTelegram = nil;
}

-(BOOL) readControlStatus{
    BOOL status=NO;
	_actionTelegram = [super.device getTelegramWithCode:0];

    return status;
}

-(CGFloat) readSceneStatus{
    NSInteger status=NO;
	SceneTelegram *scTel = [super.scene getTelegramWithAddress:_actionTelegram.address.integerValue];
	if (scTel) {
		status =[scTel.value integerValue];
		super.widgetSceneActive = YES;
	}
	return status;
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
}

-(void) buttonPressed:(id) sender{
    // send value
    [super sendData:_sendValue address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_actionTelegram.delay];

}
-(NSArray *) getSceneStatus{
	if ([super needSceneStatus]) {
		NSDictionary *info1 = @{
								@"address": _actionTelegram.address,
								@"value": _sendValue,
								@"dpt": _actionTelegram.dpt,
								};
		return [NSArray arrayWithObjects:info1, nil];
	}
	return nil;
}

@end
