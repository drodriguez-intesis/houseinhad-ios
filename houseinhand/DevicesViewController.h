//
//  DevicesViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Device+Parser.h"
#import "Telegram.h"
#import "Scene+Parser.h"
#import "SceneTelegram.h"
#define TYPE_CONTROL			1
#define TYPE_SCENE					2

#define ALPHA_UNSELECTED		0.3
@class DevicesViewController;
@protocol DevicesViewControllerDelegate <NSObject>
- (void)deviceViewControllerNextLevel:(DevicesViewController *)controller;
@end

@protocol DeviceStatusDelegate <NSObject>
-(void) checkStatus;
-(CGFloat) readControlStatus;
-(CGFloat) readSceneStatus;
@end

@protocol DevicesScenesViewControllerDelegate <NSObject>
-(NSArray *) getSceneStatus;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene;
@end

@interface DevicesViewController : UIViewController <ObjectsAnimations>
@property (strong,nonatomic) Device *device;
@property (strong,nonatomic) Scene *scene;
@property (assign,nonatomic) NSUInteger type;
@property (nonatomic, weak) id <DevicesViewControllerDelegate> delegate;
@property (assign,nonatomic) BOOL widgetSceneActive;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene;
-(void) sendData:(NSNumber *) data address:(NSNumber *) address dpt:(NSString *) dpt type:(NSNumber *) type delay:(NSNumber *) delay;
-(void) playButtonSound;
-(void) updateStatusInd;
-(void) enableSceneWidget;
-(NSArray *) getSceneStatus;
-(BOOL) needSceneStatus;
-(void) checkStatus;
@end
