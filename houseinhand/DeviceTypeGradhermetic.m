//
//  DeviceTypeGradhermetic.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeGradhermetic.h"


@implementation DeviceTypeGradhermetic

@dynamic exitTime;
@dynamic image1;
@dynamic image2;
@dynamic image3;
@dynamic lamasTime;
@dynamic stopTime;

@end
