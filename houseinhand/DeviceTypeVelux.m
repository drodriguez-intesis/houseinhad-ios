//
//  DeviceTypeVelux.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeVelux.h"


@implementation DeviceTypeVelux

@dynamic image1;
@dynamic image2;
@dynamic image3;
@dynamic stopTime;

@end
