//
//  AccountViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/13/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "AccountViewController.h"
#import "MigrationViewController.h"
#import "LicenseManager.h"
#import "KNXCommunication.h"
#define LICENSE_OLD_FILE_EXTENSION		@".hih"

@interface AccountViewController ()
@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *activeTextField;
@property (weak, nonatomic) IBOutlet UIButton *addAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *migrationButton;
@property (weak, nonatomic) IBOutlet UILabel *titleName;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UIButton *notRegisteredButton;
@property (weak, nonatomic) IBOutlet UIButton *addDeviceButton;

@end

@implementation AccountViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setButtonsSize];
	[self setTranslations];
	[self isActiveAddAccountButton:[[LicenseManager sharedInstance] hasStoredAccount]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTranslations) name:NOT_LANGUAGE_CHANGED object:nil];


    // Do any additional setup after loading the view from its nib.
}

-(void) setButtonsSize{
    _addAccountButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _addAccountButton.titleLabel.minimumScaleFactor = .5;
    _migrationButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _migrationButton.titleLabel.minimumScaleFactor = .5;
    _notRegisteredButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _notRegisteredButton.titleLabel.minimumScaleFactor = .5;
    _addDeviceButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    _addDeviceButton.titleLabel.minimumScaleFactor = .5;
}

-(void) viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NOT_LANGUAGE_CHANGED object:nil];
	[super viewDidUnload];
}
-(void) setTranslations{
	_titleName.text = NSLocalizedStringFromTableInBundle(@"Account", nil, CURRENT_LANGUAGE_BUNDLE, @"title of account settings view");
	_usernameLabel.text = NSLocalizedStringFromTableInBundle(@"Username", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view");
	_passwordLabel.text = NSLocalizedStringFromTableInBundle(@"Password", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view");
	[_notRegisteredButton setTitle:NSLocalizedStringFromTableInBundle(@"Not registered?", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateNormal];
	[_notRegisteredButton setTitle:NSLocalizedStringFromTableInBundle(@"Not registered?", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateHighlighted];
	[_migrationButton setTitle:NSLocalizedStringFromTableInBundle(@"Upgrading from previous version", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateNormal];
	[_migrationButton setTitle:NSLocalizedStringFromTableInBundle(@"Upgrading from previous version", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateHighlighted];
    [_addDeviceButton setTitle:NSLocalizedStringFromTableInBundle(@"Add device to installation", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateNormal];
	[_addDeviceButton setTitle:NSLocalizedStringFromTableInBundle(@"Add device to installation", nil, CURRENT_LANGUAGE_BUNDLE, @"account settings view") forState:UIControlStateHighlighted];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField ==_usernameTextField) {
        [_passwordTextField becomeFirstResponder];
    }
    else{
        [textField resignFirstResponder];
        //[self addAccount:nil];
    }
    
    return YES;
}

-(void) isActiveAddAccountButton:(BOOL) active{
	if (active) {
		[_addAccountButton setTitle:NSLocalizedStringFromTableInBundle(@"Sign out", nil, CURRENT_LANGUAGE_BUNDLE, @"text of uibutton") forState:UIControlStateNormal];
		_addAccountButton.tag = 2;
		_addAccountButton.backgroundColor = [UIColor darkGrayColor];
		_addAccountButton.titleLabel.textColor = [UIColor lightGrayColor];
		_usernameTextField.userInteractionEnabled = NO;
		_usernameTextField.alpha = .6;
		_passwordTextField.userInteractionEnabled = NO;
		_passwordTextField.alpha = .6;
		[self setTextFieldsValues];
		_migrationButton.hidden = NO;
		_migrationButton.userInteractionEnabled = YES;
		_notRegisteredButton.hidden = YES;
		_notRegisteredButton.userInteractionEnabled = NO;
		_addDeviceButton.hidden = NO;
		_addDeviceButton.userInteractionEnabled = YES;
	}
	else{
		[_addAccountButton setTitle:NSLocalizedStringFromTableInBundle(@"Sign in", nil, CURRENT_LANGUAGE_BUNDLE, @"text of uibutton") forState:UIControlStateNormal];
		_addAccountButton.tag = 1;
		_addAccountButton.backgroundColor = APP_COLOR_ORANGE_DARK;
		_addAccountButton.titleLabel.textColor = [UIColor whiteColor];
		_usernameTextField.userInteractionEnabled = YES;
		_usernameTextField.alpha =1.0;
		_passwordTextField.userInteractionEnabled = YES;
		_passwordTextField.alpha = 1.0;
		_migrationButton.hidden = YES;
		_migrationButton.userInteractionEnabled = NO;
		_notRegisteredButton.hidden = NO;
		_notRegisteredButton.userInteractionEnabled = YES;
		_addDeviceButton.hidden = YES;
		_addDeviceButton.userInteractionEnabled = NO;
	}
}

-(void) setTextFieldsValues{
	NSDictionary *values = [[LicenseManager sharedInstance] credentials];
	if (!values) return;
	_usernameTextField.text = values[@"username"];
	_passwordTextField.text = values[@"password"];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
	if (!_activeTextField) {
		[UIView animateWithDuration:0.3 animations:^{
			self.view.transform = CGAffineTransformMakeTranslation(0, -80);
		}];
	}
    _activeTextField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
	[UIView animateWithDuration:0.3 delay:0.1 options:UIViewAnimationOptionCurveEaseOut animations:^{
		self.view.transform = CGAffineTransformIdentity;
		
	} completion:nil];
    _activeTextField = nil;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [_activeTextField resignFirstResponder];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)addAccount:(UIButton *)sender {
	//connect to lic server
	if (sender.tag == 1) {
		[[LicenseManager sharedInstance] remoteCheckWithUsername:_usernameTextField.text password:_passwordTextField.text resultBlock:^(NSDictionary *result) {
			if (result[@"result"]) {
				[self isActiveAddAccountButton:YES];
				//[self dismissView];
			}
			else if (result[@"error"]){
				NSString *errorCode = (result[@"error"][@"code"] ? result[@"error"][@"code"] : @"-1");
				[self showAlertWitTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title for AlertView") ,errorCode] message:NSLocalizedStringFromTableInBundle(result[@"error"][@"message"], nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")];
			}
			else{
				[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 16",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Could not connect to the server", nil, CURRENT_LANGUAGE_BUNDLE, @"body of uialertview")];
			}
		}];
	}
	else{
		[[LicenseManager sharedInstance] unassign];
		_usernameTextField.text = @"";
		_passwordTextField.text = @"";
		[self isActiveAddAccountButton:NO];
	}
	
}

-(void) showAlertWitTitle:(NSString *) title message:(NSString *) message{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Dismiss", nil, CURRENT_LANGUAGE_BUNDLE, @"Button for AlertView") otherButtonTitles: nil];
	alert.delegate = self;
	[alert show];
}


- (IBAction)showMigration:(id)sender {
	[self showMail:sender];

}
- (IBAction)showUserWebPage {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://user.houseinhand.com"]];
}

- (IBAction)showMail:(UIButton *)sender {
	NSDictionary *credentials = [[LicenseManager sharedInstance] credentials];

	__block NSData *data;
	MFMailComposeViewController *mailComposer = [[MFMailComposeViewController alloc] init];
	NSString *bodyMessage = [NSString stringWithFormat:@"%@\n\n%@ %@\n%@ %@",NSLocalizedStringFromTableInBundle(@"Dear Houseinhand Team:\n\nPlease upgrade my .hih file and assign it to my user:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),NSLocalizedStringFromTableInBundle(@"Username:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),credentials[@"username"],	NSLocalizedStringFromTableInBundle(@"Secret:", nil, CURRENT_LANGUAGE_BUNDLE, @"body of email"),[[NSUserDefaults standardUserDefaults] objectForKey:USER_DEFAULTS_SECRET] ];	[mailComposer setMessageBody:bodyMessage isHTML:NO];
	__block NSString *fileName;
	NSArray *path_array = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
	NSString *publicDocumentsDir = path_array[0];
	NSString *path2 = [publicDocumentsDir stringByAppendingPathComponent:@"Old"];
	NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:path2 error:nil];
	[files enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if ([obj hasSuffix:LICENSE_OLD_FILE_EXTENSION]) {
			NSString *path = [path2 stringByAppendingPathComponent:obj];
			fileName = [NSString stringWithString:[obj lastPathComponent]];
			data = [NSData dataWithContentsOfFile:path];
			*stop = YES;
		}
	}];
	if (data){
		[mailComposer addAttachmentData:data mimeType:@"application/houseinhand" fileName:fileName];
		[mailComposer setToRecipients:@[@"sales@houseinhand.com"]];
		[mailComposer setSubject:@"HIH-Migration_License"];
		mailComposer.mailComposeDelegate = self;
		[self presentViewController:mailComposer animated:YES completion:nil];
	}
	else{
		[self showAlertWitTitle:NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview") message:NSLocalizedStringFromTableInBundle(@"No .hih file found in 'Old' Folder of iTunes File Sharing", nil, CURRENT_LANGUAGE_BUNDLE, @"message of UIalertview")];
	}
	
}

-(void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error{
	[controller dismissViewControllerAnimated:YES completion:^{
		if (result == MFMailComposeResultSent) {
			[self dismissViewControllerAnimated:YES completion:nil];
		}
	}];
}
- (IBAction)addDevice:(UIButton *)sender {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedStringFromTableInBundle(@"Add Device", nil, CURRENT_LANGUAGE_BUNDLE, @"Button for AlertView") message:NSLocalizedStringFromTableInBundle(@"Fill with the installation ip and port", nil, CURRENT_LANGUAGE_BUNDLE, @"body of uialertview") delegate:self cancelButtonTitle:NSLocalizedStringFromTableInBundle(@"Cancel", nil, CURRENT_LANGUAGE_BUNDLE, @"Button for AlertView") otherButtonTitles:NSLocalizedStringFromTableInBundle(@"Add", nil, CURRENT_LANGUAGE_BUNDLE, @"Button for AlertView"), nil];
	alert.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
	[[alert textFieldAtIndex:1] setSecureTextEntry:NO];
	NSString *existingIp = [[KNXCommunication sharedInstance] getConnectedIp];
	NSNumber *existingPort = [[KNXCommunication sharedInstance] getConnectedPort];
	if (existingIp && existingPort) {
		[[alert textFieldAtIndex:0] setText:existingIp];
		[[alert textFieldAtIndex:1] setText:[NSString stringWithFormat:@"%@",existingPort]];
	}
	[[alert textFieldAtIndex:0] setPlaceholder:@"ip"];
	[[alert textFieldAtIndex:1] setPlaceholder:@"port"];
	if ([[alert textFieldAtIndex:0] respondsToSelector:@selector(setTintColor:)]) {
		[[alert textFieldAtIndex:0] setTintColor:[UIColor blackColor]];
		[[alert textFieldAtIndex:1] setTintColor:[UIColor blackColor]];
	}
	[[alert textFieldAtIndex:0] setClearButtonMode:UITextFieldViewModeAlways];
	[[alert textFieldAtIndex:1] setClearButtonMode:UITextFieldViewModeAlways];
	alert.tag = 5;
	[alert show];
	
}

-(void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex{
	if (alertView.tag != 5 || buttonIndex == 0) return;
	[[KNXCommunication sharedInstance] getMacInstallationIp:[[alertView textFieldAtIndex:0] text] port:[[[alertView textFieldAtIndex:1] text] integerValue] usingBlock:^(NSDictionary *foundItems) {
		if (foundItems) {
			[[LicenseManager sharedInstance] remoteAddDeviceStoredLoginMac:foundItems[@"mac"] resultBlock:^(NSDictionary *result) {
				if (result) {
					if (result[@"error"]) {
						NSString *errorCode = (result[@"error"][@"code"] ? result[@"error"][@"code"] : @"-1");
						[self showAlertWitTitle:[NSString stringWithFormat:@"%@ %@",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview"),errorCode] message:NSLocalizedStringFromTableInBundle(result[@"error"][@"message"], nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")];
						if ([errorCode isEqualToString:@"1"]) {
							[[LicenseManager sharedInstance] unassign];
						}
						
					}
					else if (result[@"result"]) {
						[self showAlertWitTitle:NSLocalizedStringFromTableInBundle(@"Add Device", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview") message:NSLocalizedStringFromTableInBundle(@"Device successfuly added", nil, CURRENT_LANGUAGE_BUNDLE, @"message of UIAlertView")];
						[KNXCommunication startCommunication];
					}
					else{
						[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 5",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Unknown error", nil, CURRENT_LANGUAGE_BUNDLE, @"body of UIalertview")];
					}
				}
				else{
					[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 16",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Could not connect to the server", nil, CURRENT_LANGUAGE_BUNDLE, @"body of UIalertview")];
				}
			}];
		}
		else{
			[self showAlertWitTitle:[NSString stringWithFormat:@"%@ 18",NSLocalizedStringFromTableInBundle(@"Error", nil, CURRENT_LANGUAGE_BUNDLE, @"title of UIalertview")] message:NSLocalizedStringFromTableInBundle(@"Installation address not found", nil, CURRENT_LANGUAGE_BUNDLE, @"body of UIalertview")];
		}
	}];
}

@end
