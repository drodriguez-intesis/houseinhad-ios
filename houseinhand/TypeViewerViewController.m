//
//  TypeViewerViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/6/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeViewerViewController.h"
#import "DeviceTypeLabel.h"
@interface TypeViewerViewController ()
@property (strong,nonatomic) NSString *units;
@property (assign,nonatomic) CGFloat scale;
@property (assign,nonatomic) NSUInteger decimals;
@property (strong,nonatomic) Telegram *statusTelegram;
@end

@implementation TypeViewerViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeLabel *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _units = newDevice.units;
        _scale = [newDevice.scale floatValue];
        _decimals = [newDevice.decimals integerValue];
        super.currentValue = (CGFloat)[self readControlStatus];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [self updateLabel:NO];
    [self checkStatus];
	if ([super.device.hasStatus boolValue]) {
		[_statusTelegram addObserver:self forKeyPath:@"value" options:0 context:nil];
	}
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	[super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
	if ([object isKindOfClass:[Telegram class]]) {
		[self update:ANIMATIONS_ENABLED];
	}
}

-(void) update:(BOOL) animated{
	if (super.currentValue !=	_statusTelegram.value.floatValue) {
		super.currentValue = _statusTelegram.value.floatValue;
		[self updateLabel:YES];
	}
}

-(void) viewWillDisappear:(BOOL)animated{
	[super viewWillDisappear:animated];
}

-(void) updateLabel:(BOOL) animated{
	if (animated) {
		[super animationUpdate:super.centerLabel];
	}
	[self createValue];
}

-(void) createValue{
	NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
    if (_scale*super.currentValue > 1E8) [numberFormatter setNumberStyle:NSNumberFormatterScientificStyle];
    else [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
	[numberFormatter setMaximumFractionDigits:_decimals];
	NSString * trimmed = [numberFormatter stringFromNumber:@(_scale*super.currentValue)];
    super.centerLabel.text =  [NSString stringWithFormat:@"%@ %@",trimmed,_units];
}

-(CGFloat) readControlStatus{
    CGFloat status=0.0;
	_statusTelegram = [super.device getTelegramWithCode:0];
	status =[_statusTelegram.value floatValue];
	
	return status;
}

-(void) checkStatus{
    if ([super.device.hasStatus boolValue] && ![super.device.isSynchronized boolValue]) {
		//NSLog(@"Status telegram:%@",_statusTelegram);
        [super sendData:@0 address:_statusTelegram.address dpt:_statusTelegram.dpt type:_statusTelegram.type delay:_statusTelegram.delay];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    _units = nil;
}

-(void) dealloc{
	if(_statusTelegram.observationInfo)[_statusTelegram removeObserver:self forKeyPath:@"value"];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
