//
//  TypeGenericSegmentedViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "SegmentedViewController.h"

@interface TypeGenericSegmentedViewController : SegmentedViewController <DeviceStatusDelegate,DevicesScenesViewControllerDelegate>

@end
