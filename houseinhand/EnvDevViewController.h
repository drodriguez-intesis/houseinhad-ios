//
//  EnvDevViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 12/8/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DevicesViewController.h"
#import "ThirdLevelViewController.h"
#import "PasswordViewController.h"
@class EnvDevViewController;
@protocol EnvDevViewControllerDelegate <NSObject>
-(void) sceneDeviceDidSave:(EnvDevViewController *)controller actions:(NSArray *)actions fromStay:(Stay *) stay;
@end


@interface EnvDevViewController : UIViewController <DevicesViewControllerDelegate,ThirdLevelViewControllerDelegate,PasswordViewControllerDelegate>
@property (nonatomic,weak) id <EnvDevViewControllerDelegate> delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil stay:(Stay *) stay scene:(Scene *) scene;
@end
