//
//  KNXSocket.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"
typedef 	NS_ENUM (NSUInteger,KNXStatus) {STATE_CONNECTION_CONNECTED,STATE_CONNECTION_DISCONNECTED,STATE_CONNECTION_CONNECTING,STATE_CONNECTION_DISCONNECTING};


@class KNXSocket;
@protocol KNXSocketDelegate <NSObject>
-(void) knxSocket:(KNXSocket *) tcpServerCom didReceiveData:(NSData *) data address:(NSData *) address;
-(void) knxSocket:(KNXSocket *) tcpServerCom didChangeConnectionStatus:(KNXStatus) connectionStatus;
-(void) knxSocketUnexpectedClosed:(KNXSocket *) tcpServerCom;

@end

@interface KNXSocket : NSObject <GCDAsyncUdpSocketDelegate>
@property (weak,nonatomic)  id<KNXSocketDelegate> delegate;
@property (assign,nonatomic) KNXStatus connectionStatus;
-(void) startCommunicationWithNetworks:(NSArray *) netArray;
-(void) stopCommunication;
-(void) sendInfoToKnx:(NSData *) info address:(NSNumber *) address;
-(NSString *) getConnectedAddress;
-(NSNumber *) getConnectedPort;
-(NSString *) getConnectedMac;
-(BOOL) isLocalConnection;
@end
