//
//  LanguagesCell.m
//  houseinhand
//
//  Created by Isaac Lozano on 28/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "LanguagesCell.h"

@implementation LanguagesCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
