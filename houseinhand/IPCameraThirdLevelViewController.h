//
//  IPCameraThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/27/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
#import "Device+Parser.h"
#import "DeviceTypeIp.h"
#import "MjpegImageView.h"

@interface IPCameraThirdLevelViewController : ThirdLevelViewController <MjpegImageViewDelegate>
@property (strong,nonatomic) DeviceTypeIp *device;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice;
@end
