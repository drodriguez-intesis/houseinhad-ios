//
//  HUIAlertView.h
//  houseinhand
//
//  Created by Isaac Lozano on 11/23/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HUIAlertView : UIAlertView
@property (strong,nonatomic) id info;
@property (strong,nonatomic) UIViewController *rootController;
@end
