//
//  StaysViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StaysViewController.h"
#import "InitialViewController.h"
#import "ZonesViewController.h"
#import "DevicesViewControllerViewController.h"
#import "Stay+Parser.h"
#import "Config+Parser.h"
#import "SegueToDevices.h"
#import "SegueToZones.h"
#import "ActionPopupViewController.h"
#import "RootViewController.h"
#define Y_SPACE 10
#define X_SPACE 8
#define X_OFFSET_1 10
#define X_OFFSET_2 10
@interface StaysViewController ()
@property	 (strong,nonatomic) UIView *dimView;
@property	 (strong,nonatomic) Stay *selectedStay;
@property    (strong,nonatomic) NSTimer *drawDelayiPhone;
@end

@implementation StaysViewController

-(void) setDetailItem:(id)newDetailItem{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
		self.configName.text =  [_detailItem name];
        if (!_isZone && DEVICE_IS_IPAD) {
                  [self addToScrollView];
        }
	}
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.configName.text =  [_detailItem name];
}

-(void) viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    if (!DEVICE_IS_IPAD) {
        [_drawDelayiPhone invalidate];
        _drawDelayiPhone = [NSTimer scheduledTimerWithTimeInterval:.1 target:self selector:@selector(addToScrollView) userInfo:nil repeats:NO];
    }
}

-(void) removeOldSubviews{
    for (NSUInteger i = 0; i< self.staysScrollView.subviews.count; i++) {
        UIView * view = self.staysScrollView.subviews[i];
        [view removeFromSuperview];
    }
}

-(void) viewDidUnload{
    [super viewDidUnload];
    self.staysScrollView = nil;
    self.configName = nil;
	_dimView = nil;
	_selectedStay = nil;
}

-(void) viewWillAppear:(BOOL)animated{
	[super viewWillAppear:animated];
	[self.childViewControllers enumerateObjectsUsingBlock:^(StayViewController * obj, NSUInteger idx, BOOL *stop) {
		if ([obj respondsToSelector:@selector(unselectStayAnimated)]) [obj unselectStayAnimated];}];
}

-(void) viewDidAppear:(BOOL)animated{
	[super viewDidAppear:animated];
   // [self.childViewControllers makeObjectsPerformSelector:@selector(unselectStay)];
}

-(void) unselectStays{
	[self.childViewControllers makeObjectsPerformSelector:@selector(unselectStay)];
}

- (void)stayViewControllerDidTouched:(StayViewController *)controller{
    NSMutableSet *st = [controller.stay mutableSetValueForKey:@"r_nextStay"];
	if (!DEVICE_IS_IPAD) {
		if ([st count] > 0) {
			[self performSegueWithIdentifier:@"segueToZones" sender:controller];
		}
		else {
			[self performSegueWithIdentifier:@"segueToDevices" sender:controller];
		}
	}
	else{
        [self unselectStays];
        if ([st count] > 0) {
            [self.parentViewController performSegueWithIdentifier:@"segueToZonesPad" sender:controller];
        }
        else{
            [self.parentViewController performSegueWithIdentifier:@"segueToDevicesPad" sender:controller];
        }
	}
}

-(void) stayViewControllerDidOptionsMenu:(StayViewController *)controller{
	ActionPopupViewController *optionsMenu = [[ActionPopupViewController alloc] initWithNibName:@"ActionPopupViewController" bundle:nil title:controller.name.text items:@[NSLocalizedStringFromTableInBundle(@"Rename Stay", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Take Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Select Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible"),NSLocalizedStringFromTableInBundle(@"Remove Image", nil, CURRENT_LANGUAGE_BUNDLE, @"small cell visible")]];
	_selectedStay = controller.stay;
	[self presentViewController:optionsMenu];
}

-(void) presentViewController:(ThirdLevelViewController *) level{
    level.delegate = self;
    [level.view setCenter:self.view.window.center];
    level.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
    
	_dimView = [[UIView alloc] initWithFrame:self.view.window.bounds];
	_dimView.opaque = YES;
	_dimView.alpha = 0.0f;
	_dimView.backgroundColor = [UIColor blackColor];
	[self.staysScrollView addSubview:_dimView];
	[self.view addSubview:level.view];
	[self addChildViewController:level];
    [UIView transitionWithView:level.view duration:0.4 options:UIViewAnimationOptionCurveEaseOut animations:^{
        level.view.transform = CGAffineTransformIdentity;
		_dimView.alpha = 0.7;
    }completion:^(BOOL finished){
        [level didMoveToParentViewController:self];
		self.staysScrollView.userInteractionEnabled =NO;
    }];
    level = nil;
}

-(void) dismissThirdLevel:(ThirdLevelViewController *) third{
	[UIView transitionWithView:self.view duration:0.4 options:UIViewAnimationOptionCurveLinear animations:^{
        third.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
        third.view.alpha = 0.1;
		_dimView.alpha = 0.0f;
		
    }
	completion:^(BOOL finished){
		self.staysScrollView.userInteractionEnabled = YES;
		[third removeFromParentViewController];
		[third.view removeFromSuperview];
		[_dimView removeFromSuperview];
		_dimView = nil;
	}];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if ([segue.identifier isEqualToString:@"segueToDevices"])
	{
		DevicesViewControllerViewController *devices = segue.destinationViewController;
		SegueToDevices *customSegue = (SegueToDevices *) segue;
		StayViewController *stay = (StayViewController *) sender;
		customSegue.stay = stay;
		devices.detailItem = stay.stay;
	}
	else if ([segue.identifier isEqualToString:@"segueToZones"]){
		ZonesViewController *zones = segue.destinationViewController;
		SegueToZones *customSegue = (SegueToZones *) segue;
        StayViewController *stay = (StayViewController *) sender;
        customSegue.stay = stay;
		zones.detailItem = stay.stay;
	}
}

-(void) addToScrollView{
	NSArray *stays = [self.detailItem getOrderedStays];
	if (!stays) return;
	[stays enumerateObjectsUsingBlock:^(Stay * obj, NSUInteger idx, BOOL *stop) {
		[self addOneStay:obj index:idx];
	}];
    self.staysScrollView.contentSize=CGSizeMake(self.staysScrollView.bounds.size.width, self.staysScrollView.contentSize.height + Y_SPACE);
}

-(void) addOneStay:(Stay *) st index:(NSUInteger) i{
    StayViewController *stay = [[StayViewController alloc] initWithNibName:@"StayViewController" bundle:nil];
    stay.stay = st;
    stay.delegate = self;
    stay.tag = i;
    CGFloat x,y;
    if (DEVICE_IS_IPAD) {
        x = (_isZone ? X_OFFSET_2 :X_OFFSET_1);
              stay.view.autoresizingMask = UIViewAutoresizingFlexibleRightMargin;
        y = self.staysScrollView.contentSize.height + Y_SPACE;
    }
    else if (i % 2){
            //iphone parell
        x = self.staysScrollView.center.x + X_SPACE;

        y = self.staysScrollView.contentSize.height - stay.view.bounds.size.height;
    }
    else{
            //iphone senar
        x = self.staysScrollView.center.x - stay.view.bounds.size.width - X_SPACE;
        y = self.staysScrollView.contentSize.height + Y_SPACE;
    }

    stay.view.frame = CGRectMake(x, y, stay.view.bounds.size.width, stay.view.bounds.size.height);
    [self addChildViewController:stay];
    [self.staysScrollView addSubview:stay.view];
    self.staysScrollView.contentSize=CGSizeMake(self.staysScrollView.bounds.size.width, y + stay.view.bounds.size.height);
    [self didMoveToParentViewController:stay];
}

-(IBAction)resetScrollOffset{
    [self.staysScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
}


#pragma mark ActionView delegate

- (void)actionViewDidEndEditing:(NSDictionary *)info viewController:(ThirdLevelViewController *)level{
	[self dismissThirdLevel:level];
	if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_IMAGE"]) {
		UIImage *thumbImage = (UIImage *) [info valueForKey:@"thumbImage"];
		NSData *imageData = UIImagePNGRepresentation (thumbImage);
		_selectedStay.thumbImage = imageData;
		_selectedStay.useCustomImage = @YES;
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"DELETE_IMAGE"]){
		_selectedStay.thumbImage = nil;
		_selectedStay.useCustomImage = @NO;
	}
	else if ([[info valueForKey:@"type"] isEqualToString:@"CHANGE_NAME"]){
		_selectedStay.name = [info valueForKey:@"name"];
	}
	_selectedStay = nil;

}

-(IBAction)recognize{

}
@end
