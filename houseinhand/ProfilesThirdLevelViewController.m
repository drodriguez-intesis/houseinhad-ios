//
//  ProfilesThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/14/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "ProfilesThirdLevelViewController.h"
#import "ProfilesCell.h"
@interface ProfilesThirdLevelViewController (){
}
@property (strong,nonatomic) NSArray * configs;
@property (weak,nonatomic) IBOutlet UILabel * titleName;
@property (strong,nonatomic) NSString * name;
@property (assign,nonatomic) BOOL remove;
@end

@implementation ProfilesThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title profiles:(NSArray *) configs remove:(BOOL) remove
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_configs = configs;
		_name = title;
		_remove = remove;
    }
    return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	[self resizeFromItemsNumber];
    [self registerNib];
	_titleName.text = _name;
}


-(void) resizeFromItemsNumber{
	NSUInteger indexArr[] = {210,170,130,90,50,10,0};
	CGRect frame = self.view.frame;
	if (_configs.count<8) {
		frame.size.height -=  indexArr[[_configs count] -1];
	}
	self.view.frame = frame;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

-(void) registerNib{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    UINib *topNib = [UINib nibWithNibName:@"ProfilesCell" bundle:classBundle];
    [[self tableView] registerNib:topNib forCellReuseIdentifier:@"Profiles"];
}
- (void)viewDidUnload
{
    [super viewDidUnload];
	_configs = nil;
	_titleName = nil;
	_name	 = nil;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundColor = [UIColor clearColor];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier1 = @"Profiles";
    ProfilesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if (cell == nil) {
        cell = [[ProfilesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
			cell.backgroundColor = [UIColor clearColor];
        }
    }
	Config * cf = (Config *) _configs[[indexPath row]];
	cell.name.text =  cf.name;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_configs count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	for (Config *cf in _configs) {
		cf.isActive = @NO;
	}
	Config *cf = (Config *)_configs[[indexPath row]];
	cf.isActive = @YES;
	if (_remove) {
		[self.delegate didEndDeleting:self config:cf];
		return;
	}
	[self.delegate didEndChoosingProfile:self];
}

@end
