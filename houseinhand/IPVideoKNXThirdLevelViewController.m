//
//  IPVideoKNXThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/30/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "IPVideoKNXThirdLevelViewController.h"
#import "Telegram+Parser.h"
#import "DeviceTypeIp.h"
#import "KNXCommunication.h"

#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_DURATION_2          0.3
#define ANIM_UPDATE_MAX_SIZE_3          1.05

@interface IPVideoKNXThirdLevelViewController ()
@property (strong,nonatomic) Telegram *actionTelegram;
@property (strong,nonatomic) UIImage *image;
@property (strong,nonatomic) IBOutlet UIButton *doorButton;
@property (strong,nonatomic) NSNumber *valueOn;
@property (strong,nonatomic) NSNumber *valueOff;
@property (strong,nonatomic) NSNumber *pulseWidth;
@end

@implementation IPVideoKNXThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeIp*) newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
		_actionTelegram = [newDevice getTelegramWithCode:0];
        _image = [UIImage imageNamed:newDevice.image2];
		_valueOff = newDevice.valueOff;  
		_valueOn = newDevice.valueOn;
		_pulseWidth = newDevice.pulseWidth;
    }
    return self;
}

-(void) viewDidLoad{
	[super viewDidLoad];
	[_doorButton setImage:_image forState:UIControlStateNormal];
    [_doorButton setImage:_image forState:UIControlStateHighlighted];
}

-(IBAction)touchDownButton:(id)sender{
	[self sendData:_valueOn address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:@0];
}

-(IBAction)touchUpButton:(id)sender{
	[self sendData:_valueOff address:_actionTelegram.address dpt:_actionTelegram.dpt type:_actionTelegram.type delay:_pulseWidth];
}


-(void) sendData:(NSNumber *) data address:(NSNumber *) address dpt:(NSString *) dpt type:(NSNumber *) type delay:(NSNumber *) delay{
	NSData *info = [Telegram getDataFromInfo:data dpt:dpt write:type.boolValue];
	NSDictionary *dict = @{@"data": info,@"address":address};
    [NSTimer scheduledTimerWithTimeInterval:[delay floatValue] target:self selector:@selector(sendData:) userInfo:dict repeats:NO];
}

-(void) sendData:(NSTimer *) timer{
    NSDictionary * info = [timer userInfo];
	[[KNXCommunication sharedInstance] sendInfoToKnx:info[@"data"] address:info[@"address"] objectId:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)animationInitPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE_3,ANIM_UPDATE_MAX_SIZE_3);} completion:nil];
}

-(IBAction)animationFinPressed:(id <ObjectsAnimations>)sender{
    UIControl *control = (UIControl *) sender;
    [UIView animateWithDuration:ANIM_UPDATE_DURATION_2 delay:0 options:  UIViewAnimationOptionAllowUserInteraction animations:^{control.transform=CGAffineTransformIdentity;} completion:nil];
}

-(void) orientationChanged:(NSNotification *) not{
	return;
}

@end
