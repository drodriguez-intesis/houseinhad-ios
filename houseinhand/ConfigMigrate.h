//
//  ConfigMigrate.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/10/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigMigrate : NSObject

-(NSDictionary *) migrateConfigFile:(NSArray *) oldCfg withName:(NSString *) name;

@end
