//
//  Telegram.h
//  houseinhand
//
//  Created by Isaac Lozano on 5/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Device;

@interface Telegram : NSManagedObject

@property (nonatomic, retain) NSNumber * address;
@property (nonatomic, retain) NSNumber * code;
@property (nonatomic, retain) NSNumber * delay;
@property (nonatomic, retain) NSString * dpt;
@property (nonatomic, retain) NSString * stringValue;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSNumber * value;
@property (nonatomic, retain) Device *r_device;

@end
