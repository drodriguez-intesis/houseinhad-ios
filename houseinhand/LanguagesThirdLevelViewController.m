//
//  LanguagesThirdLevelViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 28/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "LanguagesThirdLevelViewController.h"
#import "LanguagesCell.h"
#import "NSMutableArray+MoveMutableArray.h"

@interface LanguagesThirdLevelViewController ()
@property (weak,nonatomic) IBOutlet UILabel * titleName;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) const NSArray *availableLanguages;
@end

@implementation LanguagesThirdLevelViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
		_name = title;
        _availableLanguages = ([[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"]);
    }
    return self;

}


- (void)viewDidLoad
{
	[super viewDidLoad];
	[self resizeFromItemsNumber];
    [self registerNib];
	_titleName.text = _name;
}

-(void) resizeFromItemsNumber{
	NSUInteger indexArr[] = {210,170,130,90,50,10,0};
	CGRect frame = self.view.frame;
	if (_availableLanguages.count<8) {
		frame.size.height -=  indexArr[[_availableLanguages count] -1];
	}
	self.view.frame = frame;
}

-(void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.tableView flashScrollIndicators];
}

-(void) registerNib{
    NSBundle *classBundle = [NSBundle bundleForClass:[self class]];
    UINib *topNib = [UINib nibWithNibName:@"LanguagesCell" bundle:classBundle];
    [[self tableView] registerNib:topNib forCellReuseIdentifier:@"Languages"];
}

- (void)viewDidUnload
{
    _titleName = nil;
    _availableLanguages = nil;
	_name	 = nil;
    [super viewDidUnload];

}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (cell.backgroundView == NULL) {
		cell.backgroundColor = [UIColor clearColor];
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *CellIdentifier1 = @"Languages";
    LanguagesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
    
    if (cell == nil) {
        cell = [[LanguagesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier1];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryNone;
			cell.selectionStyle = UITableViewCellSelectionStyleGray;
			cell.backgroundColor = [UIColor clearColor];
        }
    }
    NSString *isoLang = (_availableLanguages[[indexPath row]] ? _availableLanguages[[indexPath row]] : _availableLanguages[0]);
    NSLocale *currLocale = [NSLocale localeWithLocaleIdentifier:isoLang];
	cell.name.text =  [[currLocale displayNameForKey:NSLocaleIdentifier value:isoLang] capitalizedString];
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_availableLanguages count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
	NSMutableArray *languages = [NSMutableArray arrayWithArray:[_availableLanguages copy]];
    [languages moveObjectFromIndex:indexPath.row toIndex:0];
    [[NSUserDefaults standardUserDefaults] setObject:languages forKey:@"AppleLanguages"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:NOT_LANGUAGE_CHANGED object:nil];

    [self.delegate didEndChoosingLanguage:self];
}


@end
