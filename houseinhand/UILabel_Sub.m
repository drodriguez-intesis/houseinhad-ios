//
//  UILabel_Sub.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/3/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "UILabel_Sub.h"


#define ANIM_UPDATE_DURATION            0.1
#define ANIM_UPDATE_MAX_SIZE            1.05
#define ANIM_UPDATE_MIN_SIZE            0.95

@implementation UILabel_Sub

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

-(void)animationLabel{
    [UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0 options:UIViewAnimationOptionAllowUserInteraction animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE,ANIM_UPDATE_MAX_SIZE);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION delay:0.0 options:UIViewAnimationOptionBeginFromCurrentState animations:^{self.transform=CGAffineTransformIdentity;} completion:nil];}];
    
}

-(void) animationUpdateDetail{
    [UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MAX_SIZE,ANIM_UPDATE_MAX_SIZE);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformMakeScale(ANIM_UPDATE_MIN_SIZE,ANIM_UPDATE_MIN_SIZE);} completion:^(BOOL finished){[UIView animateWithDuration:ANIM_UPDATE_DURATION animations:^{self.transform=CGAffineTransformIdentity;}];}];}];
}
@end
