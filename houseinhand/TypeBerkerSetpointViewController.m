//
//  TypeBerkerSetpointViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/30/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeBerkerSetpointViewController.h"
#import "DeviceTypeSetpoint.h"

@interface TypeBerkerSetpointViewController ()

@end

@implementation TypeBerkerSetpointViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeSetpoint*) newDevice scene:(Scene *) scene
{
    return nil;
}

-(void) updateButtons{
	if (super.currentValue > super.minValue) {
		super.leftButton.userInteractionEnabled = YES;
		super.leftButton.alpha = 1.0;
	}
	else{
		super.leftButton.userInteractionEnabled = NO;
		super.leftButton.alpha = ALPHA_UNSELECTED;
	}
    if (super.currentValue< super.maxValue) {
		super.rightButton.userInteractionEnabled = YES;
		super.rightButton.alpha = 1.0;
	}
	else{
		super.rightButton.userInteractionEnabled = NO;
		super.rightButton.alpha = ALPHA_UNSELECTED;
	}
}


-(void)leftButtonPressed{
    if (super.currentValue> super.minValue) {
		[super sendButtonData:0.0];
    }
}

-(void)rightButtonPressed{
    if (super.currentValue< super.maxValue) {
		[super sendButtonData:1.0];
    }
}


-(NSArray *) getSceneStatus{
	return nil;
}


@end
