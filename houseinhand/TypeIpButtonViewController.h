//
//  TypeIpButtonViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 21/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "SimpleButtonViewController.h"

@interface TypeIpButtonViewController : SimpleButtonViewController
@property (strong,nonatomic) NSString *onAction;

@end
