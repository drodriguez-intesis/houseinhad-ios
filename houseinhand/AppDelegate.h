//
//  AppDelegate.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/30/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#define KNX_CONNECTION_ENABLED	1

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (assign,nonatomic) NSUInteger connectionStatus;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;
-(void) forceChangeConfigFile;
@end
