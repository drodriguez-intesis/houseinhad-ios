//
//  Preferences.m
//  houseinhand
//
//  Created by Isaac Lozano on 5/15/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Preferences.h"
#import "Settings.h"


@implementation Preferences

@dynamic locked;
@dynamic passcodeValue;
@dynamic saveImages;
@dynamic screenRest;
@dynamic sounds;
@dynamic vibrate;
@dynamic r_settings;

@end
