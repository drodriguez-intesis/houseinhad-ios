//
//  NewActionController.h
//  houseinhand
//
//  Created by Isaac Lozano on 9/24/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActionScrollViewController.h"
@class NewActionController;



@protocol NewActionControllerDelegate <NSObject>
- (void)actionController:(NewActionController *) action didEndEditing:(NSDictionary *)info;

@end

@interface NewActionController : NSObject <UIPopoverControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate>
@property (nonatomic, weak) id <NewActionControllerDelegate> delegate;
-(instancetype) initActionSheetWithOptions:(ActionScrollOptions) options originViewController:(id <UIActionSheetDelegate,NewActionControllerDelegate>) originViewController withFrame:(CGRect) frame setDelegate:(id <NewActionControllerDelegate,UIActionSheetDelegate>) delegate;
-(void) showModalControllerFromOption:(NSInteger) option actionSheet:(UIActionSheet *) actionSheet inViewController:(UIViewController *) viewController withButtonFrame:(CGRect) frame stayName:(NSString *) prevName;
-(void) dismissActionSheet;
@end
