//
//  KNXDescription.h
//  houseinhand
//
//  Created by Isaac Lozano on 10/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GCDAsyncUdpSocket.h"

typedef void (^KNXDescriptBlock)( NSDictionary * foundItems);


@interface KNXDescription : NSObject <GCDAsyncUdpSocketDelegate>
@property (copy,nonatomic) KNXDescriptBlock delegateBlock;



-(void) startDescriptionRequestIp:(NSString *) ip port:(NSUInteger) port usingBlock:(KNXDescriptBlock) delegateBlock;
@end
