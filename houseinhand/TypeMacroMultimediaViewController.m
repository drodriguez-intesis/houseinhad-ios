//
//  TypeMacroMultimediaViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 6/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "TypeMacroMultimediaViewController.h"
#import "DeviceTypeMultimedia+Parser.h"
#import "TCPSocketCommunication.h"
#import "MultiCommand+Parser.h"
#import "MultimediaResponseMessage.h"

@interface TypeMacroMultimediaViewController ()
@property (strong,nonatomic) DeviceTypeMultimedia *dev;
@property (strong,nonatomic) TCPSocketCommunication *com;
@property (strong,nonatomic) NSTimer *timeToDisconnect;
@end

@implementation TypeMacroMultimediaViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeMultimedia *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
    	_dev = newDevice;
    }
    return self;
}

-(void) connectToHost{
	if (!_com) {
		_com = [[TCPSocketCommunication alloc] init];
	}
	[_com connectToDevice:_dev];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setActions];
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
}

-(void) buttonPressed:(UIButton *) button{
	NSArray *commands = [_dev getOrderedCommands];
	if (!commands || commands.count == 0) return;
	[self connectToHost];
	[_timeToDisconnect invalidate];
	[self performSelector:@selector(sendCommands:) withObject:commands afterDelay:.5];
	
}

-(void) sendCommands:(NSArray *) commands{
	NSUInteger timeToDisconnect = [[commands[commands.count-1] delay] floatValue] + 1.0;
	_timeToDisconnect = [NSTimer scheduledTimerWithTimeInterval:timeToDisconnect target:_com selector:@selector(disconnect) userInfo:nil repeats:NO];
	[commands enumerateObjectsUsingBlock:^(MultiCommand * obj, NSUInteger idx, BOOL *stop) {
		[NSTimer scheduledTimerWithTimeInterval:obj.delay.floatValue target:self selector:@selector(sendDataToDevice:) userInfo:obj.command repeats:NO];
		if (commands.count >= idx+1) {
		}
	}];
}

-(void) sendDataToDevice:(NSTimer *) timer{
	NSString *info = [timer userInfo];
	if (info) {
		if ([_dev.type isEqualToString:GLOBAL_CACHE]) {
			[_com sendInfo:[NSString stringWithFormat:@"%@\r",info]];
		}
		else{
			[_com sendInfo:info];
		}
	}
}

- (void)viewDidUnload
{
	[_com disconnect];
	_com = nil;
	[_timeToDisconnect invalidate];
	_timeToDisconnect = nil;
    _dev = nil;
	[super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
