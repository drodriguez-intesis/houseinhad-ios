//
//  ZonesViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "StaysViewController.h"
#import "NewActionController.h"
@interface ZonesViewController : StaysViewController <NewActionControllerDelegate,UIActionSheetDelegate>
@end
