//
//  SegueToDevicesPad.h
//  houseinhand
//
//  Created by Isaac Lozano on 10/16/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "StayViewController.h"

@interface SegueToDevicesPad : UIStoryboardSegue
@property (strong,nonatomic) StayViewController *stay;

@end
