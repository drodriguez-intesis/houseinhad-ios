//
//  MultiCommand.h
//  houseinhand
//
//  Created by Isaac Lozano on 7/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class DeviceTypeMultimedia;

@interface MultiCommand : NSManagedObject

@property (nonatomic, retain) NSString * command;
@property (nonatomic, retain) NSNumber * delay;
@property (nonatomic, retain) NSString * tag;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) DeviceTypeMultimedia *r_deviceMulti;

@end
