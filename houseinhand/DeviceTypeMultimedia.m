//
//  DeviceTypeMultimedia.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeMultimedia.h"
#import "MultiCommand.h"


@implementation DeviceTypeMultimedia

@dynamic image1;
@dynamic localIp;
@dynamic localPort;
@dynamic remotePort;
@dynamic type;
@dynamic remoteType;
@dynamic r_command;

@end
