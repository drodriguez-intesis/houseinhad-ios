//
//  LanguagesThirdLevelViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 28/11/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "ThirdLevelViewController.h"
@class LanguagesThirdLevelViewController;
@protocol LanguagesThirdLevelViewControllerDelegate <NSObject>
-(void) didEndChoosingLanguage:(LanguagesThirdLevelViewController *) level;
@end

@interface LanguagesThirdLevelViewController : ThirdLevelViewController <UITableViewDataSource,UITableViewDelegate>
@property (weak,nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, weak) id <LanguagesThirdLevelViewControllerDelegate> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *) title;


@end
