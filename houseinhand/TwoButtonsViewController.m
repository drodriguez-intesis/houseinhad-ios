//
//  TwoButtonsViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/1/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TwoButtonsViewController.h"

@interface TwoButtonsViewController ()
@property (strong,nonatomic) UIImage *leftImage;
@property (strong,nonatomic) UIImage *rightImage;
@end

@implementation TwoButtonsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _leftImage = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];
        _rightImage = [UIImage imageNamed:[newDevice valueForKey:@"image2"]];
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(Device*) newDevice scene:(Scene *) scene
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice scene:scene];
    if (self) {
        _leftImage = [UIImage imageNamed:[newDevice valueForKey:@"image1"]];
        _rightImage = [UIImage imageNamed:[newDevice valueForKey:@"image2"]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	[self setImages];
}

- (void)viewDidUnload
{
    _leftButton = nil;
    _rightButton = nil;
	_rightImage = nil;
    _leftImage = nil;
	[super viewDidUnload];
}

-(void) setImages{
    [_leftButton setImage:_leftImage forState:(UIControlStateNormal)];
    [_leftButton setImage:_leftImage forState:(UIControlStateHighlighted)];
    [_rightButton setImage:_rightImage forState:(UIControlStateNormal)];
    [_rightButton setImage:_rightImage forState:(UIControlStateHighlighted)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
