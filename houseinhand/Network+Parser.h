//
//  Network+Parser.h
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Network.h"

@interface Network (Parser)
+(Network *) insertWithInfo:(NSDictionary *) info intoManagedObjectContext:(NSManagedObjectContext *) moc;

@end
