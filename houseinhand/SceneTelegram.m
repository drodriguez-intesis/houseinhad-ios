//
//  SceneTelegram.m
//  houseinhand
//
//  Created by Isaac Lozano on 9/9/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "SceneTelegram.h"
#import "Scene.h"


@implementation SceneTelegram

@dynamic address;
@dynamic adValue;
@dynamic delay;
@dynamic dpt;
@dynamic options;
@dynamic order;
@dynamic value;
@dynamic r_scene;

@end
