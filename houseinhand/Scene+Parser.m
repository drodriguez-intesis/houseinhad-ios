//
//  Scene+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 4/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "Scene+Parser.h"

@implementation Scene (Parser)

-(SceneTelegram *) getTelegramWithAddress:(NSUInteger) address{
	if (!self.r_sceneTelegram || self.r_sceneTelegram.count == 0) return nil;
	NSSet *telegrams = [self.r_sceneTelegram filteredSetUsingPredicate:[NSPredicate predicateWithFormat:@"address == %d",address]];
	if (!telegrams || telegrams.count == 0) return nil;
	return [telegrams anyObject];
}

@end
