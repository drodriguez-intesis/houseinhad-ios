//
//  PasswordViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/3/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef 	NS_ENUM (NSUInteger,PasswordType) {PASSWORD_TYPE_ADD,PASSWORD_TYPE_REMOVE,PASSWORD_TYPE_CHANGE,PASSWORD_TYPE_CHECK};

@class PasswordViewController;
@protocol PasswordViewControllerDelegate <NSObject>
-(void) passwordVCdidCancelCode:(PasswordViewController *)passwordVC withType:(PasswordType)type;
-(void) passwordVCdidResolveCode:(PasswordViewController *) passwordVC withType:(PasswordType) type;
@end




@interface PasswordViewController : UIViewController <UIAlertViewDelegate>


@property (weak,nonatomic)  id<PasswordViewControllerDelegate> delegate;
-(void) showPasscodeView;
-(id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil passcode:(NSString *) passcode type:(PasswordType) type;
-(NSString *) getPasscode;
@end
