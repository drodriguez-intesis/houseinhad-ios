//
//  MultiCommand.m
//  houseinhand
//
//  Created by Isaac Lozano on 7/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "MultiCommand.h"
#import "DeviceTypeMultimedia.h"


@implementation MultiCommand

@dynamic command;
@dynamic delay;
@dynamic tag;
@dynamic text;
@dynamic r_deviceMulti;

@end
