//
//  KNXDescription.m
//  houseinhand
//
//  Created by Isaac Lozano on 10/17/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "KNXDescription.h"

#define HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO	0x0610
#define HPAI_LENGTH													0x08
#define IPV4_UDP															0x01
#define MESSAGE_DESCRIPTION_REQUEST  					100
#define CODE_DESCRIPTION_REQUEST 							0x0203
#define CODE_DESCRIPTION_RESPONSE						1026
#define LENGTH_DESCRIPTION_REQUEST 						0x000E


@interface KNXDescription ()
@property (strong,nonatomic) dispatch_queue_t rxQueue;
@property (strong,nonatomic) NSString *ip;
@property (assign,nonatomic) NSUInteger port;
@end
@implementation KNXDescription


-(void) startDescriptionRequestIp:(NSString *) ip port:(NSUInteger) port usingBlock:(KNXDescriptBlock) delegateBlock{
	_delegateBlock = delegateBlock;
	_ip = ip;
	_port = port;
	[self startQueue];
	[self startSocket];
}


-(void) startQueue{
	if (_rxQueue == nil) {
		_rxQueue = dispatch_queue_create("descriptionQueue", NULL);
	}
}

-(void) startSocket{
	GCDAsyncUdpSocket *socket = [[GCDAsyncUdpSocket alloc] initWithDelegate:self delegateQueue:_rxQueue];
	[socket setIPv4Enabled:YES];
	[socket setIPv6Enabled:NO];
	[socket connectToHost:_ip onPort:_port error:nil];
}

-(void) udpSocket:(GCDAsyncUdpSocket *)sock didConnectToAddress:(NSData *)address{
	NSMutableData *data = [NSMutableData data];
	[self createHeader:data];
	[self createDataHeader:data];
	[sock receiveOnce:nil];
	[sock sendData:data withTimeout:10 tag:1];
	__block GCDAsyncUdpSocket *soc = sock;
	dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 5 * NSEC_PER_SEC), _rxQueue, ^{
		if (!soc.isClosed) {
			if (_delegateBlock) {
				_delegateBlock(nil);
			}
			[soc close];
			soc = nil;

		}
	});

}


-(void) udpSocket:(GCDAsyncUdpSocket *)sock didNotConnect:(NSError *)error{
	sock = nil;
	if (_delegateBlock) {
		_delegateBlock(nil);
	}
}
-(void) udpSocket:(GCDAsyncUdpSocket *)sock didNotSendDataWithTag:(long)tag dueToError:(NSError *)error{
	sock = nil;
	if (_delegateBlock) {
		_delegateBlock(nil);
	}
}
-(void) udpSocket:(GCDAsyncUdpSocket *)sock didReceiveData:(NSData *)data fromAddress:(NSData *)address withFilterContext:(id)filterContext{
	
	NSString *macAddress = [NSString stringWithString:[self getStringFromData:[data subdataWithRange:NSMakeRange(24, 6)]]];
	if (macAddress) {
		_delegateBlock (@{
						  @"ip" : _ip,
						  @"port" : @(_port),
						  @"mac" : macAddress
						  }
						);
	}
	else{
		_delegateBlock (nil);
	}
	[sock close];
	sock = nil;
}

-(NSString *) getStringFromData:(NSData *) data{
	NSString *string = [NSString stringWithFormat:@"%@",data];
    NSString *string_1 =[string stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSString *string_2 =[string_1 stringByReplacingOccurrencesOfString:@"<" withString:@""];
    NSString *string_3 =[string_2 stringByReplacingOccurrencesOfString:@">" withString:@""];
	return string_3;
}

-(void) createHeader:(NSMutableData *) data{
	char buffer [6];
    buffer[0] = (char) (HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO >>8);
    buffer [1] = (char) HEADER_SIZE_IO_AND_KNXNETIP_VERSION_IO;
    buffer [2] = (char) (CODE_DESCRIPTION_REQUEST >>8);
	buffer [3] = (char) CODE_DESCRIPTION_REQUEST;
	buffer [4] = (char) (LENGTH_DESCRIPTION_REQUEST >>8);
	buffer [5] = (char) LENGTH_DESCRIPTION_REQUEST;
    [data appendBytes:buffer length:sizeof(buffer)];
}

-(void) createDataHeader:(NSMutableData *) data{
    char  buffer [8];
	buffer [0] = HPAI_LENGTH;
	buffer [1] = IPV4_UDP;
	buffer[2]=0x00;
	buffer[3]=0x00;
	buffer[4]=0x00;
	buffer[5]=0x00;
	buffer[6]=0x00;
	buffer[7]=0x00;
	[data appendBytes:buffer length:sizeof(buffer)];
}



@end
