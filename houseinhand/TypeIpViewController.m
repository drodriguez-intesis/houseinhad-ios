//
//  TypeIpViewController.m
//  houseinhand
//
//  Created by Isaac Lozano on 8/10/12.
//  Copyright (c) 2012 intesis. All rights reserved.
//

#import "TypeIpViewController.h"
#import "DeviceTypeIp.h"
@interface TypeIpViewController ()
@property (assign,nonatomic) NSUInteger subType;
@end

@implementation TypeIpViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil device:(DeviceTypeIp *)newDevice
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil device:newDevice];
    if (self) {
        _subType = [newDevice.type integerValue];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setActions];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

-(void) setActions{
    [super.centerButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchDown];
}

-(void) buttonPressed:(id) sender{
    // send value
    [self.delegate deviceViewControllerNextLevel:self];
}

@end

