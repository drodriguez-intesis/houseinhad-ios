//
//  DeviceTypeSetpoint+Parser.m
//  houseinhand
//
//  Created by Isaac Lozano on 3/31/13.
//  Copyright (c) 2013 intesis. All rights reserved.
//

#import "DeviceTypeSetpoint+Parser.h"
#import "Device+Parser.h"

#define KEY_IMAGE_1			@"image1"
#define KEY_IMAGE_2			@"image2"
#define KEY_MAX_VALUE		@"maxValue"
#define KEY_MIN_VALUE		@"minValue"
#define KEY_STEP					@"step"
#define KEY_UNITS				@"units"
#define KEY_SCALE				@"scale"
#define KEY_DECIMALS			@"decimals"

@implementation DeviceTypeSetpoint (Parser)
+(Device *) insertWithInfo:(NSDictionary *) info order:(NSUInteger) idx intoManagedObjectContext:(NSManagedObjectContext *) moc{
	DeviceTypeSetpoint *dev = (DeviceTypeSetpoint *)[NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([self class]) inManagedObjectContext:moc];
	[dev insertWithInfo:info order:idx intoManagedObjectContext:moc];
	dev.image1 = (info[KEY_IMAGE_1] ? info[KEY_IMAGE_1]: dev.image1);
	dev.image2 = (info[KEY_IMAGE_2] ? info[KEY_IMAGE_2]: dev.image2);
	dev.maxValue = (info[KEY_MAX_VALUE] ? @([info[KEY_MAX_VALUE] floatValue]): dev.maxValue);
	dev.minValue = (info[KEY_MIN_VALUE] ? @([info[KEY_MIN_VALUE] floatValue]): dev.minValue);
	dev.step = (info[KEY_STEP] ? @([info[KEY_STEP] floatValue]): dev.step);
	dev.units = (info[KEY_UNITS] ? info[KEY_UNITS]: dev.units);
	dev.scale = (info[KEY_SCALE] ? @([info[KEY_SCALE] floatValue]): dev.scale);
	dev.decimals = (info[KEY_DECIMALS] ? @([info[KEY_DECIMALS] integerValue]): dev.decimals);
	
	return dev;
}
@end
