//
//  DeviceTypeSlider.m
//  houseinhand
//
//  Created by Isaac Lozano on 20/01/14.
//  Copyright (c) 2014 intesis. All rights reserved.
//

#import "DeviceTypeSlider.h"


@implementation DeviceTypeSlider

@dynamic image1;
@dynamic image2;
@dynamic maxValue;
@dynamic minValue;
@dynamic step;
@dynamic hasStop;

@end
