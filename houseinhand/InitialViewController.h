//
//  InitialViewController.h
//  houseinhand
//
//  Created by Isaac Lozano on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UISwipeViewGestureRecognizer.h"
#import "UIMovingView.h"
#import "SettingsViewController.h"
#import "EnvironmentsViewController.h"
#import "StaysViewController.h"

@class StayViewController;
@interface InitialViewController : UIViewController <UISwipeViewGestureRecognizerDelegate,SettingsControllerDelegate,UIGestureRecognizerDelegate>
@property (strong,nonatomic) IBOutlet UIView *zonesView;
@property (strong,nonatomic) IBOutlet UIView *devicesView;
@property (strong,nonatomic) IBOutlet UIMovingView *staysView;
@property (strong,nonatomic) IBOutlet UIMovingView *environmentsView;
@property (strong,nonatomic) IBOutlet UIMovingView *settingsView;
@property (strong,nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (strong,nonatomic) id detailItem;
@property (assign) NSUInteger requestedLevel;
@property (assign) NSUInteger previousLevel;
@property (assign) CGFloat previousYOffset;
-(void) changeConfigFile;
-(void) setBackgroundImage:(UIImage *) image;
-(void) removeBackgroundImage;

-(StaysViewController *) getStaysViewController;
@end
